package threads.lite;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.io.ByteStreams;
import com.google.protobuf.ByteString;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;

import threads.lite.blockstore.BLOCKS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.AutonatResult;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Keys;
import threads.lite.core.Link;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Progress;
import threads.lite.core.Reachability;
import threads.lite.core.Reservation;
import threads.lite.core.Session;
import threads.lite.crypto.Key;
import threads.lite.host.LiteHost;
import threads.lite.minidns.DnsResolver;
import threads.lite.peerstore.PEERS;
import threads.lite.push.Push;
import threads.lite.push.PushService;
import threads.lite.server.ServerSession;
import threads.lite.utils.ProgressStream;
import threads.lite.utils.Reader;
import threads.lite.utils.ReaderInputStream;
import threads.lite.utils.ReaderProgress;
import threads.lite.utils.ReaderStream;
import threads.lite.utils.Resolver;
import threads.lite.utils.Stream;

public class IPFS {


    public static final String TIME_FORMAT_IPFS = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'";  // RFC3339Nano = "2006-01-02T15:04:05.999999999Z07:00"
    public static final String MPLEX_PROTOCOL = "/mplex/6.7.0";
    public static final String HOLE_PUNCH_PROTOCOL = "/libp2p/dcutr";
    public static final String RELAY_PROTOCOL_HOP = "/libp2p/circuit/relay/0.2.0/hop";
    public static final String RELAY_PROTOCOL_STOP = "/libp2p/circuit/relay/0.2.0/stop";
    public static final String NOISE_PROTOCOL = "/noise";
    public static final String DHT_PROTOCOL = "/ipfs/kad/1.0.0";
    public static final String PUSH_PROTOCOL = "/lite/push/1.0.0";
    public static final String AUTONAT_PROTOCOL = "/libp2p/autonat/1.0.0";
    public static final String MULTISTREAM_PROTOCOL = "/multistream/1.0.0";
    public static final String BITSWAP_PROTOCOL = "/ipfs/bitswap/1.2.0";
    public static final String IDENTITY_PROTOCOL = "/ipfs/id/1.0.0";
    public static final String INDEX_HTML = "index.html";
    public static final String AGENT_PREFIX = "lite";
    public static final String AGENT = AGENT_PREFIX + "/0.9.0/";
    public static final String PROTOCOL_VERSION = "ipfs/0.1.0";
    public static final String IPFS_PATH = "/ipfs/";
    public static final String IPNS_PATH = "/ipns/";
    public static final String LIB2P_DNS = "bootstrap.libp2p.io"; // IPFS BOOTSTRAP DNS
    public static final String MINIDNS_DNS_SERVER_IP4 = "8.8.8.8"; // google
    public static final String MINIDNS_DNS_SERVER_IP6 = "[2001:4860:4860::8888]";
    public static final String NA = "na";
    public static final String ALPN = "libp2p";

    public static final int CHUNK_SIZE = 262144;
    public static final int MAX_STREAMS = 10000;
    public static final int GRACE_PERIOD = 15;
    public static final int GRACE_PERIOD_RESERVATION = 60 * 60; // 60 min
    // MessageSizeMax is a soft (recommended) maximum for network messages.
    // One can write more, as the interface is a stream. But it is useful
    // to bunch it up into multiple read/writes when the whole message is
    // a single, large serialized object.
    public static final int MESSAGE_SIZE_MAX = 1 << 22; // 4 MB
    @NonNull
    public static final List<String> DHT_BOOTSTRAP_NODES = new ArrayList<>(Arrays.asList(
            "QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN", // default dht peer
            "QmQCU2EcMqAqQPR2i9bChDtGNJchTbq5TbXJJ16u19uLTa", // default dht peer
            "QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb", // default dht peer
            "QmcZf59bWwK5XFi76CZX8cbJ4BhTzzA3gU1ZjYZcYW3dwt" // default dht peer

    ));
    public static final int DHT_TABLE_SIZE = 200;
    public static final int DHT_ALPHA = 25;
    public static final int DHT_CONCURRENCY = 5;
    public static final int AUTONAT_TIMEOUT = 10; // in seconds
    public static final int CONNECT_TIMEOUT = 5; // in seconds
    public static final int RELAY_CONNECT_TIMEOUT = 30; // in seconds
    public static final int DHT_REQUEST_TIMEOUT = 15; // in seconds
    public static final int IPNS_DURATION = 6; // 6 hours duration
    public static final int BITSWAP_REQUEST_DELAY = 1500; // 1,5 sec

    @NonNull
    public static final Duration RECORD_EOL = Duration.ofHours(24);

    private static final String LITE_KEY = "LITE_KEY";
    private static final String PORT_KEY = "PORT_KEY";
    private static final String PRIVATE_KEY = "PRIVATE_KEY";
    private static final String PUBLIC_KEY = "PUBLIC_KEY";
    private static final String TAG = IPFS.class.getSimpleName();

    // rough estimates on expected sizes
    private static final int ROUGH_LINK_BLOCK_SIZE = 1 << 13; // 8KB
    private static final int ROUGH_LINK_SIZE = 34 + 8 + 5;// sha256 multihash + size + no name + protobuf framing
    // DefaultLinksPerBlock governs how the importer decides how many links there
    // will be per block. This calculation is based on expected distributions of:
    //  * the expected distribution of block sizes
    //  * the expected distribution of link sizes
    //  * desired access speed
    // For now, we use:
    //
    //   var roughLinkBlockSize = 1 << 13 // 8KB
    //   var roughLinkSize = 34 + 8 + 5   // sha256 multihash + size + no name
    //                                    // + protobuf framing
    //   var DefaultLinksPerBlock = (roughLinkBlockSize / roughLinkSize)
    //                            = ( 8192 / 47 )
    //                            = (approximately) 174
    public static final int LINKS_PER_BLOCK = ROUGH_LINK_BLOCK_SIZE / ROUGH_LINK_SIZE;

    private static volatile IPFS INSTANCE = null;
    @NonNull
    private final LiteHost host;


    private IPFS(@NonNull Context context) throws Exception {
        int port = getPort(context);
        this.host = new LiteHost(getKeys(context), BLOCKS.getInstance(context),
                PEERS.getInstance(context), port);
    }

    public static void setPort(Context context, int port) {
        SharedPreferences sharedPref = context.getSharedPreferences(LITE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(PORT_KEY, port);
        editor.apply();
    }

    public static int getPort(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(LITE_KEY, Context.MODE_PRIVATE);
        return sharedPref.getInt(PORT_KEY, 5001);
    }

    private static void setPublicKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                LITE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PUBLIC_KEY, key);
        editor.apply();
    }

    @SuppressWarnings("UnusedReturnValue")
    public static long copy(InputStream source, OutputStream sink) throws IOException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return source.transferTo(sink);
        } else {
            return ByteStreams.copy(source, sink);
        }
    }

    public static void copy(@NonNull InputStream source,
                            @NonNull OutputStream sink,
                            @NonNull ReaderProgress progress) throws IOException {
        long nread = 0L;
        byte[] buf = new byte[4096];
        int remember = 0;
        int n;
        while ((n = source.read(buf)) > 0) {
            sink.write(buf, 0, n);
            nread += n;

            if (progress.doProgress()) {
                if (progress.getSize() > 0) {
                    int percent = (int) ((nread * 100.0f) / progress.getSize());
                    if (remember < percent) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        }
    }

    @NonNull
    private static String getPublicKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                LITE_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PUBLIC_KEY, ""));

    }

    private static void setPrivateKey(@NonNull Context context, @NonNull String key) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                LITE_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PRIVATE_KEY, key);
        editor.apply();
    }

    @NonNull
    private static String getPrivateKey(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                LITE_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(PRIVATE_KEY, ""));

    }

    @NonNull
    public static IPFS getInstance(@NonNull Context context) throws Exception {
        if (INSTANCE == null) {
            synchronized (IPFS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new IPFS(context);
                }
            }
        }
        return INSTANCE;
    }


    @NonNull
    private Keys getKeys(@NonNull Context context) {

        if (!getPrivateKey(context).isEmpty() && !getPublicKey(context).isEmpty()) {

            Base64.Decoder decoder = Base64.getDecoder();

            byte[] privateKeyData = decoder.decode(getPrivateKey(context));
            byte[] publicKeyData = decoder.decode(getPublicKey(context));

            Ed25519PrivateKeyParameters privateKey =
                    new Ed25519PrivateKeyParameters(privateKeyData, 0);
            Objects.requireNonNull(privateKey);
            Ed25519PublicKeyParameters publicKey =
                    new Ed25519PublicKeyParameters(publicKeyData, 0);
            Objects.requireNonNull(publicKey);

            return new Keys(publicKey, privateKey);

        } else {
            Keys keys = Key.generateKeys();
            Base64.Encoder encoder = Base64.getEncoder();
            setPrivateKey(context, encoder.encodeToString(keys.getPrivate().getEncoded()));
            setPublicKey(context, encoder.encodeToString(keys.getPublic().getEncoded()));
            return keys;
        }
    }

    @NonNull
    public Keys getKeys() {
        return host.getKeys();
    }

    @NonNull
    public PeerId self() {
        return host.self();
    }


    @NonNull
    public PeerInfo getIdentity() throws Exception {
        return host.getIdentity();
    }

    @NonNull
    public CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection connection) {
        return host.getPeerInfo(connection);
    }

    @NonNull
    public CompletableFuture<Void> push(@NonNull Connection connection,
                                        @NonNull IpnsRecord ipnsRecord) {
        return PushService.push(connection, ipnsRecord);
    }

    public void setIncomingPush(@Nullable Consumer<Push> incomingPush) {
        this.host.setIncomingPush(incomingPush);
    }


    @NonNull
    public Cid storeFile(@NonNull Session session, @NonNull File file) throws Exception {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            return storeInputStream(session, inputStream);
        }
    }

    @NonNull
    public Cid storeData(@NonNull Session session, byte[] data) throws Exception {

        try (InputStream inputStream = new ByteArrayInputStream(data)) {
            return storeInputStream(session, inputStream);
        }
    }

    @NonNull
    public Cid storeText(@NonNull Session session, @NonNull String text) throws Exception {

        try (InputStream inputStream = new ByteArrayInputStream(text.getBytes())) {
            return storeInputStream(session, inputStream);
        }
    }

    @NonNull
    public Cid storeInputStream(@NonNull Session session, @NonNull InputStream inputStream,
                                @NonNull Progress progress, long size) throws Exception {

        return Stream.readInputStream(session,
                new ReaderInputStream(inputStream, progress, size));

    }

    @NonNull
    public Cid storeInputStream(@NonNull Session session, @NonNull InputStream inputStream)
            throws Exception {
        return Stream.readInputStream(session,
                new ReaderInputStream(inputStream, 0));
    }


    @NonNull
    public String getText(@NonNull Session session, @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, progress);
            return outputStream.toString();
        }
    }

    @NonNull
    public String getText(@NonNull Session session, @NonNull Cid cid,
                          @NonNull Cancellable cancellable) throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toString();
        }
    }


    @NonNull
    public byte[] getData(@NonNull Session session, @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, progress);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public byte[] getData(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            fetchToOutputStream(session, outputStream, cid, cancellable);
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public Reader getReader(@NonNull Session session, @NonNull Cid cid,
                            @NonNull Cancellable cancellable)
            throws Exception {
        return Reader.getReader(cancellable, session, cid);
    }

    @NonNull
    public InputStream getInputStream(@NonNull Session session, @NonNull Cid cid,
                                      @NonNull Cancellable cancellable)
            throws Exception {
        Reader reader = getReader(session, cid, cancellable);
        return new ReaderStream(reader);
    }

    @NonNull
    public InputStream getInputStream(@NonNull Session session, @NonNull Cid cid,
                                      @NonNull Progress progress)
            throws Exception {
        Reader reader = getReader(session, cid, progress);
        return new ProgressStream(reader, progress);

    }


    public void fetchToFile(@NonNull Session session, @NonNull File file,
                            @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(session, fileOutputStream, cid, cancellable);
        }
    }

    public void fetchToFile(@NonNull Session session, @NonNull File file,
                            @NonNull Cid cid, @NonNull Progress progress)
            throws Exception {

        try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            fetchToOutputStream(session, fileOutputStream, cid, progress);
        }
    }

    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public void fetchToOutputStream(@NonNull Session session,
                                    @NonNull OutputStream outputStream,
                                    @NonNull Cid cid,
                                    @NonNull Progress progress)
            throws Exception {

        long totalRead = 0L;
        int remember = 0;

        Reader reader = getReader(session, cid, progress);
        long size = reader.getSize();

        do {
            if (progress.isCancelled()) {
                throw new Exception("cancelled");
            }

            ByteString buffer = reader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());

            // calculate progress
            totalRead += buffer.size();
            if (progress.doProgress()) {
                if (size > 0) {
                    int percent = (int) ((totalRead * 100.0f) / size);
                    if (remember < percent) {
                        remember = percent;
                        progress.setProgress(percent);
                    }
                }
            }
        } while (true);
    }

    // fetch (stores) the given cid into the output stream
    // Note: the data is automatically stored also in the block store
    public void fetchToOutputStream(@NonNull Session session,
                                    @NonNull OutputStream outputStream,
                                    @NonNull Cid cid,
                                    @NonNull Cancellable cancellable)
            throws Exception {

        Reader reader = getReader(session, cid, cancellable);

        do {
            ByteString buffer = reader.loadNextData();
            if (buffer.isEmpty()) {
                return;
            }
            outputStream.write(buffer.toByteArray());
        } while (true);
    }


    // has the session block storage the cid block
    public boolean hasBlock(@NonNull Session session, @NonNull Cid cid) {
        return session.getBlockStore().hasBlock(cid);
    }

    // remove the cid block (add all links blocks recursively) from the session block storage
    public void removeBlocks(@NonNull Session session, @NonNull Cid cid) throws Exception {
        List<Cid> cids = getBlocks(session, cid);
        cids.add(cid);
        session.getBlockStore().deleteBlocks(cids);
    }

    // returns all blocks of the cid from the session block storage,
    // If the cid block contains links, also the links cid blocks are returned (recursive)
    @NonNull
    public List<Cid> getBlocks(@NonNull Session session, @NonNull Cid cid) throws Exception {
        return Stream.getBlocks(session, cid);
    }

    // removes a link with the given name from the directory
    @NonNull
    public Cid removeFromDirectory(@NonNull Session session, @NonNull Cid dir, @NonNull String name) throws Exception {
        return Stream.removeFromDirectory(session, dir, name);
    }

    // add a link to to the directory (link should be of Type File, but not tested)
    @NonNull
    public Cid addLinkToDirectory(@NonNull Session session, @NonNull Cid dir, @NonNull Link link) throws Exception {
        return Stream.addLinkToDirectory(session, dir, link);
    }

    // creates a directory with the given links  (links should be of Type File, but not tested)
    @NonNull
    public Cid createDirectory(@NonNull Session session, @NonNull List<Link> links) throws Exception {
        return Stream.createDirectory(session, links);
    }

    @NonNull
    public Cid createEmptyDirectory(@NonNull Session session) throws Exception {
        return Stream.createEmptyDirectory(session);
    }


    @NonNull
    public Session createSession(@NonNull BlockStore blockStore,
                                 boolean findProvidersActive,
                                 boolean sendReplyActive) {
        return host.createSession(blockStore, findProvidersActive, sendReplyActive);
    }

    @NonNull
    public Session createSession(boolean findProvidersActive,
                                 boolean sendReplyActive) {
        return host.createSession(getBlockStore(), findProvidersActive, sendReplyActive);
    }

    @NonNull
    public Session createSession() {
        return createSession(getBlockStore(), true, true);
    }


    // function requires a cancellable, because the cid could be remote
    public boolean isDir(@NonNull Session session, @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {
        return Stream.isDir(cancellable, session, cid);
    }

    // function requires a cancellable, because the cid could be remote
    public boolean hasLink(@NonNull Session session, @NonNull Cid cid,
                           @NonNull String name, @NonNull Cancellable cancellable)
            throws Exception {
        return Stream.hasLink(cancellable, session, cid, name);
    }

    // function requires a cancellable, because the cid could be remote
    // this function return when all links have been evaluated
    // Note: only links are returned when they have a name (usually a file and directory)
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    @NonNull
    public List<Link> links(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                            @NonNull Cancellable cancellable) throws Exception {

        List<Link> result = new ArrayList<>();
        links(session, cid, result::add, resolveChildren, cancellable);
        return result;
    }

    // function requires a cancellable, because the cid could be remote
    // this function present immediately links, when evaluated
    // Note: only links are returned when they have a name (usually a file and directory)
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    public void links(@NonNull Session session, @NonNull Cid cid, @NonNull Consumer<Link> consumer,
                      boolean resolveChildren, @NonNull Cancellable cancellable)
            throws Exception {

        Stream.ls(cancellable, link -> {
            if (!link.getName().isEmpty()) {
                consumer.accept(link);
            }
        }, session, cid, resolveChildren);
    }

    // function requires a cancellable, because the cid could be remote
    // return all links, also of type raw and unknown
    // Note: when resolveChildren is true, the link node will be resolved and the type is known
    // Note: This function does not go recursive, only direct links are evaluated
    @NonNull
    public List<Link> allLinks(@NonNull Session session, @NonNull Cid cid, boolean resolveChildren,
                               @NonNull Cancellable cancellable) throws Exception {

        List<Link> links = new ArrayList<>();
        Stream.ls(cancellable, links::add, session, cid, resolveChildren);
        return links;
    }


    public void publishName(@NonNull Session session, long sequence,
                            @NonNull Cid cid, @NonNull Consumer<Multiaddr> consumer,
                            @NonNull Cancellable cancellable)
            throws Exception {
        publishName(session, sequence, encodeIpnsData(cid), consumer, cancellable);
    }

    public void publishName(@NonNull Session session, long sequence, byte[] data,
                            @NonNull Consumer<Multiaddr> consumer,
                            @NonNull Cancellable cancellable) throws Exception {
        IpnsRecord ipnsRecord = createSelfSignedIpnsRecord(sequence, data);
        session.putValue(cancellable, consumer,
                ipnsRecord.getIpnsKey(), ipnsRecord.getSealedRecord());
    }

    @Nullable
    public IpnsEntity resolveName(@NonNull Session session, @NonNull PeerId peerId,
                                  long lastSequence, @NonNull Cancellable cancellable) {

        AtomicReference<IpnsEntity> done = new AtomicReference<>();

        AtomicBoolean found = new AtomicBoolean(false);
        session.searchValue(() -> (found.get() || cancellable.isCancelled()),
                entry -> {
                    long sequence = entry.getSequence();

                    LogUtils.debug(TAG, "IpnsEntry : " + entry);

                    if (sequence >= lastSequence) {
                        done.set(entry);
                        found.set(true);
                    }
                }, createIpnsKey(peerId));

        return done.get();
    }


    public void provide(@NonNull Session session, @NonNull Cid cid,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cancellable cancellable) {
        session.provide(cancellable, consumer, cid);
    }


    public void findProviders(@NonNull Session session, @NonNull Consumer<Multiaddr> consumer,
                              @NonNull Cid cid, @NonNull Cancellable cancellable) {
        session.findProviders(cancellable, consumer, cid);
    }


    public void findPeer(@NonNull Session session, @NonNull PeerId peerId,
                         @NonNull Consumer<Multiaddr> consumer, @NonNull Cancellable cancellable) {
        session.findPeer(cancellable, consumer, peerId);
    }


    public void findClosestPeers(@NonNull Session session, @NonNull PeerId peerId,
                                 @NonNull Consumer<Multiaddr> consumer,
                                 @NonNull Cancellable cancellable) {
        session.findClosestPeers(cancellable, consumer, peerId);
    }

    public void updateNetwork() {
        host.updateNetwork();
    }


    // no timeout is set, it is set internally
    // for normal connections it is IPFS.CONNECT_TIMEOUT 5 sec
    // for relayed connections it is IPFS.RELAY_CONNECT_TIMEOUT 30 sec (hole punch)
    // others like dns are somewhere in between
    @NonNull
    public Connection dial(@NonNull Session session, @NonNull Multiaddr address,
                           @NonNull Parameters parameters)
            throws ConnectException, InterruptedException {
        return host.connect(session, address, parameters, false);
    }

    @NonNull
    public Parameters getConnectionParameters() {
        return Parameters.getDefault();
    }


    public int getPort() {
        return host.getPort();
    }

    @NonNull
    public ServerSession getServerSession() {
        return host.getServerSession();
    }

    public void setConnectConsumer(@Nullable Consumer<Connection> connectConsumer) {
        host.setConnectConsumer(connectConsumer);
    }

    public void setClosedConsumer(@Nullable Consumer<Connection> closedConsumer) {
        host.setClosedConsumer(closedConsumer);
    }

    public void setReachabilityConsumer(@Nullable Consumer<Reachability> reachabilityConsumer) {
        host.setReachabilityConsumer(reachabilityConsumer);
    }

    public int numServerConnections() {
        return host.numServerConnections();
    }


    @NonNull
    public Set<Multiaddr> getBootstrap() {
        return host.getBootstrap();
    }

    @NonNull
    public AutonatResult autonat() {
        return host.autonat();
    }


    public void setIsGated(@Nullable Function<PeerId, Boolean> isGated) {
        host.setIsGated(isGated);
    }


    // when using this function make sure, that the data might be "encoded" already
    // (see encodeIpnsData, for example, but maybe you do not need an encoding)
    // Note: sealed means, that remote peers can verify that you have made the record,
    // it does not mean that the content itself is encrypted
    // Note: you always signed it with your own private key (signing with another own private key
    // is not supported)
    @NonNull
    public IpnsRecord createSelfSignedIpnsRecord(long sequence, byte[] data) throws Exception {
        return new IpnsRecord(createIpnsKey(self()), host.createSelfSignedRecord(data, sequence));
    }

    // create an ipns key has the form of "/ipns/"+ <hash of peerId>
    public byte[] createIpnsKey(@NonNull PeerId peerId) {
        return host.createIpnsKey(peerId);
    }


    // Note: this is usually the data of an ipns entry
    // default : "/ipfs/ +<encoded cid>
    @NonNull
    private Cid decodeIpnsData(byte[] data) throws Exception {
        return Cid.decode(new String(data).replaceFirst(IPFS.IPFS_PATH, ""));
    }

    // Note: this is usually the data of an ipns entry
    // default : "/ipfs/ +<encoded cid>
    @NonNull
    public Cid decodeIpnsData(@NonNull IpnsEntity ipnsEntity) throws Exception {
        return decodeIpnsData(ipnsEntity.getData());
    }

    // Transform a cid object into an expected ipns data format
    // Format : "/ipfs/+ <encoded cid>
    // if cid is version 0 : encoding Base58
    // if cid is version 1 : encoding Base32
    public byte[] encodeIpnsData(@NonNull Cid cid) {
        String path = IPFS_PATH + cid.String();
        return path.getBytes(StandardCharsets.UTF_8);
    }

    @NonNull
    public Cid decodeCid(@NonNull String cid) throws Exception {
        return Cid.decode(cid);
    }

    @NonNull
    public PeerId decodePeerId(@NonNull String pid) throws Exception {
        return PeerId.fromString(pid);
    }

    // Utility function, resolves a root Cid object till the path of links is reached
    @NonNull
    public Cid resolveCid(@NonNull Session session, @NonNull Cid root, @NonNull List<String> path,
                          @NonNull Cancellable cancellable) throws Exception {
        return Resolver.resolveNode(cancellable, session, root, path);
    }

    // Utility function, to resolve a dnsaddr [Address starts with "/dnsaddr/"]
    @NonNull
    public List<Multiaddr> resolveDnsaddr(@NonNull Multiaddr multiaddr) throws Exception {
        if (!multiaddr.isDnsaddr()) {
            throw new Exception("not a dnsaddr");
        }
        return DnsResolver.resolveDnsaddr(host.getDnsClient(), host.getProtocolSupport(), multiaddr);
    }

    // Utility function, to resolve a dns address [Address starts with "/dns/", "/dns4/" or "/dns6/" ]
    // in case of return null, the dns address couldn't resolved
    // in case of an runtime exception (IllegalStateException), it is not a valid dns address
    @Nullable
    public Multiaddr resolveDns(@NonNull Multiaddr multiaddr) {
        return DnsResolver.resolveDns(host.getProtocolSupport(), multiaddr);
    }

    @NonNull
    public String resolveDnsLink(@NonNull String hostName) {
        return DnsResolver.resolveDnsLink(host.getDnsClient(), hostName);
    }

    @NonNull
    public Multiaddr decodeMultiaddr(@NonNull String address) throws Exception {
        return Multiaddr.create(address);
    }

    @NonNull
    public BlockStore getBlockStore() {
        return host.getBlockStore();
    }


    // return true, when it has reservations
    public boolean hasReservations() {
        return host.hasReservations();
    }

    // return the number of current reservations
    public int numReservations() {
        return reservations().size();
    }

    // this function returns all the valid reservations
    @NonNull
    public Set<Reservation> reservations() {
        return host.reservations();
    }

    // this function does the reservation
    // Note: only reservation of version 2 is supported
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    // static relays are marked as Kind.STATIC, where limited relays are marked
    // as Kind.LIMITED (in the Reservation class)
    @NonNull
    public Set<Reservation> reservations(@NonNull Session session,
                                         @NonNull Set<Multiaddr> multiaddrs,
                                         long timeout) {
        return host.reservations(session, multiaddrs, timeout);
    }


    // this function does the reservation
    // Note: only reservation of version 2 is supported
    // https://github.com/libp2p/specs/blob/master/relay/circuit-v2.md#introduction
    // static relays are marked as Kind.STATIC, where limited relays are marked
    // as Kind.LIMITED (in the Reservation class)
    // Note: this function is used for testing (though it still adds it to the internal
    // reservation list, when succeeds)
    @NonNull
    public CompletableFuture<Reservation> reservation(@NonNull Session session,
                                                      @NonNull Multiaddr multiaddr) {
        return host.reservation(session, multiaddr.getPeerId(), multiaddr);
    }

    // returns next reservation cycle in minutes, after the minutes have past
    // the reservation process has to be done again, due to the fact that some or at least one
    // reservation expire time comes to an end
    // it works on the local stored reservations
    // Note: static reservations are not considered, only limited
    public long nextReservationCycle() {
        long nextCycle = 0;
        for (Reservation reservation : reservations()) {
            if (reservation.getKind() == Reservation.Kind.LIMITED) {
                long expireInMinutes = reservation.expireInMinutes();
                if (nextCycle == 0) {
                    nextCycle = expireInMinutes;
                } else {
                    nextCycle = Math.min(nextCycle, expireInMinutes);
                }
            }
        }
        return nextCycle;
    }

}
