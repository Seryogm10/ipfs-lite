package threads.lite.blockstore;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;

@Dao
public interface BlockStoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBlock(Block block);

    @Query("DELETE FROM Block WHERE cid = :cid")
    void deleteBlock(Cid cid);

    @Query("SELECT 1 FROM Block WHERE cid = :cid")
    boolean hasBlock(Cid cid);

    @Query("SELECT * FROM Block WHERE cid = :cid")
    Block getBlock(Cid cid);

    @Query("SELECT data FROM Block WHERE cid = :cid")
    byte[] getData(Cid cid);
}
