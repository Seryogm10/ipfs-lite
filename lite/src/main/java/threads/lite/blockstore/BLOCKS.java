package threads.lite.blockstore;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.List;

import merkledag.pb.Merkledag;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;


public class BLOCKS implements BlockStore {

    private static volatile BLOCKS INSTANCE = null;
    private final BlocksStoreDatabase blocksStoreDatabase;

    private BLOCKS(BlocksStoreDatabase blocksStoreDatabase) {
        this.blocksStoreDatabase = blocksStoreDatabase;
    }

    @NonNull
    private static BLOCKS createBlocks(@NonNull BlocksStoreDatabase blocksStoreDatabase) {
        return new BLOCKS(blocksStoreDatabase);
    }

    public static BLOCKS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (BLOCKS.class) {
                if (INSTANCE == null) {
                    BlocksStoreDatabase blocksStoreDatabase = Room.databaseBuilder(context, BlocksStoreDatabase.class,
                                    BlocksStoreDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    INSTANCE = BLOCKS.createBlocks(blocksStoreDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Nullable
    private Merkledag.PBNode getNode(@NonNull Cid cid) throws Exception {
        Block block = getBlocksDatabase().blockDao().getBlock(cid);
        if (block != null) {
            return Merkledag.PBNode.parseFrom(block.getData());
        }
        return null;
    }

    @Override
    public void clear() {
        getBlocksDatabase().clearAllTables();
    }

    @NonNull
    public BlocksStoreDatabase getBlocksDatabase() {
        return blocksStoreDatabase;
    }

    @NonNull
    private Block createBlock(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        return Block.createBlock(cid, data.toByteArray());
    }

    private void storeBlock(@NonNull Block block) {
        getBlocksDatabase().blockDao().insertBlock(block);
    }


    private void insertBlock(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        storeBlock(createBlock(cid, data));
    }


    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return getBlocksDatabase().blockDao().hasBlock(cid);
    }

    @Nullable
    @Override
    public threads.lite.core.Block getBlock(@NonNull Cid cid) throws Exception {
        Merkledag.PBNode data = getNode(cid);
        if (data == null) {
            return null;
        }
        return threads.lite.core.Block.createBlockWithCid(cid, data);
    }

    @Nullable
    @Override
    public byte[] getData(@NonNull Cid cid) {
        return getBlocksDatabase().blockDao().getData(cid);
    }

    @Override
    public void putBlock(@NonNull threads.lite.core.Block block) {
        insertBlock(block.getCid(), block.getNode());
    }

    @Override
    public void deleteBlock(@NonNull Cid cid) {
        getBlocksDatabase().blockDao().deleteBlock(cid);
    }

    @Override
    public void deleteBlocks(@NonNull List<Cid> cids) {
        for (Cid cid : cids) {
            deleteBlock(cid);
        }
    }

}
