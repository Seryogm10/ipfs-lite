package threads.lite.relay;

import androidx.annotation.NonNull;

import com.southernstorm.noise.protocol.CipherStatePair;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import circuit.pb.Circuit;
import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Reservation;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.holepunch.HolePunch;
import threads.lite.host.LiteHost;
import threads.lite.host.LiteStream;
import threads.lite.ident.IdentityService;
import threads.lite.mplex.MuxedStream;
import threads.lite.mplex.MuxedTransport;
import threads.lite.noise.Handshake;
import threads.lite.noise.Noise;
import threads.lite.noise.SecuredStream;
import threads.lite.noise.SecuredTransport;
import threads.lite.utils.DataHandler;

public abstract class RelayResponder implements StreamHandler {
    private static final String TAG = RelayResponder.class.getSimpleName();
    private static final String NOISE = "NOISE";
    private static final String PEER = "PEER";
    private static final String OBSERVED = "OBSERVED";

    private final Session session;
    private final LiteHost host;
    private final PeerId relayId;

    public RelayResponder(LiteHost host, Session session, PeerId relayId) {
        this.session = session;
        this.host = host;
        this.relayId = relayId;
    }

    @NonNull
    public Multiaddr getObserved(Stream stream) {
        return Objects.requireNonNull((Multiaddr) stream.getAttribute(OBSERVED));
    }

    @NonNull
    public abstract Reservation.Kind getReservationKind();

    public Noise.NoiseState getResponder(Stream stream) throws NoSuchAlgorithmException {
        Noise.NoiseState state = (Noise.NoiseState) stream.getAttribute(NOISE);
        PeerId peerId = Objects.requireNonNull((PeerId) stream.getAttribute(PEER));

        if (state == null) {
            state = host.getNoise().getResponder(peerId,
                    host.getKeys());
            stream.setAttribute(NOISE, state);
        }
        return state;
    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable.getMessage());
        stream.close();
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {

        LogUtils.error(TAG, "Protocol " + protocol +
                " streamId " + stream.getStreamId() + " initiator " + stream.isInitiator());

        stream.setAttribute(TOKEN, protocol);
        switch (protocol) {
            case IPFS.MULTISTREAM_PROTOCOL:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL));
                }
                break;
            case IPFS.HOLE_PUNCH_PROTOCOL: {
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.HOLE_PUNCH_PROTOCOL));
                } else {
                    HolePunch.initializeConnect(stream, getObserved(stream));
                }
                break;
            }
            case IPFS.RELAY_PROTOCOL_STOP:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.RELAY_PROTOCOL_STOP));
                } else {
                    throw new Exception("wrong initiator");
                }
                break;
            case IPFS.IDENTITY_PROTOCOL:
                if (!stream.isInitiator()) {

                    byte[] observed = Protocol.decode(
                            stream.getConnection().getRemoteAddress(), relayId);

                    Set<String> protocols = new HashSet<>();
                    protocols.add(IPFS.RELAY_PROTOCOL_STOP);
                    protocols.add(IPFS.MULTISTREAM_PROTOCOL);
                    protocols.add(IPFS.MPLEX_PROTOCOL);
                    protocols.add(IPFS.IDENTITY_PROTOCOL);
                    protocols.add(IPFS.NOISE_PROTOCOL);
                    if (host.holePunchAllowed()) {
                        protocols.add(IPFS.HOLE_PUNCH_PROTOCOL);
                    }

                    if (getReservationKind() == Reservation.Kind.STATIC) {
                        protocols.addAll(session.getProtocolNames());
                    }


                    IdentifyOuterClass.Identify response =
                            host.createIdentity(protocols,
                                    host.networkListenAddresses(), observed);

                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.IDENTITY_PROTOCOL));
                    stream.writeOutput(DataHandler.encode(response))
                            .thenApply(Stream::closeOutput);
                } else {
                    stream.closeOutput();
                }
                break;
            case IPFS.MPLEX_PROTOCOL:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.MPLEX_PROTOCOL));

                    // make sure that the stream is secured stream
                    SecuredStream securedStream = Objects.requireNonNull((SecuredStream) stream);

                    // finished connection
                    MuxedTransport transport = new MuxedTransport(securedStream.getQuicStream(),
                            securedStream.getSender(), securedStream.getReceiver());
                    stream.setAttribute(TRANSPORT, transport);

                    LogUtils.error(TAG, "Transport set to MuxedTransport, do initiateIdentity");

                    MuxedStream muxedStream = MuxedStream.createStream(transport).get(
                            IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                    muxedStream.writeOutput(DataHandler.encodeProtocols(
                            IPFS.MULTISTREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL)).get(
                            IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                } else {
                    throw new Exception("wrong initiator");
                }
                break;
            case IPFS.NOISE_PROTOCOL:
                if (!stream.isInitiator()) {
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.NOISE_PROTOCOL));
                    LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);
                    stream.setAttribute(TRANSPORT, new Handshake(liteStream.getQuicStream()));
                    LogUtils.error(TAG, "Transport set to Handshake");
                } else {
                    throw new Exception("wrong initiator");
                }
                break;
            default:
                ProtocolHandler protocolHandler = null;

                if (getReservationKind() == Reservation.Kind.STATIC) {
                    protocolHandler = session.getProtocolHandler(protocol);
                }
                if (protocolHandler != null) {
                    protocolHandler.protocol(stream);
                } else {
                    LogUtils.error(TAG, "Ignore " + protocol);

                    if (!stream.isInitiator()) {
                        stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));
                    } else {
                        LogUtils.error(TAG, "something wrong not " + protocol);
                        throw new Exception("wrong initiator");
                    }
                }
        }
    }


    @Override
    public void fin() {
        LogUtils.error(TAG, "fin received");
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {

        String protocol = (String) stream.getAttribute(TOKEN);
        LogUtils.error(TAG, "data streamId " + stream.getStreamId() +
                " protocol " + protocol + " initiator " + stream.isInitiator());
        Objects.requireNonNull(protocol);

        switch (protocol) {
            case IPFS.IDENTITY_PROTOCOL: {

                if (stream.isInitiator()) {
                    PeerInfo peerInfo = IdentityService.getPeerInfo(host,
                            IdentifyOuterClass.Identify.parseFrom(data.array()));

                    stream.setAttribute(OBSERVED, peerInfo.getObserved());
                    MuxedStream muxedStream = Objects.requireNonNull((MuxedStream) stream);

                    LogUtils.error(TAG, peerInfo.toString());

                    Multiaddrs addresses = peerInfo.getMultiaddrs();
                    Set<Multiaddr> addrs = new HashSet<>();
                    for (Multiaddr addr : addresses) {
                        // todo strictSupported false is only for my tests, I think
                        if (!addr.isCircuitAddress() && addr.protocolSupported(
                                host.getProtocolSupport(), false)) {
                            addrs.add(addr);
                        }

                    }
                    LogUtils.error(TAG, addrs.toString());
                    if (addrs.isEmpty()) {
                        if (peerInfo.hasHolePunch()) {
                            if (host.holePunchAllowed()) {
                                HolePunch.initiate(muxedStream.getTransport());
                            }
                        }
                    } else {

                        Multiaddr multiaddr = host.resolveToAny(addrs);
                        Objects.requireNonNull(multiaddr);

                        // this operation blocks
                        try {
                            host.connect(session, multiaddr,
                                    Parameters.getDefault(), false);
                        } catch (Throwable ignore) {
                            if (peerInfo.hasHolePunch()) {
                                if (host.holePunchAllowed()) {
                                    HolePunch.initiate(muxedStream.getTransport());
                                }
                            }
                        }
                    }
                    break;
                } else {
                    throw new Exception("wrong initiator");
                }

            }
            case IPFS.NOISE_PROTOCOL: {

                if (!stream.isInitiator()) {
                    Noise.Response response = getResponder(stream).handshake(data.array());

                    byte[] msg = response.getMessage();
                    if (msg != null) {
                        stream.writeOutput(Noise.encodeNoiseMessage(msg));
                    }

                    CipherStatePair cipherStatePair = response.getCipherStatePair();
                    if (cipherStatePair != null) {
                        LiteStream liteStream = Objects.requireNonNull((LiteStream) stream);
                        stream.setAttribute(TRANSPORT, new SecuredTransport(
                                liteStream.getQuicStream(),
                                cipherStatePair.getSender(),
                                cipherStatePair.getReceiver()));

                        LogUtils.error(TAG, "Transport set to SecuredTransport");
                    }
                } else {
                    throw new Exception("wrong initiator");
                }
                break;
            }

            case IPFS.HOLE_PUNCH_PROTOCOL: {
                if (!stream.isInitiator()) {
                    throw new Exception("wrong initiator");
                } else {
                    PeerId peerId = Objects.requireNonNull((PeerId) stream.getAttribute(PEER));
                    HolePunch.sendSync(host, stream, peerId, data);
                }
                break;
            }
            case IPFS.RELAY_PROTOCOL_STOP: {
                if (!stream.isInitiator()) {
                    Circuit.StopMessage stopMessage = Circuit.StopMessage.parseFrom(data.array());
                    Objects.requireNonNull(stopMessage);

                    if (stopMessage.hasPeer()) {
                        PeerId peerId = PeerId.create(stopMessage.getPeer().getId().toByteArray());
                        if (!host.isPeerGated(peerId)) {
                            Circuit.StopMessage.Builder builder =
                                    Circuit.StopMessage.newBuilder()
                                            .setType(Circuit.StopMessage.Type.STATUS);
                            builder.setStatus(Circuit.Status.OK);
                            stream.setAttribute(PEER, peerId);
                            stream.writeOutput(DataHandler.encode(builder.build()));
                        } else {
                            Circuit.StopMessage.Builder builder =
                                    Circuit.StopMessage.newBuilder()
                                            .setType(Circuit.StopMessage.Type.STATUS);
                            builder.setStatus(Circuit.Status.RESERVATION_REFUSED);
                            stream.writeOutput(DataHandler.encode(builder.build()));
                        }
                    } else {
                        Circuit.StopMessage.Builder builder =
                                Circuit.StopMessage.newBuilder()
                                        .setType(Circuit.StopMessage.Type.STATUS);
                        builder.setStatus(Circuit.Status.MALFORMED_MESSAGE);
                        stream.writeOutput(DataHandler.encode(builder.build()));
                    }
                }
                break;
            }
            default:
                ProtocolHandler protocolHandler = null;
                // Note : also the relay can only use server functions when it is static
                if (getReservationKind() == Reservation.Kind.STATIC) {
                    protocolHandler = session.getProtocolHandler(protocol);
                }
                if (protocolHandler != null) {
                    protocolHandler.protocol(stream);
                } else {
                    throw new Exception("Protocol " + protocol + " not supported data " + data);
                }
        }

    }

    @Override
    public void streamTerminated(QuicStream quicStream) {
        LogUtils.error(TAG, "todo stream is terminated ");
    }

}
