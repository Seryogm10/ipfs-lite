package threads.lite.bitswap;


import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.BitSwap;
import threads.lite.core.Block;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteHost;
import threads.lite.utils.DataHandler;


public class BitSwapManager implements BitSwap {

    private static final String TAG = BitSwapManager.class.getSimpleName();
    @NonNull
    private final LiteHost host;
    @NonNull
    private final Session session;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final Set<Multiaddr> motherfuckers = ConcurrentHashMap.newKeySet();
    @NonNull
    private final ConcurrentHashMap<Multiaddr, Connection> peers = new ConcurrentHashMap<>();
    @NonNull
    private final BitSwapEngine bitSwapEngine;
    @NonNull
    private final ExecutorService executorService = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors());

    @NonNull
    private final Registry registry = new Registry();

    public BitSwapManager(@NonNull LiteHost host, @NonNull Session session) {
        this.host = host;
        this.session = session;
        this.blockStore = session.getBlockStore();
        this.bitSwapEngine = new BitSwapEngine(blockStore);
    }

    private void writeMessage(@NonNull Connection connection,
                              @NonNull MessageOuterClass.Message msg) {


        connection.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        stream.getConnection().close();
                    }

                    @Override
                    public void protocol(Stream stream, String protocol) throws Exception {
                        if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                                IPFS.BITSWAP_PROTOCOL).contains(protocol)) {
                            throw new Exception("Token " + protocol + " not supported");
                        }
                        if (Objects.equals(protocol, IPFS.BITSWAP_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(msg))
                                    .thenApply(Stream::closeOutput);
                        }
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) {
                        LogUtils.error(TAG, "data writeMessage invoked");

                    }
                })
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL)));

    }

    @Override
    public void close() {
        try {
            executorService.shutdown();
            executorService.shutdownNow();
            peers.values().forEach(Connection::close);
        } finally {
            registry.clear();
            peers.clear();
            motherfuckers.clear();
            bitSwapEngine.close();
        }
    }

    private void findProviders(@NonNull Cancellable cancellable, @NonNull Cid cid) {

        session.findProviders(cancellable, (multiaddr) -> {

            if (motherfuckers.contains(multiaddr)) {
                // not possible to connect
                return;
            }

            Connection conn = peers.get(multiaddr);
            if (conn != null) {
                if (!conn.isConnected()) {
                    peers.remove(multiaddr);
                } else {
                    // nothing to do here there
                    return;
                }
            }

            if (cancellable.isCancelled()) {
                return;
            }

            try {
                executorService.execute(() -> {
                    try {
                        if(cancellable.isCancelled()){
                            return;
                        }
                        Connection connection = host.connect(session, multiaddr,
                                Parameters.getDefault(), false);
                        LogUtils.error(TAG, "New connection " + multiaddr);
                        peers.put(multiaddr, connection);
                    } catch (ConnectException e) {
                        motherfuckers.add(multiaddr);
                    } catch (InterruptedException ignore) {
                        // ignore
                    }
                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage());
            }

        }, cid);


    }


    @NonNull
    public Block runWantHaves(@NonNull Cancellable cancellable, @NonNull Cid cid) throws Exception {

        registry.register(cid);

        AtomicBoolean providerStart = new AtomicBoolean(false);
        try {

            Set<Multiaddr> haves = new HashSet<>();

            while (!blockStore.hasBlock(cid)) {

                if (cancellable.isCancelled()) {
                    throw new Exception("canceled");
                }

                // fill the peers with the swarm connections
                session.getSwarm().forEach(connection ->
                        peers.putIfAbsent(connection.remoteMultiaddr(), connection));

                for (Multiaddr multiaddr : peers.keySet()) {
                    Connection conn = peers.get(multiaddr);
                    if (conn != null) {
                        if (!conn.isConnected()) {
                            peers.remove(multiaddr);
                            continue;
                        }
                        if (!haves.contains(multiaddr)) {
                            haves.add(multiaddr);
                            writeMessage(conn, BitSwapMessage.create(
                                    MessageOuterClass.Message.Wantlist.WantType.Have,
                                    Collections.singletonList(cid)));
                        }
                    }
                }

                if (cancellable.isCancelled()) {
                    throw new Exception("canceled");
                }

                if (session.isFindProvidersActive()) {

                    if (!providerStart.getAndSet(true)) {
                        Timer timer = new Timer();

                        int delay = 0;
                        if (!peers.isEmpty()) {
                            delay = IPFS.BITSWAP_REQUEST_DELAY * 2;
                        }

                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {

                                long start = System.currentTimeMillis();

                                findProviders(cancellable, cid);

                                LogUtils.info(TAG, "Load Provider Finish "
                                        + cid.String() +
                                        " onStart [" +
                                        (System.currentTimeMillis() - start) +
                                        "]...");

                                if (!cancellable.isCancelled()) {
                                    providerStart.set(false);
                                }
                            }
                        }, delay);

                    }

                }
            }
        } finally {
            registry.unregister(cid);
        }

        return Objects.requireNonNull(blockStore.getBlock(cid));
    }


    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {

        try {
            Block block = blockStore.getBlock(cid);
            if (block == null) {
                AtomicBoolean done = new AtomicBoolean(false);
                LogUtils.info(TAG, "Block Get " + cid.String());

                try {
                    return runWantHaves(() -> cancellable.isCancelled() || done.get(), cid);
                } finally {
                    done.set(true);
                }
            }
            return block;

        } finally {
            LogUtils.info(TAG, "Block Release  " + cid.String());
        }
    }

    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {

        BitSwapMessage msg = BitSwapMessage.create(bsm);

        for (Block block : msg.blocks()) {
            Cid cid = block.getCid();
            if (registry.isRegistered(cid)) {
                LogUtils.info(TAG, "Received Block " + cid.String() +
                        " " + connection.getRemoteAddress());
                blockStore.putBlock(block);
                registry.unregister(cid);
            }
        }

        for (Cid cid : msg.haves()) {
            if (registry.isRegistered(cid)) {
                registry.scheduleWants(cid, connection);
            }
        }

        if (session.isSendReplyActive()) {
            bitSwapEngine.receiveMessage(connection, bsm);
        }
    }


    private class Registry extends ConcurrentHashMap<Cid, Timer> {
        private final ConcurrentHashMap<Cid, Integer> delays = new ConcurrentHashMap<>();
        private final ConcurrentHashMap<Cid, List<PeerId>> sends = new ConcurrentHashMap<>();


        public void register(@NonNull Cid cid) {
            this.put(cid, new Timer());
            this.delays.put(cid, 0);
            this.sends.put(cid, new ArrayList<>());
        }

        public void unregister(@NonNull Cid cid) {
            Timer timer = this.remove(cid);
            if (timer != null) {
                timer.cancel();
            }
            this.delays.remove(cid);
            this.sends.remove(cid);
        }

        public boolean isRegistered(@NonNull Cid cid) {
            return this.containsKey(cid);
        }


        public void clear() {
            values().forEach(Timer::cancel);
            super.clear();
            delays.clear();
            sends.clear();
        }

        private boolean hasSend(@NonNull Cid cid, @NonNull PeerId peerId) {
            List<PeerId> peerIds = sends.get(cid);
            Objects.requireNonNull(peerIds);
            if (peerIds.contains(peerId)) {
                return true;
            } else {
                peerIds.add(peerId);
                return false;
            }
        }

        public void scheduleWants(@NonNull Cid cid, @NonNull Connection conn) {

            Timer timer = this.get(cid);
            if (timer != null) {

                if (hasSend(cid, conn.remotePeerId())) {
                    return;
                }

                int delay = delays.computeIfAbsent(cid, cid1 -> 0);

                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        LogUtils.debug(TAG, "scheduleWants " +
                                cid.String() + " " + conn.getRemoteAddress());
                        try {
                            writeMessage(conn, BitSwapMessage.create(
                                    MessageOuterClass.Message.Wantlist.WantType.Block,
                                    Collections.singletonList(cid)));
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    }
                }, delay);

                delays.put(cid, delay + IPFS.BITSWAP_REQUEST_DELAY);
            }

        }
    }
}
