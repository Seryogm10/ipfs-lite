package threads.lite.bitswap;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.utils.DataHandler;


public final class BitSwapEngine {
    private static final String TAG = BitSwapEngine.class.getSimpleName();
    private final BlockStore blockstore;
    private final ExecutorService service = Executors.newFixedThreadPool(
            Runtime.getRuntime().availableProcessors());
    private final Map<Task, Future<?>> tasks = new ConcurrentHashMap<>();
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public BitSwapEngine(@NonNull BlockStore blockStore) {
        this.blockstore = blockStore;
    }


    public void close() {
        closed.set(true);
        try {
            service.shutdown();
            boolean termination = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            tasks.clear();
        }
    }

    public void receiveMessage(@NonNull Connection conn, @NonNull MessageOuterClass.Message bsm) {

        if (!closed.get()) {
            BitSwapMessage.evaluateResponse(blockstore, bsm, cid -> {
                // this is for cancel task
                Task task = new Task(conn.remotePeerId(), cid, Type.BLOCK);
                removeTask(task);
            }, cid -> {
                // this is for a block message
                Task task = new Task(conn.remotePeerId(), cid, Type.BLOCK);
                if (!tasks.containsKey(task)) {
                    tasks.put(task, service.submit(() -> {
                        try {
                            MessageOuterClass.Message message =
                                    BitSwapMessage.createBlockMessage(blockstore, cid);
                            sendReply(conn, message).get(IPFS.GRACE_PERIOD, TimeUnit.SECONDS); // can take time
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            removeTask(task);
                        }
                    }));
                }
            }, cid -> {
                // this is for create dont have messages
                Task task = new Task(conn.remotePeerId(), cid, Type.DONT_HAVE);
                if (!tasks.containsKey(task)) {
                    tasks.put(task, service.submit(() -> {
                        try {
                            MessageOuterClass.Message message = BitSwapMessage.createDontHave(cid);
                            sendReply(conn, message).get(1, TimeUnit.SECONDS);  // should go fast
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            removeTask(task);
                        }
                    }));
                }
            }, cid -> {
                // this is for create have messages
                Task task = new Task(conn.remotePeerId(), cid, Type.HAVE);
                if (!tasks.containsKey(task)) {
                    tasks.put(task, service.submit(() -> {
                        try {
                            MessageOuterClass.Message message = BitSwapMessage.createHave(cid);
                            sendReply(conn, message).get(1, TimeUnit.SECONDS); // should go fast
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        } finally {
                            removeTask(task);
                        }
                    }));
                }
            });
        }
    }

    private void removeTask(Task task) {
        try {
            Future<?> future = tasks.remove(task);
            if (future != null) {
                if (!future.isCancelled()) {
                    future.cancel(true);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private CompletableFuture<Void> sendReply(
            @NonNull Connection connection, @NonNull MessageOuterClass.Message msg) {

        CompletableFuture<Void> done = new CompletableFuture<>();
        connection.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        done.completeExceptionally(throwable);
                        connection.close();
                    }

                    @Override
                    public void protocol(Stream stream, String protocol) throws Exception {
                        if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                                IPFS.BITSWAP_PROTOCOL).contains(protocol)) {
                            throw new Exception("Token " + protocol + " not supported");
                        }
                        if (Objects.equals(protocol, IPFS.BITSWAP_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(msg))
                                    .thenApply(Stream::closeOutput)
                                    .whenComplete((streamCompletableFuture, throwable) -> {
                                        if (throwable != null) {
                                            done.completeExceptionally(throwable);
                                        } else {
                                            done.complete(null);
                                        }
                                    });
                        }
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) {
                        LogUtils.error(TAG, "data sendReply invoked");
                    }

                })
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL)));
        return done;
    }

    public enum Type {
        BLOCK, HAVE, DONT_HAVE
    }

    public static class Task {
        private final PeerId peerId;
        private final Cid cid;
        private final Type type;

        public Task(PeerId peerId, Cid cid, Type type) {
            this.peerId = peerId;
            this.cid = cid;
            this.type = type;
        }

        @NonNull
        @Override
        public String toString() {
            return "Task{" +
                    "peerId=" + peerId +
                    ", cid=" + cid +
                    ", type=" + type +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Task task = (Task) o;
            return peerId.equals(task.peerId) && cid.equals(task.cid) && type == task.type;
        }

        @Override
        public int hashCode() {
            return Objects.hash(peerId, cid, type);
        }
    }

}
