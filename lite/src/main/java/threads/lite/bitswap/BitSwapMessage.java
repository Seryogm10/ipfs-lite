package threads.lite.bitswap;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.cid.Prefix;
import threads.lite.core.Block;
import threads.lite.core.BlockStore;


public final class BitSwapMessage {

    private final HashMap<Cid, Block> blocks = new HashMap<>();
    private final List<Cid> haves = new ArrayList<>();


    public static BitSwapMessage create(MessageOuterClass.Message pbm) throws Exception {
        BitSwapMessage m = new BitSwapMessage();


        // deprecated
        for (ByteString data : pbm.getBlocksList()) {
            // CIDv0, sha256, protobuf only
            Block block = Block.createBlock(Merkledag.PBNode.parseFrom(data.toByteArray()));
            m.blocks.put(block.getCid(), block);
        }

        for (MessageOuterClass.Message.Block payload : pbm.getPayloadList()) {
            ByteString prefix = payload.getPrefix();
            Prefix pref = Prefix.getPrefixFromBytes(prefix.toByteArray());
            Cid cid = Prefix.sum(pref, payload.getData().toByteArray());
            Block block = Block.createBlockWithCid(cid,
                    Merkledag.PBNode.parseFrom(payload.getData().toByteArray()));
            m.blocks.put(block.getCid(), block);
        }

        for (MessageOuterClass.Message.BlockPresence bi : pbm.getBlockPresencesList()) {
            Cid cid = Cid.fromArray(bi.getCid().toByteArray());
            Objects.requireNonNull(cid);
            if (!m.blocks.containsKey(cid)) {
                if (bi.getType() == MessageOuterClass.Message.BlockPresenceType.Have) {
                    m.haves.add(cid);
                }
            }
        }

        return m;
    }


    public static void evaluateResponse(@NonNull BlockStore blockstore,
                                        @NonNull MessageOuterClass.Message bsm,
                                        @NonNull Consumer<Cid> cancelConsumer,
                                        @NonNull Consumer<Cid> wantsConsumer,
                                        @NonNull Consumer<Cid> dontHaveConsumer,
                                        @NonNull Consumer<Cid> haveConsumer) {

        for (MessageOuterClass.Message.Wantlist.Entry entry :
                bsm.getWantlist().getEntriesList()) {
            if (!entry.getCancel()) {
                // For each want-have / want-block
                Cid cid = Cid.fromArray(entry.getBlock().toByteArray());
                Objects.requireNonNull(cid);
                if (!blockstore.hasBlock(cid)) {
                    // Only add the task to the queue if the requester wants a DONT_HAVE
                    if (entry.getSendDontHave()) {
                        // Add DONT_HAVEs to the message
                        dontHaveConsumer.accept(cid);
                    }
                } else {

                    boolean isWantBlock =
                            entry.getWantType() == MessageOuterClass.Message.Wantlist.WantType.Block;

                    if (isWantBlock) {
                        // Add BLOCK
                        wantsConsumer.accept(cid);
                    } else {
                        // Add HAVES
                        haveConsumer.accept(cid);
                    }
                }
            } else {
                cancelConsumer.accept(Cid.fromArray(entry.getBlock().toByteArray()));
            }
        }

    }


    @NonNull
    public static MessageOuterClass.Message create(
            @NonNull MessageOuterClass.Message.Wantlist.WantType wantType,
            @NonNull List<Cid> cids) throws Exception {

        if (cids.isEmpty()) {
            throw new Exception("no cids defined");
        }

        List<MessageOuterClass.Message.Wantlist.Entry> wantlist = new ArrayList<>();

        int priority = Integer.MAX_VALUE;

        for (Cid cid : cids) {
            wantlist.add(MessageOuterClass.Message.Wantlist.Entry
                    .newBuilder()
                    .setBlock(ByteString.copyFrom(cid.bytes()))
                    .setPriority(priority)
                    .setCancel(false)
                    .setWantType(wantType)
                    .setSendDontHave(false)
                    .build());

            priority--;
        }

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        MessageOuterClass.Message.Wantlist.Builder wantBuilder =
                MessageOuterClass.Message.Wantlist.newBuilder();


        for (MessageOuterClass.Message.Wantlist.Entry entry : wantlist) {
            wantBuilder.addEntries(entry);
        }
        wantBuilder.setFull(false);
        builder.setWantlist(wantBuilder.build());
        builder.setPendingBytes(0);

        return builder.build();


    }


    @NonNull
    public static MessageOuterClass.Message createDontHave(Cid cid) {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        builder.addBlockPresences(MessageOuterClass.Message.BlockPresence.newBuilder()
                .setType(MessageOuterClass.Message.BlockPresenceType.DontHave)
                .setCid(ByteString.copyFrom(cid.bytes())));
        builder.setPendingBytes(0);

        return builder.build();

    }

    @NonNull
    public static MessageOuterClass.Message createHave(Cid cid) {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        builder.addBlockPresences(MessageOuterClass.Message.BlockPresence.newBuilder()
                .setType(MessageOuterClass.Message.BlockPresenceType.Have)
                .setCid(ByteString.copyFrom(cid.bytes())));
        builder.setPendingBytes(0);

        return builder.build();

    }

    @NonNull
    public static MessageOuterClass.Message createBlockMessage(
            BlockStore blockstore, Cid cid) throws IOException {

        MessageOuterClass.Message.Builder builder = MessageOuterClass.Message.newBuilder();

        byte[] data = blockstore.getData(cid);
        Objects.requireNonNull(data);
        builder.addPayload(MessageOuterClass.Message.Block.newBuilder()
                .setData(ByteString.copyFrom(data))
                .setPrefix(ByteString.copyFrom(cid.getPrefix().bytes())).build());
        builder.setPendingBytes(0);

        return builder.build();

    }

    public Collection<Block> blocks() {
        return blocks.values();
    }

    public List<Cid> haves() {
        return haves;
    }


}
