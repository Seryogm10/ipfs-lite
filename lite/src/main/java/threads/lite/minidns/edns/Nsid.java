/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns.edns;

import java.nio.charset.StandardCharsets;

import threads.lite.minidns.DnsUtility;
import threads.lite.minidns.edns.Edns.OptionCode;

public class Nsid extends EdnsOption {

    public Nsid(byte[] payload) {
        super(payload);
    }

    @Override
    public OptionCode getOptionCode() {
        return OptionCode.NSID;
    }

    @Override
    protected CharSequence toStringInternal() {
        String res = OptionCode.NSID + ": ";
        res += new String(optionData, StandardCharsets.US_ASCII);
        return res;
    }

    @Override
    protected CharSequence asTerminalOutputInternal() {
        return DnsUtility.from(optionData);
    }

}
