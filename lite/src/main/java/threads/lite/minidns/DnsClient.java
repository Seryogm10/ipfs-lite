/*
 * Copyright 2015-2022 the original author or authors
 *
 * This software is licensed under the Apache License, Version 2.0,
 * the GNU Lesser General Public License version 2 or later ("LGPL")
 * and the WTFPL.
 * You may choose either license to govern your use of this software only
 * upon the condition that you accept all of the terms of either
 * the Apache License 2.0, the LGPL 2.1+ or the WTFPL.
 */
package threads.lite.minidns;

import java.io.IOException;
import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import threads.lite.LogUtils;
import threads.lite.minidns.DnsException.ErrorResponseException;
import threads.lite.minidns.DnsException.NoQueryPossibleException;
import threads.lite.minidns.record.Data;
import threads.lite.minidns.record.Record;

/**
 * A minimal DNS client for TXT lookups, with IDN support.
 * This circumvents the missing javax.naming package on android.
 */
public final class DnsClient {
    private static final String TAG = DnsClient.class.getSimpleName();

    /**
     * The internal random class for sequence generation.
     */
    private final Random random;

    /**
     * The internal DNS cache.
     */
    private final DnsCache cache;
    private final DnsDataSource dataSource = new DnsDataSource();
    /**
     * This callback is used by the synchronous query() method <b>and</b> by the asynchronous queryAync() method in order to update the
     * cache. In the asynchronous case, hand this callback into the async call, so that it can get called once the result is available.
     */
    private final DnsDataSource.OnResponseCallback onResponseCallback = new DnsDataSource.OnResponseCallback() {
        @Override
        public void onResponse(DnsMessage requestMessage, DnsQueryResult responseMessage) {
            final DnsQuestion q = requestMessage.getQuestion();
            if (cache != null && isResponseCacheable(q, responseMessage)) {
                cache.put(requestMessage.asNormalizedVersion(), responseMessage);
            }
        }
    };
    private final Set<InetAddress> nonRaServers =
            Collections.newSetFromMap(new ConcurrentHashMap<>(4));
    private final Supplier<List<InetAddress>> settingSupplier;

    /**
     * Create a new DNS client with the given DNS cache.
     *
     * @param dnsCache The backend DNS cache.
     */
    public DnsClient(Supplier<List<InetAddress>> settingSupplier, DnsCache dnsCache) {
        this.settingSupplier = settingSupplier;

        Random random;
        try {
            random = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e1) {
            random = new SecureRandom();
        }
        this.random = random;
        this.cache = dnsCache;
    }

    /**
     * Query the system nameservers for a single entry of the class IN
     * (which is used for MX, SRV, A, AAAA and most other RRs).
     *
     * @param name The DNS name to request.
     * @param type The DNS type to request (SRV, A, AAAA, ...).
     * @return The response (or null on timeout/error).
     * @throws IOException if an IO error occurs.
     */
    public DnsQueryResult query(CharSequence name, Record.TYPE type) throws IOException {
        DnsQuestion q = new DnsQuestion(name, type, Record.CLASS.IN);
        return query(q);
    }

    public DnsQueryResult query(DnsQuestion q) throws IOException {
        DnsMessage.Builder query = buildMessage(q);
        return query(query);
    }

    public DnsQueryResult query(DnsMessage requestMessage, InetAddress address, int port) throws IOException {
        // See if we have the answer to this question already cached
        DnsQueryResult responseMessage = (cache == null) ? null : cache.get(requestMessage);
        if (responseMessage != null) {
            return responseMessage;
        }

        requestMessage.getQuestion();

        try {
            responseMessage = dataSource.query(requestMessage, address, port);
        } catch (IOException exception) {
            LogUtils.debug(TAG, exception.getMessage());
            throw exception;
        }

        onResponseCallback.onResponse(requestMessage, responseMessage);

        return responseMessage;
    }

    /**
     * Whether a response from the DNS system should be cached or not.
     *
     * @param q      The question the response message should answer.
     * @param result The DNS query result.
     * @return True, if the response should be cached, false otherwise.
     */
    private boolean isResponseCacheable(DnsQuestion q, DnsQueryResult result) {
        DnsMessage dnsMessage = result.response;
        for (Record<? extends Data> record : dnsMessage.answerSection) {
            if (record.isAnswer(q)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Builds a {@link DnsMessage} object carrying the given Question.
     *
     * @param question {@link DnsQuestion} to be put in the DNS request.
     * @return A {@link DnsMessage} requesting the answer for the given Question.
     */
    DnsMessage.Builder buildMessage(DnsQuestion question) {
        DnsMessage.Builder message = DnsMessage.builder();
        message.setQuestion(question);
        message.setId(random.nextInt());
        newQuestion(message);
        return message;
    }

    public DnsQueryResult query(DnsMessage query, InetAddress host) throws IOException {
        return query(query, host, 53);
    }

    private DnsMessage.Builder newQuestion(DnsMessage.Builder message) {
        message.setRecursionDesired(true);
        boolean askForDnssec = false;
        message.getEdnsBuilder().setUdpPayloadSize(dataSource.getUdpPayloadSize()).setDnssecOk(askForDnssec);
        return message;
    }

    private List<InetAddress> getServerAddresses() {
        return settingSupplier.get();
    }

    public DnsQueryResult query(DnsMessage.Builder queryBuilder) throws IOException {
        DnsMessage q = newQuestion(queryBuilder).build();
        // While this query method does in fact re-use query(Question, String)
        // we still do a cache lookup here in order to avoid unnecessary
        // findDNS()calls, which are expensive on Android. Note that we do not
        // put the results back into the Cache, as this is already done by
        // query(Question, String).
        DnsQueryResult dnsQueryResult = (cache == null) ? null : cache.get(q);
        if (dnsQueryResult != null) {
            return dnsQueryResult;
        }

        List<InetAddress> dnsServerAddresses = getServerAddresses();

        List<IOException> ioExceptions = new ArrayList<>(dnsServerAddresses.size());
        for (InetAddress dns : dnsServerAddresses) {
            if (nonRaServers.contains(dns)) {
                LogUtils.debug(TAG, "Skipping " + dns + " because it was marked as \"recursion not available\"");
                continue;
            }

            try {
                dnsQueryResult = query(q, dns);
            } catch (IOException ioe) {
                ioExceptions.add(ioe);
                continue;
            }

            DnsMessage responseMessage = dnsQueryResult.response;
            if (!responseMessage.recursionAvailable) {
                boolean newRaServer = nonRaServers.add(dns);
                if (newRaServer) {
                    LogUtils.warning(TAG, "The DNS server " + dns
                            + " returned a response without the \"recursion available\" (RA) flag " +
                            "set. This likely indicates a misconfiguration because the " +
                            "server is not suitable for DNS resolution");
                }
                continue;
            }

            switch (responseMessage.responseCode) {
                case NO_ERROR:
                case NX_DOMAIN:
                    break;
                default:
                    String warning = "Response from " + dns + " asked for " + q.getQuestion() +
                            " with error code: " + responseMessage.responseCode + '.';
                    LogUtils.warning(TAG, warning);

                    ErrorResponseException exception = new ErrorResponseException(dnsQueryResult);
                    ioExceptions.add(exception);
                    continue;
            }

            return dnsQueryResult;
        }
        MultipleIoException.throwIfRequired(ioExceptions);

        throw new NoQueryPossibleException();
    }

}
