package threads.lite.ident;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.PeerInfo;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;
import threads.lite.host.LiteHost;
import threads.lite.utils.DataHandler;

public class IdentityService {

    @NonNull
    public static CompletableFuture<PeerInfo> getPeerInfo(LiteHost host, Connection conn) {
        CompletableFuture<PeerInfo> done = new CompletableFuture<>();
        IdentityService.getIdentity(conn).whenComplete((identify, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                try {
                    done.complete(getPeerInfo(host, identify));
                } catch (Throwable throwable1) {
                    done.completeExceptionally(throwable1);
                }
            }
        });
        return done;
    }

    @NonNull
    public static PeerInfo getPeerInfo(@NonNull LiteHost host,
                                       @NonNull IdentifyOuterClass.Identify identify) throws Exception {

        String agent = identify.getAgentVersion();
        String version = identify.getProtocolVersion();


        PubKey pk = Key.unmarshalPublicKey(identify.getPublicKey().toByteArray());
        PeerId peerId = PeerId.fromPubKey(pk);
        Multiaddr observedAddr = Multiaddr.create(host.self(),
                identify.getObservedAddr().asReadOnlyByteBuffer());

        List<String> protocols = identify.getProtocolsList();
        Multiaddrs addresses = Multiaddr.create(peerId, identify.getListenAddrsList());

        return new PeerInfo(peerId, agent, version, addresses, protocols, observedAddr);
    }

    @NonNull
    private static CompletableFuture<IdentifyOuterClass.Identify> getIdentity(
            @NonNull Connection conn) {

        CompletableFuture<IdentifyOuterClass.Identify> done = new CompletableFuture<>();

        conn.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {
                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }
                if (Objects.equals(protocol, IPFS.IDENTITY_PROTOCOL)) {
                    stream.closeOutput();
                }
            }

            @Override
            public void data(Stream stream, ByteBuffer data) throws Exception {
                done.complete(IdentifyOuterClass.Identify.parseFrom(data.array()));
            }

        }).whenComplete((stream, throwable) -> {
            if (throwable != null) {
                done.completeExceptionally(throwable);
            } else {
                stream.writeOutput(
                        DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL, IPFS.IDENTITY_PROTOCOL));
            }
        });

        return done;

    }
}
