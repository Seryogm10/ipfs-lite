package threads.lite.utils;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Cid;
import threads.lite.core.Block;
import threads.lite.core.BlockStore;

public class BlockStoreMemory implements BlockStore {
    private final ConcurrentHashMap<Cid, Block> blocks = new ConcurrentHashMap<>();

    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return blocks.containsKey(cid);
    }

    @Override
    public Block getBlock(@NonNull Cid cid) {
        return blocks.get(cid);
    }

    @Override
    public byte[] getData(@NonNull Cid cid) {
        try {
            return Objects.requireNonNull(getBlock(cid)).getNode().toByteArray();
        } catch (Throwable throwable) {
            return null;
        }
    }

    @Override
    public void deleteBlock(@NonNull Cid cid) {
        blocks.remove(cid);
    }

    @Override
    public void deleteBlocks(@NonNull List<Cid> cids) {
        for (Cid cid : cids) {
            deleteBlock(cid);
        }
    }

    @Override
    public void putBlock(@NonNull Block block) {
        blocks.put(block.getCid(), block);
    }

    @Override
    public void clear() {
        blocks.clear();
    }
}
