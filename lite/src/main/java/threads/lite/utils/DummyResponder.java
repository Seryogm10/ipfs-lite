package threads.lite.utils;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;

import threads.lite.LogUtils;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;

public class DummyResponder implements StreamHandler {
    private static final String TAG = DummyResponder.class.getSimpleName();

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable);
        stream.close();
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {
        throw new Exception("no incoming stream");
    }

    @Override
    public void fin() {
        LogUtils.error(TAG, "fin invoked on exception handler");
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        throw new Exception("no incoming stream");
    }

    @NonNull
    @Override
    public Transport getTransport(QuicStream quicStream) {
        quicStream.closeInput();
        quicStream.closeOutput();
        LogUtils.error(TAG, "getTransport invoked on exception handler");
        return new LiteTransport(quicStream);
    }

    @Override
    public void streamTerminated(QuicStream quicStream) {
        quicStream.closeInput();
        quicStream.closeOutput();
        LogUtils.error(TAG, "streamTerminated invoked on exception handler");
    }
}
