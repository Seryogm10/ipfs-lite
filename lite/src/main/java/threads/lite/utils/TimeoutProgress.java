package threads.lite.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import threads.lite.core.Cancellable;
import threads.lite.core.Progress;

public abstract class TimeoutProgress implements Progress {

    private final long timeout;
    @Nullable
    private final Cancellable cancellable;
    private final long start;

    public TimeoutProgress(long timeout) {
        this.cancellable = null;
        this.timeout = timeout;
        this.start = System.currentTimeMillis();
    }

    public TimeoutProgress(@NonNull Cancellable cancellable, long timeout) {
        this.cancellable = cancellable;
        this.timeout = timeout;
        this.start = System.currentTimeMillis();
    }

    @Override
    public boolean isCancelled() {
        if (cancellable != null) {
            return cancellable.isCancelled() || (System.currentTimeMillis() - start) > (timeout * 1000);
        }
        return (System.currentTimeMillis() - start) > (timeout * 1000);
    }


}
