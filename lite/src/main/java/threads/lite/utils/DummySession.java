package threads.lite.utils;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.Block;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;


public final class DummySession implements Session {


    public DummySession() {
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        throw new Exception("should not be invoked");
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) {

    }

    @Override
    public boolean swarmHas(@NonNull PeerId peerId) {
        return false;
    }

    @Override
    public void swarmReduce(@NonNull PeerId peerId) {
    }

    @Override
    public boolean swarmEnhance(@NonNull Connection connection) {
        return false;
    }

    @NonNull
    @Override
    public BlockStore getBlockStore() {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public boolean isFindProvidersActive() {
        return false;
    }

    @Override
    public boolean isSendReplyActive() {
        return false;
    }

    @NonNull
    @Override
    public Map<String, ProtocolHandler> getProtocols() {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) {
        throw new RuntimeException("should not be invoked");
    }

    @NonNull
    @Override
    public StreamHandler getStreamHandler() {
        return new DummyResponder();
    }

    @Override
    @NonNull
    public List<Connection> getSwarm() {
        return Collections.emptyList();
    }

    @Override
    public boolean swarmContains(@NonNull Connection connection) {
        return false;
    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] key, @NonNull byte[] data) {
        throw new RuntimeException("should not be invoked");
    }

    @Override

    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] key) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked");
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked");
    }

    @NonNull
    @Override
    public Set<Peer> getBootstrapPeers() {
        return Collections.emptySet();
    }

    @Override
    public void close() {
    }
}

