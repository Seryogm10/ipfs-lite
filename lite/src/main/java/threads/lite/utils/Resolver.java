package threads.lite.utils;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;
import threads.lite.dag.DagReader;
import threads.lite.dag.DagService;


public class Resolver {

    @NonNull
    public static Cid resolveNode(@NonNull Cancellable cancellable,
                                  @NonNull Session session, @NonNull Cid root,
                                  @NonNull List<String> path) throws Exception {

        DagService dagService = DagService.createDagService(session);
        Pair<Cid, List<String>> resolved = resolveToLastNode(cancellable, dagService, root, path);
        Cid cid = resolved.first;
        Objects.requireNonNull(cid);
        // make sure it is resolved
        Merkledag.PBNode res = resolveNode(cancellable, dagService, cid);
        Objects.requireNonNull(res);
        return cid;
    }

    @NonNull
    public static Merkledag.PBNode resolveNode(@NonNull Cancellable cancellable,
                                               @NonNull DagService nodeGetter,
                                               @NonNull Cid cid) throws Exception {
        return nodeGetter.getNode(cancellable, cid);
    }

    @NonNull
    private static Pair<Cid, List<String>> resolveToLastNode(@NonNull Cancellable cancellable,
                                                             @NonNull DagService dagService,
                                                             @NonNull Cid root,
                                                             @NonNull List<String> path)
            throws Exception {

        if (path.size() == 0) {
            return Pair.create(root, Collections.emptyList());
        }

        Cid cid = root;
        Merkledag.PBNode node = dagService.getNode(cancellable, cid);

        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            Objects.requireNonNull(lnk);
            cid = Objects.requireNonNull(Cid.fromArray(lnk.getHash().toByteArray()));
            node = dagService.getNode(cancellable, cid);
        }

        return Pair.create(cid, Collections.emptyList());

    }

}
