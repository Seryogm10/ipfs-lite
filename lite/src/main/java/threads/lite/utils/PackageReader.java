package threads.lite.utils;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import threads.lite.LogUtils;
import threads.lite.core.FrameReader;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.mplex.MuxedStream;
import threads.lite.noise.SecuredStream;

public class PackageReader implements Consumer<RawStreamData> {
    private static final String TAG = PackageReader.class.getSimpleName();

    @NonNull
    private final StreamHandler streamHandler;
    @NonNull
    private final State state = new State();

    public PackageReader(@NonNull StreamHandler streamHandler) {
        this.streamHandler = streamHandler;
    }

    private void accept(Stream stream, List<ByteBuffer> frames, boolean isFinal) {
        for (ByteBuffer frame : frames) {
            accept(stream, frame, isFinal);
        }
    }

    private void accept(Stream stream, ByteBuffer frame, boolean isFinal) {
        try {

            byte[] data = frame.array();
            if (DataHandler.isProtocol(data)) {
                try {
                    streamHandler.protocol(stream, new String(data, 0, data.length - 1));
                } catch (Throwable throwable) {
                    streamHandler.throwable(stream, throwable);
                }
            } else {
                streamHandler.data(stream, frame);
            }

            if (isFinal) {
                streamHandler.fin();
            }

        } catch (Throwable exception) {
            streamHandler.throwable(stream, exception);
        }
    }

    private void iteration(State state, QuicStream quicStream, ByteBuffer bytes,
                           Consumer<ByteBuffer> frameConsumer) throws Exception {

        if (!state.reset) {
            if (state.length() == 0) {
                Transport transport = streamHandler.getTransport(quicStream);
                FrameReader frameReader = Transport.getFrameReader(transport);
                state.frame = frameReader.getFrame(bytes);
                if (state.length() <= 0) {
                    state.frame = null;
                    throw new Exception("invalid length of <= 0");
                } else {

                    int read = Math.min(state.length(), bytes.remaining());
                    for (int i = 0; i < read; i++) {
                        state.frame.put(bytes.get());
                    }

                    if (read == state.length()) {
                        ByteBuffer frame = Objects.requireNonNull(state.frame);
                        state.frame = null;
                        frameConsumer.accept(frame);
                    }

                    // check for a next iteration
                    if (bytes.remaining() > 0) {
                        iteration(state, quicStream, bytes, frameConsumer);
                    }
                }
            } else {
                ByteBuffer frame = Objects.requireNonNull(state.frame);
                int read = Math.min(frame.remaining(), bytes.remaining());
                for (int i = 0; i < read; i++) {
                    frame.put(bytes.get());
                }
                if (frame.remaining() == 0) { // frame is full
                    state.frame = null;
                    frameConsumer.accept(frame);
                }
                // check for a next iteration
                if (bytes.remaining() > 0) {
                    iteration(state, quicStream, bytes, frameConsumer);
                }
            }
        }
    }

    @Override
    public void accept(RawStreamData rawData) {

        QuicStream quicStream = rawData.getStream();

        if (rawData.isTerminated()) {
            state.reset();
            streamHandler.streamTerminated(quicStream);
            return;
        }


        try {
            iteration(state, quicStream, rawData.getStreamData(), frame -> {
                // upgrade stream
                Stream stream = streamHandler.getTransport(quicStream).getStream();
                try {
                    if (stream instanceof MuxedStream) {
                        MuxedStream muxedStream = (MuxedStream) stream;
                        accept(muxedStream, muxedStream.readFrames(frame), rawData.isFinal());
                    } else if (stream instanceof SecuredStream) {
                        SecuredStream securedStream = (SecuredStream) stream;
                        accept(securedStream, securedStream.readFrames(frame), rawData.isFinal());
                    } else {
                        accept(stream, frame, rawData.isFinal());
                    }
                } catch (Throwable throwable) {
                    streamHandler.throwable(stream, throwable);
                    state.reset();
                }
            });
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            LogUtils.error(TAG, state.toString());
            state.reset();
            quicStream.closeOutput();
            quicStream.closeInput();
            streamHandler.streamTerminated(quicStream);
        }

        if (rawData.isFinal()) {
            state.reset();
        }
    }

    public static class State {

        public ByteBuffer frame = null;
        public boolean reset = false;

        public void reset() {
            frame = null;
            reset = true;
        }

        public int length() {
            if (frame != null) {
                return frame.capacity();
            }
            return 0;
        }

        @NonNull
        @Override
        public String toString() {
            return "State{" + "frame=" + frame + ", reset=" + reset + '}';
        }
    }
}
