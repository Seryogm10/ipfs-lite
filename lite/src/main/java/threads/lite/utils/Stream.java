package threads.lite.utils;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Block;
import threads.lite.core.Cancellable;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.dag.Adder;
import threads.lite.dag.DagDirectory;
import threads.lite.dag.DagReader;
import threads.lite.dag.DagService;
import unixfs.pb.Unixfs;


public class Stream {


    private static Adder getFileAdder(@NonNull Session session) {
        return Adder.createAdder(session);
    }

    public static boolean isDir(@NonNull Cancellable cancellable,
                                @NonNull Session session,
                                @NonNull Cid cid) throws Exception {


        DagService dagService = DagService.createDagService(session);

        Merkledag.PBNode node = Resolver.resolveNode(cancellable, dagService, cid);
        Objects.requireNonNull(node);
        return DagDirectory.isDirectory(node);
    }

    @NonNull
    public static Cid createEmptyDirectory(@NonNull Session session) throws Exception {
        Adder fileAdder = getFileAdder(session);
        return fileAdder.createEmptyDirectory();
    }

    @NonNull
    public static Cid addLinkToDirectory(@NonNull Session session,
                                         @NonNull Cid directory,
                                         @NonNull Link link) throws Exception {

        Adder fileAdder = getFileAdder(session);
        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.getNode();
        Objects.requireNonNull(dirNode);
        return fileAdder.addChild(dirNode, link);

    }

    @NonNull
    public static Cid createDirectory(@NonNull Session session, @NonNull List<Link> links) throws Exception {
        Adder fileAdder = getFileAdder(session);
        return fileAdder.createDirectory(links);
    }

    @NonNull
    public static Cid removeFromDirectory(@NonNull Session session,
                                          @NonNull Cid directory,
                                          @NonNull String name) throws Exception {

        Adder fileAdder = getFileAdder(session);
        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.getNode();
        Objects.requireNonNull(dirNode);
        return fileAdder.removeChild(dirNode, name);
    }

    @NonNull
    public static List<Cid> getBlocks(@NonNull Session session, @NonNull Cid cid) throws Exception {
        List<Cid> result = new ArrayList<>();

        Block block = session.getBlockStore().getBlock(cid);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode node = block.getNode();
        List<Merkledag.PBLink> links = node.getLinksList();

        for (Merkledag.PBLink link : links) {
            Cid child = Objects.requireNonNull(Cid.fromArray(link.getHash().toByteArray()));
            result.add(child);
            result.addAll(getBlocks(session, child));
        }

        return result;
    }

    public static void ls(@NonNull Cancellable cancellable, @NonNull Consumer<Link> consumer,
                          @NonNull Session session, @NonNull Cid cid, boolean resolveChildren)
            throws Exception {

        DagService dagService = DagService.createDagService(session);


        Merkledag.PBNode node = Resolver.resolveNode(cancellable, dagService, cid);
        Objects.requireNonNull(node);
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            processLink(cancellable, consumer, dagService, link, resolveChildren);
        }
    }

    public static boolean hasLink(@NonNull Cancellable cancellable,
                                  @NonNull Session session, @NonNull Cid cid,
                                  @NonNull String name) throws Exception {

        DagService dagService = DagService.createDagService(session);

        Merkledag.PBNode node = Resolver.resolveNode(cancellable, dagService, cid);
        Objects.requireNonNull(node);
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    @NonNull
    public static Cid readInputStream(@NonNull Session session,
                                      @NonNull ReaderInputStream readerInputStream) throws Exception {

        Adder fileAdder = getFileAdder(session);
        return fileAdder.createFromStream(readerInputStream);
    }

    private static void processLink(@NonNull Cancellable cancellable,
                                    @NonNull Consumer<Link> consumer,
                                    @NonNull DagService dagService,
                                    @NonNull Merkledag.PBLink link, boolean resolveChildren)
            throws Exception {

        String name = link.getName();
        long size = link.getTsize();
        Cid cid = Objects.requireNonNull(Cid.fromArray(link.getHash().toByteArray()));


        if (!resolveChildren) {
            consumer.accept(Link.create(cid, name, size, Link.Unknown));
        } else {

            Merkledag.PBNode linkNode = dagService.getNode(cancellable, cid);

            Unixfs.Data data = DagReader.getData(linkNode);
            int type;
            switch (data.getType()) {
                case File:
                    type = Link.File;
                    break;
                case Raw:
                    type = Link.Raw;
                    break;
                case Directory:
                    type = Link.Dir;
                    break;
                case Symlink:
                case HAMTShard:
                case Metadata:
                default:
                    type = Link.Unknown;
            }
            size = data.getFilesize();
            consumer.accept(Link.create(cid, name, size, type));


        }

    }
}
