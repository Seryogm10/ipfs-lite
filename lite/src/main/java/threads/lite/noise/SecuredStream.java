package threads.lite.noise;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.southernstorm.noise.protocol.CipherState;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.host.LiteConnection;
import threads.lite.utils.DataHandler;

public class SecuredStream implements Stream {
    @NonNull
    private final QuicStream quicStream;
    @NonNull
    private final CipherState sender;
    @NonNull
    private final CipherState receiver;

    private SecuredStream(@NonNull QuicStream quicStream, @NonNull CipherState sender,
                          @NonNull CipherState receiver) {
        this.quicStream = quicStream;
        this.sender = sender;
        this.receiver = receiver;
    }

    public static SecuredStream createStream(SecuredTransport transport) {
        return new SecuredStream(transport.getQuicStream(),
                transport.getSender(),
                transport.getReceiver());
    }

    @NonNull
    public CipherState getSender() {
        return sender;
    }

    @NonNull
    public CipherState getReceiver() {
        return receiver;
    }

    @NonNull
    @Override
    public Connection getConnection() {
        return new LiteConnection(quicStream.getConnection());
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> closeOutput() {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        quicStream.closeOutput().whenComplete((quicStream, throwable) -> {
            if (throwable != null) {
                stream.completeExceptionally(throwable);
            } else {
                stream.complete(this);
            }
        });
        return stream;
    }

    @NonNull
    @Override
    public CompletableFuture<Stream> writeOutput(byte[] data) {
        CompletableFuture<Stream> stream = new CompletableFuture<>();
        try {
            byte[] encrypted = Noise.encrypt(sender, data);
            byte[] encoded = Noise.encodeNoiseMessage(encrypted);
            quicStream.writeOutput(encoded).whenComplete((quicStream, throwable) -> {
                if (throwable != null) {
                    stream.completeExceptionally(throwable);
                } else {
                    stream.complete(this);
                }
            });

        } catch (Throwable throwable) {
            stream.completeExceptionally(throwable);
        }
        return stream;
    }

    @Override
    public int getStreamId() {
        return quicStream.getStreamId();
    }

    @Override
    public void close() {
        quicStream.closeOutput();
        quicStream.closeInput();
    }

    @Override
    public void setAttribute(String key, Object value) {
        quicStream.setAttribute(key, value);
    }

    @Nullable
    @Override
    public Object getAttribute(String key) {
        return quicStream.getAttribute(key);
    }

    @Override
    public boolean isInitiator() {
        return getStreamId() % 2 == 0;
    }

    @NonNull
    public List<ByteBuffer> readFrames(ByteBuffer data) throws Exception {
        byte[] decrypted = Noise.decrypt(receiver, data.array());
        return DataHandler.decode(ByteBuffer.wrap(decrypted));

    }

    @NonNull
    public QuicStream getQuicStream() {
        return quicStream;
    }
}
