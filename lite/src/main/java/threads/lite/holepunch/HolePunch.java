package threads.lite.holepunch;


import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import holepunch.pb.Holepunch;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.cid.ProtocolSupport;
import threads.lite.core.Connection;
import threads.lite.core.Stream;
import threads.lite.host.LiteHost;
import threads.lite.mplex.MuxedStream;
import threads.lite.mplex.MuxedTransport;
import threads.lite.relay.RelayConnection;
import threads.lite.utils.DataHandler;

// Documentation https://github.com/libp2p/specs/blob/master/relay/DCUtR.md

// NAT traversal is a quintessential problem in peer-to-peer networks.
//
// We currently utilize relays, which allow us to traverse NATs by using a third party as proxy.
// Relays are a reliable fallback, that can connect peers behind NAT albeit with a high-latency,
// low-bandwidth connection. Unfortunately, they are expensive to scale and maintain if they have
// to carry all the NATed node traffic in the network.
//
// It is often possible for two peers behind NAT to communicate directly by utilizing a
// technique called hole punching[1]. The technique relies on the two peers synchronizing
// and simultaneously opening connections to each other to their predicted external address.
// It works well for UDP, and reasonably well for TCP.
//
// The problem in hole punching, apart from not working all the time, is the need for rendezvous
// and synchronization. This is usually accomplished using dedicated signaling servers [2]. However,
// this introduces yet another piece of infrastructure, while still requiring the use of relays
// as a fallback for the cases where a direct connection is not possible.
//
// In this specification, we describe a synchronization protocol for direct connectivity with
// hole punching that eschews signaling servers and utilizes existing relay connections instead.
// That is, peers start with a relay connection and synchronize directly, without the use of a
// signaling server. If the hole punching attempt is successful, the peers upgrade their
// connection to a direct connection and they can close the relay connection. If the hole punching
// attempt fails, they can keep using the relay connection as they were.
//
// Consider two peers, A and B. A wants to connect to B, which is behind a NAT and advertises
// relay addresses. A may itself be behind a NAT or be a public node.
//
// The protocol starts with the completion of a relay connection from A to B. Upon observing
// the new connection, the inbound peer (here B) checks the addresses advertised by A via identify.
// If that set includes public addresses, then A may be reachable by a direct connection,
// in which case B attempts a unilateral connection upgrade by initiating a direct connection to A.
//
// If the unilateral connection upgrade attempt fails or if A is itself a NATed peer that doesn't
// advertise public address, then B initiates the direct connection upgrade protocol as follows:
// (1) B opens a stream to A using the /libp2p/dcutr protocol. (->this has done before any of
// the function has to
//
// [here it starts the implementation see initialize]
//
public class HolePunch {
    private static final String TAG = HolePunch.class.getSimpleName();
    private static final String TIMER = "TIMER";
    private static final String ADDRS = "ADDRS";

    // initiate is invoking by [B]
    public static void initiate(MuxedTransport muxedTransport) throws Exception {
        // (1) B opens a stream to A using the /libp2p/dcutr protocol.

        MuxedStream muxedStream = MuxedStream.createStream(muxedTransport).get(
                IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        muxedStream.writeOutput(DataHandler.encodeProtocols(
                IPFS.MULTISTREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL)).get(
                IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
    }

    // initializeConnect is invoked by [B]
    public static void initializeConnect(Stream stream, Multiaddr observed) {
        // (2) B sends to A a Connect message containing its observed (and possibly predicted)
        // addresses from identify and starts a timer to measure RTT of the relay connection.
        Holepunch.HolePunch.Builder builder = Holepunch.HolePunch.newBuilder()
                .setType(Holepunch.HolePunch.Type.CONNECT);

        // Note: observed is B's observed address
        LogUtils.error(TAG, "[B] observed address " + observed);
        builder.addObsAddrs(ByteString.copyFrom(observed.getBytes()));

        Holepunch.HolePunch message = builder.build();
        stream.setAttribute(TIMER, System.currentTimeMillis());
        stream.writeOutput(DataHandler.encode(message));

    }

    // responseConnect is invoked by [A]
    public static void response(RelayConnection relayConnection,
                                Stream stream, ByteBuffer data) throws Exception {

        Holepunch.HolePunch holePunch = Holepunch.HolePunch.parseFrom(data.array());

        if (holePunch.getType() == Holepunch.HolePunch.Type.CONNECT) {

            // Upon receiving the Connect, A responds back with a Connect message containing
            // its observed (and possibly predicted) addresses.

            // peerId is from B
            LogUtils.error(TAG, "PeerId [B] " + relayConnection.getPeerId());
            Multiaddrs multiaddrs = create(relayConnection.getPeerId(), holePunch.getObsAddrsList(),
                    relayConnection.getHost().getProtocolSupport());

            LogUtils.error(TAG, "PeerId [B] Multiaddrs " + multiaddrs);

            // TODO maybe not throwing an exception, when static relay
            if (multiaddrs.size() == 0) {
                throw new Exception("[B] Empty Observed Multiaddrs from [A], abort");
            }

            stream.setAttribute(ADDRS, multiaddrs);

            LogUtils.error(TAG, "[A] Send Observed Address " +
                    relayConnection.getObserved());
            Holepunch.HolePunch.Builder builder =
                    Holepunch.HolePunch.newBuilder()
                            .setType(Holepunch.HolePunch.Type.CONNECT);

            builder.addObsAddrs(ByteString.copyFrom(relayConnection.getObserved().getBytes()));
            stream.writeOutput(DataHandler.encode(builder.build())).thenApply(Stream::closeOutput);
        } else if (holePunch.getType() == Holepunch.HolePunch.Type.SYNC) {
            // Upon receiving the Sync, A immediately dials the address to B.
            Multiaddrs multiaddrs = (Multiaddrs) stream.getAttribute(ADDRS);
            Objects.requireNonNull(multiaddrs, "No Multiaddrs"); // should not happen


            Multiaddr multiaddr = relayConnection.getHost().resolveToAny(multiaddrs);

            LogUtils.error(TAG, "[A] Connect Addresses [B] " + multiaddrs);

            Objects.requireNonNull(multiaddr);

            // this is blocking
            try {
                Connection connection = relayConnection.getHost().connect(
                        relayConnection.getSession(), multiaddr, relayConnection.getParameters(),
                        true);

                LogUtils.error(TAG, "[A] Success Hole Punching to [B] " +
                        connection.getRemoteAddress());
                relayConnection.upgradeConnection(connection);
            } catch (Throwable throwable) {
                LogUtils.error(TAG,
                        "[A] Failure Connect Address to [B] " + multiaddrs);
                relayConnection.throwable(throwable);
            }

        } else {
            throw new Exception("not expected hole punch type");
        }
    }

    // sendSync is is invoked by [B]
    public static void sendSync(LiteHost host, Stream stream, PeerId peerId,
                                ByteBuffer data) throws Exception {
        // (4) Upon receiving the Connect, B sends a Sync message and starts a timer for half the
        // RTT measured from the time between sending the initial Connect and receiving the
        // response. The purpose of the Sync message and B's timer is to allow the two peers
        // to synchronize so that they perform a simultaneous open that allows hole
        // punching to succeed.

        // note peerId is user [A]
        LogUtils.error(TAG, "[A] PeerId " + peerId);

        Long timer = (Long) stream.getAttribute(TIMER); // timer set earlier
        Objects.requireNonNull(timer, "Timer not set on stream");
        long rtt = System.currentTimeMillis() - timer;

        // B receives the Connect message from A
        Holepunch.HolePunch msg = Holepunch.HolePunch.parseFrom(data.array());

        LogUtils.error(TAG, "[B] Request took " + rtt);
        Objects.requireNonNull(msg);


        if (msg.getType() != Holepunch.HolePunch.Type.CONNECT) {
            throw new Exception("[A] send wrong message connect type, abort");
        }

        Multiaddrs multiaddrs = create(peerId, msg.getObsAddrsList(), host.getProtocolSupport());

        if (multiaddrs.size() == 0) {
            throw new Exception("[A] send no observed addresses, abort");
        }

        // now B sends a Sync message to A
        msg = Holepunch.HolePunch.newBuilder().
                setType(Holepunch.HolePunch.Type.SYNC).build();
        stream.writeOutput(DataHandler.encode(msg));


        // Next, B wait for rtt/2
        Thread.sleep(rtt / 2);

        LogUtils.error(TAG, "[B] Hole Punch Addresses [A] " + multiaddrs);

        // (5) Simultaneous Connect. The two nodes follow the steps below in parallel for every
        // address obtained from the Connect message

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        host.holePunchConnect(multiaddrs);


    }

    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings,
                                    ProtocolSupport protocolSupport) {
        Multiaddrs result = new Multiaddrs();
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings);
        for (Multiaddr addr : multiaddrs) {
            if (!addr.isCircuitAddress()) {
                if (addr.protocolSupported(protocolSupport, true)) {
                    result.add(addr);
                }
            }
        }
        return result;
    }
}
