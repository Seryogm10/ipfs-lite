package threads.lite.push;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;

import threads.lite.IPFS;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.host.LiteHost;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.DataHandler;

public class PushHandler implements ProtocolHandler {

    private final LiteHost host;

    public PushHandler(@NonNull LiteHost host) {
        this.host = host;
    }

    @Override
    public String getProtocol() {
        return IPFS.PUSH_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.PUSH_PROTOCOL))
                .thenApply(Stream::closeOutput);
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        IpnsEntity entry = IpnsService.createIpnsEntity(data.array());
        host.push(stream.getConnection(), entry);
    }
}
