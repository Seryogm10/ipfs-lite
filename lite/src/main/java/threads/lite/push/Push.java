package threads.lite.push;


import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;

public class Push {
    private final Connection connection;
    private final IpnsEntity ipnsEntity;

    public Push(Connection connection, IpnsEntity ipnsEntity) {
        this.connection = connection;
        this.ipnsEntity = ipnsEntity;
    }

    public Connection getConnection() {
        return connection;
    }

    public IpnsEntity getIpnsEntity() {
        return ipnsEntity;
    }
}
