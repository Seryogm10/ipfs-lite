package threads.lite.push;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.Connection;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.utils.DataHandler;

public class PushService {

    public static CompletableFuture<Void> push(@NonNull Connection connection,
                                               @NonNull IpnsRecord ipnsRecord) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date());

        Dht.Message.Record rec = Dht.Message.Record.newBuilder()
                .setKey(ByteString.copyFrom(ipnsRecord.getIpnsKey()))
                .setValue(ByteString.copyFrom(ipnsRecord.getSealedRecord()))
                .setTimeReceived(format).build();


        connection.createStream(new StreamHandler() {
            @Override
            public void throwable(Stream stream, Throwable throwable) {
                done.completeExceptionally(throwable);
            }

            @Override
            public void protocol(Stream stream, String protocol) throws Exception {
                if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.PUSH_PROTOCOL).contains(protocol)) {
                    throw new Exception("Token " + protocol + " not supported");
                }
                if (Objects.equals(protocol, IPFS.PUSH_PROTOCOL)) {
                    stream.writeOutput(DataHandler.encode(rec)).thenApply(Stream::closeOutput);
                }
            }

            @Override
            public void fin() {
                done.complete(null);
            }

            @Override
            public void data(Stream stream, ByteBuffer data) {
                LogUtils.error(TAG, "data notify invoked");
            }

        }).thenApply(quicStream -> quicStream.writeOutput(
                DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL, IPFS.PUSH_PROTOCOL)));
        return done;
    }
}
