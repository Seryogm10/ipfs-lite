package threads.lite.ipns;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import dht.pb.Dht;
import ipns.pb.Ipns;
import ipns.pb.Ipns.IpnsEntry;
import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Validator;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;
import threads.lite.utils.DataHandler;


public class IpnsService implements Validator {

    @NonNull
    private static PubKey extractPublicKey(@NonNull PeerId id) throws Exception {
        ByteBuffer wrap = ByteBuffer.wrap(id.getBytes());
        int version = DataHandler.readUnsignedVariant(wrap);
        if (version != Cid.IDENTITY) {
            throw new Exception("not supported codec");
        }
        int length = DataHandler.readUnsignedVariant(wrap);
        byte[] data = new byte[length];
        wrap.get(data);
        return Key.unmarshalPublicKey(data);
    }


    @SuppressLint("SimpleDateFormat")
    @NonNull
    public static Date getDate(@NonNull String format) throws ParseException {
        return Objects.requireNonNull(new SimpleDateFormat(IPFS.TIME_FORMAT_IPFS).parse(format));
    }


    @NonNull
    public static IpnsEntry create(@NonNull Ed25519PrivateKeyParameters key, byte[] value,
                                   long sequence, @NonNull Date eol,
                                   @NonNull Duration duration) throws Exception {

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(eol);


        IpnsEntry entry = IpnsEntry.newBuilder()
                .setValidityType(IpnsEntry.ValidityType.EOL)
                .setSequence(sequence)
                .setTtl(duration.toNanos())
                .setValue(ByteString.copyFrom(value))
                .setData(ByteString.copyFrom(new byte[0]))
                .setValidity(ByteString.copyFrom(format.getBytes())).buildPartial();

        byte[] sig1 = Key.sign(key, ipnsEntryDataForSigV1(entry));
        byte[] sig2 = Key.sign(key, ipnsEntryDataForSigV2(entry));

        IpnsEntry.Builder builder = entry.toBuilder();
        builder.setSignatureV1(ByteString.copyFrom(sig1));
        builder.setSignatureV2(ByteString.copyFrom(sig2));

        return builder.build();
    }


    public static IpnsEntry embedPublicKey(Ed25519PublicKeyParameters publicKey,
                                           Ipns.IpnsEntry ipnsEntry) {

        byte[] pkBytes = Key.createCryptoKey(publicKey).toByteArray();
        return ipnsEntry.toBuilder().setPubKey(ByteString.copyFrom(pkBytes)).build();
    }

    public static byte[] ipnsEntryDataForSigV1(Ipns.IpnsEntry entry) throws Exception {
        ByteString value = entry.getValue();
        ByteString validity = entry.getValidity();
        String type = entry.getValidityType().toString();

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            outputStream.write(value.toByteArray());
            outputStream.write(validity.toByteArray());
            outputStream.write(type.getBytes());
            return outputStream.toByteArray();
        }
    }

    @NonNull
    public static IpnsEntity createIpnsEntity(@NonNull byte[] data) throws Exception {
        IpnsService ipnsService = new IpnsService();
        Dht.Message.Record record = Dht.Message.Record.parseFrom(data);
        return ipnsService.validate(record.getKey().toByteArray(), record.getValue().toByteArray());
    }

    private static byte[] ipnsEntryDataForSigV2(Ipns.IpnsEntry entry) {

        byte[] dataForSig = "ipns-signature:".getBytes();

        byte[] data = entry.getData().toByteArray();

        ByteBuffer out = ByteBuffer.allocate(dataForSig.length + data.length);
        out.put(dataForSig);
        out.put(data);
        return out.array();
    }

    @NonNull
    @Override
    public IpnsEntity validate(byte[] key, byte[] value) throws Exception {

        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        int index = DataHandler.indexOf(key, ipns);
        if (index != 0) {
            throw new Exception("parsing issue");
        }

        Ipns.IpnsEntry entry = IpnsEntry.parseFrom(value);
        Objects.requireNonNull(entry);

        byte[] pid = Arrays.copyOfRange(key, ipns.length, key.length);

        Multihash mh = Multihash.deserialize(pid);
        PeerId peerId = PeerId.fromBase58(mh.toBase58());

        PubKey pubKey = extractPublicKey(peerId, entry);
        Objects.requireNonNull(pubKey);

        validate(pubKey, entry);

        return new IpnsEntity(peerId, getEOL(entry),
                entry.getValue().toByteArray(), entry.getSequence());
    }

    @Override
    public int compare(@NonNull IpnsEntity a, @NonNull IpnsEntity b) {

        long as = a.getSequence();
        long bs = b.getSequence();

        if (as > bs) {
            return 1;
        } else if (as < bs) {
            return -1;
        }

        Date at = a.getEol();
        Date bt = b.getEol();

        if (at.after(bt)) {
            return 1;
        } else if (bt.after(at)) {
            return -1;
        }
        return 0;
    }

    @NonNull
    private Date getEOL(Ipns.IpnsEntry entry) throws Exception {
        if (entry.getValidityType() != Ipns.IpnsEntry.ValidityType.EOL) {
            throw new Exception("validity type");
        }
        String date = new String(entry.getValidity().toByteArray());
        return getDate(date);
    }

    // ExtractPublicKey extracts a public key matching `pid` from the IPNS record,
    // if possible.
    //
    // This function returns (nil, nil) when no public key can be extracted and
    // nothing is malformed.
    @NonNull
    private PubKey extractPublicKey(PeerId pid, Ipns.IpnsEntry entry)
            throws Exception {


        if (entry.hasPubKey()) {
            byte[] pubKey = entry.getPubKey().toByteArray();

            PubKey pk = Key.unmarshalPublicKey(pubKey);
            PeerId expPid = PeerId.fromPubKey(pk);

            if (!Objects.equals(pid, expPid)) {
                throw new Exception("invalid peer");
            }
            return pk;
        }

        return extractPublicKey(pid);
    }

    // Validates validates the given IPNS entry against the given public key
    private void validate(PubKey pubKey, Ipns.IpnsEntry entry) throws Exception {

        if (entry.hasSignatureV2()) {
            pubKey.verify(ipnsEntryDataForSigV2(entry), entry.getSignatureV2().toByteArray());
        } else if (entry.hasSignatureV1()) {
            pubKey.verify(ipnsEntryDataForSigV1(entry), entry.getSignatureV1().toByteArray());
        } else {
            throw new Exception("no signature");
        }

        if (new Date().after(getEOL(entry))) {
            throw new Exception("outdated");
        }
    }

}
