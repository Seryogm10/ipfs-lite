package threads.lite.dht;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Parameters;
import threads.lite.core.PeerStore;
import threads.lite.core.Routing;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Validator;
import threads.lite.host.LiteHost;
import threads.lite.utils.DataHandler;
import threads.lite.utils.DummySession;


public class KadDht implements Routing, AutoCloseable {

    private static final String TAG = KadDht.class.getSimpleName();
    public final LiteHost host;
    public final PeerId self;
    @NonNull
    public final RoutingTable routingTable = new RoutingTable();
    @NonNull
    private final Set<PeerId> motherfuckers = ConcurrentHashMap.newKeySet();
    @NonNull
    private final Set<Future<Void>> works = ConcurrentHashMap.newKeySet();
    @NonNull
    private final Validator validator;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final ReentrantLock lock = new ReentrantLock();

    public KadDht(@NonNull LiteHost host,
                  @NonNull PeerStore peerStore,
                  @NonNull Validator validator) {
        this.host = host;
        this.peerStore = peerStore;
        this.validator = validator;
        this.self = host.self();
    }

    @Override
    @NonNull
    public Set<Peer> getBootstrapPeers() {
        Set<Peer> peers = new HashSet<>();
        Set<Multiaddr> bootstrap = host.getBootstrap();

        Set<PeerId> peerIds = new HashSet<>();
        for (String name : new HashSet<>(IPFS.DHT_BOOTSTRAP_NODES)) {
            try {
                PeerId peerId = PeerId.fromBase58(name);
                Objects.requireNonNull(peerId);
                peerIds.add(peerId);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        for (Multiaddr addr : bootstrap) {
            for (PeerId peerId : peerIds) {
                try {
                    PeerId cmpPeerId = addr.getPeerId();
                    if (Objects.equals(cmpPeerId, peerId)) {
                        Peer peer = Peer.create(addr);
                        peer.setReplaceable(false);
                        peers.add(peer);
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        }
        return peers;
    }

    void bootstrap() {
        if (routingTable.isEmpty()) {
            lock.lock();
            try {
                Set<Peer> peers = getBootstrapPeers();
                for (Peer peer : peers) {
                    routingTable.addPeer(peer);
                }
                List<Peer> randomPeers = peerStore.getRandomPeers(50);
                for (Peer peer : randomPeers) {
                    if (peer.getMultiaddr().protocolSupported(
                            host.getProtocolSupport(), true)) {
                        routingTable.addPeer(peer);
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }


    @NonNull
    private List<Peer> evalClosestPeers(@NonNull Dht.Message pms) {

        List<Peer> peers = new ArrayList<>();
        List<Dht.Message.Peer> list = pms.getCloserPeersList();
        for (Dht.Message.Peer entry : list) {

            PeerId peerId = createPeerId(entry.getId().toByteArray());
            if (peerId == null) {
                continue;
            }
            if (!motherfuckers.contains(peerId)) {
                Multiaddrs multiaddrs = Multiaddr.create(peerId, entry.getAddrsList());

                for (Multiaddr multiaddr : multiaddrs) {
                    if (multiaddr.protocolSupported(host.getProtocolSupport(),
                            true)) {
                        if (!multiaddr.isCircuitAddress()) {
                            try {
                                peers.add(Peer.create(multiaddr));
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    }
                }
            }


        }
        return peers;
    }


    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer,
                                 @NonNull PeerId id) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        try {
            Cancellable done = () -> cancellable.isCancelled() || future.isDone();
            bootstrap();
            Set<PeerId> handled = ConcurrentHashMap.newKeySet();
            byte[] target = id.getBytes();
            ID key = ID.convertKey(target);
            getClosestPeers(done, key, target, peers -> {
                for (Peer peer : peers) {
                    if (!handled.contains(peer.getPeerId())) {
                        handled.add(peer.getPeerId());
                        consumer.accept(peer.getMultiaddr());
                    }
                }
            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    future.completeExceptionally(throwable);
                } else {
                    future.complete(unused);
                }
            });
            works.add(future);
            future.get();
        } catch (Throwable throwable) {
            future.cancel(true);
        } finally {
            works.remove(future);
        }

    }


    private CompletableFuture<Void> getClosestPeers(@NonNull Cancellable cancellable,
                                                    @NonNull ID key,
                                                    @NonNull byte[] target,
                                                    @NonNull Consumer<List<Peer>> channel) {

        return runQuery(cancellable, key, (ctx1, p) -> {

            Dht.Message pms = findPeerSingle(ctx1, p, target);

            List<Peer> peers = evalClosestPeers(pms);

            channel.accept(peers);

            return peers;
        });

    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] target, @NonNull byte[] value) {

        CompletableFuture<Void> future = new CompletableFuture<>();

        try {
            Cancellable done = () -> cancellable.isCancelled() || future.isDone();
            bootstrap();

            // don't allow local users to put bad values.
            IpnsEntity entry = validator.validate(target, value);
            Objects.requireNonNull(entry);


            @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                    IPFS.TIME_FORMAT_IPFS).format(new Date());

            Dht.Message.Record rec = Dht.Message.Record.newBuilder()
                    .setKey(ByteString.copyFrom(target))
                    .setValue(ByteString.copyFrom(value))
                    .setTimeReceived(format).build();

            Dht.Message pms = Dht.Message.newBuilder()
                    .setType(Dht.Message.MessageType.PUT_VALUE)
                    .setKey(rec.getKey())
                    .setRecord(rec)
                    .setClusterLevelRaw(0).build();


            Set<PeerId> handled = ConcurrentHashMap.newKeySet();
            ID key = ID.convertKey(target);
            getClosestPeers(done, key, target, peers -> {

                ExecutorService executorService = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());

                for (Peer peer : peers) {
                    if (!handled.contains(peer.getPeerId())) {
                        handled.add(peer.getPeerId());
                        executorService.execute(() -> {
                            try {
                                Dht.Message res = sendRequest(cancellable, peer, pms);
                                Objects.requireNonNull(res);

                                if (res.hasRecord()) {
                                    // no extra validation, should be fine
                                    consumer.accept(peer.getMultiaddr());
                                }

                            } catch (Throwable ignore) {
                            }
                        });
                    }
                }
                executorService.shutdown();

                try {
                    boolean termination = executorService.awaitTermination(
                            Long.MAX_VALUE, TimeUnit.SECONDS);
                    if (!termination) {
                        executorService.shutdownNow();
                    }
                } catch (Throwable ignore) {
                }

            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    future.completeExceptionally(throwable);
                } else {
                    future.complete(unused);
                }
            });
            works.add(future);
            future.get();
        } catch (Throwable throwable) {
            future.cancel(true);
        } finally {
            works.remove(future);
        }
    }

    @Nullable
    private PeerId createPeerId(byte[] data) {
        try {
            return PeerId.create(data);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            return null;
        }
    }


    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer,
                              @NonNull Cid cid) {
        CompletableFuture<Void> future = new CompletableFuture<>();
        try {
            Cancellable done = () -> cancellable.isCancelled() || future.isDone();
            bootstrap();

            byte[] target = cid.getMultihash();
            ID key = ID.convertKey(target);
            Set<Multiaddr> handledAddrs = ConcurrentHashMap.newKeySet();
            runQuery(done, key, (ctx, p) -> {

                Dht.Message pms = findProvidersSingle(ctx, p, target);

                List<Dht.Message.Peer> list = pms.getProviderPeersList();
                for (Dht.Message.Peer entry : list) {

                    if (done.isCancelled()) {
                        throw new InterruptedException();
                    }

                    PeerId peerId = createPeerId(entry.getId().toByteArray());
                    if (peerId == null) {
                        continue;
                    }

                    Multiaddrs multiaddrs = Multiaddr.create(peerId, entry.getAddrsList());
                    for (Multiaddr multiaddr : multiaddrs) {

                        if (multiaddr.protocolSupported(
                                host.getProtocolSupport(), true)) {
                            if (!handledAddrs.contains(multiaddr)) {
                                handledAddrs.add(multiaddr);

                                LogUtils.error(TAG, "findProviders " +
                                        " Cid Version : " + cid.getVersion() +
                                        " peer " + multiaddr);

                                consumer.accept(multiaddr);
                            }
                        }
                    }

                }
                return evalClosestPeers(pms);

            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    future.completeExceptionally(throwable);
                } else {
                    future.complete(unused);
                }
            });
            works.add(future);
            future.get();
        } catch (Throwable throwable) {
            future.cancel(true);
        } finally {
            works.remove(future);
        }
    }

    public void addToRouting(@NonNull QueryPeer queryPeer) {

        Peer peer = queryPeer.getPeer();
        boolean added = routingTable.addPeer(peer);

        if (added) {
            // only replaceable peer are adding (the replaceable peers are bootstrap peers)
            if (peer.isReplaceable()) {
                peerStore.insertPeer(peer);
            }
        }
    }

    public void removeFromRouting(QueryPeer peer) {
        boolean result = routingTable.removePeer(peer);
        if (result) {
            LogUtils.info(TAG, "Remove from routing " + peer);
        }
    }

    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer,
                        @NonNull Cid cid) {

        CompletableFuture<Void> future = new CompletableFuture<>();

        try {
            Cancellable done = () -> cancellable.isCancelled() || future.isDone();
            bootstrap();

            byte[] target = cid.getMultihash();
            ID key = ID.convertKey(target);
            Set<Multiaddr> addresses = host.listenAddresses();

            if (addresses.isEmpty()) {
                LogUtils.error(TAG, "nothing to do here, no addresses to offer");
                return;
            }

            Dht.Message.Builder builder = Dht.Message.newBuilder()
                    .setType(Dht.Message.MessageType.ADD_PROVIDER)
                    .setKey(ByteString.copyFrom(target))
                    .setClusterLevelRaw(0);

            Dht.Message.Peer.Builder peerBuilder = Dht.Message.Peer.newBuilder()
                    .setId(ByteString.copyFrom(self.getBytes()));
            for (Multiaddr ma : addresses) {
                peerBuilder.addAddrs(ByteString.copyFrom(ma.getBytes()));
            }
            builder.addProviderPeers(peerBuilder.build());

            Dht.Message message = builder.build();


            Set<PeerId> handled = ConcurrentHashMap.newKeySet();

            getClosestPeers(done, key, target, peers -> {

                List<Peer> notHandled = new ArrayList<>();
                for (Peer peer : peers) {

                    if (!handled.contains(peer.getPeerId())) {
                        handled.add(peer.getPeerId());
                        notHandled.add(peer);
                    }

                }

                for (Peer peer : notHandled) {
                    if (motherfuckers.contains(peer.getPeerId())) {
                        return;
                    }

                    try {
                        Connection connection = host.connect(new DummySession(), peer.getMultiaddr(),
                                Parameters.getDefaultUniDirection(
                                        IPFS.CONNECT_TIMEOUT, 20480),
                                false);
                        try {
                            sendMessage(connection, message);
                            // success assumed
                            consumer.accept(connection.remoteMultiaddr());
                        } catch (Throwable ignore) {
                            // ignore exception
                        } finally {
                            connection.close();
                        }

                    } catch (ConnectException ignore) {
                        motherfuckers.add(peer.getPeerId());
                    } catch (InterruptedException ignore) {
                    }
                }
            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    future.completeExceptionally(throwable);
                } else {
                    future.complete(unused);
                }
            });
            works.add(future);
            future.get();
        } catch (Throwable throwable) {
            future.cancel(true);
        } finally {
            works.remove(future);
        }
    }

    private void sendMessage(@NonNull Connection connection, @NonNull Dht.Message message)
            throws ExecutionException, InterruptedException, TimeoutException {

        CompletableFuture<Void> done = new CompletableFuture<>();

        connection.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        done.completeExceptionally(throwable);
                    }

                    @Override
                    public void protocol(Stream stream, String protocol) throws Exception {
                        if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL,
                                IPFS.DHT_PROTOCOL).contains(protocol)) {
                            throw new Exception("Token " + protocol + " not supported");
                        }
                        if (Objects.equals(protocol, IPFS.DHT_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message))
                                    .thenApply(Stream::closeOutput)
                                    .thenRun(() -> done.complete(null)); // this is not 100% correct
                        }
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) {
                        LogUtils.error(TAG, "data sendMessage invoked");
                    }

                })
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL, IPFS.DHT_PROTOCOL)));
        done.get(IPFS.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);

    }

    private Dht.Message makeRequest(@NonNull Connection connection, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws InterruptedException, ExecutionException, TimeoutException {

        try {
            CompletableFuture<Dht.Message> done = new CompletableFuture<>();

            connection.createStream(new StreamHandler() {
                @Override
                public void throwable(Stream stream, Throwable throwable) {
                    done.completeExceptionally(throwable);
                }

                @Override
                public void protocol(Stream stream, String protocol) throws Exception {
                    if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.DHT_PROTOCOL).contains(protocol)) {
                        throw new Exception("Token " + protocol + " not supported");
                    }
                    if (Objects.equals(protocol, IPFS.DHT_PROTOCOL)) {
                        stream.writeOutput(DataHandler.encode(message))
                                .thenApply(Stream::closeOutput);
                    }
                }

                @Override
                public void data(Stream stream, ByteBuffer data) throws Exception {
                    done.complete(Dht.Message.parseFrom(data.array()));
                }

            }).thenApply(quicStream ->
                    quicStream.writeOutput(
                            DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL, IPFS.DHT_PROTOCOL)));

            Dht.Message msg = done.get(IPFS.DHT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
            Objects.requireNonNull(msg);

            return msg;
        } finally {
            peer.setSessionTicket(connection.getSessionTicket());
            connection.close();
        }
    }

    @NonNull
    private Dht.Message sendRequest(@NonNull Cancellable cancellable, @NonNull Peer peer,
                                    @NonNull Dht.Message message)
            throws InterruptedException, ConnectException {

        if (cancellable.isCancelled()) {
            throw new InterruptedException();
        }
        if (motherfuckers.contains(peer.getPeerId())) {
            throw new ConnectException("peer can not be connected too");
        }
        try {
            Connection connection = host.connect(new DummySession(), peer.getMultiaddr(),
                    Parameters.getDefaultUniDirection(IPFS.CONNECT_TIMEOUT, IPFS.MESSAGE_SIZE_MAX),
                    false);

            if (cancellable.isCancelled()) {
                throw new InterruptedException();
            }
            return makeRequest(connection, peer, message);
        } catch (ConnectException connectException) {
            motherfuckers.add(peer.getPeerId());
            throw connectException;
        } catch (ExecutionException | TimeoutException exception) {
            motherfuckers.add(peer.getPeerId());
            throw new ConnectException(exception.getClass().getSimpleName());
        }
    }


    private Dht.Message getValueSingle(@NonNull Cancellable ctx, @NonNull Peer peer, @NonNull byte[] key)
            throws InterruptedException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_VALUE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
        return sendRequest(ctx, peer, pms);
    }

    private Dht.Message findPeerSingle(@NonNull Cancellable ctx, @NonNull Peer peer, @NonNull byte[] key)
            throws InterruptedException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.FIND_NODE)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();

        return sendRequest(ctx, peer, pms);
    }

    private Dht.Message findProvidersSingle(@NonNull Cancellable ctx, @NonNull Peer peer, @NonNull byte[] key)
            throws InterruptedException, ConnectException {
        Dht.Message pms = Dht.Message.newBuilder()
                .setType(Dht.Message.MessageType.GET_PROVIDERS)
                .setKey(ByteString.copyFrom(key))
                .setClusterLevelRaw(0).build();
        return sendRequest(ctx, peer, pms);
    }

    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId id) {

        CompletableFuture<Void> future = new CompletableFuture<>();

        try {
            Cancellable done = () -> cancellable.isCancelled() || future.isDone();
            bootstrap();

            byte[] target = id.getBytes();
            ID key = ID.convertKey(target);

            Set<Multiaddr> handled = ConcurrentHashMap.newKeySet();
            runQuery(done, key, (ctx, p) -> {

                Dht.Message pms = findPeerSingle(ctx, p, target);

                List<Dht.Message.Peer> list = pms.getCloserPeersList();
                for (Dht.Message.Peer entry : list) {

                    PeerId peerId = createPeerId(entry.getId().toByteArray());
                    if (peerId == null) {
                        continue;
                    }
                    if (Objects.equals(peerId, id)) {
                        Multiaddrs multiaddrs = Multiaddr.create(peerId, entry.getAddrsList());
                        for (Multiaddr multiaddr : multiaddrs) {
                            if (multiaddr.protocolSupported(
                                    host.getProtocolSupport(), false)) {
                                if (!handled.contains(multiaddr)) {
                                    handled.add(multiaddr);
                                    consumer.accept(multiaddr);
                                }
                            }
                        }
                    }

                }
                return evalClosestPeers(pms);

            }).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    future.completeExceptionally(throwable);
                } else {
                    future.complete(unused);
                }
            });
            works.add(future);
            future.get();
        } catch (Throwable throwable) {
            future.cancel(true);
        } finally {
            works.remove(future);
        }
    }

    private CompletableFuture<Void> runQuery(@NonNull Cancellable cancellable,
                                             @NonNull ID key,
                                             @NonNull QueryFunc queryFn) {
        // pick the K closest peers to the key in our Routing table.
        List<QueryPeer> seedPeers = routingTable.nearestPeers(key);
        return Query.runQuery(this, cancellable, key, seedPeers, queryFn);

    }

    private List<Peer> getRecordOfPeers(@NonNull Cancellable cancellable,
                                        @NonNull Peer peer,
                                        @NonNull Consumer<IpnsEntity> consumer,
                                        @NonNull byte[] key)
            throws InterruptedException, ConnectException {


        Dht.Message pms = getValueSingle(cancellable, peer, key);

        List<Peer> peers = evalClosestPeers(pms);

        if (pms.hasRecord()) {

            Dht.Message.Record rec = pms.getRecord();
            try {
                byte[] record = rec.getValue().toByteArray();
                if (record != null && record.length > 0) {
                    IpnsEntity entry = validator.validate(rec.getKey().toByteArray(), record);
                    consumer.accept(entry);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable.getMessage());
            }
        }

        return peers;
    }


    private void processValues(@Nullable IpnsEntity best,
                               @NonNull IpnsEntity current,
                               @NonNull Consumer<IpnsEntity> reporter) {

        if (best != null) {
            int value = validator.compare(best, current);
            if (value == -1) { // "current" is newer entry
                reporter.accept(current);
            }
        } else {
            reporter.accept(current);
        }
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer,
                            @NonNull byte[] target) {

        CompletableFuture<Void> future = new CompletableFuture<>();

        try {
            Cancellable done = () -> cancellable.isCancelled() || future.isDone();
            bootstrap();

            AtomicReference<IpnsEntity> best = new AtomicReference<>();
            ID key = ID.convertKey(target);

            runQuery(done, key, (ctx1, peer) -> getRecordOfPeers(ctx1, peer,
                    entry -> processValues(best.get(), entry, (current) -> {
                        consumer.accept(current);
                        best.set(current);
                    }), target)).whenComplete((unused, throwable) -> {
                if (throwable != null) {
                    future.completeExceptionally(throwable);
                } else {
                    future.complete(unused);
                }
            });

            works.add(future);
            future.get();

        } catch (Throwable throwable) {
            future.cancel(true);
        } finally {
            works.remove(future);
        }
    }

    @Override
    public void close() {
        motherfuckers.clear();
        routingTable.clear();
        works.forEach(voidFuture -> voidFuture.cancel(false));
        works.clear();
    }

    public interface QueryFunc {
        @NonNull
        List<Peer> query(@NonNull Cancellable cancellable, @NonNull Peer peer)
                throws InterruptedException, ConnectException;
    }


}
