package threads.lite.dht;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;

import threads.lite.IPFS;

public class QueryPeerSet extends ConcurrentSkipListSet<QueryPeer> {

    public QueryPeerSet() {
        super(QueryPeer::compareTo);
    }

    // TryAdd adds the peer p to the peer set.
    // If the peer is already present, no action is taken.
    // Otherwise, the peer is added with state set to PeerHeard.
    public void tryAdd(@NonNull QueryPeer peer) {
        this.add(peer);
    }


    // GetClosestNInStates returns the closest to the key peers, which are in one of the given states.
    // It returns n peers or less, if fewer peers meet the condition.
    // The returned peers are sorted in ascending order by their distance to the key.
    public List<QueryPeer> getClosestNInStates(int maxLength, @NonNull List<PeerState> states) {
        List<QueryPeer> peers = new ArrayList<>();
        int count = 0;
        for (QueryPeer state : this) {
            if (states.contains(state.getState())) {
                peers.add(state);
                count++;
                if (count == maxLength) {
                    break;
                }
            }
        }
        return peers;
    }

    @NonNull
    List<QueryPeer> getClosestInStates(int maxLength, @NonNull List<PeerState> states) {
        return getClosestNInStates(maxLength, states);
    }

    public int numWaitingOrHeard() {
        return getClosestInStates(size(),
                List.of(PeerState.PeerHeard, PeerState.PeerWaiting)).size();
    }

    public List<QueryPeer> nextHeardPeers() {
        // The peers we query next should be ones that we have only Heard about.
        return getClosestInStates(IPFS.DHT_CONCURRENCY,
                Collections.singletonList(PeerState.PeerHeard));

    }
}
