package threads.lite.server;

import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.core.Transport;
import threads.lite.host.LiteTransport;
import threads.lite.utils.DataHandler;

public final class ServerResponder implements StreamHandler {

    private static final String TAG = ServerResponder.class.getSimpleName();


    @NonNull
    private final ServerSession serverSession;

    public ServerResponder(@NonNull ServerSession serverSession) {
        this.serverSession = serverSession;
    }

    @Override
    public void throwable(Stream stream, Throwable throwable) {
        LogUtils.error(TAG, throwable.getMessage());
        stream.close();
    }

    @NonNull
    @Override
    public Transport getTransport(QuicStream quicStream) {
        return new LiteTransport(quicStream);
    }

    @Override
    public void streamTerminated(QuicStream quicStream) {
        // noting to do here
    }

    @Override
    public void protocol(Stream stream, String protocol) throws Exception {
        stream.setAttribute(TOKEN, protocol);
        ProtocolHandler protocolHandler = serverSession.getProtocolHandler(protocol);
        if (protocolHandler != null) {
            protocolHandler.protocol(stream);
        } else {
            LogUtils.error(TAG, "Ignore " + protocol);
            stream.writeOutput(DataHandler.encodeProtocols(IPFS.NA));
        }
    }

    @Override
    public void fin() {
        LogUtils.info(TAG, "nothing to do here");
    }


    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        String protocol = (String) stream.getAttribute(TOKEN);
        if (protocol != null) {
            ProtocolHandler protocolHandler = serverSession.getProtocolHandler(protocol);
            if (protocolHandler != null) {
                protocolHandler.data(stream, data);
            } else {
                throwable(stream, new Exception("StreamHandler invalid protocol"));
            }
        } else {
            throwable(stream, new Exception("StreamHandler invalid protocol"));
        }
    }
}
