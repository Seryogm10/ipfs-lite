package threads.lite.server;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import threads.lite.bitswap.BitSwapEngine;
import threads.lite.bitswap.BitSwapEngineHandler;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.Block;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteHost;
import threads.lite.host.LiteResponder;
import threads.lite.ident.IdentityHandler;
import threads.lite.push.PushHandler;
import threads.lite.utils.MultistreamHandler;


public final class ServerSession implements Session {

    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final BitSwapEngine bitSwapEngine;
    @NonNull
    private final Map<String, ProtocolHandler> protocols = new ConcurrentHashMap<>();

    public ServerSession(@NonNull BlockStore blockStore, @NonNull LiteHost host) {
        this.blockStore = blockStore;
        this.bitSwapEngine = new BitSwapEngine(blockStore);

        // add the default
        addProtocolHandler(new MultistreamHandler());
        addProtocolHandler(new PushHandler(host));
        addProtocolHandler(new IdentityHandler(host, this));
        addProtocolHandler(new BitSwapEngineHandler(bitSwapEngine));
    }


    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) {
        // some small tests
        String protocol = protocolHandler.getProtocol();
        Objects.requireNonNull(protocol);

        if (protocol.isEmpty()) {
            throw new RuntimeException("invalid protocol name");
        }

        if (!protocol.startsWith("/")) {
            throw new RuntimeException("invalid protocol name");
        }

        if (protocols.containsKey(protocol)) {
            throw new RuntimeException("protocol name already exists");
        }

        protocols.put(protocol, protocolHandler);
    }

    @NonNull
    public Map<String, ProtocolHandler> getProtocols() {
        // important, protocols should note be modified "outside"
        return Collections.unmodifiableMap(protocols);
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        throw new Exception("should not be invoked here");
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {
        throw new Exception("should not be invoked here");
    }

    @Override
    public boolean swarmHas(@NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void swarmReduce(@NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public boolean swarmEnhance(@NonNull Connection connection) {
        throw new RuntimeException("should not be invoked here");
    }

    @NonNull
    @Override
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @Override
    public boolean isFindProvidersActive() {
        return false;
    }

    @Override
    public boolean isSendReplyActive() {
        return false;
    }

    @NonNull
    @Override
    public StreamHandler getStreamHandler() {
        return new LiteResponder(protocols);
    }

    @Override
    @NonNull
    public List<Connection> getSwarm() {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public boolean swarmContains(@NonNull Connection connection) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] key, @NonNull byte[] data) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] key) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked here");
    }

    @Override
    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        throw new RuntimeException("should not be invoked here");
    }

    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        throw new RuntimeException("should not be invoked here");
    }

    @NonNull
    @Override
    public Set<Peer> getBootstrapPeers() {
        return Collections.emptySet();
    }

    @Override
    public void close() {
        bitSwapEngine.close();
    }


}

