package threads.lite.cid;

import android.os.Build;

import androidx.annotation.NonNull;

import com.google.common.primitives.UnsignedBytes;
import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Random;

import crypto.pb.Crypto;
import threads.lite.crypto.PubKey;
import threads.lite.utils.DataHandler;


public final class PeerId implements Comparable<PeerId> {

    private final byte[] bytes;

    private PeerId(byte[] bytes) {
        this.bytes = bytes;
    }

    public static PeerId create(byte[] bytes) throws Exception {
        if (bytes.length < 32 || bytes.length > 50) {
            throw new Exception("Invalid peerId length: " + bytes.length);
        }
        return new PeerId(bytes);
    }

    public static PeerId random() {
        byte[] bytes = new byte[32];
        new Random().nextBytes(bytes);
        return new PeerId(bytes);
    }


    @NonNull
    public static PeerId fromString(@NonNull String name) throws Exception {

        if (name.startsWith("Qm") || name.startsWith("1")) {
            // base58 encoded sha256 or identity multihash
            return PeerId.fromBase58(name);
        }

        byte[] data = Multibase.decode(name);

        if (data[0] == 0) {
            Multihash mh = new Multihash(Multihash.Type.id, data);
            return PeerId.create(mh.getHash());
        } else {
            ByteBuffer wrap = ByteBuffer.wrap(data);
            DataHandler.readUnsignedVariant(wrap); // skip version
            DataHandler.readUnsignedVariant(wrap); // skip codec
            Multihash mh = Multihash.deserialize(wrap);
            return PeerId.fromBase58(mh.toBase58());

        }
    }

    public static PeerId fromBase58(String str) throws Exception {
        return PeerId.create(Base58.decode(str));
    }

    @NonNull
    public static PeerId fromPubKey(@NonNull PubKey pubKey) throws Exception {

        byte[] pubKeyBytes = Crypto.PublicKey.newBuilder().setType(pubKey.getKeyType()).
                setData(ByteString.copyFrom(pubKey.raw())).build().toByteArray();

        if (pubKeyBytes.length <= 42) {
            Multihash hash = Cid.encode(pubKeyBytes, Multihash.Type.id);
            return PeerId.fromBase58(hash.toBase58());
        } else {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            Multihash hash = Cid.encode(digest.digest(pubKeyBytes), Multihash.Type.sha2_256);
            return PeerId.fromBase58(hash.toBase58());
        }
    }

    @NonNull
    public String toBase36() {
        return Multibase.encode(Multibase.Base.Base36, Cid.newCidV1(Cid.Libp2pKey, bytes).bytes());
    }

    @NonNull
    public String toBase58() {
        return Base58.encode(this.bytes);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeerId peer = (PeerId) o;
        return Arrays.equals(bytes, peer.bytes);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @NonNull
    public String toString() {
        return this.toBase58();
    }

    @NonNull
    public byte[] getBytes() {
        return this.bytes;
    }

    @Override
    public int compareTo(PeerId o) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return Arrays.compareUnsigned(this.bytes, o.bytes);
        } else {
            return UnsignedBytes.lexicographicalComparator().compare(this.bytes, o.bytes);
        }
    }
}
