package threads.lite.cid;


import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import com.google.protobuf.ByteString;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import identify.pb.MultiaddrOuterClass;
import threads.lite.utils.DataHandler;


public final class Multiaddr {
    private final List<String> address;
    private final PeerId peerId;

    private Multiaddr(PeerId peerId, List<String> address) {
        this.peerId = peerId;
        this.address = address;
    }


    @NonNull
    @TypeConverter
    public static Multiaddr fromArray(byte[] data) {
        if (data == null) {
            throw new IllegalStateException("data can not be null");
        }
        try {
            MultiaddrOuterClass.Multiaddr stored =
                    MultiaddrOuterClass.Multiaddr.parseFrom(data);
            return new Multiaddr(PeerId.create(stored.getPeerId().toByteArray()),
                    stored.getPartsList());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }

    @NonNull
    @TypeConverter
    public static byte[] toArray(Multiaddr multiaddr) {
        if (multiaddr == null) {
            throw new IllegalStateException("multiaddr can not be null");
        }
        return MultiaddrOuterClass.Multiaddr.newBuilder().setPeerId(ByteString.copyFrom(
                        multiaddr.getPeerId().getBytes())).addAllParts(multiaddr.address)
                .build().toByteArray();

    }

    @NonNull
    public static Multiaddr getLocalHost(PeerId peerId, int port) throws Exception {
        return Multiaddr.create(peerId, new InetSocketAddress(
                InetAddress.getLocalHost(), port));
    }

    @NonNull
    public static Multiaddr getSiteLocalAddress(PeerId peerId, int port) throws Exception {
        InetSocketAddress inetSocketAddress = Network.getSiteLocalAddress(port);
        return create(peerId, inetSocketAddress);
    }

    @NonNull
    public static Multiaddr create(String address) throws Exception {
        PeerId peerId = Protocol.getPeerId(address);
        return create(peerId, address);
    }

    @NonNull
    private static Multiaddr validate(PeerId peerId, List<String> address) {

        boolean isCircuit = false;
        List<String> list = new ArrayList<>();
        for (int i = 0; i < address.size(); i++) {
            String token = address.get(i);
            if (Objects.equals(token, Type.P2P)) {
                list.add(address.get(i + 1));
            }
            if (Objects.equals(token, Type.P2PCIRCUIT)) {
                isCircuit = true;
            }
        }
        if (list.size() > 1) {
            throw new RuntimeException("contains invalid num of peers");
        } else if (list.size() == 1) {
            if (!isCircuit) {
                throw new RuntimeException("invalid address");
            }
        }
        return new Multiaddr(peerId, address);
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, String address) throws Exception {
        byte[] data = Protocol.decode(address);
        List<String> parts = parseMultiaddr(ByteBuffer.wrap(data));
        List<String> reduced = Protocol.reducePeerId(peerId, parts);
        return validate(peerId, reduced);
    }


    @NonNull
    public static Multiaddr createCircuit(PeerId peerId, ByteBuffer raw) throws Exception {
        List<String> parts = parseMultiaddr(raw);
        parts.add(Type.P2PCIRCUIT);
        return validate(peerId, parts);
    }

    @NonNull
    public static Multiaddr create(PeerId peerId, ByteBuffer raw) throws Exception {
        List<String> parts = parseMultiaddr(raw);
        return validate(peerId, parts);
    }


    private static List<String> parseMultiaddr(ByteBuffer in) throws Exception {
        List<String> parts = new ArrayList<>();

        while (in.hasRemaining()) {
            int code = DataHandler.readUnsignedVariant(in);
            Protocol p = Protocol.get(code);
            parts.add(p.getType());
            if (p.size() == 0)
                continue;

            String addr = p.readAddress(in);
            if (addr.length() > 0) {
                parts.add(addr);
            }
        }
        return parts;
    }


    @NonNull
    public static Multiaddrs create(PeerId peerId, List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = new Multiaddrs();
        for (ByteString entry : byteStrings) {
            try {
                multiaddrs.add(Multiaddr.create(peerId, entry.asReadOnlyByteBuffer()));
            } catch (Throwable ignore) {
                // can happen of not supported multi addresses like tcp, etc.
            }
        }
        return multiaddrs;
    }


    @NonNull
    public static Multiaddr create(PeerId peerId, InetSocketAddress inetSocketAddress) {
        List<String> parts = Protocol.createAddress(inetSocketAddress);
        return validate(peerId, parts);
    }

    @NonNull
    public static Multiaddr createCircuit(PeerId peerId, PeerId relayId, InetSocketAddress inetSocketAddress) {
        List<String> parts = Protocol.createAddress(inetSocketAddress);
        parts.add(Type.P2P);
        parts.add(relayId.toBase58());
        parts.add(Type.P2PCIRCUIT);
        return validate(peerId, parts);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress())
                || (inetAddress.isSiteLocalAddress());
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean weakLocalAddress(InetAddress inetAddress) {
        return inetAddress.isAnyLocalAddress() || inetAddress.isLinkLocalAddress()
                || (inetAddress.isLoopbackAddress());
    }


    @NonNull
    public InetSocketAddress getInetSocketAddress() {
        return InetSocketAddress.createUnresolved(getHost(), getPort());
    }

    public boolean isAnyDns() {
        return isDnsaddr() || isDns() || isDns4() || isDns6();
    }

    public boolean strictSupportedAddress() {
        try {
            if (!isAnyDns()) {
                InetAddress inetAddress = getInetAddress();
                return !Multiaddr.isLocalAddress(inetAddress);
            } else {
                return true;
            }
        } catch (Throwable ignore) {
            // nothing to do here
        }
        return false;
    }

    public boolean weakSupportedAddress() {
        try {
            if (isAnyDns()) {
                return true;
            } else {
                InetAddress inetAddress = getInetAddress();
                return !Multiaddr.weakLocalAddress(inetAddress);
            }
        } catch (Throwable ignore) {
            // nothing to do here
        }
        return false;
    }

    public boolean protocolSupported(ProtocolSupport protocolSupport,
                                     boolean strictSupportAddress) {

        if (protocolSupport == ProtocolSupport.IPv4) {
            if (isIP6()) {
                return false;
            }
        }
        if (protocolSupport == ProtocolSupport.IPv6) {
            if (isIP4()) {
                return false;
            }
        }
        if (isDnsaddr()) {
            return true;
        }
        if (isDns()) {
            return has(Type.QUIC);
        }
        if (isDns4()) {
            return has(Type.QUIC);
        }
        if (isDns6()) {
            return has(Type.QUIC);
        }
        if (has(Type.QUIC)) {
            if (strictSupportAddress) {
                return strictSupportedAddress();
            } else {
                return weakSupportedAddress();
            }
        }
        return false;
    }

    public byte[] getBytes() {
        return Protocol.decode(address);
    }

    public String getHost() {
        return address.get(1);
    }

    public InetAddress getInetAddress() throws UnknownHostException {
        return InetAddress.getByName(getHost());
    }

    @NonNull
    private List<String> getPeerIds() {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < address.size(); i++) {
            String token = address.get(i);
            if (Objects.equals(token, Type.P2P)) {
                result.add(address.get(i + 1));
            }
        }
        return result;
    }

    public int getPort() {
        return Integer.parseInt(address.get(3));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Multiaddr multiaddr = (Multiaddr) o;
        return Objects.equals(address, multiaddr.address) && Objects.equals(peerId, multiaddr.peerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, peerId);
    }

    @NonNull
    @Override
    public String toString() {
        return Protocol.toAddress(address) + "/" + Type.P2P + "/" + peerId.toString();
    }

    public boolean has(String type) {
        for (String token : address) {
            if (Objects.equals(token, type)) {
                return true;
            }
        }
        return false;
    }


    public boolean isIP4() {
        return Objects.equals(address.get(0), Type.IP4);
    }

    public boolean isIP6() {
        return Objects.equals(address.get(0), Type.IP6);
    }

    public boolean isDns() {
        return Objects.equals(address.get(0), Type.DNS);
    }

    public boolean isDns6() {
        return Objects.equals(address.get(0), Type.DNS6);
    }

    public boolean isDns4() {
        return Objects.equals(address.get(0), Type.DNS4);
    }

    public boolean isDnsaddr() {
        return Objects.equals(address.get(0), Type.DNSADDR);
    }

    public boolean isCircuitAddress() {
        return has(Type.P2PCIRCUIT);
    }


    @NonNull
    public PeerId getPeerId() {
        return peerId;
    }

    @NonNull
    public PeerId getRelayId() throws Exception {
        if (!isCircuitAddress()) {
            throw new Exception("no circuit address");
        }
        List<String> list = getPeerIds();
        if (list.size() != 1) {
            throw new RuntimeException("no valid circuit address");
        }
        return PeerId.fromString(list.get(0));
    }

}
