package threads.lite.cid;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity
public class Block {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "cid")
    @TypeConverters(Cid.class)
    private final Cid cid;

    @NonNull
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private final byte[] data;

    public Block(@NonNull Cid cid, @NonNull byte[] data) {
        this.cid = cid;
        this.data = data;

    }

    @NonNull
    public static Block createBlock(@NonNull Cid cid, @NonNull byte[] data) {
        return new Block(cid, data);
    }

    @NonNull
    public byte[] getData() {
        return data;
    }

    @NonNull
    public Cid getCid() {
        return cid;
    }


}
