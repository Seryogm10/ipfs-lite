package threads.lite.core;

import androidx.annotation.NonNull;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;

public class Block {

    @NonNull
    private final Cid cid;
    @NonNull
    private final Merkledag.PBNode node;

    private Block(@NonNull Cid cid, @NonNull Merkledag.PBNode node) {
        this.cid = cid;
        this.node = node;
    }

    @NonNull
    public static Block createBlockWithCid(@NonNull Cid cid, @NonNull Merkledag.PBNode data) {
        return new Block(cid, data);
    }

    @NonNull
    public static Block createBlock(@NonNull Merkledag.PBNode node) throws Exception {
        Cid cid = Cid.createCidV0(node.toByteArray());
        return Block.createBlockWithCid(cid, node);
    }

    @NonNull
    public Merkledag.PBNode getNode() {
        return node;
    }

    @NonNull
    public Cid getCid() {
        return cid;
    }

}
