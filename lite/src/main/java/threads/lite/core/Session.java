package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.Closeable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import threads.lite.cid.PeerId;


public interface Session extends Closeable, Routing, BitSwap {

    // returns all supported protocols within the session
    @NonNull
    Map<String, ProtocolHandler> getProtocols();

    @NonNull
    default Set<String> getProtocolNames() {
        return new HashSet<>(getProtocols().keySet());
    }

    @Nullable
    default ProtocolHandler getProtocolHandler(@NonNull String protocol) {
        return getProtocols().get(protocol);
    }

    // add a protocol handler to the supported list of protocols.
    // Note: a protocol handler is only invoked, when a remote peer initiate a
    // stream over an existing connection
    void addProtocolHandler(@NonNull ProtocolHandler protocolHandler);

    // the stream handler which is invoked on peer initiated streams
    @NonNull
    StreamHandler getStreamHandler();

    // returns the block store where all data is stored
    @NonNull
    BlockStore getBlockStore();

    // returns true, when in bitswap the provider search is enabled
    boolean isFindProvidersActive();

    // return true, when send a reply in bitswap is active
    boolean isSendReplyActive();

    // returns all open connections of the swarm
    @NonNull
    List<Connection> getSwarm();

    boolean swarmContains(@NonNull Connection connection);

    boolean swarmEnhance(@NonNull Connection connection);

    // removes all connections from the swarm when a connection has the given peerId
    void swarmReduce(@NonNull PeerId peerId);

    // return true, when at least one "open" connection exists to the peer with the given peerId
    boolean swarmHas(@NonNull PeerId peerId);
}

