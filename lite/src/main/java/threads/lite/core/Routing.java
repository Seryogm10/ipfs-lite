package threads.lite.core;

import androidx.annotation.NonNull;

import java.util.Set;
import java.util.function.Consumer;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;

public interface Routing {

    void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                  @NonNull byte[] key, @NonNull byte[] value);

    void findProviders(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                       @NonNull Cid cid);

    void provide(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                 @NonNull Cid cid);


    void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                  @NonNull PeerId peerId);

    void searchValue(@NonNull Cancellable cancellable,
                     @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] target);

    void findClosestPeers(@NonNull Cancellable cancellable,
                          @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId);

    // initial peers for the DHT
    @NonNull
    Set<Peer> getBootstrapPeers();
}
