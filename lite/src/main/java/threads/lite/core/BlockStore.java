package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import threads.lite.cid.Cid;

public interface BlockStore {

    boolean hasBlock(@NonNull Cid cid);

    @Nullable
    Block getBlock(@NonNull Cid cid) throws Exception;

    @Nullable
    byte[] getData(@NonNull Cid cid);

    void deleteBlock(@NonNull Cid cid);

    void deleteBlocks(@NonNull List<Cid> cids);

    void putBlock(@NonNull Block block);

    void clear();
}


