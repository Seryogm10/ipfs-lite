package threads.lite.core;


import androidx.annotation.NonNull;

import net.luminis.quic.QuicStream;

import java.nio.ByteBuffer;

import threads.lite.LogUtils;
import threads.lite.host.LiteTransport;

public interface StreamHandler {
    String TAG = StreamHandler.class.getSimpleName();
    String TRANSPORT = "TRANSPORT";
    String TOKEN = "TOKEN";

    void throwable(Stream stream, Throwable throwable);

    void protocol(Stream stream, String protocol) throws Exception;

    default void fin() {
        LogUtils.debug(TAG, "stream finished");
    }

    void data(Stream stream, ByteBuffer data) throws Exception;

    @NonNull
    default Transport getTransport(QuicStream quicStream) {
        Transport transport = (Transport) quicStream.getAttribute(TRANSPORT);
        if (transport != null) {
            return transport;
        }
        return new LiteTransport(quicStream);
    }

    default void streamTerminated(QuicStream quicStream) {
        LogUtils.debug(TAG, "stream terminated");
    }
}
