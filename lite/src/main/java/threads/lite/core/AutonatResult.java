package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import threads.lite.cid.Multiaddr;

public interface AutonatResult {

    // returns NatType (SYMMETRIC, FULL_CONE, RESTRICTED_CONE,
    // PORT_RESTRICTED_CONE, UNKNOWN), only matters when there is no
    // dialable address
    // Note: right now it does not yet distinguish between FULL_CONE, RESTRICTED_CONE,
    // and PORT_RESTRICTED_CONE -> it returns always RESTRICTED_CONE
    @NonNull
    NatType getNatType();

    @Nullable
    Multiaddr dialableAddress();

    boolean success();

    enum NatType {
        SYMMETRIC, FULL_CONE, RESTRICTED_CONE, PORT_RESTRICTED_CONE, UNKNOWN
    }

}
