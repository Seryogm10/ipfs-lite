package threads.lite.core;

import androidx.annotation.NonNull;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;


public class Keys {
    @NonNull
    private final Ed25519PublicKeyParameters publicKey;
    @NonNull
    private final Ed25519PrivateKeyParameters privateKey;

    public Keys(@NonNull Ed25519PublicKeyParameters publicKey,
                @NonNull Ed25519PrivateKeyParameters privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    @NonNull
    public Ed25519PublicKeyParameters getPublic() {
        return publicKey;
    }

    @NonNull
    public Ed25519PrivateKeyParameters getPrivate() {
        return privateKey;
    }
}
