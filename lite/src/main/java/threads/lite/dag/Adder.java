package threads.lite.dag;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Block;
import threads.lite.core.BlockStore;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.ReaderInputStream;
import threads.lite.utils.Splitter;

public class Adder {

    @NonNull
    private final BlockStore blockStore;

    private Adder(@NonNull BlockStore blockStore) {
        this.blockStore = blockStore;
    }

    public static Adder createAdder(@NonNull Session session) {
        return new Adder(session.getBlockStore());
    }


    public Cid createDirectory(@NonNull List<Link> links) throws Exception {
        Block block = DagDirectory.createDirectory(links);
        blockStore.putBlock(block);
        return block.getCid();
    }

    public Cid createEmptyDirectory() throws Exception {
        Block block = DagDirectory.createDirectory();
        blockStore.putBlock(block);
        return block.getCid();
    }

    public Cid addChild(@NonNull Merkledag.PBNode dirNode, @NonNull Link child) throws Exception {

        if (!DagDirectory.isDirectory(dirNode)) {
            throw new Exception("not a directory");
        }
        List<Merkledag.PBLink> originals = dirNode.getLinksList();
        List<Link> links = new ArrayList<>();
        for (Merkledag.PBLink link : originals) {
            if (!Objects.equals(link.getName(), child.getName())) {
                links.add(Link.create(
                        Objects.requireNonNull(Cid.fromArray(link.getHash().toByteArray())),
                        link.getName(), link.getTsize(), Link.Unknown));
            }
        }
        links.add(child);
        return createDirectory(links);
    }


    public Cid removeChild(@NonNull Merkledag.PBNode dirNode, @NonNull String name) throws Exception {

        if (!DagDirectory.isDirectory(dirNode)) {
            throw new Exception("not a directory");
        }
        List<Merkledag.PBLink> originals = dirNode.getLinksList();
        List<Link> links = new ArrayList<>();
        for (Merkledag.PBLink link : originals) {
            if (!Objects.equals(link.getName(), name)) {
                links.add(Link.create(
                        Objects.requireNonNull(Cid.fromArray(link.getHash().toByteArray())),
                        link.getName(), link.getTsize(), Link.Unknown));
            }
        }
        return createDirectory(links);
    }


    @NonNull
    public Cid createFromStream(@NonNull final ReaderInputStream reader) throws Exception {

        Splitter splitter = new Splitter() {

            @Override
            public int nextBytes(byte[] bytes) throws Exception {
                return reader.read(bytes);
            }

            @Override
            public boolean done() {
                return reader.done();
            }
        };

        DagWriter db = new DagWriter(blockStore, splitter);

        return db.trickle();
    }

}
