package threads.lite.dag;

import androidx.annotation.NonNull;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Block;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;

public class DagService {
    private final Session session;

    public DagService(Session session) {
        this.session = session;
    }

    @NonNull
    public static DagService createDagService(@NonNull Session session) {
        return new DagService(session);
    }

    @NonNull
    private Block getBlock(Cancellable cancellable, Cid cid) throws Exception {
        Block block = session.getBlockStore().getBlock(cid);
        if (block != null) {
            return block;
        }
        return session.getBlock(cancellable, cid);
    }

    @NonNull
    public Merkledag.PBNode getNode(@NonNull Cancellable cancellable,
                                    @NonNull Cid cid) throws Exception {
        Block block = getBlock(cancellable, cid);
        return block.getNode();
    }


}
