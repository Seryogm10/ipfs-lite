package threads.lite.crypto;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.generators.Ed25519KeyPairGenerator;
import org.bouncycastle.crypto.params.Ed25519KeyGenerationParameters;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.util.Objects;

import crypto.pb.Crypto;
import threads.lite.cid.PeerId;
import threads.lite.core.Keys;

public interface Key {

    static PubKey unmarshalPublicKey(byte[] data) throws Exception {

        Crypto.PublicKey pms = Crypto.PublicKey.parseFrom(data);

        byte[] pubKeyData = pms.getData().toByteArray();

        switch (pms.getType()) {
            case RSA:
                return Rsa.unmarshalRsaPublicKey(pubKeyData);
            case ECDSA:
                return Ecdsa.unmarshalEcdsaPublicKey(pubKeyData);
            case Secp256k1:
                return Secp256k1.unmarshalSecp256k1PublicKey(pubKeyData);
            case Ed25519:
                return Ed25519.unmarshalEd25519PublicKey(pubKeyData);
            default:
                throw new Exception("BadKeyTypeException");
        }
    }

    // Note: Only RSA support
    static byte[] sign(PrivateKey key, byte[] data) throws Exception {
        Signature sha256withRSA = Signature.getInstance("SHA256withRSA");
        sha256withRSA.initSign(key);
        sha256withRSA.update(data);
        return sha256withRSA.sign();
    }

    // Note: Only Ed25519 support
    static byte[] sign(Ed25519PrivateKeyParameters privateKeyParameters, byte[] data)
            throws CryptoException {
        Objects.requireNonNull(privateKeyParameters);
        // Generate new signature
        Signer signer = new Ed25519Signer();
        signer.init(true, privateKeyParameters);
        signer.update(data, 0, data.length);
        return signer.generateSignature();
    }

    // Note: Only Ed25519 support
    static Crypto.PublicKey createCryptoKey(Ed25519PublicKeyParameters publicKeyParams) {
        Objects.requireNonNull(publicKeyParams);
        return Crypto.PublicKey.newBuilder().setType(crypto.pb.Crypto.KeyType.Ed25519)
                .setData(ByteString.copyFrom(publicKeyParams.getEncoded()))
                .build();
    }

    // Note: Only Ed25519 support
    static PeerId createPeerId(Ed25519PublicKeyParameters publicKeyParams) throws Exception {
        Ed25519.Ed25519PublicKey pubKey = new Ed25519.Ed25519PublicKey(publicKeyParams);
        Objects.requireNonNull(pubKey);
        return PeerId.fromPubKey(pubKey);
    }

    // Note: Only RSA support
    static Crypto.PublicKey createCryptoKey(PublicKey publicKey) {
        return Crypto.PublicKey.newBuilder().setType(Crypto.KeyType.RSA).
                setData(ByteString.copyFrom(publicKey.getEncoded()))
                .build();
    }

    // Note: Only Ed25519 support
    static void verify(Ed25519PublicKeyParameters publicKey,
                       byte[] data, byte[] signature) throws Exception {
        Ed25519.Ed25519PublicKey pubKey = new Ed25519.Ed25519PublicKey(publicKey);
        pubKey.verify(data, signature);
    }

    // Note: Only Ed25519 support
    static Keys generateKeys() {
        Ed25519KeyPairGenerator gen = new Ed25519KeyPairGenerator();
        gen.init(new Ed25519KeyGenerationParameters(new SecureRandom()));
        AsymmetricCipherKeyPair keyPair = gen.generateKeyPair();
        byte[] sk = new byte[32];
        Ed25519PrivateKeyParameters privateKeyParams =
                (Ed25519PrivateKeyParameters) keyPair.getPrivate();
        Objects.requireNonNull(privateKeyParams);
        privateKeyParams.encode(sk, 0);
        Ed25519PublicKeyParameters publicKeyParams = (Ed25519PublicKeyParameters) keyPair.getPublic();
        Objects.requireNonNull(publicKeyParams);
        return new Keys(publicKeyParams, privateKeyParams);
    }


    @NonNull
    byte[] raw();
}


