package threads.lite.autonat;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteHost;
import threads.lite.ident.IdentityService;
import threads.lite.utils.DataHandler;

public class AutonatService {

    private static final String TAG = AutonatService.class.getSimpleName();

    public static void autonat(LiteHost host, Session session,
                               Autonat autonat, Set<Multiaddr> multiaddrs) {
        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        Set<Multiaddr> addresses = host.networkPublicAddresses();

        Set<Connection> connections = getAutonatConnections(
                host, session, autonat, multiaddrs);
        if (!autonat.abort()) {

            for (Connection connection : connections) {
                service.execute(() -> {
                    try {
                        if (!autonat.abort()) {
                            try {
                                autonat.addAddr(AutonatService.autonat(
                                                connection, host.self(), addresses)
                                        .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS));

                            } catch (Throwable ignore) {
                            } finally {
                                connection.close();
                            }
                        }
                    } catch (Throwable ignore) {
                    }
                });
            }
        }
        service.shutdown();
        try {
            boolean termination = service.awaitTermination(IPFS.AUTONAT_TIMEOUT, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    public static Set<Connection> getAutonatConnections(
            LiteHost host, Session session, Autonat autonat, Set<Multiaddr> multiaddrs) {

        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        Set<Connection> connections = ConcurrentHashMap.newKeySet();

        for (Multiaddr multiaddr : multiaddrs) {
            service.execute(() -> {
                try {
                    if (!autonat.abort()) {
                        Connection conn = host.connect(session, multiaddr,
                                Parameters.getDefaultUniDirection(IPFS.GRACE_PERIOD,
                                        IPFS.MESSAGE_SIZE_MAX), true);

                        PeerInfo peerInfo = IdentityService.getPeerInfo(host, conn)
                                .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                        if (peerInfo.hasProtocol(IPFS.AUTONAT_PROTOCOL)) {
                            Multiaddr observed = peerInfo.getObserved();
                            autonat.evaluateNatType(observed);
                            connections.add(conn);
                        } else {
                            conn.close();
                        }
                    }
                } catch (Throwable ignore) {
                }
            });
        }
        service.shutdown();
        try {
            boolean termination = service.awaitTermination(IPFS.AUTONAT_TIMEOUT, TimeUnit.SECONDS);
            if (!termination) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
        return connections;

    }

    public static CompletableFuture<Multiaddr> autonat(
            @NonNull Connection conn, @NonNull PeerId self, @NonNull Set<Multiaddr> addresses) {
        CompletableFuture<Multiaddr> multiaddrCompletableFuture = new CompletableFuture<>();

        AutonatService.dial(conn, self, addresses).whenComplete((message, throwable) -> {
            if (throwable != null) {
                multiaddrCompletableFuture.completeExceptionally(throwable);
            } else {
                if (message.hasDialResponse()) {
                    autonat.pb.Autonat.Message.DialResponse dialResponse =
                            message.getDialResponse();
                    if (dialResponse.hasStatusText()) {
                        LogUtils.error(TAG, "Autonat Dial Text : "
                                + dialResponse.getStatusText());
                    }
                    if (dialResponse.hasStatus()) {
                        if (dialResponse.getStatus() ==
                                autonat.pb.Autonat.Message.ResponseStatus.OK) {
                            try {
                                ByteString raw = dialResponse.getAddr();
                                multiaddrCompletableFuture.complete(Multiaddr.create(self,
                                        raw.asReadOnlyByteBuffer()));
                            } catch (Throwable exception) {
                                multiaddrCompletableFuture.completeExceptionally(exception);
                            }
                        } else {
                            multiaddrCompletableFuture.completeExceptionally(
                                    new Exception(dialResponse.getStatusText())
                            );
                        }
                    } else {
                        multiaddrCompletableFuture.completeExceptionally(
                                new Exception("invalid status")
                        );
                    }
                }
            }
        });

        return multiaddrCompletableFuture;
    }

    @NonNull
    private static CompletableFuture<autonat.pb.Autonat.Message> dial(
            @NonNull Connection conn, @NonNull PeerId peerId, @NonNull Set<Multiaddr> addresses) {

        CompletableFuture<autonat.pb.Autonat.Message> response = new CompletableFuture<>();
        autonat.pb.Autonat.Message.PeerInfo.Builder peerInfoBuilder =
                autonat.pb.Autonat.Message.PeerInfo.newBuilder();

        peerInfoBuilder.setId(ByteString.copyFrom(peerId.getBytes()));

        if (addresses.isEmpty()) {
            response.completeExceptionally(new RuntimeException("No addresses defined"));
            return response;
        }

        for (Multiaddr addr : addresses) {
            peerInfoBuilder.addAddrs(ByteString.copyFrom(addr.getBytes()));
        }

        autonat.pb.Autonat.Message.Dial dial = autonat.pb.Autonat.Message.Dial.newBuilder()
                .setPeer(peerInfoBuilder.build()).build();

        autonat.pb.Autonat.Message message = autonat.pb.Autonat.Message.newBuilder().
                setType(autonat.pb.Autonat.Message.MessageType.DIAL).
                setDial(dial).build();


        conn.createStream(new StreamHandler() {
                    @Override
                    public void throwable(Stream stream, Throwable throwable) {
                        response.completeExceptionally(throwable);
                    }

                    @Override
                    public void protocol(Stream stream, String protocol) throws Exception {
                        if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL).contains(protocol)) {
                            throw new Exception("Token " + protocol + " not supported");
                        }
                        if (Objects.equals(protocol, IPFS.AUTONAT_PROTOCOL)) {
                            stream.writeOutput(DataHandler.encode(message)).
                                    thenApply(Stream::closeOutput);
                        }
                    }

                    @Override
                    public void data(Stream stream, ByteBuffer data) throws Exception {
                        response.complete(autonat.pb.Autonat.Message.parseFrom(data.array()));
                    }
                })
                .thenApply(quicStream -> quicStream.writeOutput(
                        DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL, IPFS.AUTONAT_PROTOCOL)));

        return response;

    }
}
