package threads.lite.host;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.Version;
import net.luminis.quic.server.ApplicationProtocolConnectionFactory;
import net.luminis.quic.server.ServerConnector;

import java.net.ConnectException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

import identify.pb.IdentifyOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.autonat.Autonat;
import threads.lite.autonat.AutonatService;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.ProtocolSupport;
import threads.lite.core.AutonatResult;
import threads.lite.core.BlockStore;
import threads.lite.core.Connection;
import threads.lite.core.ConnectionHandler;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Keys;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.PeerStore;
import threads.lite.core.Reachability;
import threads.lite.core.Reservation;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.crypto.Key;
import threads.lite.ident.IdentityService;
import threads.lite.ipns.IpnsService;
import threads.lite.minidns.DnsClient;
import threads.lite.minidns.DnsResolver;
import threads.lite.noise.Noise;
import threads.lite.push.Push;
import threads.lite.relay.RelayConnection;
import threads.lite.relay.RelayService;
import threads.lite.server.ServerResponder;
import threads.lite.server.ServerSession;
import threads.lite.utils.PackageReader;


public final class LiteHost {
    private static final String TAG = LiteHost.class.getSimpleName();

    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);
    @NonNull
    private final AtomicBoolean symmetricNatDetected = new AtomicBoolean(false);
    @NonNull
    private final AtomicReference<ProtocolSupport> protocol = new AtomicReference<>(ProtocolSupport.UNKNOWN);
    @NonNull
    private final Set<Reservation> reservations = ConcurrentHashMap.newKeySet();
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final Keys keys;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteCertificate liteCertificate;
    @NonNull
    private final AtomicReference<Multiaddr> dialableAddress = new AtomicReference<>();
    @NonNull
    private final ReentrantLock autonat = new ReentrantLock();
    @NonNull
    private final ReentrantLock reserve = new ReentrantLock();
    @NonNull
    private final DatagramSocket socket;
    @NonNull
    private final ServerConnector server;
    @NonNull
    private final DnsClient dnsClient;
    @NonNull
    private final Noise noise;
    @NonNull
    private final Set<ConnectionHandler> connectionHandlers = new HashSet<>();
    @NonNull
    private final ServerSession serverSession;
    @Nullable
    private Consumer<Push> incomingPush;
    @Nullable
    private Consumer<Connection> connectConsumer;
    @Nullable
    private Consumer<Reachability> reachabilityConsumer;
    @Nullable
    private Function<PeerId, Boolean> isGated;

    public LiteHost(@NonNull Keys keys, @NonNull BlockStore blockStore,
                    @NonNull PeerStore peerStore, int port)
            throws Exception {

        this.liteCertificate = LiteCertificate.createCertificate(keys);
        this.keys = keys;
        this.blockStore = blockStore;
        this.peerStore = peerStore;

        this.noise = Noise.getInstance();
        this.self = Key.createPeerId(keys.getPublic());
        evaluateProtocol();
        this.socket = getSocket(port);
        List<Version> supportedVersions = new ArrayList<>();
        supportedVersions.add(Version.IETF_draft_29);
        supportedVersions.add(Version.QUIC_version_1);


        this.dnsClient = DnsResolver.getInstance(() -> {
            switch (protocol.get()) {
                case IPv4:
                    return new ArrayList<>(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                case IPv6:
                    return new ArrayList<>(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                default:
                    ArrayList<InetAddress> list = new ArrayList<>();
                    list.addAll(DnsResolver.STATIC_IPV4_DNS_SERVERS);
                    list.addAll(DnsResolver.STATIC_IPV6_DNS_SERVERS);
                    return list;
            }
        });

        this.serverSession = new ServerSession(blockStore, this);
        this.server = new ServerConnector(socket, new LiteTrust(), liteCertificate.certificate(),
                liteCertificate.privateKey(), supportedVersions,
                quicStream -> new PackageReader(new ServerResponder(serverSession)), false);

        server.registerApplicationProtocol(IPFS.ALPN, new ApplicationProtocolConnectionFactory() {
            @Override
            public void createConnection(String protocol, QuicConnection quicConnection) {

                LogUtils.error(TAG, "Server connection established " +
                        quicConnection.getRemoteAddress().toString());

                LiteConnection liteConnection = new LiteConnection(quicConnection);

                try {
                    X509Certificate cert = quicConnection.getRemoteCertificate();
                    Objects.requireNonNull(cert);
                    PeerId remotePeerId = LiteCertificate.extractPeerId(cert);
                    Objects.requireNonNull(remotePeerId);

                    if (isPeerGated(remotePeerId)) {
                        throw new Exception("Peer is gated " + remotePeerId);
                    }

                    // now the remote PeerId is available
                    quicConnection.setAttribute(Connection.REMOTE_PEER, remotePeerId);

                } catch (Throwable throwable) {
                    quicConnection.close();
                    LogUtils.error(TAG, throwable);
                    return;
                }

                try {
                    if (connectConsumer != null) {
                        connectConsumer.accept(liteConnection);
                    }
                } catch (Throwable ignore) {
                }

                try {
                    for (ConnectionHandler listener : connectionHandlers) {
                        listener.incomingConnection(liteConnection);
                    }
                } catch (Throwable ignore) {
                }

                try {
                    InetSocketAddress address = quicConnection.getRemoteAddress();

                    if (!Multiaddr.isLocalAddress(address.getAddress())) {
                        if (reachabilityConsumer != null) {
                            reachabilityConsumer.accept(Reachability.GLOBAL);
                        }
                    }
                } catch (Throwable ignore) {
                }

            }
        });

        server.start();
        if (reachabilityConsumer != null) {
            reachabilityConsumer.accept(Reachability.UNKNOWN);
        }
    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NonNull
    public ServerSession getServerSession() {
        return serverSession;
    }

    @NonNull
    public DnsClient getDnsClient() {
        return dnsClient;
    }

    public void addConnectionHandler(ConnectionHandler connectionHandler) {
        connectionHandlers.add(connectionHandler);
    }

    public void removeConnectionHandler(ConnectionHandler connectionHandler) {
        connectionHandlers.remove(connectionHandler);
    }

    @NonNull
    public Noise getNoise() {
        return noise;
    }

    public void setIsGated(@Nullable Function<PeerId, Boolean> isGated) {
        this.isGated = isGated;
    }

    @NonNull
    private DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }


    public void setConnectConsumer(@Nullable Consumer<Connection> connectConsumer) {
        this.connectConsumer = connectConsumer;
    }

    public void setClosedConsumer(@Nullable Consumer<Connection> closedConsumer) {

        server.setClosedConsumer(connection -> {
            if (closedConsumer != null) {
                closedConsumer.accept(new LiteConnection(connection));
            }
        });
    }

    public void shutdown() {
        server.shutdown();
        if (reachabilityConsumer != null) {
            reachabilityConsumer.accept(Reachability.NONE);
        }
    }

    public void setReachabilityConsumer(@Nullable Consumer<Reachability> reachabilityConsumer) {
        this.reachabilityConsumer = reachabilityConsumer;
    }

    @NonNull
    public Keys getKeys() {
        return keys;
    }


    @NonNull
    public Set<Reservation> reservations() {
        return reservations;
    }


    @NonNull
    public Session createSession(@NonNull BlockStore blockStore,
                                 boolean findProvidersActive, boolean sendReplyActive) {
        return new LiteSession(blockStore, this, findProvidersActive, sendReplyActive);
    }

    @NonNull
    public PeerId self() {
        return self;
    }


    @NonNull
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @Nullable
    private Multiaddr resolveAddress(@NonNull Multiaddr multiaddr) {
        if (multiaddr.isDns() || multiaddr.isDns6() || multiaddr.isDns4()) {
            return DnsResolver.resolveDns(getProtocolSupport(), multiaddr);
        } else if (multiaddr.isDnsaddr()) {
            List<Multiaddr> list = DnsResolver.resolveDnsaddr(getDnsClient(),
                    getProtocolSupport(), multiaddr);
            if (!list.isEmpty()) {
                return list.get(0);
            }
        } else {
            return multiaddr;
        }
        return null;
    }


    @Nullable
    public Multiaddr resolveToAny(@NonNull Set<Multiaddr> multiaddrs) {
        List<Multiaddr> all = new ArrayList<>();
        for (Multiaddr multiaddr : multiaddrs) {
            Multiaddr resolved = resolveAddress(multiaddr);
            if (resolved != null) {
                all.add(resolved);
            }
        }
        return all.stream().findAny().orElse(null);
    }

    @NonNull
    public Set<Multiaddr> networkPublicAddresses() {
        Set<Multiaddr> set = new HashSet<>();
        for (Multiaddr multiaddr : networkListenAddresses()) {
            if (multiaddr.protocolSupported(protocol.get(), true)) {
                set.add(multiaddr);
            }
        }
        return set;
    }

    @NonNull
    public Set<Multiaddr> networkListenAddresses() {
        Set<Multiaddr> set = new HashSet<>();
        int port = socket.getLocalPort();
        if (port > 0) {
            for (InetAddress inetAddress : networkAddresses()) {
                try {
                    set.add(Multiaddr.create(self, new InetSocketAddress(inetAddress, port)));
                } catch (Throwable ignore) {
                }
            }
        }

        return set;
    }


    @NonNull
    public Set<Multiaddr> listenAddresses() {
        Set<Multiaddr> set = networkListenAddresses();
        Multiaddr dialable = dialableAddress.get();
        if (dialable != null) {
            set.add(dialable);
        }
        for (Reservation reservation : reservations()) {
            try {
                if (reservation.getConnection().isConnected()) {
                    set.addAll(reservation.getAddresses());
                } else {
                    reservations.remove(reservation);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        return set;
    }

    @NonNull
    public Connection connect(@NonNull Session session, @NonNull Multiaddr multiaddr,
                              @NonNull Parameters parameters, boolean serverConnect)
            throws ConnectException, InterruptedException {
        Multiaddr resolved = resolveAddress(multiaddr);
        if (resolved == null) {
            throw new ConnectException("no addresses left");
        }
        return dial(session, resolved, parameters, serverConnect);
    }

    public boolean holePunchAllowed() {
        return !symmetricNatDetected.get();
    }

    @NonNull
    public Connection dial(@NonNull Session session, @NonNull Multiaddr address,
                           @NonNull Parameters parameters, boolean serverConnect)
            throws ConnectException, InterruptedException {

        if (address.isCircuitAddress()) {
            try {
                PeerId relayId = address.getRelayId();
                Objects.requireNonNull(relayId);
                PeerId peerId = address.getPeerId();

                RelayConnection relayConnection = RelayService.createRelayConnection(
                        this, session, address, relayId, peerId, parameters);

                return relayConnection.upgradeConnection();
            } catch (InterruptedException | ConnectException exception) {
                throw exception;
            } catch (Throwable throwable) {
                throw new ConnectException(throwable.getMessage());
            }
        } else {
            PeerId peerId = address.getPeerId();
            return dial(session.getStreamHandler(), peerId, address,
                    serverConnect, parameters);

        }
    }


    @NonNull
    public LiteConnection dial(StreamHandler streamHandler, PeerId peerId, Multiaddr address,
                               boolean serverConnect, Parameters parameters)
            throws ConnectException, InterruptedException {

        long start = System.currentTimeMillis();
        boolean run = false;

        QuicClientConnection connection = getClientConnection(streamHandler, peerId, address.getHost(),
                address.getPort(), serverConnect, parameters);

        try {
            // now the remote peerId is available in Connection (LiteConnection)
            connection.setAttribute(Connection.REMOTE_PEER, peerId);
            connection.connect(IPFS.CONNECT_TIMEOUT);

            run = true;
            return new LiteConnection(connection);
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.debug(TAG, "Dial " +
                        " Success " + run +
                        " Count Success " + success.get() +
                        " Count Failure " + failure.get() +
                        " ServerConnect " + serverConnect +
                        " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    private QuicClientConnection getClientConnection(
            StreamHandler streamHandler, PeerId peerId, String host, int port, boolean serverConnect,
            Parameters parameters) throws ConnectException {

        QuicClientConnectionImpl.Builder builder = QuicClientConnectionImpl.newBuilder()
                .version(Version.IETF_draft_29) // in the future switch to version 1
                .trustManager(new LiteTrust(peerId))
                .clientCertificate(liteCertificate.x509Certificate())
                .clientCertificateKey(liteCertificate.getKey())
                .host(host)
                .port(port)
                .alpn(IPFS.ALPN)
                .transportParams(parameters);

        if (serverConnect) {
            builder = builder.serverConnector(server);
        }

        return builder.build(quicStream -> new PackageReader(streamHandler));
    }


    public void push(@NonNull Connection connection, @NonNull IpnsEntity ipnsEntity) {
        try {
            if (incomingPush != null) {
                incomingPush.accept(new Push(connection, ipnsEntity));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void setIncomingPush(@Nullable Consumer<Push> incomingPush) {
        this.incomingPush = incomingPush;
    }

    public int numServerConnections() {
        return server.numConnections();
    }


    @NonNull
    public IdentifyOuterClass.Identify createIdentity(@NonNull Set<String> protocols,
                                                      @NonNull Set<Multiaddr> multiaddrs,
                                                      @Nullable byte[] observed) {

        IdentifyOuterClass.Identify.Builder builder = IdentifyOuterClass.Identify.newBuilder()
                .setAgentVersion(IPFS.AGENT)
                .setPublicKey(ByteString.copyFrom(Key.createCryptoKey(
                        keys.getPublic()).toByteArray()))
                .setProtocolVersion(IPFS.PROTOCOL_VERSION);


        if (!multiaddrs.isEmpty()) {
            for (Multiaddr addr : multiaddrs) {
                builder.addListenAddrs(ByteString.copyFrom(addr.getBytes()));
            }
        }
        for (String protocol : protocols) {
            builder.addProtocols(protocol);
        }

        if (observed != null) {
            builder.setObservedAddr(ByteString.copyFrom(observed));
        }

        return builder.build();
    }


    public void updateNetwork() {
        evaluateProtocol();
    }

    private void evaluateProtocol() {
        List<InetAddress> addresses = networkAddresses();
        if (!addresses.isEmpty()) {
            protocol.set(getProtocol(addresses));
        } else {
            protocol.set(ProtocolSupport.IPv4);
        }
    }

    private List<InetAddress> networkAddresses() {
        List<InetAddress> inetAddresses = new ArrayList<>();
        try {
            List<NetworkInterface> interfaces = Collections.list(
                    NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface networkInterface : interfaces) {
                List<InetAddress> addresses =
                        Collections.list(networkInterface.getInetAddresses());
                for (InetAddress inetAddress : addresses) {
                    if (!Multiaddr.weakLocalAddress(inetAddress)) {
                        inetAddresses.add(inetAddress);
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return inetAddresses;
    }

    @NonNull
    private ProtocolSupport getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return ProtocolSupport.IPv6;
        } else if (ipv4) {
            return ProtocolSupport.IPv4;
        } else if (ipv6) {
            return ProtocolSupport.IPv6;
        } else {
            return ProtocolSupport.UNKNOWN;
        }
    }

    @NonNull
    public CompletableFuture<PeerInfo> getPeerInfo(@NonNull Connection conn) {
        return IdentityService.getPeerInfo(this, conn);
    }

    public int getPort() {
        return socket.getLocalPort();
    }

    @NonNull
    public PeerInfo getIdentity() throws Exception {
        IdentifyOuterClass.Identify identity = createIdentity(
                Set.of(IPFS.MULTISTREAM_PROTOCOL, IPFS.HOLE_PUNCH_PROTOCOL,
                        IPFS.PUSH_PROTOCOL, IPFS.BITSWAP_PROTOCOL,
                        IPFS.IDENTITY_PROTOCOL, IPFS.DHT_PROTOCOL, IPFS.RELAY_PROTOCOL_STOP),
                listenAddresses(), null);
        return IdentityService.getPeerInfo(this, identity);

    }


    @NonNull
    public AutonatResult autonat() {
        autonat.lock();
        try {

            Autonat autonat = new Autonat(this);

            AutonatService.autonat(this, serverSession, autonat, getBootstrap());

            Multiaddr winner = autonat.winner();
            if (winner != null) {
                dialableAddress.set(winner);
            }

            if (autonat.getNatType() == AutonatResult.NatType.SYMMETRIC) {
                // first notify the user, that the node is not dialable

                if (reachabilityConsumer != null) {
                    reachabilityConsumer.accept(Reachability.LOCAL);
                }

                // second hole punch attempts are fruitless
                symmetricNatDetected.set(true);
            }

            return new AutonatResult() {
                @NonNull
                @Override
                public String toString() {
                    return "Success " + success() +
                            " Address " + dialableAddress() +
                            " NatType " + getNatType();
                }

                @NonNull
                @Override
                public NatType getNatType() {
                    return autonat.getNatType();
                }

                @Nullable
                @Override
                public Multiaddr dialableAddress() {
                    return winner;
                }

                @Override
                public boolean success() {
                    return winner != null;
                }
            };
        } finally {
            autonat.unlock();
        }
    }


    @NonNull
    public Set<Reservation> reservations(@NonNull Session session,
                                         @NonNull Set<Multiaddr> multiaddrs, long timeout) {
        reserve.lock();
        try {
            Set<PeerId> relaysStillValid = ConcurrentHashMap.newKeySet();
            Set<Reservation> list = reservations();
            // check if reservations are still valid and not expired
            for (Reservation reservation : list) {

                // check if still a connection
                // only for safety here
                if (!reservation.getConnection().isConnected()) {
                    list.remove(reservation);
                    continue;
                }

                if (reservation.isStaticRelay()) {
                    if (reservation.expireInMinutes() < 2) {
                        try {
                            RelayService.reservation(this, session, reservation.getRelayId(),
                                    reservation.getRelayAddress()).get();
                            relaysStillValid.add(reservation.getRelayId());
                        } catch (Throwable throwable) {
                            // note RelayService reservation throws exceptions
                            list.remove(reservation);
                        }
                    } else {
                        // still valid
                        relaysStillValid.add(reservation.getRelayId());
                    }
                }
            }

            if (!multiaddrs.isEmpty()) {
                ExecutorService service = Executors.newFixedThreadPool(
                        Runtime.getRuntime().availableProcessors());
                for (Multiaddr address : multiaddrs) {
                    service.execute(() -> {
                        try {
                            PeerId peerId = address.getPeerId();
                            Objects.requireNonNull(peerId);

                            if (!relaysStillValid.contains(peerId)) {
                                reservation(session, peerId, address).get();
                            } // else case: nothing to do here, reservation is sill valid

                        } catch (Throwable ignore) {
                        }
                    });

                }
                service.shutdown();
                if (timeout > 0) {
                    try {
                        boolean termination = service.awaitTermination(timeout, TimeUnit.SECONDS);
                        if (!termination) {
                            service.shutdownNow();
                        }
                    } catch (Throwable ignore) {
                    }
                }
            }
        } finally {
            reserve.unlock();
        }
        return reservations();
    }

    public CompletableFuture<Reservation> reservation(@NonNull Session session,
                                                      @NonNull PeerId peerId,
                                                      @NonNull Multiaddr multiaddr) {
        return RelayService.reservation(this, session, peerId, multiaddr)
                .whenComplete((reservation, throwable) -> {
                            if (throwable != null) {
                                LogUtils.info(TAG, throwable.getMessage());
                            } else {
                                reservations.add(reservation);
                            }
                        }
                );
    }

    public boolean hasReservations() {
        return reservations.size() > 0;
    }

    public Set<Multiaddr> getBootstrap() {
        return DnsResolver.resolveDnsaddrHost(getDnsClient(), getProtocolSupport(), IPFS.LIB2P_DNS);
    }

    public boolean isPeerGated(@NonNull PeerId peerId) {
        if (isGated != null) {
            return isGated.apply(peerId);
        }
        return false;
    }

    @NonNull
    public DatagramSocket getSocket() {
        return socket;
    }

    @NonNull
    public ProtocolSupport getProtocolSupport() {
        return protocol.get();
    }

    // creates a self signed record (used for ipns)
    public byte[] createSelfSignedRecord(byte[] data, long sequence) throws Exception {
        Date eol = Date.from(new Date().toInstant().plus(IPFS.RECORD_EOL));

        Duration duration = Duration.ofHours(IPFS.IPNS_DURATION);
        ipns.pb.Ipns.IpnsEntry record =
                IpnsService.create(keys.getPrivate(), data, sequence, eol, duration);

        record = IpnsService.embedPublicKey(keys.getPublic(), record);

        return record.toByteArray();
    }

    public byte[] createIpnsKey(@NonNull PeerId peerId) {
        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        byte[] selfKey = peerId.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.allocate(ipns.length + selfKey.length);
        byteBuffer.put(ipns);
        byteBuffer.put(selfKey);
        return byteBuffer.array();
    }

    public void punching(Multiaddr multiaddr) {

        // Upon expiry of the timer, B starts to send UDP packets filled with random bytes to A's
        // address. Packets should be sent repeatedly in random intervals between 10 and 200 ms.
        try {
            InetAddress address = multiaddr.getInetAddress();
            int port = multiaddr.getPort();

            byte[] datagramData = new byte[64];
            Random rd = new Random();
            rd.nextBytes(datagramData);

            DatagramPacket datagram = new DatagramPacket(
                    datagramData, datagramData.length, address, port);

            LogUtils.error(TAG, "[B] Hole Punch Send Random " + address + " " + port);

            getSocket().send(datagram);

            Thread.sleep(getRandomPunch()); // sleep for random value between 10 and 200

            if (!Thread.currentThread().isInterrupted()) {
                punching(multiaddr);
            }

            // interrupt exception should occur, but it simply ends the loop
        } catch (Throwable ignore) {
        }
    }

    private int getRandomPunch() {
        return new Random().nextInt(190) + 10;
    }

    public void holePunchConnect(@NonNull Set<Multiaddr> multiaddrs) {
        try {
            ExecutorService service = Executors.newFixedThreadPool(multiaddrs.size());
            for (Multiaddr multiaddr : multiaddrs) {
                service.execute(() -> punching(multiaddr));
            }
            boolean finished = service.awaitTermination(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            if (!finished) {
                service.shutdownNow();
            }
        } catch (Throwable ignore) {
        }
    }

    @NonNull
    public PeerStore getPeerStore() {
        return peerStore;
    }
}


