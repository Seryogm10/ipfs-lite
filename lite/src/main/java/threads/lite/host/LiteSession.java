package threads.lite.host;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import bitswap.pb.MessageOuterClass;
import threads.lite.LogUtils;
import threads.lite.bitswap.BitSwapHandler;
import threads.lite.bitswap.BitSwapManager;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BitSwap;
import threads.lite.core.Block;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.StreamHandler;
import threads.lite.dht.KadDht;
import threads.lite.ident.IdentityHandler;
import threads.lite.ipns.IpnsService;
import threads.lite.push.PushHandler;
import threads.lite.utils.MultistreamHandler;


public final class LiteSession implements Session {
    private static final String TAG = LiteSession.class.getSimpleName();
    @NonNull
    private final BitSwap bitSwap;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final ConcurrentHashMap<PeerId, Connection> swarm = new ConcurrentHashMap<>();
    @NonNull
    private final Map<String, ProtocolHandler> protocols = new ConcurrentHashMap<>();
    @NonNull
    private final KadDht routing;
    private final boolean findProvidersActive;
    private final boolean sendReplyActive;

    public LiteSession(@NonNull BlockStore blockStore, @NonNull LiteHost host,
                       boolean findProvidersActive, boolean sendReplyActive) {
        this.blockStore = blockStore;
        this.findProvidersActive = findProvidersActive;
        this.sendReplyActive = sendReplyActive;
        this.bitSwap = new BitSwapManager(host, this);
        this.routing = new KadDht(host, host.getPeerStore(), new IpnsService());

        // add the default
        addProtocolHandler(new MultistreamHandler());
        addProtocolHandler(new PushHandler(host));
        addProtocolHandler(new IdentityHandler(host, this));
        addProtocolHandler(new BitSwapHandler(this));
    }


    public void addProtocolHandler(@NonNull ProtocolHandler protocolHandler) {
        // some small tests
        String protocol = protocolHandler.getProtocol();
        Objects.requireNonNull(protocol);

        if (protocol.isEmpty()) {
            throw new RuntimeException("invalid protocol name");
        }

        if (!protocol.startsWith("/")) {
            throw new RuntimeException("invalid protocol name");
        }

        if (protocols.containsKey(protocol)) {
            throw new RuntimeException("protocol name already exists");
        }

        protocols.put(protocol, protocolHandler);
    }

    @NonNull
    public Map<String, ProtocolHandler> getProtocols() {
        // important, protocols should note be modified "outside"
        return Collections.unmodifiableMap(protocols);
    }

    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {
        return bitSwap.getBlock(cancellable, cid);
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {
        bitSwap.receiveMessage(connection, bsm);
    }

    @Override
    public boolean swarmHas(@NonNull PeerId peerId) {
        Connection conn = swarm.get(peerId);
        if (conn != null) {
            if (!conn.isConnected()) {
                swarmReduce(peerId);
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public void swarmReduce(@NonNull PeerId peerId) {
        swarm.remove(peerId);
    }

    @Override
    public boolean swarmEnhance(@NonNull Connection connection) {
        PeerId remotePeerId = connection.remotePeerId();
        if (!swarmHas(remotePeerId)) {
            swarm.put(remotePeerId, connection);
            return true;
        }
        return false;
    }

    @NonNull
    @Override
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @Override
    public boolean isFindProvidersActive() {
        return findProvidersActive;
    }

    @Override
    public boolean isSendReplyActive() {
        return sendReplyActive;
    }

    @NonNull
    @Override
    public StreamHandler getStreamHandler() {
        return new LiteResponder(protocols);
    }

    @Override
    @NonNull
    public List<Connection> getSwarm() {
        List<Connection> result = new ArrayList<>();
        for (Connection connection : swarm.values()) {
            if (connection.isConnected()) {
                result.add(connection);
            } else {
                swarm.remove(connection.remotePeerId());
            }
        }
        return result;
    }

    @Override
    public boolean swarmContains(@NonNull Connection connection) {
        return swarm.contains(connection);
    }

    @Override
    public void putValue(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull byte[] key, @NonNull byte[] data) {
        routing.putValue(cancellable, consumer, key, data);
    }

    @Override
    public void findPeer(@NonNull Cancellable cancellable, @NonNull Consumer<Multiaddr> consumer,
                         @NonNull PeerId peerId) {
        routing.findPeer(cancellable, consumer, peerId);
    }

    @Override

    public void searchValue(@NonNull Cancellable cancellable,
                            @NonNull Consumer<IpnsEntity> consumer, @NonNull byte[] key) {
        routing.searchValue(cancellable, consumer, key);
    }

    @Override
    public void findProviders(@NonNull Cancellable cancellable,
                              @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        routing.findProviders(cancellable, consumer, cid);
    }

    @Override
    public void provide(@NonNull Cancellable cancellable,
                        @NonNull Consumer<Multiaddr> consumer, @NonNull Cid cid) {
        routing.provide(cancellable, consumer, cid);
    }

    @Override
    public void findClosestPeers(@NonNull Cancellable cancellable,
                                 @NonNull Consumer<Multiaddr> consumer, @NonNull PeerId peerId) {
        routing.findClosestPeers(cancellable, consumer, peerId);
    }

    @NonNull
    @Override
    public Set<Peer> getBootstrapPeers() {
        return routing.getBootstrapPeers();
    }

    @Override
    public void close() {
        try {
            swarm.values().forEach(Connection::close);
            bitSwap.close();
            routing.close();
            swarm.clear();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}

