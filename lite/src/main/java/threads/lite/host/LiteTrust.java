package threads.lite.host;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Objects;

import javax.net.ssl.X509TrustManager;

import threads.lite.LogUtils;
import threads.lite.cid.PeerId;

@SuppressLint("CustomX509TrustManager")
public final class LiteTrust implements X509TrustManager {
    private static final String TAG = LiteTrust.class.getSimpleName();
    @Nullable
    private final PeerId expected;

    public LiteTrust(@NonNull PeerId expected) {
        this.expected = expected;
    }

    public LiteTrust() {
        this.expected = null;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        try {

            if (chain.length != 1) {
                throw new Exception("only one certificate allowed");
            }

            // here the expected peerId is null (because it is simply not known)
            // just check if the extracted peerId is not null
            for (X509Certificate cert : chain) {

                LiteCertificate.validCertificate(cert);

                PeerId peerId = LiteCertificate.extractPeerId(cert);
                Objects.requireNonNull(peerId);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new CertificateException(throwable);
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType)
            throws CertificateException {
        try {

            if (chain.length != 1) {
                throw new Exception("only one certificate allowed");
            }

            Objects.requireNonNull(expected); // expected is required
            // now check the extracted peer ID with the expected value
            for (X509Certificate cert : chain) {

                LiteCertificate.validCertificate(cert);

                PeerId peerId = LiteCertificate.extractPeerId(cert);
                Objects.requireNonNull(peerId);
                if (!Objects.equals(peerId, expected)) {
                    throw new CertificateException("PeerIds do not match " + peerId.toBase58() +
                            " " + expected.toBase58());
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
            throw new CertificateException(throwable);
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }

}
