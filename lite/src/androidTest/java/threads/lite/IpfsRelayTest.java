package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Session;


@RunWith(AndroidJUnit4.class)
public class IpfsRelayTest {
    private static final String TAG = IpfsRelayTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_relay_reserve_and_connect() throws Throwable {
        IPFS ipfs = TestEnv.getTestInstance(context);


        DUMMY dummy = DUMMY.getInstance();

        try (Session dummySession = dummy.createSession()) {
            assertTrue(ipfs.hasReservations());

            Set<Reservation> reservations = ipfs.reservations();
            assertFalse(reservations.isEmpty());
            Reservation reservation = reservations.stream().
                    findAny().orElseThrow((Supplier<Throwable>) () ->
                            new RuntimeException("at least one present"));


            PeerId relayID = reservation.getRelayId();
            assertNotNull(relayID);
            Multiaddr relay = reservation.getRelayAddress();
            assertNotNull(relay);

            assertTrue(reservation.getLimitData() > 0);
            assertTrue(reservation.getLimitDuration() > 0);
            Multiaddr multiaddr = reservation.getAddresses().iterator().next();

            Connection conn = dummy.getHost().dial(dummySession,
                    multiaddr, Parameters.getDefault(), false);
            Objects.requireNonNull(conn);

            // TEST 1
            PeerInfo peerInfo = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(peerInfo);
            Multiaddr observed = peerInfo.getObserved();
            assertNotNull(observed);
            assertEquals(peerInfo.getAgent(), IPFS.AGENT);
            LogUtils.debug(TAG, peerInfo.toString());


            // TEST 2
            AtomicBoolean success = new AtomicBoolean(false);
            byte[] test = "moin".getBytes(StandardCharsets.UTF_8);
            IpnsRecord data = ipfs.createSelfSignedIpnsRecord(0, test);

            ipfs.setIncomingPush(push -> {
                IpnsEntity ipnsEntry = push.getIpnsEntity();
                assertNotNull(push.getConnection());
                success.set(Arrays.equals(ipnsEntry.getData(), test));
            });

            ipfs.push(conn, data).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            Thread.sleep(2000);
            Assert.assertTrue(success.get());

            // TEST 3
            peerInfo = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(peerInfo);

            // TEST 4
            success.set(false);
            byte[] test2 = "zehn".getBytes(StandardCharsets.UTF_8);
            IpnsRecord ipnsRecord = ipfs.createSelfSignedIpnsRecord(0, test2);

            ipfs.setIncomingPush(push -> {
                IpnsEntity ipnsEntry = push.getIpnsEntity();
                success.set(Arrays.equals(ipnsEntry.getData(), test2));
            });


            ipfs.push(conn, ipnsRecord).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            Thread.sleep(2000);
            Assert.assertTrue(success.get());

            // will close the relay connection from the dummy
            conn.close();

            assertTrue(ipfs.hasReservations());

        } finally {
            dummy.getHost().shutdown();
            dummy.clearDatabase();
        }

    }

}
