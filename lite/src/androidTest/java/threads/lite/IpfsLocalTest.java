package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsLocalTest {

    private static final String TAG = IpfsLocalTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void own_service_connect() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            Multiaddr multiaddr = Multiaddr.getSiteLocalAddress(ipfs.self(), ipfs.getPort());

            Connection conn = ipfs.dial(session, multiaddr, ipfs.getConnectionParameters());
            assertNotNull(conn);

            PeerInfo info = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());
        }

    }

    //@Test
    public void direct_dial_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            Multiaddr multiaddr = ipfs.decodeMultiaddr(
                    "/ip4/192.168.43.172/udp/4001/quic/p2p/12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            Connection conn = ipfs.dial(session, multiaddr, ipfs.getConnectionParameters());
            assertNotNull(conn);


            PeerInfo info = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());
        }

    }


    //@Test // not working since it is impossible to put the pc as a static relay, when behind a NAT
    public void pc_static_relay() throws Exception {
        // Precondition: pc is configured as static relay (and running)


        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            // (1) create a reservation to the pc (from ipfs node)

            Multiaddr pc = ipfs.decodeMultiaddr("/ip4/192.168.43.172/udp/4001/quic/p2p/12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");

            Reservation reservation =
                    ipfs.reservation(session, pc).get(15, TimeUnit.SECONDS);

            Objects.requireNonNull(reservation);
            assertEquals(reservation.getKind(), Reservation.Kind.STATIC);

            Multiaddr multiaddr = reservation.getAddresses().iterator().next();

            // (2) create a dummy and connect to ipfs node multiaddrs
            DUMMY dummy = DUMMY.getInstance();

            try (Session dummySession = dummy.createSession()) {

                Connection connection = dummy.getHost().connect(dummySession, multiaddr,
                        ipfs.getConnectionParameters(), false);

                assertNotNull(connection);

                // (3) peer info test
                PeerInfo info = dummy.getHost().getPeerInfo(connection)
                        .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                assertNotNull(info);

                LogUtils.error(TAG, info.toString());

                // (4) add much more tests


                connection.close();
            }

        }


    }

    //@Test
    public void direct_dial_via_relay_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Thread.sleep(500000);

        try (Session session = ipfs.createSession()) {

            Multiaddr multiaddr = ipfs.decodeMultiaddr("/ip6/2604:1380:45d1:3c00::13/udp/4001/quic/p2p/12D3KooW9swwwtAj83nNXZjvzNUvV1pqEqoY7MMr8ZCU5yRaSbXQ/p2p-circuit/p2p/12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            Connection conn = ipfs.dial(session, multiaddr, ipfs.getConnectionParameters());
            assertNotNull(conn);

            PeerInfo info = ipfs.getPeerInfo(conn).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
            assertNotNull(info);
            LogUtils.error(TAG, info.toString());
        }
    }

    //@Test
    public void find_pc_data() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {

            Cid cid = Cid.decode("QmdjoDyD7JC8K3JnYqtEE31BoZC7yxa5pzJbWZfFB41gcF");

            byte[] data = ipfs.getData(session, cid, new TimeoutCancellable(60));
            assertNotNull(data);

        }
    }


    //@Test
    public void find_pc() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            PeerId pc = PeerId.fromBase58("12D3KooWSR9wEwGHjd6Rj3CViDPvHX5FGkXop4y6wNVzRvbZSXoK");
            AtomicBoolean connect = new AtomicBoolean(false);
            ipfs.findPeer(session, pc,
                    multiaddr -> {
                        LogUtils.error(TAG, multiaddr.toString());

                        try {
                            Connection conn = ipfs.dial(
                                    session, multiaddr, ipfs.getConnectionParameters());
                            assertNotNull(conn);
                            connect.set(true);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }


                    }, connect::get);

            assertTrue(connect.get());

        }
    }
}
