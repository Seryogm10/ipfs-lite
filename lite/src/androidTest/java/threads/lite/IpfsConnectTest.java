package threads.lite;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.ConnectException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Session;


@RunWith(AndroidJUnit4.class)
public class IpfsConnectTest {
    private static final String TAG = IpfsConnectTest.class.getSimpleName();

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test(expected = ConnectException.class)
    public void swarm_connect() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String address = "/ip4/139.178.68.146/udp/4001/quic/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR";

            Multiaddr multiaddr = ipfs.decodeMultiaddr(address);
            assertNotNull(multiaddr.getInetSocketAddress());
            assertEquals(multiaddr.getInetSocketAddress().getPort(), multiaddr.getPort());
            assertEquals(multiaddr.getInetSocketAddress().getHostName(), multiaddr.getHost());

            assertEquals(multiaddr.getPeerId(),
                    ipfs.decodePeerId("12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR"));


            // multiaddress is just a fiction
            Connection conn = ipfs.dial(session, multiaddr, ipfs.getConnectionParameters());
            assertNull(conn); // does not come to this point

            fail(); // exception is thrown
        }
    }


    @Test
    public void test_print_swarm_peers() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Set<Reservation> reservations = ipfs.reservations();
            for (Reservation reservation : reservations) {

                Connection conn = reservation.getConnection();
                assertTrue(conn.isConnected());
                assertFalse(session.swarmContains(conn));
                assertTrue(session.swarmEnhance(conn));
            }

            List<Connection> connections = session.getSwarm();

            assertNotNull(connections);
            LogUtils.debug(TAG, "Peers : " + connections.size());
            for (Connection conn : connections) {

                try {
                    assertNotNull(conn.getRemoteAddress());
                    PeerInfo peerInfo = ipfs.getPeerInfo(conn).
                            get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

                    LogUtils.debug(TAG, peerInfo.toString());
                    assertNotNull(peerInfo.getMultiaddrs());
                    assertNotNull(peerInfo.getAgent());

                    Multiaddr observed = peerInfo.getObserved();
                    LogUtils.debug(TAG, observed.toString());

                } catch (Throwable throwable) {
                    LogUtils.debug(TAG, "" + throwable.getClass().getName());
                } finally {
                    session.swarmReduce(conn.remotePeerId());
                }

            }
        }
    }

}
