package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsHolepunchTest {

    private static final String TAG = IpfsBootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_incoming() throws Throwable {

        IPFS ipfs = TestEnv.getTestInstance(context);
        DUMMY dummy = DUMMY.getInstance();


        try (Session dummySession = dummy.createSession()) {
            assertTrue(ipfs.hasReservations());

            Set<Reservation> reservations = ipfs.reservations();
            assertFalse(reservations.isEmpty());
            Reservation reservation = reservations.stream().
                    findAny().orElseThrow((Supplier<Throwable>) () ->
                            new RuntimeException("at least one present"));

            LogUtils.error(TAG, reservation.toString());
            Multiaddr multiaddr = reservation.getAddresses().iterator().next();

            assertNotNull(multiaddr.getPeerId());
            assertNotNull(multiaddr.getRelayId());
            assertTrue(multiaddr.isCircuitAddress());


            Connection conn = dummy.getHost().dial(dummySession, multiaddr,
                    Parameters.getDefault(), false);

            assertNotNull(conn);

            PeerInfo info = dummy.getHost().getPeerInfo(conn)
                    .get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);

            assertNotNull(info);
            LogUtils.error(TAG, info.toString());

            conn.close();

        } finally {
            dummy.getHost().shutdown();

        }
    }

}