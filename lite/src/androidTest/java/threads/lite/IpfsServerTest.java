package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Parameters;
import threads.lite.core.PeerInfo;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsServerTest {

    private static final String TAG = IpfsServerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void server_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String text = "Hallo das ist ein Test";
            Cid cid = ipfs.storeText(session, text);
            assertNotNull(cid);


            DUMMY dummy = DUMMY.getInstance();

            try (Session dummySession = dummy.createSession()) {
                Multiaddr multiaddr = Multiaddr.getLocalHost(ipfs.self(), ipfs.getPort());

                PeerId host = ipfs.self();
                assertNotNull(host);

                Connection conn = dummy.getHost().dial(
                        dummySession, multiaddr, Parameters.getDefault(), false);
                Objects.requireNonNull(conn);
                dummySession.swarmEnhance(conn);
                assertTrue(dummySession.swarmContains(conn));


                PeerInfo info = dummy.getHost().getPeerInfo(conn).
                        get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                assertNotNull(info);
                assertEquals(info.getAgent(), IPFS.AGENT);
                assertNotNull(info.getObserved());

                // simple push test
                byte[] test = "moin".getBytes(StandardCharsets.UTF_8);
                IpnsRecord data = ipfs.createSelfSignedIpnsRecord(0, test);

                AtomicBoolean notified = new AtomicBoolean(false);
                ipfs.setIncomingPush(push -> {
                    IpnsEntity ipnsEntry = push.getIpnsEntity();
                    notified.set(Arrays.equals(ipnsEntry.getData(), test));
                });
                ipfs.push(conn, data).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);


                Thread.sleep(1000);
                assertTrue(notified.get());


                String cmpText = dummy.getText(dummySession, cid, new TimeoutCancellable(10));
                assertEquals(text, cmpText);

                conn.close();
            } finally {
                dummy.getHost().shutdown();
                dummy.clearDatabase();
            }
        }
    }


    @Test
    public void server_multiple_dummy_conn() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            byte[] input = TestEnv.getRandomBytes(50000);

            Cid cid = ipfs.storeData(session, input);
            assertNotNull(cid);

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;
            int timeSeconds = 200;
            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {

                    try {
                        DUMMY dummy = DUMMY.getInstance();

                        try (Session dummySession = dummy.createSession()) {

                            PeerId server = ipfs.self();
                            LogUtils.debug(TAG, "Server :" + server.toBase58());
                            PeerId client = dummy.self();
                            LogUtils.debug(TAG, "Client :" + client.toBase58());

                            Multiaddr multiaddr = Multiaddr.getLocalHost(ipfs.self(), ipfs.getPort());


                            Connection conn = dummy.getHost().dial(dummySession, multiaddr,
                                    Parameters.getDefault(), false);
                            assertNotNull(conn);
                            dummySession.swarmEnhance(conn);

                            PeerInfo info = dummy.getHost().getPeerInfo(conn).
                                    get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                            assertNotNull(info);
                            assertEquals(info.getAgent(), IPFS.AGENT);
                            assertNotNull(info.getObserved());


                            byte[] output = dummy.getData(dummySession, cid,
                                    new TimeoutCancellable(timeSeconds));
                            assertArrayEquals(input, output);

                            conn.close();

                            LogUtils.info(TAG, "finished " + finished.incrementAndGet());
                        } finally {
                            dummy.getHost().shutdown();

                            dummy.clearDatabase();
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        fail();
                    }

                });
            }

            executors.shutdown();

            assertTrue(executors.awaitTermination(timeSeconds, TimeUnit.SECONDS));
            assertEquals(finished.get(), instances);
        }

    }
}