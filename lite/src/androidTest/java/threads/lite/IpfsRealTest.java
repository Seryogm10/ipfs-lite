package threads.lite;


import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsRealTest {
    private static final String TAG = IpfsRealTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_corbett() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            //CorbettReport ipns://k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm
            String key = "k2k4r8jllj4k33jxoa4vaeleqkrwu8b7tqz7tgczhptbfkhqr2i280fm";

            IpnsEntity res = ipfs.resolveName(session, PeerId.fromString(key),
                    0, new TimeoutCancellable(30));
            assertNotNull(res);


            Cid cid = ipfs.decodeIpnsData(res);
            assertEquals(cid.getVersion(), 0);

            AtomicBoolean done = new AtomicBoolean(false);

            ipfs.findProviders(session, (multiaddr) -> done.set(true), cid,
                    new TimeoutCancellable(done::get, 30));

            assertTrue(done.get());
        }
    }

    @Test
    public void test_blog_ipfs_io() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession(true, true)) {
            String link = ipfs.resolveDnsLink("blog.ipfs.io");

            assertNotNull(link);
            assertFalse(link.isEmpty());
            Cid cid = Cid.decode(link.replace(IPFS.IPFS_PATH, ""));

            Cid node = ipfs.resolveCid(session, cid, Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(node);
            assertEquals(node.getVersion(), 0);

            List<Link> links = ipfs.links(session, node, false, () -> false);
            assertNotNull(links);
            for (Link lnk : links) {
                LogUtils.debug(TAG, lnk.toString());
            }

            node = ipfs.resolveCid(session, node, List.of(IPFS.INDEX_HTML), new TimeoutCancellable(60));
            assertNotNull(node);
            assertEquals(node.getVersion(), 0);

            String text = ipfs.getText(session, node, new TimeoutCancellable(30));

            assertNotNull(text);
            assertFalse(text.isEmpty());
        }

    }

    @Test
    public void test_unknown() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid node = ipfs.resolveCid(
                    session, Cid.decode("QmavE42xtK1VovJFVTVkCR5Jdf761QWtxmvak9Zx718TVr"),
                    Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(node);
            assertEquals(node.getVersion(), 0);


            List<Link> links = ipfs.links(session, node, false, new TimeoutCancellable(1));
            assertNotNull(links);
            assertFalse(links.isEmpty());
        }
    }

    @Test
    public void test_unknown_2() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid node = ipfs.resolveCid(session, Cid.decode("QmfQiLpdBDbSkb2oySwFHzNucvLkHmGFxgK4oA2BUSwi4t"),
                    Collections.emptyList(), new TimeoutCancellable(60));
            assertNotNull(node);
            assertEquals(node.getVersion(), 0);


            List<Link> links = ipfs.links(session, node, false, new TimeoutCancellable(1));
            assertNotNull(links);
            assertFalse(links.isEmpty());
        }
    }

}
