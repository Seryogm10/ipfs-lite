package threads.lite;


import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@RunWith(AndroidJUnit4.class)
public class IpfsBasicThreadTest {

    private static final String TAG = IpfsBasicThreadTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_completable_cancel() throws Exception {

        ExecutorService service = Executors.newSingleThreadExecutor();
        CompletableFuture<Void> done = new CompletableFuture<>();
        AtomicBoolean success = new AtomicBoolean(false);
        service.execute(new Runnable() {
            @Override
            public void run() {
                while (!done.isDone()) {
                    LogUtils.error(TAG, "running");
                }
                done.complete(null);
                success.set(true);
            }
        });
        Thread.sleep(1000);
        done.cancel(true);

        Thread.sleep(1000);
        assertTrue(success.get());

    }
}
