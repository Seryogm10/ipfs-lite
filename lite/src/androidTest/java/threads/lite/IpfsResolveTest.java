package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.google.common.primitives.Bytes;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Session;
import threads.lite.ipns.IpnsService;
import threads.lite.utils.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsResolveTest {
    private static final String TAG = IpfsResolveTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_resolve_publish() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String test = new String(TestEnv.getRandomBytes(20));
            Cid cid = ipfs.storeText(session, test);
            assertNotNull(cid);

            int sequence = TestEnv.getSequence(context);
            int newSequence = sequence + 1;
            TestEnv.setSequence(context, newSequence);

            long start = System.currentTimeMillis();

            Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();

            ipfs.publishName(session, sequence, cid, providers::add,
                    new TimeoutCancellable(() -> false, 30));

            LogUtils.error(TAG, "Time provide " + (System.currentTimeMillis() - start) +
                    " number of providers " + providers.size());

            String key = ipfs.self().toBase36();

            IpnsEntity entry = ipfs.resolveName(session, PeerId.fromString(key),
                    sequence, () -> false);
            assertNotNull(entry);

            LogUtils.verbose(TAG, entry.toString());
            Cid cmp = ipfs.decodeIpnsData(entry);
            assertEquals(cmp, cid);
        }
    }

    @Test
    public void test_time_format() throws ParseException {
        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date(System.currentTimeMillis()));
        assertNotNull(format);

        Date date = IpnsService.getDate(format);
        Objects.requireNonNull(date);


        Date cmp = IpnsService.getDate("2021-04-15T06:14:21.184394868Z");
        Objects.requireNonNull(cmp);

    }

    @Test
    public void test_peer_id() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        PeerId peerId = ipfs.self();
        assertNotNull(peerId);

        byte[] data = peerId.getBytes();


        Multihash mh = Multihash.deserialize(data);

        PeerId cmp = PeerId.fromBase58(mh.toBase58());

        assertEquals(cmp, peerId);

        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        String cipns = new String(ipns);

        assertEquals(cipns, IPFS.IPNS_PATH);

        byte[] result = Bytes.concat(ipns, data);

        int index = Bytes.indexOf(result, ipns);
        assertEquals(index, 0);

        byte[] key = Arrays.copyOfRange(result, ipns.length, result.length);

        Multihash mh1 = Multihash.deserialize(key);

        PeerId cmp1 = PeerId.fromBase58(mh1.toBase58());

        assertEquals(cmp1, peerId);
    }

}
