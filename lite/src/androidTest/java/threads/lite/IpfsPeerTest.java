package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Parameters;
import threads.lite.core.Reservation;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsPeerTest {
    private static final String TAG = IpfsPeerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_connectAndFindPeer() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);


        try (Session session = ipfs.createSession()) {
            Multiaddr multiaddr = ipfs.decodeMultiaddr("/ip4/139.178.68.145/udp/4001/quic/p2p/" +
                    "12D3KooWSzPeHsfxULJwFiLeq6Qsx6TruezAwjZ619qsLhqC7cUR");
            assertTrue(multiaddr.isIP4());
            Parameters parameters = ipfs.getConnectionParameters();
            PeerId peerId = multiaddr.getPeerId();
            Connection conn = ipfs.dial(session, multiaddr, parameters);
            assertNotNull(conn);
            TestCase.assertTrue(conn.isConnected());
            byte[] test = "moin".getBytes(StandardCharsets.UTF_8);
            IpnsRecord data = ipfs.createSelfSignedIpnsRecord(0, test);
            try {
                ipfs.push(conn, data).get(IPFS.CONNECT_TIMEOUT, TimeUnit.SECONDS);
                fail();
            } catch (Throwable throwable) {
                LogUtils.info(TAG, throwable.getMessage());
            }

            AtomicBoolean found = new AtomicBoolean(false);
            ipfs.findPeer(session, peerId, multiaddr1 -> {
                LogUtils.debug(TAG, multiaddr1.toString());
                found.set(true);
            }, new TimeoutCancellable(found::get, 30));
            assertTrue(found.get());
            conn.close();

        }
    }

    @Test
    public void test_findPeer() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);


        try (Session session = ipfs.createSession()) {
            Set<Reservation> reservations = ipfs.reservations();
            assertFalse(reservations.isEmpty());
            for (Reservation reservation : reservations) {
                PeerId relayId = reservation.getRelayId();
                LogUtils.debug(TAG, relayId.toString());
                assertEquals(reservation.getKind(), Reservation.Kind.LIMITED);
                Connection conn = reservation.getConnection();
                assertNotNull(conn);
                assertTrue(conn.isConnected());
                AtomicBoolean found = new AtomicBoolean(false);
                ipfs.findPeer(session, relayId, multiaddr -> {
                    LogUtils.debug(TAG, multiaddr.toString());
                    found.set(true);
                }, new TimeoutCancellable(found::get, 30));
                assertTrue(found.get());
            }
        }
    }
}