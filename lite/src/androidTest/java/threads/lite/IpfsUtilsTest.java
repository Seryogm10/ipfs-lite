package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;
import android.util.Pair;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.ec.CustomNamedCurves;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.NamedParameterSpec;
import java.util.Base64;

import threads.lite.cid.Cid;
import threads.lite.cid.ID;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.cid.Prefix;
import threads.lite.core.Keys;
import threads.lite.crypto.Key;
import threads.lite.dht.Util;
import threads.lite.mplex.Mplex;
import threads.lite.mplex.MuxFlag;
import threads.lite.mplex.MuxId;
import threads.lite.noise.Noise;
import threads.lite.utils.DataHandler;


@RunWith(AndroidJUnit4.class)
public class IpfsUtilsTest {
    private static final String TAG = IpfsUtilsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void testDnsAddr() throws Exception {
        String address = "/dnsaddr/bootstrap.libp2p.io/p2p/QmbLHAnMoJPWSCR5Zhtx6BHJX9KiKNN6tpvbUcqanj75Nb";
        Multiaddr multiaddr = Multiaddr.create(address);
        assertNotNull(multiaddr);
        assertFalse(multiaddr.isCircuitAddress());
        assertTrue(multiaddr.strictSupportedAddress());
        assertTrue(multiaddr.weakSupportedAddress());
        assertTrue(multiaddr.isDnsaddr());
        assertNotNull(multiaddr.getPeerId());

    }

    @Test
    public void testDns4() throws Exception {

        String address = "/dns4/quicweb3-storage-am6.web3.dwebops.net/udp/4001/quic/p2p/12D3KooWPySxxWQjBgX9Jp6uAHQfVmdq8HG1gVvS1fRawHNSrmqW";
        Multiaddr multiaddr = Multiaddr.create(address);
        assertNotNull(multiaddr);
        assertFalse(multiaddr.isCircuitAddress());
        assertTrue(multiaddr.strictSupportedAddress());
        assertTrue(multiaddr.weakSupportedAddress());
        assertTrue(multiaddr.isDns4());
        assertNotNull(multiaddr.getPeerId());
        assertEquals(address, multiaddr.toString());


        address = "/dns4/luflosi.de/udp/4002/quic/p2p/12D3KooWBqQrnTqx9Wp89p2bD1hrwmXYJQ5x1fDfigRCfZJGKQfr/p2p-circuit/p2p/12D3KooWKQTnJ3vkV3aQFKiZQKJTfqrzj75Gfpp6CLMdBvqpu6Ac";
        multiaddr = Multiaddr.create(address);
        assertNotNull(multiaddr);
        assertTrue(multiaddr.isCircuitAddress());
        assertTrue(multiaddr.strictSupportedAddress());
        assertTrue(multiaddr.weakSupportedAddress());
        assertTrue(multiaddr.isDns4());
        assertNotNull(multiaddr.getPeerId());
        assertEquals(address, multiaddr.toString());

    }

    @Test
    public void testAddress() throws Exception {
        String address = "/ip4/139.178.68.146/udp/4001/quic";
        Multiaddr multiaddr = Multiaddr.create(PeerId.random(), address);
        assertNotNull(multiaddr);

        Multiaddr cmp = Multiaddr.fromArray(Multiaddr.toArray(multiaddr));
        assertNotNull(cmp);
        assertEquals(cmp, multiaddr);
    }

    @Test
    public void muxHeaderTest() {
        MuxId muxId = new MuxId(0, true);
        int header = Mplex.encodeHeader(muxId, MuxFlag.OPEN);
        assertEquals(header, 0);

        Pair<MuxId, MuxFlag> pair = Mplex.decodeHeader(header);
        assertNotNull(pair);
        MuxId cmp = pair.first;
        assertEquals(muxId.getStreamId(), cmp.getStreamId());
        assertTrue(muxId.isInitiator());
        assertFalse(cmp.isInitiator());
        assertEquals(pair.second, MuxFlag.OPEN);
    }


    @Test
    public void bigEndianTest() {
        int test = 30;
        ByteBuffer bigEndian = Noise.getBigEndianShort(test);
        bigEndian.flip();
        int length = DataHandler.getBigEndianUnsignedShort(bigEndian);
        assertEquals(length, test);
    }

    @Test
    public void bigEndianTestBig() {
        int test = 666;
        ByteBuffer bigEndian = Noise.getBigEndianShort(test);
        bigEndian.flip();
        int length = DataHandler.getBigEndianUnsignedShort(bigEndian);
        assertEquals(length, test);
    }

    @Test
    public void cidV1() throws Exception {

        Cid cid = Cid.nsToCid("moin welt");
        assertNotNull(cid);
        Prefix prefix = cid.getPrefix();
        assertNotNull(prefix);
        assertEquals(prefix.getCodec(), Cid.Raw);
        assertEquals(prefix.getType(), Multihash.Type.sha2_256);
        assertEquals(prefix.getVersion(), 1);
        assertNotNull(cid.getMultihash());

        Cid cmp = Cid.fromArray(cid.bytes());
        assertNotNull(cmp);
        assertEquals(cmp, cid);
        assertEquals(cmp.getPrefix(), cid.getPrefix());
        assertArrayEquals(cmp.getMultihash(), cid.getMultihash());


    }

    @Test
    public void decode_name() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        // test of https://docs.ipfs.io/how-to/address-ipfs-on-web/#http-gateways
        PeerId test = PeerId.fromBase58("QmNnooDu7bfjPFoTZYxMNLWUQJyrVwtbZg5gBMjTezGAJN");
        assertEquals("k2k4r8jl0yz8qjgqbmc2cdu5hkqek5rj6flgnlkyywynci20j0iuyfuj", test.toBase36());

        Cid cid = Cid.decode("QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");

        assertEquals(cid.String(),
                "QmbWqxBEKC3P8tqsKc98xmWNzrzDtRLMiMPL8wBuTGsMnR");
        assertEquals(Cid.newCidV1(cid.getCodec(), cid.bytes()).String(),
                "bafybeigdyrzt5sfp7udm7hu76uh7y26nf3efuylqabf3oclgtqy55fbzdi");

        assertEquals(ipfs.self(), ipfs.decodePeerId(ipfs.self().toBase36()));
    }

    @Test
    public void network() throws Exception {

        int port = 4001;

        IPFS ipfs = TestEnv.getTestInstance(context);

        Multiaddr ma = Multiaddr.getSiteLocalAddress(ipfs.self(), port);
        assertNotNull(ma);
        LogUtils.error(TAG, ma.toString());

        ma = Multiaddr.getLocalHost(ipfs.self(), port);
        assertNotNull(ma);
        LogUtils.error(TAG, ma.toString());


    }

    @Test
    public void cat_utils() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        PeerId peerId = ipfs.self();


        ID a = ID.convertPeerID(peerId);
        ID b = ID.convertPeerID(peerId);


        BigInteger dist = Util.distance(a, b);
        assertEquals(dist.longValue(), 0L);

        int res = Util.commonPrefixLen(a, b);
        assertEquals(res, (a.data.length * 8));

        int cmp = a.compareTo(b);
        assertEquals(0, cmp);


        PeerId randrom = PeerId.random();

        ID r1 = ID.convertPeerID(randrom);
        ID r2 = ID.convertPeerID(randrom);

        BigInteger distCmp = Util.distance(a, r1);
        assertNotEquals(distCmp.longValue(), 0L);

        int rres = Util.commonPrefixLen(r1, r2);
        assertEquals(rres, (r1.data.length * 8));

        int rcmp = r1.compareTo(r2);
        assertEquals(0, rcmp);


        Cid cid = Cid.nsToCid("time");
        assertNotNull(cid);
        assertEquals(cid.getPrefix().getVersion(), 1);

    }

    // not yet supported
    @Test(expected = NoSuchAlgorithmException.class)
    public void test_curve25519() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("XDH");
        NamedParameterSpec paramSpec = new NamedParameterSpec("X25519");
        kpg.initialize(paramSpec); // equivalent to kpg.initialize(255)
        // alternatively: kpg = KeyPairGenerator.getInstance("X25519")
        KeyPair kp = kpg.generateKeyPair();
        assertNotNull(kp.getPrivate());
        assertNotNull(kp.getPublic());
    }


    @Test
    public void test_bouncycastle_curve25519() throws Exception {
        X9ECParameters curveParams = CustomNamedCurves.getByName("Curve25519");
        ECParameterSpec ecSpec = new ECParameterSpec(curveParams.getCurve(), curveParams.getG(),
                curveParams.getN(), curveParams.getH(), curveParams.getSeed());

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
        kpg.initialize(ecSpec);

        KeyPair keyPair = kpg.generateKeyPair();
        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();
        assertNotNull(privateKey);
        assertNotNull(publicKey);
    }

    @Test
    public void test_bouncycastle_ed25519() throws Exception {
        Keys keys = Key.generateKeys();

        PeerId peerId = Key.createPeerId(keys.getPublic());
        LogUtils.error(TAG, peerId.toString());

        byte[] msg = "moin moin".getBytes();
        byte[] signature = Key.sign(keys.getPrivate(), msg);

        Base64.Encoder encoder = Base64.getEncoder();
        String privateKeyAsString = encoder.encodeToString(keys.getPrivate().getEncoded());
        assertNotNull(privateKeyAsString);
        String publicKeyAsString = encoder.encodeToString(keys.getPublic().getEncoded());
        assertNotNull(publicKeyAsString);

        Base64.Decoder decoder = Base64.getDecoder();

        byte[] privateKeyBytes = decoder.decode(privateKeyAsString);
        byte[] publicKeyBytes = decoder.decode(publicKeyAsString);

        Ed25519PrivateKeyParameters privateKey = new Ed25519PrivateKeyParameters(privateKeyBytes, 0);
        assertNotNull(privateKey);
        Ed25519PublicKeyParameters publicKey = new Ed25519PublicKeyParameters(publicKeyBytes, 0);
        assertNotNull(publicKey);

        PeerId peerIdCmp = Key.createPeerId(publicKey);
        LogUtils.error(TAG, peerIdCmp.toString());

        assertEquals(peerId, peerIdCmp);

        Key.verify(publicKey, msg, signature);

    }

}
