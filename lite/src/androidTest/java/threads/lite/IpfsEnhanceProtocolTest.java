package threads.lite;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Multiaddr;
import threads.lite.core.Connection;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Session;
import threads.lite.core.Stream;
import threads.lite.core.StreamHandler;
import threads.lite.utils.DataHandler;

@RunWith(AndroidJUnit4.class)
public class IpfsEnhanceProtocolTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void enhance_protocol() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        // this is for testing, if protocol was successful
        CompletableFuture<String> done = new CompletableFuture<>();


        // (1) first step define is defining a protocol handler
        ProtocolHandler testHandler = new ProtocolHandler() {
            @Override
            public String getProtocol() {
                return "/test";           // protocol starts with "/"
            }

            // is invoked, when the your protocol "/test" is requested
            @Override
            public void protocol(Stream stream) {
                // first thing to do is accept that you have the protocol
                // just replying the protocol name

                stream.writeOutput(DataHandler.encodeProtocols("/test"))
                        .thenApply(Stream::closeOutput);

                // closing the output is only valid for your protocol handler,
                // because you are not writing nothing more on the stream,
                // you just want to read-out data, which is done in
                // the data function
            }


            // is invoked, when data is arriving associated with your protocol
            @Override
            public void data(Stream stream, ByteBuffer data) {

                // now just reads out the data
                done.complete(new String(data.array()));
            }
        };


        // (2) when you add the handler to server session, it will be used for incoming
        // connections too
        ipfs.getServerSession().addProtocolHandler(testHandler);

        Map<String, ProtocolHandler> serverProtocols = ipfs.getServerSession().getProtocols();
        assertNotNull(serverProtocols);

        assertTrue(serverProtocols.containsKey("/test"));
        assertTrue(serverProtocols.containsKey(IPFS.PUSH_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.MULTISTREAM_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.IDENTITY_PROTOCOL));
        assertTrue(serverProtocols.containsKey(IPFS.BITSWAP_PROTOCOL));
        assertEquals(serverProtocols.size(), 5);

        try (Session session = ipfs.createSession()) {

            // add a the protocol with handler to a new session
            session.addProtocolHandler(testHandler);

            Map<String, ProtocolHandler> protocols = session.getProtocols();
            assertNotNull(protocols);

            assertTrue(protocols.containsKey("/test"));
            assertTrue(protocols.containsKey(IPFS.PUSH_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.MULTISTREAM_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.IDENTITY_PROTOCOL));
            assertTrue(protocols.containsKey(IPFS.BITSWAP_PROTOCOL));
            assertEquals(protocols.size(), 5);


            // now you have added the protocol to a session and server server session

            // (3) now invoke the test protocol via a connection and stream

            // for testing we are connecting to our own server
            Multiaddr ownLocalServerAddress = Multiaddr.getSiteLocalAddress(
                    ipfs.self(), ipfs.getPort());
            Objects.requireNonNull(ownLocalServerAddress);

            Connection connection = ipfs.dial(session, ownLocalServerAddress,
                    ipfs.getConnectionParameters());
            assertNotNull(connection);


            // create a stream and invoke the protocol as initiator

            connection.createStream(new StreamHandler() {
                @Override
                public void throwable(Stream stream, Throwable throwable) {
                    fail();
                }

                @Override
                public void protocol(Stream stream, String protocol) {

                    // the remote accepts your protocol, anyway check if it is your protocol
                    // all protocols are going via this function (at least
                    // the multistream are also visible here)
                    if (Objects.equals(protocol, "/test")) {
                        // Note each data must be encoded (extra information like length of the
                        // data are added) IMPORTANT

                        // write a message "moin moin" and then close output
                        stream.writeOutput(DataHandler.encode("moin moin".getBytes()))
                                .thenApply(Stream::closeOutput);
                    }
                }

                @Override
                public void data(Stream stream, ByteBuffer data) throws Exception {
                    // nothing expected here
                    throw new Exception("not expected here");
                }
            }).whenComplete((stream, throwable) -> {
                if (throwable != null) {
                    fail();
                } else {
                    // Note: this invokes the protocol, the multistream must be used in front
                    // for negotiate reasons
                    stream.writeOutput(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL,
                            "/test"));
                }
            });

            // now wait for 5 sec timeout
            String message = done.get(5, TimeUnit.SECONDS);

            // when the server received the message, this should be true
            assertEquals(message, "moin moin");
        }

    }
}
