package threads.lite;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Multiaddr;
import threads.lite.core.AutonatResult;
import threads.lite.core.Reservation;
import threads.lite.core.Session;

class TestEnv {

    private static final String TAG = TestEnv.class.getSimpleName();
    private static final AtomicBoolean started = new AtomicBoolean(false);

    public static void setSequence(Context context, int sequence) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                TestEnv.class.getSimpleName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("SEQ", sequence);
        editor.apply();
    }

    public static int getSequence(@NonNull Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(
                TestEnv.class.getSimpleName(), Context.MODE_PRIVATE);
        return sharedPref.getInt("SEQ", 0);
    }

    public static byte[] getRandomBytes(int number) {
        byte[] bytes = new byte[number];
        new Random().nextBytes(bytes);
        return bytes;
    }

    public static IPFS getTestInstance(@NonNull Context context) throws Exception {

        IPFS.setPort(context, 5001);

        IPFS ipfs = IPFS.getInstance(context);
        ipfs.getBlockStore().clear(); // clears the default blockStore


        Session session = ipfs.createSession();

        ipfs.updateNetwork();
        ipfs.setConnectConsumer(connection -> LogUtils.error(TAG, "Incoming connection : "
                + connection.getRemoteAddress()));
        ipfs.setIsGated(peerId -> {
            LogUtils.error(TAG, "Peer Gated : " + peerId.toBase58());
            return false;
        });


        if (!started.getAndSet(true)) {
            AutonatResult result = ipfs.autonat();
            LogUtils.error(TAG, "Success Autonat : " + result);


            Set<Reservation> reservations = ipfs.reservations(session,
                    ipfs.getBootstrap(), 30);
            for (Reservation reservation : reservations) {
                LogUtils.error(TAG, reservation.toString());
            }

            for (Multiaddr ma : ipfs.getIdentity().getMultiaddrs()) {
                LogUtils.error(TAG, "Listen Address " + ma.toString());
            }

            LogUtils.error(TAG, "Next Reservation Cycle " +
                    ipfs.nextReservationCycle() + " [min]");
        }

        System.gc();
        return ipfs;
    }


}
