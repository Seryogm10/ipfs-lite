package threads.lite;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.BlockStoreMemory;
import threads.lite.utils.TimeoutCancellable;
import threads.lite.utils.TimeoutProgress;


@RunWith(AndroidJUnit4.class)
public class IpfsCatTest {

    private static final String TAG = IpfsCatTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void cat_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid cid = ipfs.decodeCid("Qmaisz6NMhDB51cCvNWa1GMS7LU1pAxdF4Ld6Ft9kZEP2a");

            assertTrue(cid.isSupported());
            long time = System.currentTimeMillis();
            Set<Multiaddr> provs = ConcurrentHashMap.newKeySet();

            ipfs.findProviders(session, provs::add, cid,
                    new TimeoutCancellable(() -> provs.size() > 10, 30));

            assertFalse(provs.isEmpty());

            for (Multiaddr prov : provs) {
                LogUtils.info(TAG, "Provider " + prov);
            }
            LogUtils.debug(TAG, "Time Providers : " + (System.currentTimeMillis() - time) + " [ms]");


            time = System.currentTimeMillis();


            List<Link> res = ipfs.links(session, cid, true, new TimeoutCancellable(30));
            LogUtils.debug(TAG, "Time : " + (System.currentTimeMillis() - time) + " [ms]");
            assertNotNull(res);
            assertTrue(res.isEmpty());

            time = System.currentTimeMillis();
            byte[] content = ipfs.getData(session, cid, new TimeoutProgress(() -> false, 10) {
                @Override
                public void setProgress(int progress) {
                    LogUtils.debug(TAG, "" + progress);
                }

                @Override
                public boolean doProgress() {
                    return true;
                }
            });

            LogUtils.debug(TAG, "Time : " + (System.currentTimeMillis() - time) + " [ms]");

            assertNotNull(content);


            time = System.currentTimeMillis();
            ipfs.removeBlocks(session, cid);
            LogUtils.debug(TAG, "Time : " + (System.currentTimeMillis() - time) + " [ms]");
        }
    }


    @Test
    public void cat_not_exist() throws Exception {


        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid cid = ipfs.decodeCid("QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nt");
            try {
                ipfs.getData(session, cid, new TimeoutCancellable(10));
                fail();
            } catch (Exception ignore) {
                //
            }
        }
    }


    @Test
    public void cat_test_local() throws Exception {


        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid local = ipfs.storeText(session, "Moin Moin Moin");
            assertNotNull(local);

            byte[] content = ipfs.getData(session, local, () -> false);

            assertNotNull(content);
        }
    }

    @Test
    public void cat_empty() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession(new BlockStoreMemory(),
                true, true)) {
            Cid cid = Cid.decode("QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn");
            List<Link> res = ipfs.links(session, cid, true, new TimeoutCancellable(30));
            assertNotNull(res);

            assertTrue(res.isEmpty());

            byte[] data = ipfs.getData(session, cid, new TimeoutCancellable(10));
            assertEquals(0, data.length);


            ipfs.removeBlocks(session, cid);
        }
    }
}