package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.core.Keys;
import threads.lite.core.Link;
import threads.lite.core.PeerInfo;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;


@RunWith(AndroidJUnit4.class)
public class IpfsTest {
    private static final String TAG = IpfsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_keys() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Keys keys = ipfs.getKeys();
        assertNotNull(keys.getPrivate());
        assertNotNull(keys.getPublic());
    }

    @Test
    public void test_listenAddresses() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        Multiaddrs result = ipfs.getIdentity().getMultiaddrs();
        assertNotNull(result);
        for (Multiaddr ma : result) {
            LogUtils.debug(TAG, "Listen Address : " + ma.toString());
        }
        assertTrue(result.size() >= 1);

        PeerInfo info = ipfs.getIdentity();
        Objects.requireNonNull(info);

        assertNotNull(info.getPeerId());
        assertNotNull(info.getVersion());
        assertFalse(info.hasRelayHop());
        assertEquals(info.getAgent(), IPFS.AGENT);
        assertEquals(info.getPeerId(), ipfs.self());

        List<String> protocols = info.getProtocols();
        for (String protocol : protocols) {
            assertNotNull(protocol);
        }

        LogUtils.debug(TAG, info.toString());

        Multiaddrs multiaddrs = ipfs.getIdentity().getMultiaddrs();
        assertNotNull(multiaddrs);
        assertFalse(multiaddrs.isEmpty());

    }


    @Test
    public void test_bootstrap() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Set<Multiaddr> bootstrap = ipfs.getBootstrap();
        assertNotNull(bootstrap);

        assertTrue(bootstrap.size() >= 7);
        for (Multiaddr address : bootstrap) {
            LogUtils.error(TAG, address.toString());
        }
    }

    @Test
    public void test_bootstrap_peers() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        try (Session session = ipfs.createSession()) {
            Set<Peer> peers = session.getBootstrapPeers();
            assertNotNull(peers);
            assertFalse(peers.isEmpty());

            for (Peer peer : peers) {
                LogUtils.error(TAG, peer.toString());
            }
        }
    }

    @Test
    public void streamTest() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String test = "Moin";
            Cid cid = ipfs.storeText(session, test);
            assertNotNull(cid);
            byte[] bytes = ipfs.getData(session, cid, () -> false);
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));


            try {
                Cid fault = Cid.decode(ipfs.self().toBase58());
                ipfs.getData(session, fault, new TimeoutCancellable(10));
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }

    }

    @Test
    public void textProgressTest() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String test = "Moin bla bla";
            Cid cid = ipfs.storeText(session, test);
            assertNotNull(cid);

            AtomicInteger check = new AtomicInteger(0);

            byte[] bytes = ipfs.getData(session, cid, new Progress() {
                @Override
                public void setProgress(int progress) {
                    check.set(progress);
                }

                @Override
                public boolean doProgress() {
                    return true;
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }
            });
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));


            String text = ipfs.getText(session, cid, new Progress() {
                @Override
                public void setProgress(int progress) {
                    check.set(progress);
                }

                @Override
                public boolean doProgress() {
                    return true;
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }
            });
            assertNotNull(text);
            assertEquals(test, text);

            assertEquals(100, check.get());
        }

    }


    @Test(expected = Exception.class)
    public void textProgressAbortTest() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String test = "Moin bla bla";
            Cid cid = ipfs.storeText(session, test);
            assertNotNull(cid);

            ipfs.getData(session, cid, new Progress() {
                @Override
                public void setProgress(int progress) {
                    fail("should go here");
                }

                @Override
                public boolean doProgress() {
                    return true;
                }

                @Override
                public boolean isCancelled() {
                    return true;
                }
            });

            fail("expected fail");
        }

    }

    @Test
    public void test_timeout_cat() throws Exception {

        Cid notValid = Cid.decode("QmaFuc7VmzwT5MAx3EANZiVXRtuWtTwALjgaPcSsZ2Jdip");
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            try {
                ipfs.getData(session, notValid, new TimeoutCancellable(10));
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }
    }


    @Test
    public void test_add_cat() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            byte[] content = TestEnv.getRandomBytes(400000);

            Cid hash58Base = ipfs.storeData(session, content);
            assertNotNull(hash58Base);
            LogUtils.debug(TAG, hash58Base.String());

            byte[] fileContents = ipfs.getData(session, hash58Base, () -> false);
            assertNotNull(fileContents);
            assertEquals(content.length, fileContents.length);
            assertEquals(new String(content), new String(fileContents));

            ipfs.removeBlocks(session, hash58Base);
        }
    }


    @Test(expected = Exception.class)
    public void test_ls_timeout() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            ipfs.links(session, Cid.decode("QmXm3f7uKuFKK3QUL1V1oJZnpJSYX8c3vdhd94evSQUPCH"),
                    true, new TimeoutCancellable(20));

        }
    }

    @Test
    public void test_ls_small() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid cid = ipfs.storeText(session, "hallo");
            assertNotNull(cid);
            List<Link> links = ipfs.links(session, cid, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 0);
            links = ipfs.links(session, cid, true, new TimeoutCancellable(20));
            assertNotNull(links);
            assertEquals(links.size(), 0);
        }
    }
}
