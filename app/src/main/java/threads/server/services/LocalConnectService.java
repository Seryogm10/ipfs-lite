package threads.server.services;

import android.content.Context;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Protocol;
import threads.lite.core.Connection;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.server.core.DOCS;
import threads.server.core.locals.LOCALS;
import threads.server.core.locals.Local;

public class LocalConnectService {

    private static final String TAG = LocalConnectService.class.getSimpleName();


    public static void connect(@NonNull Context context, @NonNull NsdServiceInfo serviceInfo) {

        AtomicBoolean regularNode = new AtomicBoolean(false);
        try {
            PeerId.fromString(serviceInfo.getServiceName());
        } catch (Throwable throwable) {
            regularNode.set(true);
        }


        try {
            LOCALS locals = LOCALS.getInstance(context);
            IPFS ipfs = IPFS.getInstance(context);
            DOCS docs = DOCS.getInstance(context);
            if (!regularNode.get()) {
                InetAddress inetAddress = serviceInfo.getHost();

                String pre = "/ip4";
                if (inetAddress instanceof Inet6Address) {
                    pre = "/ip6";
                }

                String multiAddress = pre + inetAddress + "/udp/" + serviceInfo.getPort()
                        + "/quic/p2p/" + serviceInfo.getServiceName();
                Multiaddr multiaddr = ipfs.decodeMultiaddr(multiAddress);
                Local local = locals.insertLocal(multiaddr);

                connect(docs.getSession(), ipfs, List.of(local));

            } else {
                Map<String, byte[]> maps = serviceInfo.getAttributes();
                byte[] value = maps.get(Protocol.DNSADDR.getType());
                String address = new String(value);

                Multiaddr multiaddr = ipfs.decodeMultiaddr(address);
                Local local = locals.insertLocal(multiaddr);
                connect(docs.getSession(), ipfs, List.of(local));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public static void connect(@NonNull Session session,
                               @NonNull IPFS ipfs,
                               @NonNull List<Local> locals) {

        ExecutorService executorService = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        for (Local local : locals) {
            executorService.execute(() -> {
                try {
                    Multiaddr multiaddr = local.getMultiaddr();

                    PeerId peerId = multiaddr.getPeerId();
                    if (session.swarmHas(peerId)) {
                        return;
                    }

                    InetAddress inetAddress = multiaddr.getInetAddress();

                    if (inetAddress.isSiteLocalAddress()) {

                        Connection conn = ipfs.dial(session, multiaddr,
                                ipfs.getConnectionParameters());
                        session.swarmEnhance(conn);

                        LogUtils.error(TAG, "Success adding to swarm " + multiaddr);

                    } else {

                        AtomicBoolean connected = new AtomicBoolean(false);
                        ipfs.findPeer(session, peerId, multiaddr1 -> {
                            if (!multiaddr1.isCircuitAddress()) {
                                try {
                                    Connection conn = ipfs.dial(session, multiaddr1,
                                            ipfs.getConnectionParameters());
                                    session.swarmEnhance(conn);
                                    connected.set(true);
                                    LogUtils.error(TAG, "Success adding to swarm " +
                                            conn.getRemoteAddress());

                                } catch (Throwable ignore) {
                                }
                            }
                        }, new TimeoutCancellable(connected::get, 30));
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });
        }
        executorService.shutdown();

    }

}

