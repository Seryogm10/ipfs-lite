package threads.server.services;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import threads.lite.LogUtils;

public class RegistrationService implements NsdManager.RegistrationListener {
    private static final String TAG = RegistrationService.class.getSimpleName();

    public static RegistrationService getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
        LogUtils.error(TAG, "RegistrationFailed : " + errorCode);
    }

    @Override
    public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
        LogUtils.error(TAG, "Un-RegistrationFailed : " + errorCode);
    }

    @Override
    public void onServiceRegistered(NsdServiceInfo serviceInfo) {
        LogUtils.info(TAG, "ServiceRegistered : " + serviceInfo.getServiceName());
    }

    @Override
    public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
        LogUtils.info(TAG, "Un-ServiceRegistered : " + serviceInfo.getServiceName());
    }

    private static final class InstanceHolder {
        private static final RegistrationService INSTANCE = new RegistrationService();
    }
}
