package threads.server.core;


import android.content.Context;
import android.net.Uri;
import android.webkit.WebResourceResponse;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.IpnsEntity;
import threads.lite.core.Link;
import threads.lite.core.Session;
import threads.lite.utils.TimeoutCancellable;
import threads.server.R;
import threads.server.core.books.BOOKS;
import threads.server.core.books.Bookmark;
import threads.server.core.files.FILES;
import threads.server.core.files.Proxy;
import threads.server.core.locals.LOCALS;
import threads.server.core.pages.PAGES;
import threads.server.core.pages.Page;
import threads.server.magic.ContentInfo;
import threads.server.magic.ContentInfoUtil;
import threads.server.services.LocalConnectService;
import threads.server.services.MimeTypeService;

public class DOCS {

    public static final long MIN_SEQUENCE = 2000;
    private static final String TAG = DOCS.class.getSimpleName();
    private static final ConcurrentHashMap<String, Session> sessions = new ConcurrentHashMap<>();
    private static final Set<Uri> uris = ConcurrentHashMap.newKeySet();
    private static volatile DOCS INSTANCE = null;
    public final AtomicBoolean darkMode = new AtomicBoolean(false);
    private final ReentrantLock lock = new ReentrantLock();
    private final IPFS ipfs;
    private final FILES files;
    private final PAGES pages;
    private final BOOKS books;
    private final String host;
    private final String homepage;
    private final Session session;
    private final LOCALS locals;
    private final ConcurrentHashMap<PeerId, Cid> resolves = new ConcurrentHashMap<>();


    private DOCS(@NonNull Context context) throws Exception {
        ipfs = IPFS.getInstance(context);
        files = FILES.getInstance(context);
        pages = PAGES.getInstance(context);
        books = BOOKS.getInstance(context);
        locals = LOCALS.getInstance(context);
        session = ipfs.createSession(true, true);
        try {
            host = ipfs.self().toBase36();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
        homepage = context.getString(R.string.homepage);
    }

    public static DOCS getInstance(@NonNull Context context) throws Exception {

        if (INSTANCE == null) {
            synchronized (DOCS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DOCS(context);
                }
            }
        }
        return INSTANCE;
    }

    private static String getNameWithoutExtension(@NonNull String file) {
        String fileName = new File(file).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? fileName : fileName.substring(0, dotIndex);
    }

    private static String getFileExtension(@NonNull String fullName) {
        String fileName = new File(fullName).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
    }

    @NonNull
    public static String getFileName(@NonNull Uri uri) {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            return paths.get(paths.size() - 1);
        } else {
            return "" + uri.getHost();
        }

    }

    @NonNull
    public Session getSession(@NonNull String authority) {
        Session session = sessions.get(authority);
        if (session == null) {
            LogUtils.error(TAG, "create session for authority " + authority);
            session = ipfs.createSession(true, false);
            LocalConnectService.connect(session, ipfs, locals.getLocals());
            sessions.put(authority, session);
        }
        return session;
    }

    public boolean hasSession(@NonNull String authority) {
        return sessions.containsKey(authority);
    }


    // release all sessions except the session with the given authority
    public void releaseSessions(@NonNull String authority) {
        for (Map.Entry<String,Session> entry: sessions.entrySet()) {
            if(!Objects.equals(entry.getKey(), authority)){
                entry.getValue().close();
                sessions.remove(entry.getKey());
            }
        }
    }

    @Nullable
    public Multiaddr getSiteLocalAddress() throws Exception {
        return Multiaddr.getSiteLocalAddress(ipfs.self(), ipfs.getPort());
    }

    public List<Multiaddr> listenAddresses() throws Exception {
        return new ArrayList<>(ipfs.getIdentity().getMultiaddrs());
    }

    @NonNull
    public Session getSession() {
        return session;
    }


    public long createDirectory(long parent, @NonNull String name) throws Exception {
        lock.lock();
        try {
            long idx = createDocument(parent, MimeTypeService.DIR_MIME_TYPE,
                    ipfs.createEmptyDirectory(session), null, name, 0L, true);
            finishDocument(idx);
            return idx;
        } finally {
            lock.unlock();
        }
    }

    public void createTextFile(long parent, @NonNull String text) {
        lock.lock();
        try {
            Cid cid = ipfs.storeText(session, text);
            String timeStamp = DateFormat.getDateTimeInstance().
                    format(new Date()).
                    replace(":", "").
                    replace(".", "_").
                    replace("/", "_").
                    replace(" ", "_");

            String name = "TXT_" + timeStamp + ".txt";

            long idx = createDocument(parent, MimeTypeService.PLAIN_MIME_TYPE, cid,
                    null, name, text.length(), true);

            finishDocument(idx);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    public void publishPage(@NonNull Session session, @NonNull Cancellable cancellable)
            throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());

        Page page = getHomePage();
        if (page != null) {

            long seq = page.getSequence();
            Cid cid = page.getCid();
            Objects.requireNonNull(cid);


            executor.execute(() -> {
                try {
                    Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();

                    ipfs.publishName(session, seq, cid, providers::add, new TimeoutCancellable(
                            () -> cancellable.isCancelled() || providers.size() > 25, 120));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            });

            List<Cid> files = getFilesContents(session, cancellable, cid);
            for (Cid file : files) {
                executor.execute(() -> {
                    try {
                        Set<Multiaddr> providers = ConcurrentHashMap.newKeySet();
                        ipfs.provide(session, file, providers::add, new TimeoutCancellable(
                                () -> cancellable.isCancelled() || providers.size() > 50
                                , 30));
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });
            }
        }

        executor.shutdown();

        boolean result = executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        if (!result) {
            executor.shutdownNow();
        }

    }

    private List<Cid> getFilesContents(@NonNull Session session, @NonNull Cancellable cancellable,
                                       @NonNull Cid content) throws Exception {
        List<Cid> contents = new ArrayList<>();
        contents.add(content);
        List<Link> links = ipfs.links(session, content, true, cancellable);
        for (Link linkInfo : links) {
            if (linkInfo.isDirectory()) {
                contents.addAll(getFilesContents(session, cancellable, linkInfo.getCid()));
            }
        }
        return contents;
    }

    public int numUris() {
        return uris.size();
    }

    public void detachUri(@NonNull Uri uri) {
        uris.remove(uri);

    }

    public void attachUri(@NonNull Uri uri) {
        uris.add(uri);

    }

    public String getHost() {
        return host;
    }

    @NonNull
    public Uri getHomePageUri() {
        return Uri.parse(Content.IPNS + "://" + getHost());
    }

    @Nullable
    private Cid getLocalName() {
        return pages.getPageContent(ipfs.self().toBase58());
    }

    public void deleteDocuments(long... idxs) {
        lock.lock();
        try {
            for (long idx : idxs) {
                List<Proxy> children = files.getChildren(idx);
                for (Proxy proxy : children) {
                    deleteDocuments(proxy.getIdx());
                }

                try {
                    removeFromParentDocument(idx);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                files.setFilesDeleting(idx);

                try {
                    updateParentDocumentSize(idx);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }

    }

    private String getUniqueName(@NonNull String name, long parent) {
        return getName(name, parent, 0);
    }

    public void cleanup() {
        lock.lock();
        try {
            for (Proxy proxy : files.getDeletedFiles()) {
                try {
                    Cid cid = proxy.getCid();
                    files.removeFile(proxy);
                    if (cid != null) {
                        if (!files.isReferenced(cid)) {
                            ipfs.removeBlocks(session, cid);
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        } finally {
            lock.unlock();
        }
    }

    private String getName(@NonNull String name, long parent, int index) {
        String searchName = name;
        if (index > 0) {
            try {
                String base = getNameWithoutExtension(name);
                String extension = getFileExtension(name);
                if (extension.isEmpty()) {
                    searchName = searchName.concat(" (" + index + ")");
                } else {
                    String end = " (" + index + ")";
                    if (base.endsWith(end)) {
                        String realBase = base.substring(0, base.length() - end.length());
                        searchName = realBase.concat(" (" + index + ")").concat(".").concat(extension);
                    } else {
                        searchName = base.concat(" (" + index + ")").concat(".").concat(extension);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
                searchName = searchName.concat(" (" + index + ")"); // just backup
            }
        }
        List<Proxy> names = files.getFilesByNameAndParent(
                searchName, parent);
        if (!names.isEmpty()) {
            return getName(name, parent, ++index);
        }
        return searchName;
    }

    public void finishDocument(long idx) throws Exception {
        lock.lock();
        try {
            updateParentDocument(idx, "");
            updateParentDocumentSize(idx);
        } finally {
            lock.unlock();
        }
    }

    private void updateParentDocumentSize(long idx) {
        long parent = files.getParentFile(idx);
        if (parent > 0) {
            long parentSize = files.getChildrenSummarySize(parent);
            files.setFileSize(parent, parentSize);
            updateParentDocumentSize(parent);
        }
    }

    private void updateParentDocument(long childIdx, @NonNull String oldChildName) throws Exception {

        Proxy child = files.getFileByIdx(childIdx);
        Objects.requireNonNull(child);

        long parent = child.getParent();

        Cid cid = child.getCid();
        Objects.requireNonNull(cid);

        if (parent > 0) {
            // child is located in a directory
            Cid dirCid = Objects.requireNonNull(files.getFileContent(parent));
            Objects.requireNonNull(dirCid);

            if (!oldChildName.isEmpty()) {
                dirCid = ipfs.removeFromDirectory(session, dirCid, oldChildName);
            }

            dirCid = ipfs.addLinkToDirectory(session, dirCid,
                    Link.create(cid, child.getName(), child.getSize(), Link.Unknown));
            Objects.requireNonNull(dirCid);
            files.setFileContent(parent, dirCid);
            files.setFileLastModified(parent, System.currentTimeMillis());
            updateParentDocument(parent, "");
        } else {
            // child is top level file (normally nothing to do, but we will update the page content)
            Cid dirCid = pages.getPageContent(ipfs.self().toBase58());
            Objects.requireNonNull(dirCid);
            if (!oldChildName.isEmpty()) {
                dirCid = ipfs.removeFromDirectory(session, dirCid, oldChildName);
            }
            Objects.requireNonNull(dirCid);
            Cid newDir = ipfs.addLinkToDirectory(session, dirCid,
                    Link.create(cid, child.getName(), child.getSize(), Link.Unknown));
            Objects.requireNonNull(newDir);
            pages.setPageContent(ipfs.self().toBase58(), newDir);
            pages.incrementPageSequence(ipfs.self().toBase58());
        }
    }

    private void removeFromParentDocument(long idx) throws Exception {

        Proxy child = files.getFileByIdx(idx);
        if (child != null) {
            String name = child.getName();
            long parent = child.getParent();
            if (parent > 0) {
                Cid dirCid = Objects.requireNonNull(files.getFileContent(parent));
                Objects.requireNonNull(dirCid);
                Cid newDir = ipfs.removeFromDirectory(session, dirCid, name);
                files.setFileContent(parent, newDir);
                files.setFileLastModified(parent, System.currentTimeMillis());
                updateParentDocument(parent, "");
            } else {
                Cid dirCid = pages.getPageContent(ipfs.self().toBase58());
                Objects.requireNonNull(dirCid);
                Cid newDir = ipfs.removeFromDirectory(session, dirCid, name);
                pages.setPageContent(ipfs.self().toBase58(), newDir);
                pages.incrementPageSequence(ipfs.self().toBase58());
            }
        }
    }

    private String checkMimeType(@Nullable String mimeType, @NonNull String name) {
        boolean evalDisplayName = false;
        if (mimeType == null) {
            evalDisplayName = true;
        } else {
            if (mimeType.isEmpty()) {
                evalDisplayName = true;
            } else {
                if (Objects.equals(mimeType, MimeTypeService.OCTET_MIME_TYPE)) {
                    evalDisplayName = true;
                }
            }
        }
        if (evalDisplayName) {
            mimeType = MimeTypeService.getMimeType(name);
        }
        return mimeType;
    }

    public long createDocument(long parent, @Nullable String type, @Nullable Cid cid,
                               @Nullable Uri uri, String displayName, long size,
                               boolean seeding) {
        String mimeType = checkMimeType(type, displayName);
        Proxy proxy = files.createFile(parent);
        if (Objects.equals(mimeType, MimeTypeService.DIR_MIME_TYPE)) {
            proxy.setMimeType(MimeTypeService.DIR_MIME_TYPE);
        } else {
            proxy.setMimeType(mimeType);
        }
        proxy.setCid(cid);
        proxy.setName(getUniqueName(displayName, parent));
        proxy.setSize(size);
        proxy.setSeeding(seeding);
        proxy.setLeaching(false);
        if (uri != null) {
            proxy.setUri(uri.toString());
        }
        return files.storeFile(proxy);
    }

    public void renameDocument(long idx, String newName) throws Exception {
        lock.lock();
        try {
            String oldName = files.getFileName(idx);
            if (!Objects.equals(oldName, newName)) {
                files.setFileName(idx, newName);
                updateParentDocument(idx, oldName);
            }
        } finally {
            lock.unlock();
        }
    }

    public void initPinsPage() {
        lock.lock();

        try {
            Page page = getHomePage();
            if (page == null) {
                page = pages.createPage(ipfs.self().toBase58());
                Cid dir = ipfs.createEmptyDirectory(session);
                Objects.requireNonNull(dir);
                page.setCid(dir);
                page.setSequence(MIN_SEQUENCE);
                pages.storePage(page);
            }
            // just for backup, in case something happen before
            page = getHomePage();
            Objects.requireNonNull(page);

            long sequence = Math.max(page.getSequence(), MIN_SEQUENCE);

            List<Proxy> pins = files.getPins();

            List<Link> fileLinks = new ArrayList<>();
            boolean isEmpty = pins.isEmpty();
            if (!isEmpty) {
                for (Proxy pin : pins) {
                    Cid link = pin.getCid();
                    Objects.requireNonNull(link);
                    fileLinks.add(Link.create(link, pin.getName(), pin.getSize(), Link.Unknown));
                }
            }
            Cid dir = ipfs.createDirectory(session, fileLinks);
            Objects.requireNonNull(dir);
            long seq = ++sequence;
            page.setCid(dir);
            page.setSequence(seq);
            pages.storePage(page);

            Uri pinsPageUri = getHomePageUri();
            if (books.getBookmark(pinsPageUri.toString()) == null) {
                Bookmark bookmark = books.createBookmark(
                        pinsPageUri.toString(), homepage);
                books.storeBookmark(bookmark);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    private String getMimeType(@NonNull Session session, @NonNull Context context, @NonNull Cid cid,
                               @NonNull Cancellable cancellable) throws Exception {

        if (ipfs.isDir(session, cid, cancellable)) {
            return MimeTypeService.DIR_MIME_TYPE;
        }
        return getContentMimeType(session, context, cid, cancellable);
    }

    @NonNull
    private String getContentMimeType(@NonNull Session session, @NonNull Context context,
                                      @NonNull Cid cid, @NonNull Cancellable cancellable)
            throws Exception {


        String mimeType = MimeTypeService.OCTET_MIME_TYPE;

        try (InputStream in = ipfs.getInputStream(session, cid, cancellable)) {
            ContentInfo info = ContentInfoUtil.getInstance(context).findMatch(in);

            if (info != null) {
                mimeType = info.getMimeType();
            }

        }
        return mimeType;
    }


    @NonNull
    public Uri getIpnsPath(@NonNull Proxy proxy, boolean localLink) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Content.IPNS)
                .authority(getHost());
        List<Proxy> ancestors = files.getAncestors(proxy.getIdx());
        for (Proxy ancestor : ancestors) {
            builder.appendPath(ancestor.getName());
        }
        if (localLink) {
            builder.appendQueryParameter("download", "0");
        }
        return builder.build();
    }

    @NonNull
    public Uri getPath(@NonNull Proxy proxy) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Content.IPFS).authority(Objects.requireNonNull(proxy.getCid()).String());
        return builder.build();
    }

    public WebResourceResponse createRedirectMessage(@NonNull Uri uri) {
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                new ByteArrayInputStream(("<!DOCTYPE HTML>\n" +
                        "<html lang=\"en-US\">\n" +
                        "    <head>\n" + MimeTypeService.META +
                        "        <meta http-equiv=\"refresh\" content=\"0; url=" + uri + "\">\n" +
                        "        <title>Page Redirection</title>\n" +
                        "    </head>\n" + MimeTypeService.STYLE +
                        "    <body>\n" +
                        "        Automatically redirected to the <a style=\"word-wrap: break-word;\" href='" + uri + "'>" + uri + "</a> location\n" +
                        "</html>").getBytes()));
    }

    public WebResourceResponse createEmptyResource() {
        return new WebResourceResponse(MimeTypeService.PLAIN_MIME_TYPE, Content.UTF8,
                new ByteArrayInputStream("".getBytes()));
    }

    public WebResourceResponse createErrorMessage(@NonNull Throwable exception) {
        String message = generateErrorHtml(exception);
        return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                new ByteArrayInputStream(message.getBytes()));
    }

    public String generateErrorHtml(@NonNull Throwable throwable) {

        return "<html>" + "<head>" + MimeTypeService.META +
                "<title>" + "Error" + "</title>" +
                "</head>\n" + MimeTypeService.STYLE +
                "<body><div <div>" + throwable.getMessage() + "</div></body></html>";
    }

    public String generateDirectoryHtml(@NonNull Uri uri, @NonNull List<String> paths, @Nullable List<Link> links) {
        String title = uri.getHost();

        if (Objects.equals(uri, getHomePageUri())) {
            title = homepage;
        }


        StringBuilder answer = new StringBuilder("<html>" + "<head>" + MimeTypeService.META + "<title>" + title + "</title>");

        answer.append("</head>");
        answer.append(MimeTypeService.STYLE);
        answer.append("<body>");

        answer.append("<div style=\"word-break:break-word; padding: 15px; background-color: #333333; color: white;\">Index of ").append(uri).append("</div>");


        if (links != null) {
            if (!links.isEmpty()) {
                answer.append("<form><table  width=\"100%\" style=\"border-spacing: 4px;\">");
                for (Link link : links) {

                    Uri.Builder builder = new Uri.Builder();
                    builder.scheme(uri.getScheme()).authority(uri.getAuthority());
                    for (String path : paths) {
                        builder.appendPath(path);
                    }
                    builder.appendPath(link.getName());
                    builder.appendQueryParameter("download", "0");
                    Uri linkUri = builder.build();
                    answer.append("<tr>");

                    answer.append("<td>");
                    answer.append(MimeTypeService.getSvgResource(link.getName(), darkMode.get()));
                    answer.append("</td>");

                    answer.append("<td width=\"100%\" style=\"word-break:break-word\">");
                    answer.append("<a href=\"");
                    answer.append(linkUri.toString());
                    answer.append("\">");
                    answer.append(link.getName());
                    answer.append("</a>");
                    answer.append("</td>");

                    answer.append("<td>");
                    answer.append(getFileSize(link.getSize()));
                    answer.append("</td>");

                    answer.append("<td align=\"center\">");
                    String text = "<button style=\"float:none!important;display:inline;\" name=\"download\" value=\"1\" formenctype=\"text/plain\" formmethod=\"get\" type=\"submit\" formaction=\"" +
                            linkUri + "\">" + MimeTypeService.getSvgDownload() + "</button>";
                    answer.append(text);
                    answer.append("</td>");
                    answer.append("</tr>");
                }
                answer.append("</table></form>");
            }

        }
        answer.append("</body></html>");


        return answer.toString();
    }

    private String getFileSize(long size) {

        String fileSize;

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize.concat(" B");
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize.concat(" KB");
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize.concat(" MB");
        }
    }

    @NonNull
    public Cid resolveName(@NonNull Session session, @NonNull Uri uri, @NonNull PeerId peerId,
                           @NonNull Cancellable cancellable) throws Exception {

        if (Objects.equals(ipfs.self(), peerId)) {
            Cid local = getLocalName();
            if (local != null) {
                return local;
            }
        }

        Cid resolved = resolves.get(peerId);
        if (resolved != null) {
            return resolved;
        }

        long sequence = 0L;
        Cid cid = null;

        Page page = pages.getPage(peerId.toBase58());
        if (page != null) {
            sequence = page.getSequence();
            cid = page.getCid();
        } else {
            page = pages.createPage(peerId.toBase58());
            pages.storePage(page);
        }


        IpnsEntity ipnsEntity = ipfs.resolveName(session, peerId, sequence,
                new TimeoutCancellable(cancellable, 30));
        if (ipnsEntity == null) {

            if (cid != null) {
                resolves.put(peerId, cid);
                return cid;
            }

            throw new ResolveNameException(uri.toString());
        }

        Cid resolvedCid = ipfs.decodeIpnsData(ipnsEntity);
        addResolves(peerId, resolvedCid);

        pages.setPageContent(peerId.toBase58(), resolvedCid);
        pages.setPageSequence(peerId.toBase58(), ipnsEntity.getSequence());
        return resolvedCid;
    }

    public void addResolves(PeerId peerId, Cid hash) {
        resolves.put(peerId, hash);
    }

    @NonNull
    public WebResourceResponse getResponse(@NonNull Session session,
                                           @NonNull Context context,
                                           @NonNull Uri uri,
                                           @NonNull Cid root,
                                           @NonNull List<String> paths,
                                           @NonNull Cancellable cancellable) throws Exception {

        if (paths.isEmpty()) {
            if (ipfs.isDir(session, root, cancellable)) {
                List<Link> links = ipfs.links(session, root, false, cancellable);
                String answer = generateDirectoryHtml(uri, paths, links);
                return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                        new ByteArrayInputStream(answer.getBytes()));
            } else {
                String mimeType = getContentMimeType(session, context, root, cancellable);
                return getContentResponse(session, root, mimeType, cancellable);
            }


        } else {
            Cid cid = ipfs.resolveCid(session, root, paths, cancellable);
            if (ipfs.isDir(session, cid, cancellable)) {
                List<Link> links = ipfs.links(session, cid, false, cancellable);
                String answer = generateDirectoryHtml(uri, paths, links);
                return new WebResourceResponse(MimeTypeService.HTML_MIME_TYPE, Content.UTF8,
                        new ByteArrayInputStream(answer.getBytes()));

            } else {
                String mimeType = getMimeType(session, context, uri, cid, cancellable);
                return getContentResponse(session, cid, mimeType, cancellable);
            }
        }
    }

    @NonNull
    private WebResourceResponse getContentResponse(@NonNull Session session,
                                                   @NonNull Cid cid,
                                                   @NonNull String mimeType,
                                                   @NonNull Cancellable cancellable)
            throws Exception {

        try (InputStream in = ipfs.getInputStream(session, cid, cancellable)) {

            Map<String, String> responseHeaders = new HashMap<>();
            return new WebResourceResponse(mimeType, Content.UTF8, 200,
                    "OK", responseHeaders, new BufferedInputStream(in));
        }


    }

    @NonNull
    public Cid getContent(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {

        String host = uri.getHost();
        Objects.requireNonNull(host);

        Cid root = getRoot(session, uri, cancellable);

        List<String> paths = uri.getPathSegments();
        if (paths.isEmpty()) {
            return root;
        }

        return Objects.requireNonNull(ipfs.resolveCid(session, root, paths, cancellable));
    }

    @NonNull
    public String getMimeType(@NonNull Session session, @NonNull Context context,
                              @NonNull Uri uri,
                              @NonNull Cid cid,
                              @NonNull Cancellable cancellable) throws Exception {

        List<String> paths = uri.getPathSegments();
        if (!paths.isEmpty()) {
            String name = paths.get(paths.size() - 1);
            String mimeType = MimeTypeService.getMimeType(name);
            if (!mimeType.equals(MimeTypeService.OCTET_MIME_TYPE)) {
                return mimeType;
            } else {
                return getMimeType(session, context, cid, cancellable);
            }
        } else {
            return getMimeType(session, context, cid, cancellable);
        }

    }

    @NonNull
    public Uri redirectUri(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {


        if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                Objects.equals(uri.getScheme(), Content.IPFS)) {
            List<String> paths = uri.getPathSegments();
            Cid root = getRoot(session, uri, cancellable);
            return redirect(session, uri, root, paths, cancellable);
        }
        return uri;
    }

    @NonNull
    private Uri redirect(@NonNull Session session, @NonNull Uri uri, @NonNull Cid root,
                         @NonNull List<String> paths, @NonNull Cancellable cancellable)
            throws Exception {


        Cid cid = ipfs.resolveCid(session, root, paths, cancellable);


        if (ipfs.isDir(session, cid, cancellable)) {
            boolean exists = ipfs.hasLink(session, cid, IPFS.INDEX_HTML, cancellable);

            if (exists) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(uri.getScheme())
                        .authority(uri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                builder.appendPath(IPFS.INDEX_HTML);
                return builder.build();
            }
        }


        return uri;
    }


    @NonNull
    private Cid resolveDnsLink(@NonNull Session session, @NonNull Uri uri, @NonNull String link,
                               @NonNull Cancellable cancellable)
            throws Exception {

        List<String> paths = uri.getPathSegments();
        if (link.startsWith(IPFS.IPFS_PATH)) {
            return Cid.decode(link.replaceFirst(IPFS.IPFS_PATH, ""));
        } else if (link.startsWith(IPFS.IPNS_PATH)) {
            String pid = link.replaceFirst(IPFS.IPNS_PATH, "");
            try {
                PeerId peerId = ipfs.decodePeerId(pid);
                // is is assume like /ipns/<dns_link> = > therefore <dns_link> is url
                return resolveName(session, uri, peerId, cancellable);

            } catch (Throwable ignore) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(Content.IPNS)
                        .authority(pid);
                for (String path : paths) {
                    builder.appendPath(path);
                }
                return resolveUri(session, builder.build(), cancellable);
            }
        } else {
            // is is assume that links is  <dns_link> is url

            Uri dnsUri = Uri.parse(link);
            if (dnsUri != null) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme(Content.IPNS)
                        .authority(dnsUri.getAuthority());
                for (String path : paths) {
                    builder.appendPath(path);
                }
                return resolveUri(session, builder.build(), cancellable);
            }
        }
        throw new ResolveNameException(uri.toString());
    }

    @NonNull
    private Cid resolveHost(@NonNull Session session, @NonNull Uri uri,
                            @NonNull String host, @NonNull Cancellable cancellable)
            throws Exception {

        String link = ipfs.resolveDnsLink(host);
        if (link.isEmpty()) {
            // could not resolved, maybe no NETWORK
            String dnsLink = books.getDnsLink(uri.toString());
            if (dnsLink == null) {
                throw new DOCS.ResolveNameException(uri.toString());
            } else {
                return resolveDnsLink(session, uri, dnsLink, cancellable);
            }
        } else {
            if (link.startsWith(IPFS.IPFS_PATH)) {
                // try to store value
                books.storeDnsLink(uri.toString(), link);
                return resolveDnsLink(session, uri, link, cancellable);
            } else if (link.startsWith(IPFS.IPNS_PATH)) {
                return resolveHost(session, uri,
                        link.replaceFirst(IPFS.IPNS_PATH, ""),
                        cancellable);

            } else {
                throw new DOCS.ResolveNameException(uri.toString());
            }
        }
    }

    @NonNull
    private Cid resolveUri(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {
        String host = uri.getHost();
        Objects.requireNonNull(host);

        if (!Objects.equals(uri.getScheme(), Content.IPNS)) {
            throw new RuntimeException();
        }

        try {
            PeerId peerId = ipfs.decodePeerId(host);
            return resolveName(session, uri, peerId, cancellable);
        } catch (Throwable ignore) {
            return resolveHost(session, uri, host, cancellable);
        }
    }

    @NonNull
    public Cid getRoot(@NonNull Session session, @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {
        String host = uri.getHost();
        Objects.requireNonNull(host);

        Cid root;
        if (Objects.equals(uri.getScheme(), Content.IPNS)) {
            root = resolveUri(session, uri, cancellable);
        } else {
            try {
                root = Cid.decode(host);
            } catch (Throwable throwable) {
                throw new InvalidNameException(uri.toString());
            }
        }

        if (!root.isSupported()) {
            throw new ResolveNameException("Encoding type '" + root.getPrefix().getType().name() +
                    "' is not supported." +
                    "Currently only 'sha2_256' encoded CID's are supported.");
        }
        return root;
    }

    @Nullable
    public Page getHomePage() {
        return pages.getPage(ipfs.self().toBase58());
    }


    @NonNull
    public WebResourceResponse getResponse(@NonNull Session session, @NonNull Context context,
                                           @NonNull Uri uri, @NonNull Cancellable cancellable)
            throws Exception {

        List<String> paths = uri.getPathSegments();
        Cid root = getRoot(session, uri, cancellable);

        return getResponse(session, context, uri, root, paths, cancellable);
    }


    @NonNull
    public Uri redirectHttp(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.HTTP)) {
                String host = uri.getHost();
                Objects.requireNonNull(host);
                if (Objects.equals(host, "localhost") || Objects.equals(host, "127.0.0.1")) {
                    List<String> paths = uri.getPathSegments();
                    if (paths.size() >= 2) {
                        String protocol = paths.get(0);
                        String authority = paths.get(1);
                        List<String> subPaths = new ArrayList<>(paths);
                        subPaths.remove(protocol);
                        subPaths.remove(authority);
                        Cid.decode(authority); // just make sure is valid Cid
                        if (Objects.equals(protocol, Content.IPFS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPFS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        } else if (Objects.equals(protocol, Content.IPNS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPNS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        }
                    }

                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return uri;
    }


    @NonNull
    public Uri redirectHttps(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.HTTPS)) {


                List<String> paths = uri.getPathSegments();
                if (paths.size() >= 2) {
                    String protocol = paths.get(0);
                    if (Objects.equals(protocol, Content.IPFS) ||
                            Objects.equals(protocol, Content.IPNS)) {
                        String authority = paths.get(1);
                        List<String> subPaths = new ArrayList<>(paths);
                        subPaths.remove(protocol);
                        subPaths.remove(authority);
                        Cid.decode(authority); // just make sure is valid Cid

                        if (Objects.equals(protocol, Content.IPFS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPFS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        } else if (Objects.equals(protocol, Content.IPNS)) {
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme(Content.IPNS)
                                    .authority(authority);

                            for (String path : subPaths) {
                                builder.appendPath(path);
                            }
                            return builder.build();
                        }

                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return uri;
    }


    @Nullable
    public String getHost(@NonNull Uri uri) {
        try {
            if (Objects.equals(uri.getScheme(), Content.IPNS)) {
                return uri.getHost();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return null;
    }

    public void cleanupResolver(@NonNull Uri uri) {
        try {
            String host = getHost(uri);
            if (host != null) {
                PeerId peerId = ipfs.decodePeerId(host);
                resolves.remove(peerId);
            }
        } catch (Throwable ignore) {
            // ignore common failure
        }
    }

    public static class ResolveNameException extends Exception {


        public ResolveNameException(@NonNull String name) {
            super("Resolve name failed for " + name);
        }

    }

    public static class InvalidNameException extends Exception {


        public InvalidNameException(@NonNull String name) {
            super("Invalid name detected for " + name);
        }

    }
}
