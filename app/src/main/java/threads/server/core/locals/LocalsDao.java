package threads.server.core.locals;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LocalsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLocal(Local local);

    @Query("SELECT * FROM Local")
    List<Local> getLocals();
}
