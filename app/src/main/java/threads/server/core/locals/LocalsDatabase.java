package threads.server.core.locals;

import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {Local.class}, version = 1, exportSchema = false)
public abstract class LocalsDatabase extends RoomDatabase {

    public abstract LocalsDao localsDao();

}
