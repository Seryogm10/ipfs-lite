package threads.server.core.locals;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import java.util.List;

import threads.lite.cid.Multiaddr;


public class LOCALS {
    private static volatile LOCALS INSTANCE = null;
    private final LocalsDatabase localsDatabase;

    private LOCALS(LocalsDatabase localsDatabase) {
        this.localsDatabase = localsDatabase;
    }

    @NonNull
    private static LOCALS createLocals(@NonNull LocalsDatabase localsDatabase) {
        return new LOCALS(localsDatabase);
    }

    public static LOCALS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (LOCALS.class) {
                if (INSTANCE == null) {
                    LocalsDatabase localsDatabase =
                            Room.inMemoryDatabaseBuilder(context, LocalsDatabase.class).
                                    allowMainThreadQueries().build();

                    INSTANCE = LOCALS.createLocals(localsDatabase);
                }
            }
        }
        return INSTANCE;
    }


    @NonNull
    public List<Local> getLocals() {
        return localsDatabase.localsDao().getLocals();
    }

    public Local insertLocal(@NonNull Multiaddr multiaddr) {
        Local local = new Local(multiaddr.toString());
        localsDatabase.localsDao().insertLocal(local);
        return local;
    }

}
