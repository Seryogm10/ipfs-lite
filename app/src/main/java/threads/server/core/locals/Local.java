package threads.server.core.locals;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import threads.lite.cid.Multiaddr;

@Entity
public class Local {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "address")
    private final String address;

    public Local(@NonNull String address) {
        this.address = address;
    }

    @NonNull
    String getAddress() {
        return address;
    }

    @NonNull
    public Multiaddr getMultiaddr() throws Exception {
        return Multiaddr.create(getAddress());
    }
}
