package threads.server.core.events;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class EventViewModel extends AndroidViewModel {

    private final EventsDatabase eventsDatabase;

    public EventViewModel(@NonNull Application application) {
        super(application);
        eventsDatabase = EVENTS.getInstance(
                application.getApplicationContext()).getEventsDatabase();
    }

    public LiveData<Event> getError() {
        return eventsDatabase.eventDao().getEvent(EVENTS.ERROR);
    }

    public LiveData<Event> getFatal() {
        return eventsDatabase.eventDao().getEvent(EVENTS.FATAL);
    }

    public LiveData<Event> getDelete() {
        return eventsDatabase.eventDao().getEvent(EVENTS.DELETE);
    }

    public LiveData<Event> getPermission() {
        return eventsDatabase.eventDao().getEvent(EVENTS.PERMISSION);
    }

    public LiveData<Event> getWarning() {
        return eventsDatabase.eventDao().getEvent(EVENTS.WARNING);
    }

    public LiveData<Event> getInfo() {
        return eventsDatabase.eventDao().getEvent(EVENTS.INFO);
    }

    public LiveData<Event> getToolbar() {
        return eventsDatabase.eventDao().getEvent(EVENTS.TOOLBAR);
    }

    public void removeEvent(@NonNull final Event event) {
        eventsDatabase.eventDao().deleteEvent(event);
    }

}