package threads.server.core.files;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@androidx.room.Database(entities = {Proxy.class}, version = 2, exportSchema = false)
@TypeConverters({CidConverter.class})
public abstract class FilesDatabase extends RoomDatabase {

    public abstract ProxyDao proxyDao();

}
