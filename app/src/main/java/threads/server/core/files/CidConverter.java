package threads.server.core.files;

import androidx.annotation.Nullable;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import threads.lite.cid.Cid;

@TypeConverters
public class CidConverter {

    @Nullable
    @TypeConverter
    public static Cid fromArray(byte[] data) {
        return Cid.fromArray(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        return Cid.toArray(cid);
    }
}
