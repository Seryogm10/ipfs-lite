package threads.server;


import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ListPopupWindow;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.divider.MaterialDivider;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.books.BOOKS;
import threads.server.core.books.Bookmark;
import threads.server.core.events.EVENTS;
import threads.server.core.events.EventViewModel;
import threads.server.core.files.FILES;
import threads.server.core.files.SortOrder;
import threads.server.fragments.AccessFragment;
import threads.server.fragments.BookmarksDialogFragment;
import threads.server.fragments.BrowserFragment;
import threads.server.fragments.ContentDialogFragment;
import threads.server.fragments.FilesFragment;
import threads.server.fragments.NewFolderDialogFragment;
import threads.server.fragments.SettingsDialogFragment;
import threads.server.fragments.TextDialogFragment;
import threads.server.provider.FileProvider;
import threads.server.services.DaemonService;
import threads.server.services.LiteService;
import threads.server.services.MimeTypeService;
import threads.server.services.QRCodeService;
import threads.server.utils.CodecDecider;
import threads.server.utils.PermissionAction;
import threads.server.utils.SearchesAdapter;
import threads.server.utils.SelectionViewModel;
import threads.server.work.BackupWorker;
import threads.server.work.DownloadContentWorker;
import threads.server.work.DownloadFileWorker;
import threads.server.work.UploadFilesWorker;
import threads.server.work.UploadFolderWorker;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private final ActivityResultLauncher<Intent> mFileForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    try {
                        Objects.requireNonNull(data);

                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);
                        if (!FileProvider.hasWritePermission(getApplicationContext(), uri)) {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.file_has_no_write_permission));
                            return;
                        }
                        LiteService.FileInfo fileInfo = LiteService.getFileInfo(getApplicationContext());
                        Objects.requireNonNull(fileInfo);
                        DownloadFileWorker.download(getApplicationContext(), uri, fileInfo.getUri(),
                                fileInfo.getFilename(), fileInfo.getMimeType(), fileInfo.getSize());


                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            });
    private final ActivityResultLauncher<Intent> mContentForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    try {
                        Objects.requireNonNull(data);
                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);
                        if (!FileProvider.hasWritePermission(getApplicationContext(), uri)) {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.file_has_no_write_permission));
                            return;
                        }
                        Uri contentUri = LiteService.getContentUri(getApplicationContext());
                        Objects.requireNonNull(contentUri);
                        DownloadContentWorker.download(getApplicationContext(), uri, contentUri);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            });

    private final ActivityResultLauncher<Intent> mFolderImportForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                long parent = getThread(getApplicationContext());
                                if (data.getClipData() != null) {
                                    ClipData mClipData = data.getClipData();
                                    int items = mClipData.getItemCount();
                                    if (items > 0) {
                                        for (int i = 0; i < items; i++) {
                                            ClipData.Item item = mClipData.getItemAt(i);
                                            Uri uri = item.getUri();

                                            if (!FileProvider.hasReadPermission(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_has_no_read_permission));
                                                return;
                                            }

                                            if (FileProvider.isPartial(getApplicationContext(), uri)) {
                                                EVENTS.getInstance(getApplicationContext()).error(
                                                        getString(R.string.file_not_valid));
                                                return;
                                            }

                                            UploadFolderWorker.load(getApplicationContext(), parent, uri);
                                        }
                                    }
                                } else {
                                    Uri uri = data.getData();
                                    if (uri != null) {
                                        if (!FileProvider.hasReadPermission(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_has_no_read_permission));
                                            return;
                                        }

                                        if (FileProvider.isPartial(getApplicationContext(), uri)) {
                                            EVENTS.getInstance(getApplicationContext()).error(
                                                    getString(R.string.file_not_valid));
                                            return;
                                        }

                                        UploadFolderWorker.load(getApplicationContext(), parent, uri);
                                    }
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private final ActivityResultLauncher<Intent> mBackupForResult =
            registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                    result -> {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            Intent data = result.getData();
                            try {
                                Objects.requireNonNull(data);
                                Uri uri = data.getData();
                                Objects.requireNonNull(uri);


                                if (!FileProvider.hasWritePermission(getApplicationContext(), uri)) {
                                    EVENTS.getInstance(getApplicationContext()).error(
                                            getString(R.string.file_has_no_write_permission));
                                    return;
                                }
                                BackupWorker.backup(getApplicationContext(), uri);

                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                        }
                    });
    private long mLastClickTime = 0;
    private CoordinatorLayout mDrawerLayout;
    private SelectionViewModel mSelectionViewModel;
    private final ActivityResultLauncher<ScanOptions>
            mScanRequestForResult = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() != null) {
                    try {
                        Uri uri = Uri.parse(result.getContents());
                        if (uri != null) {
                            String scheme = uri.getScheme();
                            if (Objects.equals(scheme, Content.IPNS) ||
                                    Objects.equals(scheme, Content.IPFS) ||
                                    Objects.equals(scheme, Content.HTTP) ||
                                    Objects.equals(scheme, Content.HTTPS)) {
                                mSelectionViewModel.setUri(uri);
                            } else {
                                EVENTS.getInstance(getApplicationContext()).error(
                                        getString(R.string.codec_not_supported));
                            }
                        } else {
                            EVENTS.getInstance(getApplicationContext()).error(
                                    getString(R.string.codec_not_supported));
                        }
                    } catch (Throwable throwable) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.codec_not_supported));
                    }
                }
            });
    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    invokeScan();
                } else {
                    EVENTS.getInstance(getApplicationContext()).permission(
                            getString(R.string.permission_camera_denied));
                }
            });
    private MaterialTextView mBrowserText;
    private ActionMode mActionMode;
    private BrowserFragment mBrowserFragment;
    private boolean hasCamera;


    private static long getThread(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                TAG, Context.MODE_PRIVATE);
        return sharedPref.getLong(Content.IDX, -1);
    }

    private static void setThread(@NonNull Context context, long idx) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(Content.IDX, idx);
        editor.apply();
    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntents(intent);
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getCurrentFragment();
        if (fragment instanceof BrowserFragment) {
            BrowserFragment browserFragment = (BrowserFragment) fragment;
            if (browserFragment.isResumed()) {
                boolean result = browserFragment.onBackPressed();
                if (result) {
                    return;
                }
            }
        }
        super.onBackPressed();
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.fragment_container);
    }

    public AccessFragment getCurrentAccessFragment() {
        return (AccessFragment) getCurrentFragment();
    }

    private void handleIntents(Intent intent) {

        final String action = intent.getAction();
        try {
            ShareCompat.IntentReader intentReader = new ShareCompat.IntentReader(this);
            if (Intent.ACTION_SEND.equals(action) ||
                    Intent.ACTION_SEND_MULTIPLE.equals(action)) {
                handleSend(intentReader);
            } else if (Intent.ACTION_VIEW.equals(action)) {
                Uri uri = intent.getData();
                if (uri != null) {
                    String scheme = uri.getScheme();
                    if (Objects.equals(scheme, Content.IPNS) ||
                            Objects.equals(scheme, Content.IPFS) ||
                            Objects.equals(scheme, Content.HTTP) ||
                            Objects.equals(scheme, Content.HTTPS)) {
                        mSelectionViewModel.setUri(uri);
                    }
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, "" + e.getLocalizedMessage());
        }
    }

    private void handleSend(ShareCompat.IntentReader intentReader) {

        try {
            Objects.requireNonNull(intentReader);
            if (intentReader.isMultipleShare()) {
                int items = intentReader.getStreamCount();

                if (items > 0) {
                    FileProvider fileProvider =
                            FileProvider.getInstance(getApplicationContext());
                    File file = fileProvider.createTempDataFile();

                    try (PrintStream out = new PrintStream(file)) {
                        for (int i = 0; i < items; i++) {
                            Uri uri = intentReader.getStream(i);
                            if (uri != null) {
                                out.println(uri);
                            }
                        }
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                    Uri uri = androidx.core.content.FileProvider.getUriForFile(
                            getApplicationContext(), BuildConfig.APPLICATION_ID, file);
                    Objects.requireNonNull(uri);
                    UploadFilesWorker.load(getApplicationContext(), 0L, uri);
                }
            } else {
                String type = intentReader.getType();
                if (Objects.equals(type, MimeTypeService.PLAIN_MIME_TYPE)) {
                    CharSequence textObject = intentReader.getText();
                    Objects.requireNonNull(textObject);
                    String text = textObject.toString();
                    if (!text.isEmpty()) {

                        Uri uri = Uri.parse(text);
                        if (uri != null) {
                            if (Objects.equals(uri.getScheme(), Content.IPFS) ||
                                    Objects.equals(uri.getScheme(), Content.IPNS) ||
                                    Objects.equals(uri.getScheme(), Content.HTTP) ||
                                    Objects.equals(uri.getScheme(), Content.HTTPS)) {
                                mSelectionViewModel.setUri(uri);
                                return;
                            }
                        }

                        CodecDecider result = CodecDecider.evaluate(text);

                        if (result.getCodex() == CodecDecider.Codec.MULTIHASH) {
                            // it is assumed a CID
                            mSelectionViewModel.setUri(Uri.parse(Content.IPFS + "://" + result.getMultihash()));

                        } else {
                            if (URLUtil.isValidUrl(text)) {
                                mSelectionViewModel.setUri(Uri.parse(text));
                            } else {
                                storeText(text);
                            }
                        }
                    }
                } else if (Objects.equals(type, MimeTypeService.HTML_MIME_TYPE)) {
                    String html = intentReader.getHtmlText();
                    Objects.requireNonNull(html);
                    if (!html.isEmpty()) {
                        storeText(html);
                    }
                } else {
                    Uri uri = intentReader.getStream();
                    Objects.requireNonNull(uri);

                    if (!FileProvider.hasReadPermission(getApplicationContext(), uri)) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.file_has_no_read_permission));
                        return;
                    }

                    if (FileProvider.isPartial(getApplicationContext(), uri)) {

                        EVENTS.getInstance(getApplicationContext()).error(
                                getString(R.string.file_not_found));

                        return;
                    }

                    LiteService.file(getApplicationContext(), 0L, uri);

                }
            }


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void storeText(@NonNull String text) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                DOCS docs = DOCS.getInstance(getApplicationContext());
                docs.createTextFile(0L, text);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }


    private ActionMode.Callback createSearchActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_searchable, menu);
                mode.setTitle("");

                MenuItem scanMenuItem = menu.findItem(R.id.action_scan);
                if (!hasCamera) {
                    scanMenuItem.setVisible(false);
                }
                MenuItem searchMenuItem = menu.findItem(R.id.action_search);
                SearchView mSearchView = (SearchView) searchMenuItem.getActionView();

                TextView textView = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_src_text);
                textView.setTextSize(16);
                textView.setMinHeight(dp48ToPixels());

                ImageView magImage = mSearchView.findViewById(
                        androidx.appcompat.R.id.search_mag_icon);
                magImage.setVisibility(View.GONE);
                magImage.setImageDrawable(null);

                mSearchView.setMinimumHeight(actionBarSize());
                mSearchView.setIconifiedByDefault(false);
                mSearchView.setIconified(false);
                mSearchView.setSubmitButtonEnabled(false);
                mSearchView.setQueryHint(getString(R.string.enter_url));
                mSearchView.setFocusable(true);
                mSearchView.requestFocus();


                ListPopupWindow mPopupWindow = new ListPopupWindow(MainActivity.this,
                        null, R.attr.listPopupWindowStyle) {

                    @Override
                    public boolean isInputMethodNotNeeded() {
                        return true;
                    }
                };
                mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
                mPopupWindow.setModal(false);
                mPopupWindow.setAnchorView(mSearchView);
                mPopupWindow.setAnimationStyle(android.R.style.Animation);


                mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {

                        try {
                            mPopupWindow.dismiss();

                            if (mActionMode != null) {
                                mActionMode.finish();
                            }
                            if (query != null && !query.isEmpty()) {
                                Uri uri = Uri.parse(query);
                                String scheme = uri.getScheme();
                                if (Objects.equals(scheme, Content.IPNS) ||
                                        Objects.equals(scheme, Content.IPFS) ||
                                        Objects.equals(scheme, Content.HTTP) ||
                                        Objects.equals(scheme, Content.HTTPS)) {
                                    mSelectionViewModel.setUri(uri);
                                } else {

                                    try {
                                        mSelectionViewModel.setUri(Uri.parse(Content.IPFS + "://" +
                                                Cid.decode(query).String()));
                                    } catch (Throwable ignore) {
                                        mSelectionViewModel.setUri(Uri.parse(
                                                Settings.getDefaultSearchEngine(query)));
                                    }
                                }
                            }
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {

                        if (!newText.isEmpty()) {
                            BOOKS books = BOOKS.getInstance(getApplicationContext());
                            List<Bookmark> bookmarks = books.getBookmarksByQuery(newText);

                            if (!bookmarks.isEmpty()) {
                                mPopupWindow.setAdapter(new SearchesAdapter(
                                        MainActivity.this, new ArrayList<>(bookmarks)) {
                                    @Override
                                    public void onClick(@NonNull Bookmark bookmark) {
                                        try {
                                            java.lang.Thread.sleep(150);
                                            mSelectionViewModel.setUri(Uri.parse(bookmark.getUri()));
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        } finally {
                                            mPopupWindow.dismiss();
                                            releaseActionMode();
                                        }
                                    }
                                });
                                mPopupWindow.show();
                                return true;
                            } else {
                                mPopupWindow.dismiss();
                            }
                        } else {
                            mPopupWindow.dismiss();
                        }

                        return false;
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.action_scan) {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return false;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();


                        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                            return false;
                        }

                        invokeScan();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    } finally {
                        mode.finish();
                    }
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                mActionMode = null;
            }
        };

    }

    private int dp48ToPixels() {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) 48 * density);
    }

    private int actionBarSize() {
        TypedValue tv = new TypedValue();
        getTheme().resolveAttribute(R.attr.actionBarSize, tv, true);
        return getResources().getDimensionPixelSize(tv.resourceId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PackageManager pm = getPackageManager();
        hasCamera = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
        mDrawerLayout = findViewById(R.id.drawer_layout);


        AppBarLayout mAppBar = findViewById(R.id.appbar);


        mAppBar.addOnOffsetChangedListener(new AppBarStateChangedListener() {
            @Override
            public void onStateChanged(State state) {
                if (state == State.EXPANDED) {
                    mBrowserFragment.enableSwipeRefresh(true);
                } else if (state == State.COLLAPSED) {
                    mBrowserFragment.enableSwipeRefresh(false);
                }
            }
        });

        MaterialToolbar materialToolbar = findViewById(R.id.toolbar);
        materialToolbar.setNavigationOnClickListener(v -> showFilesFragment());
        mBrowserFragment = new BrowserFragment();


        Button mActionBookmarks = findViewById(R.id.action_bookmarks);
        mActionBookmarks.setOnClickListener(v -> {
            try {
                if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                BookmarksDialogFragment dialogFragment = new BookmarksDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), BookmarksDialogFragment.TAG);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        Button mActionOverflow = findViewById(R.id.action_overflow);
        mActionOverflow.setOnClickListener(v -> {

            try {
                if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);


                View menuOverflow = inflater.inflate(
                        R.layout.menu_overflow, mDrawerLayout, false);

                PopupWindow dialog = new PopupWindow(
                        MainActivity.this, null, R.attr.popupMenuStyle);
                dialog.setContentView(menuOverflow);
                dialog.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                dialog.setOutsideTouchable(true);
                dialog.setFocusable(true);
                dialog.showAsDropDown(mActionOverflow, 0, -actionBarSize());


                Button actionNextPage = menuOverflow.findViewById(R.id.action_next_page);
                actionNextPage.setEnabled(mBrowserFragment.canGoForward());
                actionNextPage.setOnClickListener(v1 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        mBrowserFragment.goForward();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                MaterialButton actionBookmark = menuOverflow.findViewById(R.id.action_bookmark);

                BOOKS books = BOOKS.getInstance(getApplicationContext());
                Uri uri = mSelectionViewModel.getUri().getValue();
                boolean isBrowserFragment = (getCurrentFragment() instanceof BrowserFragment);

                if (uri != null) {
                    if (books.hasBookmark(uri.toString())) {
                        actionBookmark.setIcon(AppCompatResources.getDrawable(
                                getApplicationContext(), R.drawable.star));
                    } else {
                        actionBookmark.setIcon(AppCompatResources.getDrawable(
                                getApplicationContext(), R.drawable.star_outline));
                    }
                } else {
                    actionBookmark.setEnabled(false);
                }

                actionBookmark.setOnClickListener(v1 -> {

                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        mBrowserFragment.bookmark(getApplicationContext());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                Button actionFindPage = menuOverflow.findViewById(R.id.action_find_page);
                actionFindPage.setEnabled(true);
                actionFindPage.setOnClickListener(v12 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        getCurrentAccessFragment().findInPage();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionDownload = menuOverflow.findViewById(R.id.action_download);

                actionDownload.setEnabled(downloadActive());

                actionDownload.setOnClickListener(v13 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        Objects.requireNonNull(uri);
                        contentDownloader(uri);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionShare = menuOverflow.findViewById(R.id.action_share);
                actionShare.setEnabled(true);
                actionShare.setOnClickListener(v14 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        DOCS docs = DOCS.getInstance(getApplicationContext());
                        String link = docs.getHomePageUri().toString();
                        if (isBrowserFragment) {
                            if (uri != null) {
                                link = uri.toString();
                            }
                        }

                        ComponentName[] names = {new ComponentName(getApplicationContext(),
                                MainActivity.class)};

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_link));
                        intent.putExtra(Intent.EXTRA_TEXT, link);
                        intent.setType(MimeTypeService.PLAIN_MIME_TYPE);
                        intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                        Intent chooser = Intent.createChooser(intent, getText(R.string.share));
                        chooser.putExtra(Intent.EXTRA_EXCLUDE_COMPONENTS, names);
                        startActivity(chooser);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                Button actionReload = menuOverflow.findViewById(R.id.action_reload);

                actionReload.setEnabled(isBrowserFragment);
                actionReload.setOnClickListener(v15 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        mBrowserFragment.reload();
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionClearData = menuOverflow.findViewById(R.id.action_clear_data);

                actionClearData.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        MaterialAlertDialogBuilder alertDialog = new MaterialAlertDialogBuilder(MainActivity.this);
                        alertDialog.setTitle(getString(R.string.warning));
                        alertDialog.setMessage(getString(R.string.delete_browser_data_warning));
                        alertDialog.setPositiveButton(getString(android.R.string.ok),
                                (dialogInterface, which) -> {
                                    mBrowserFragment.clearBrowserData();
                                    dialog.dismiss();
                                });
                        alertDialog.setNeutralButton(getString(android.R.string.cancel),
                                (dialogInterface, which) -> dialog.dismiss());
                        alertDialog.show();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionInformation = menuOverflow.findViewById(R.id.action_information);
                if (mSelectionViewModel.getUri().getValue() != null) {
                    actionInformation.setVisibility(View.VISIBLE);
                } else {
                    actionInformation.setVisibility(View.GONE);
                }
                actionInformation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        Objects.requireNonNull(uri);
                        Uri uriImage = QRCodeService.getImage(getApplicationContext(), uri.toString());
                        ContentDialogFragment.newInstance(uriImage,
                                        getString(R.string.url_access), uri.toString())
                                .show(getSupportFragmentManager(), ContentDialogFragment.TAG);


                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView mActionSorting = menuOverflow.findViewById(R.id.action_sorting);
                if (!isBrowserFragment) {
                    mActionSorting.setVisibility(View.VISIBLE);
                } else {
                    mActionSorting.setVisibility(View.GONE);
                }
                mActionSorting.setOnClickListener(v22 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        SortOrder sortOrder = Settings.getSortOrder(getApplicationContext());
                        PopupMenu popup = new PopupMenu(MainActivity.this, v);
                        popup.inflate(R.menu.popup_sorting);

                        popup.getMenu().getItem(0).setChecked(sortOrder == SortOrder.NAME);
                        popup.getMenu().getItem(1).setChecked(sortOrder == SortOrder.NAME_INVERSE);
                        popup.getMenu().getItem(2).setChecked(sortOrder == SortOrder.DATE);
                        popup.getMenu().getItem(3).setChecked(sortOrder == SortOrder.DATE_INVERSE);
                        popup.getMenu().getItem(4).setChecked(sortOrder == SortOrder.SIZE);
                        popup.getMenu().getItem(5).setChecked(sortOrder == SortOrder.SIZE_INVERSE);

                        popup.setOnMenuItemClickListener(item -> {
                            try {
                                int itemId = item.getItemId();
                                if (itemId == R.id.sort_date) {

                                    Settings.setSortOrder(getApplicationContext(), SortOrder.DATE);

                                    mSelectionViewModel.setSortOrder(SortOrder.DATE);
                                    return true;
                                } else if (itemId == R.id.sort_date_inverse) {

                                    Settings.setSortOrder(getApplicationContext(), SortOrder.DATE_INVERSE);

                                    mSelectionViewModel.setSortOrder(SortOrder.DATE_INVERSE);
                                    return true;
                                } else if (itemId == R.id.sort_name) {

                                    Settings.setSortOrder(getApplicationContext(), SortOrder.NAME);

                                    mSelectionViewModel.setSortOrder(SortOrder.NAME);
                                    return true;
                                } else if (itemId == R.id.sort_name_inverse) {

                                    Settings.setSortOrder(getApplicationContext(), SortOrder.NAME_INVERSE);

                                    mSelectionViewModel.setSortOrder(SortOrder.NAME_INVERSE);
                                    return true;
                                } else if (itemId == R.id.sort_size) {

                                    Settings.setSortOrder(getApplicationContext(), SortOrder.SIZE);

                                    mSelectionViewModel.setSortOrder(SortOrder.SIZE);
                                    return true;
                                } else if (itemId == R.id.sort_size_inverse) {

                                    Settings.setSortOrder(getApplicationContext(), SortOrder.SIZE_INVERSE);

                                    mSelectionViewModel.setSortOrder(SortOrder.SIZE_INVERSE);
                                    return true;
                                }
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            }
                            return false;
                        });
                        popup.show();

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });


                TextView actionNewFolder = menuOverflow.findViewById(R.id.action_new_folder);
                if (!isBrowserFragment) {
                    actionNewFolder.setVisibility(View.VISIBLE);
                } else {
                    actionNewFolder.setVisibility(View.GONE);
                }
                actionNewFolder.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);


                        NewFolderDialogFragment.newInstance(
                                        mSelectionViewModel.getParentProxyFileValue()).
                                show(getSupportFragmentManager(), NewFolderDialogFragment.TAG);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionImportFolder = menuOverflow.findViewById(R.id.action_import_folder);
                if (!isBrowserFragment) {
                    actionImportFolder.setVisibility(View.VISIBLE);
                } else {
                    actionImportFolder.setVisibility(View.GONE);
                }
                actionImportFolder.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        setThread(getApplicationContext(),
                                mSelectionViewModel.getParentProxyFileValue());

                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                        mFolderImportForResult.launch(intent);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });


                TextView actionNewText = menuOverflow.findViewById(R.id.action_new_text);
                if (!isBrowserFragment) {
                    actionNewText.setVisibility(View.VISIBLE);
                } else {
                    actionNewText.setVisibility(View.GONE);
                }
                actionNewText.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        TextDialogFragment.newInstance(mSelectionViewModel.getParentProxyFileValue()).
                                show(getSupportFragmentManager(), TextDialogFragment.TAG);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionBackup = menuOverflow.findViewById(R.id.action_backup);
                if (!isBrowserFragment) {
                    actionBackup.setVisibility(View.VISIBLE);
                } else {
                    actionBackup.setVisibility(View.GONE);
                }
                actionBackup.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);
                        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, true);
                        mBackupForResult.launch(intent);

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                MaterialDivider divider = menuOverflow.findViewById(R.id.divider);
                if (!isBrowserFragment) {
                    divider.setVisibility(View.VISIBLE);
                } else {
                    divider.setVisibility(View.GONE);
                }

                TextView actionSettings = menuOverflow.findViewById(R.id.action_settings);
                actionSettings.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        SettingsDialogFragment dialogFragment = new SettingsDialogFragment();
                        dialogFragment.show(getSupportFragmentManager(), SettingsDialogFragment.TAG);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionMultiaddrs = menuOverflow.findViewById(R.id.action_multiaddrs);
                if (!isBrowserFragment) {
                    actionMultiaddrs.setVisibility(View.VISIBLE);
                } else {
                    actionMultiaddrs.setVisibility(View.GONE);
                }
                actionMultiaddrs.setOnClickListener(v20 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(
                                MainActivity.this);
                        builder.setTitle(R.string.multiaddr);

                        String link = "";
                        DOCS docs = DOCS.getInstance(getApplicationContext());
                        for (Multiaddr multiaddr : docs.listenAddresses()) {
                            link = link.concat("\n").concat(multiaddr.toString()).concat("\n");
                        }
                        builder.setMessage(link);

                        builder.setPositiveButton(getString(android.R.string.ok),
                                (dialogInterface, which) -> dialogInterface.cancel());
                        String finalLink = link;
                        builder.setNeutralButton(getString(android.R.string.copy),
                                (dialogInterface, which) -> {
                                    LogUtils.error(TAG, finalLink);
                                    ClipboardManager clipboardManager = (ClipboardManager)
                                            getSystemService(Context.CLIPBOARD_SERVICE);
                                    ClipData clipData = ClipData.newPlainText(
                                            getString(R.string.multiaddr), finalLink);
                                    clipboardManager.setPrimaryClip(clipData);
                                });
                        builder.show();


                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });

                TextView actionDocumentation = menuOverflow.findViewById(R.id.action_documentation);
                actionDocumentation.setOnClickListener(v19 -> {
                    try {
                        if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                            return;
                        }
                        mLastClickTime = SystemClock.elapsedRealtime();
                        tearDown(dialog);

                        mSelectionViewModel.setUri(
                                Uri.parse("https://gitlab.com/remmer.wilts/ipfs-lite"));
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                });
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        mBrowserText = findViewById(R.id.action_browser);
        mBrowserText.setLines(1);
        mBrowserText.setCompoundDrawablePadding(8);
        mBrowserText.getBackground().setAlpha(50);


        mBrowserText.setOnClickListener(view -> {
            try {
                try {
                    mActionMode = startSupportActionMode(
                            createSearchActionModeCallback());
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mSelectionViewModel = new ViewModelProvider(this).get(SelectionViewModel.class);

        mSelectionViewModel.getUri().observe(this, (uri) -> {
            if (uri != null) {
                if (getCurrentFragment() != mBrowserFragment) {
                    showFragment(mBrowserFragment);
                }
            }
        });

        mSelectionViewModel.getParentProxyFile().observe(this, (idx) -> {

            try {
                if (idx != null && idx >= 0) {
                    FilesFragment filesFragment = new FilesFragment();
                    Bundle args = new Bundle();
                    args.putLong(Content.IDX, idx);
                    filesFragment.setArguments(args);
                    showFragment(filesFragment);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mSelectionViewModel.setSortOrder(Settings.getSortOrder(getApplicationContext()));
        try {
            updateTitle(DOCS.getInstance(getApplicationContext()).getHomePageUri());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        if (savedInstanceState == null) {
            FilesFragment filesFragment = new FilesFragment();
            Bundle args = new Bundle();
            args.putLong(Content.IDX, mSelectionViewModel.getParentProxyFileValue());
            filesFragment.setArguments(args);


            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_container, filesFragment)
                    .commit();

        }

        EventViewModel eventViewModel =
                new ViewModelProvider(this).get(EventViewModel.class);


        eventViewModel.getDelete().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {

                        String data = content
                                .replace("[", "")
                                .replace("]", "")
                                .trim();

                        String[] parts = data.split(",");

                        long[] idxs = new long[parts.length];
                        for (int i = 0; i < parts.length; i++) {
                            idxs[i] = Long.parseLong(parts[i].trim());
                        }


                        String message;
                        if (idxs.length == 1) {
                            message = getString(R.string.delete_file);
                        } else {
                            message = getString(
                                    R.string.delete_files, "" + idxs.length);
                        }
                        AtomicBoolean deleteThreads = new AtomicBoolean(true);
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, message, Snackbar.LENGTH_LONG);

                        snackbar.setAction(getString(R.string.revert_operation), (view) -> {

                            try {
                                deleteThreads.set(false);
                                ExecutorService executor = Executors.newSingleThreadExecutor();
                                FILES files = FILES.getInstance(getApplicationContext());
                                executor.execute(() -> files.resetFilesDeleting(idxs));
                            } catch (Throwable e) {
                                LogUtils.error(TAG, e);
                            } finally {
                                snackbar.dismiss();
                            }

                        });

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (deleteThreads.get()) {
                                    ExecutorService executor = Executors.newSingleThreadExecutor();
                                    executor.execute(() -> {
                                        try {
                                            DOCS.getInstance(getApplicationContext())
                                                    .deleteDocuments(idxs);
                                        } catch (Throwable throwable) {
                                            LogUtils.error(TAG, throwable);
                                        }
                                    });

                                }
                                mSelectionViewModel.setShowFab(true);

                            }
                        });
                        mSelectionViewModel.setShowFab(false);
                        snackbar.show();
                    }

                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }

        });
        eventViewModel.getError().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                mSelectionViewModel.setShowFab(true);

                            }
                        });
                        mSelectionViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }

        });

        eventViewModel.getFatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                mSelectionViewModel.setShowFab(true);

                            }
                        });
                        mSelectionViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.getPermission().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.app_settings, new PermissionAction());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                mSelectionViewModel.setShowFab(true);

                            }
                        });
                        mSelectionViewModel.setShowFab(false);
                        snackbar.show();

                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }

        });

        eventViewModel.getWarning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mDrawerLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                mSelectionViewModel.setShowFab(true);

                            }
                        });
                        mSelectionViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }

        });

        eventViewModel.getInfo().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Toast.makeText(getApplicationContext(), content, Toast.LENGTH_SHORT).show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }

        });

        eventViewModel.getToolbar().observe(this, (event) -> {
            try {
                if (event != null) {
                    updateTitle(Uri.parse(event.getContent()));
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        Intent intent = getIntent();
        handleIntents(intent);

        DaemonService.start(getApplicationContext());

        try {
            DOCS.getInstance(getApplicationContext())
                    .darkMode.set(isDarkTheme());

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showFilesFragment() {
        try {
            if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            java.lang.Thread.sleep(150);
            mSelectionViewModel.setParentProxyFile(0L);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void showFragment(Fragment fragment) {

        releaseActionMode();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(Content.NAME)
                .setReorderingAllowed(true)
                .commit();


    }

    private String prettyUri(@NonNull Uri uri, @NonNull String replace) {
        return uri.toString().replaceFirst(replace, "");
    }

    public void updateTitle(@NonNull Uri uri) {

        try {
            if (Objects.equals(uri.getScheme(), Content.HTTPS)) {
                mBrowserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.lock, 0, 0, 0
                );
                mBrowserText.setText(prettyUri(uri, "https://"));
            } else if (Objects.equals(uri.getScheme(), Content.HTTP)) {
                mBrowserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.lock_open, 0, 0, 0
                );
                mBrowserText.setText(prettyUri(uri, "http://"));
            } else {

                mBrowserText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        R.drawable.lock, 0, 0, 0
                );

                BOOKS books = BOOKS.getInstance(getApplicationContext());
                Bookmark bookmark = books.getBookmark(uri.toString());

                String title = uri.toString();
                if (bookmark != null) {
                    String bookmarkTitle = bookmark.getTitle();
                    if (!bookmarkTitle.isEmpty()) {
                        title = bookmarkTitle;
                    }
                }

                mBrowserText.setText(title);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private boolean downloadActive() {
        Uri uri = mSelectionViewModel.getUri().getValue();
        if (uri != null) {
            return Objects.equals(uri.getScheme(), Content.IPFS) ||
                    Objects.equals(uri.getScheme(), Content.IPNS);
        }
        return false;
    }

    private void releaseActionMode() {
        try {
            if (mActionMode != null) {
                mActionMode.finish();
                mActionMode = null;
            }
            getCurrentAccessFragment().releaseActionMode();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void invokeScan() {
        try {
            PackageManager pm = getPackageManager();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                ScanOptions options = new ScanOptions();
                options.setDesiredBarcodeFormats(ScanOptions.ALL_CODE_TYPES);
                options.setPrompt(getString(R.string.scan_url));
                options.setCameraId(0);  // Use a specific camera of the device
                options.setBeepEnabled(true);
                options.setOrientationLocked(false);
                mScanRequestForResult.launch(options);
            } else {
                EVENTS.getInstance(getApplicationContext()).permission(
                        getString(R.string.feature_camera_required));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private boolean isDarkTheme() {
        int nightModeFlags = getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK;
        return nightModeFlags == Configuration.UI_MODE_NIGHT_YES;
    }

    private void tearDown(@NonNull PopupWindow dialog) {
        try {
            java.lang.Thread.sleep(150);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            dialog.dismiss();
        }
    }


    public void fileDownloader(@NonNull Uri uri, @NonNull String filename,
                               @NonNull String mimeType, long size) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.download_title);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {


            LiteService.setFileInfo(getApplicationContext(), uri, filename, mimeType, size);

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI,
                    Uri.parse(Settings.DOWNLOADS));
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            mFileForResult.launch(intent);

        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> dialog.cancel());
        builder.show();

    }

    public void contentDownloader(@NonNull Uri uri) {

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        builder.setTitle(R.string.download_title);
        String filename = DOCS.getFileName(uri);
        builder.setMessage(filename);

        builder.setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {

            LiteService.setContentUri(getApplicationContext(), uri);

            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI,
                    Uri.parse(Settings.DOWNLOADS));
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            mContentForResult.launch(intent);
        });
        builder.setNeutralButton(getString(android.R.string.cancel),
                (dialog, which) -> dialog.cancel());
        builder.show();

    }

    public abstract static class AppBarStateChangedListener implements AppBarLayout.OnOffsetChangedListener {

        private State mCurrentState = State.IDLE;

        @Override
        public final void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            if (verticalOffset == 0) {
                setCurrentStateAndNotify(State.EXPANDED);
            } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                setCurrentStateAndNotify(State.COLLAPSED);
            } else {
                setCurrentStateAndNotify(State.IDLE);
            }
        }

        private void setCurrentStateAndNotify(State state) {
            if (mCurrentState != state) {
                onStateChanged(state);
            }
            mCurrentState = state;
        }

        public abstract void onStateChanged(State state);

        public enum State {
            EXPANDED,
            COLLAPSED,
            IDLE
        }
    }
}