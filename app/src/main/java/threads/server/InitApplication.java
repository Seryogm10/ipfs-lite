package threads.server;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingPeriodicWorkPolicy;

import com.google.android.material.color.DynamicColors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.push.Push;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.pages.PAGES;
import threads.server.core.pages.Page;
import threads.server.work.CleanupPeriodicWorker;
import threads.server.work.PagePeriodicWorker;

public class InitApplication extends Application {

    public static final String STORAGE_CHANNEL_ID = "STORAGE_CHANNEL_ID";
    public static final String DAEMON_CHANNEL_ID = "DAEMON_CHANNEL_ID";
    public static final String DAEMON_GROUP_ID = "DAEMON_GROUP_ID";
    public static final String STORAGE_GROUP_ID = "STORAGE_GROUP_ID";
    private static final String TAG = InitApplication.class.getSimpleName();


    private void createStorageChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.storage_channel_name);
            String description = context.getString(R.string.storage_channel_description);
            NotificationChannel mChannel = new NotificationChannel(
                    STORAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannelGroup(
                    new NotificationChannelGroup(STORAGE_GROUP_ID, name));

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void createDaemonChannel(@NonNull Context context) {


        try {
            CharSequence name = context.getString(R.string.daemon_channel_name);
            String description = context.getString(R.string.daemon_channel_description);
            NotificationChannel mChannel = new NotificationChannel(DAEMON_CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_DEFAULT);

            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);


            notificationManager.createNotificationChannelGroup(
                    new NotificationChannelGroup(DAEMON_GROUP_ID, name));

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();

        IPFS.setPort(getApplicationContext(), 5001);
        DynamicColors.applyToActivitiesIfAvailable(this);

        createStorageChannel(getApplicationContext());
        createDaemonChannel(getApplicationContext());

        EVENTS events = EVENTS.getInstance(getApplicationContext());
        CleanupPeriodicWorker.cleanup(getApplicationContext());
        PagePeriodicWorker.publish(getApplicationContext(),
                ExistingPeriodicWorkPolicy.KEEP);

        long time = System.currentTimeMillis();
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            ipfs.setIncomingPush(this::onMessageReceived);
        } catch (Throwable throwable) {
            events.fatal(getString(R.string.fatal_error,
                    throwable.getClass().getSimpleName(),
                    "" + throwable.getMessage()));
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "finish start daemon ... " +
                    (System.currentTimeMillis() - time));
        }

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(docs::initPinsPage);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    public void onMessageReceived(@NonNull Push push) {

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            IpnsEntity ipnsEntity = push.getIpnsEntity();
            LogUtils.error(TAG, "Push Message : " + ipnsEntity);

            Cid cid = ipfs.decodeIpnsData(ipnsEntity);
            PeerId peerId = ipnsEntity.getPeerId();

            long sequence = ipnsEntity.getSequence();
            if (sequence >= 0) {
                PAGES pages = PAGES.getInstance(getApplicationContext());
                Page page = pages.createPage(peerId.toBase58());
                page.setCid(cid);
                page.setSequence(sequence);
                pages.storePage(page);

                DOCS.getInstance(getApplicationContext()).addResolves(peerId, cid);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
