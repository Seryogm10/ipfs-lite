package threads.server.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemKeyProvider;

public class FilesItemKeyProvider extends ItemKeyProvider<Long> {

    private final FilesViewAdapter mFilesViewAdapter;


    public FilesItemKeyProvider(@NonNull FilesViewAdapter adapter) {
        super(SCOPE_CACHED);
        mFilesViewAdapter = adapter;
    }

    @Nullable
    @Override
    public Long getKey(int position) {
        return mFilesViewAdapter.getIdx(position);
    }

    @Override
    public int getPosition(@NonNull Long key) {
        return mFilesViewAdapter.getPosition(key);
    }

}