package threads.server.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.color.MaterialColors;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import threads.lite.LogUtils;
import threads.server.R;
import threads.server.core.files.Proxy;
import threads.server.services.MimeTypeService;

public class FilesViewAdapter extends RecyclerView.Adapter<FilesViewAdapter.ViewHolder> implements FileItemPosition {

    private static final String TAG = FilesViewAdapter.class.getSimpleName();
    private final Context mContext;
    private final FilesViewAdapterListener mListener;
    private final List<Proxy> proxies = new ArrayList<>();

    @Nullable
    private SelectionTracker<Long> mSelectionTracker;

    public FilesViewAdapter(@NonNull Context context,
                            @NonNull FilesViewAdapterListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    private static String getCompactString(@NonNull String title) {

        return title.replace("\n", " ");
    }

    public void setSelectionTracker(SelectionTracker<Long> selectionTracker) {
        this.mSelectionTracker = selectionTracker;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.file;
    }

    @Override
    @NonNull
    public FilesViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ThreadViewHolder(this, v);
    }

    long getIdx(int position) {
        return proxies.get(position).getIdx();
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Proxy proxy = proxies.get(position);


        ThreadViewHolder threadViewHolder = (ThreadViewHolder) holder;

        boolean isSelected = false;
        if (mSelectionTracker != null) {
            if (mSelectionTracker.isSelected(proxy.getIdx())) {
                isSelected = true;
            }
        }

        threadViewHolder.bind(isSelected, proxy);
        try {
            if (isSelected) {
                int color = MaterialColors.getColor(mContext,
                        R.attr.colorControlHighlight, Color.GRAY);
                threadViewHolder.view.setBackgroundColor(color);
            } else {
                threadViewHolder.view.setBackgroundResource(android.R.color.transparent);
            }

            int resId = MimeTypeService.getMediaResource(proxy.getMimeType());

            String mimeType = proxy.getMimeType();
            if (mimeType.startsWith(MimeTypeService.VIDEO) ||
                    mimeType.startsWith(MimeTypeService.IMAGE)) {
                Glide.with(mContext).
                        load(proxy.getUri()).
                        placeholder(resId).
                        error(resId).
                        into(threadViewHolder.main_image);
            } else {
                threadViewHolder.main_image.setImageResource(resId);
            }

            threadViewHolder.view.setOnClickListener((v) -> {
                try {
                    mListener.onClick(proxy);
                } catch (Throwable e) {
                    LogUtils.error(TAG, e);
                }
            });


            String title = getCompactString(proxy.getName());
            threadViewHolder.name.setText(title);
            String info = getSize(proxy);
            threadViewHolder.size.setText(info);
            Date date = new Date(proxy.getLastModified());
            String dateInfo = getDate(date);
            threadViewHolder.date.setText(dateInfo);

            if (proxy.isLeaching()) {
                threadViewHolder.progress_bar.setVisibility(View.VISIBLE);
            } else {
                threadViewHolder.progress_bar.setVisibility(View.INVISIBLE);
            }

            if (proxy.isLeaching()) {
                threadViewHolder.general_action.setEnabled(true);
                threadViewHolder.general_action.setIcon(AppCompatResources.getDrawable(
                        mContext, R.drawable.pause));
                threadViewHolder.general_action.setOnClickListener((v) ->
                        mListener.invokePauseAction(proxy)
                );

            } else if (proxy.isSeeding()) {
                threadViewHolder.general_action.setEnabled(true);
                threadViewHolder.general_action.setIcon(AppCompatResources.getDrawable(
                        mContext, R.drawable.menu_down));
                threadViewHolder.general_action.setOnClickListener((v) ->
                        mListener.invokeAction(proxy, v)
                );
            } else {
                threadViewHolder.general_action.setEnabled(false);
                threadViewHolder.general_action.setIcon(AppCompatResources.getDrawable(
                        mContext, R.drawable.menu_down));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


    }

    private String getDate(@NonNull Date date) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 0);
        Date lastYear = c.getTime();

        if (date.before(today)) {
            if (date.before(lastYear)) {
                return android.text.format.DateFormat.format("dd.MM.yyyy", date).toString();
            } else {
                return android.text.format.DateFormat.format("dd.MMMM", date).toString();
            }
        } else {
            return android.text.format.DateFormat.format("HH:mm", date).toString();
        }
    }

    private String getSize(@NonNull Proxy proxy) {

        String fileSize;
        long size = proxy.getSize();

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize + " B";
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize + " KB";
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize + " MB";
        }
    }

    @Override
    public int getItemCount() {
        return proxies.size();
    }

    public void updateData(@NonNull List<Proxy> proxies) {

        final FileDiffCallback diffCallback = new FileDiffCallback(this.proxies, proxies);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.proxies.clear();
        this.proxies.addAll(proxies);
        diffResult.dispatchUpdatesTo(this);


    }


    public void selectAllThreads() {
        try {
            for (Proxy proxy : proxies) {
                if (mSelectionTracker != null) {
                    mSelectionTracker.select(proxy.getIdx());
                }
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }
    }

    @Override
    public int getPosition(long idx) {
        for (int i = 0; i < proxies.size(); i++) {
            if (proxies.get(i).getIdx() == idx) {
                return i;
            }
        }
        return 0;
    }

    public interface FilesViewAdapterListener {

        void invokeAction(@NonNull Proxy proxy, @NonNull View view);

        void onClick(@NonNull Proxy proxy);

        void invokePauseAction(@NonNull Proxy proxy);

    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final TextView name;
        final TextView date;
        final TextView size;

        ViewHolder(View v) {
            super(v);
            v.setLongClickable(true);
            v.setClickable(true);
            v.setFocusable(false);
            view = v;
            name = v.findViewById(R.id.name);
            date = v.findViewById(R.id.date);
            size = v.findViewById(R.id.size);
        }
    }


    static class ThreadViewHolder extends ViewHolder {
        final ImageView main_image;
        final MaterialButton general_action;
        final LinearProgressIndicator progress_bar;
        final FileItemDetails fileItemDetails;

        ThreadViewHolder(FileItemPosition pos, View v) {
            super(v);
            general_action = v.findViewById(R.id.general_action);
            progress_bar = v.findViewById(R.id.progress_bar);
            main_image = v.findViewById(R.id.main_image);
            fileItemDetails = new FileItemDetails(pos);

        }

        void bind(boolean isSelected, Proxy proxy) {

            fileItemDetails.idx = proxy.getIdx();

            itemView.setActivated(isSelected);


        }

        ItemDetailsLookup.ItemDetails<Long> getThreadsItemDetails() {

            return fileItemDetails;
        }
    }
}
