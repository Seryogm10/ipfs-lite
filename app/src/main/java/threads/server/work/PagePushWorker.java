package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.List;
import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Connection;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Session;
import threads.server.core.DOCS;
import threads.server.core.pages.Page;

public class PagePushWorker extends Worker {

    private static final String TAG = PagePushWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public PagePushWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork() {
        return new OneTimeWorkRequest.Builder(PageRefreshWorker.class)
                .addTag(TAG)
                .build();
    }

    public static void publish(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.REPLACE, getWork());
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, "Worker Start " + getId() + " ...");

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            Page page = docs.getHomePage();
            if (page != null) {

                Cid cid = page.getCid();
                Objects.requireNonNull(cid);

                IpnsRecord ipnsRecord =
                        ipfs.createSelfSignedIpnsRecord(page.getSequence(),
                                ipfs.encodeIpnsData(cid));

                Session session = docs.getSession();  // todo make separate session and add locals
                List<Connection> swarm = session.getSwarm();
                for (Connection connection : swarm) {
                    if (connection.isConnected()) {
                        try {
                            ipfs.push(connection, ipnsRecord);
                        } catch (Throwable throwable) {
                            // this might fail, in case the peer of the connection does
                            // not support the push protocol
                            LogUtils.error(TAG, throwable);
                        }
                    }

                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, "Worker Finish " + getId() +
                    " onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }
}
