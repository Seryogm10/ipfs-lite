package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Link;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.InitApplication;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.locals.LOCALS;
import threads.server.services.LocalConnectService;
import threads.server.services.MimeTypeService;

public class DownloadContentWorker extends Worker {

    private static final String TAG = DownloadContentWorker.class.getSimpleName();
    private final NotificationManager mNotificationManager;
    private final AtomicBoolean success = new AtomicBoolean(true);

    @SuppressWarnings("WeakerAccess")
    public DownloadContentWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private static OneTimeWorkRequest getWork(@NonNull Uri uri, @NonNull Uri content) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putString(Content.ADDR, content.toString());

        return new OneTimeWorkRequest.Builder(DownloadContentWorker.class)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void download(@NonNull Context context, @NonNull Uri uri, @NonNull Uri content) {
        WorkManager.getInstance(context).enqueue(getWork(uri, content));
    }

    @NonNull
    @Override
    public Result doWork() {

        String dest = getInputData().getString(Content.URI);
        Objects.requireNonNull(dest);
        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + dest);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            LOCALS locals = LOCALS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            try (Session session = ipfs.createSession()) {

                // fist fill the session with known locals
                LocalConnectService.connect(session, ipfs, locals.getLocals());

                Uri uriDest = Uri.parse(dest);
                DocumentFile doc = DocumentFile.fromTreeUri(getApplicationContext(), uriDest);
                Objects.requireNonNull(doc);


                String url = getInputData().getString(Content.ADDR);
                Objects.requireNonNull(url);
                Uri uri = Uri.parse(url);
                String name = DOCS.getFileName(uri);

                reportProgress(name, 0);

                if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                        Objects.equals(uri.getScheme(), Content.IPFS)) {

                    try {

                        Cid content = docs.getContent(session, uri, this::isStopped);
                        Objects.requireNonNull(content);
                        String mimeType = docs.getMimeType(session, getApplicationContext(),
                                uri, content, this::isStopped);

                        if (Objects.equals(mimeType, MimeTypeService.DIR_MIME_TYPE)) {
                            doc = doc.createDirectory(name);
                            Objects.requireNonNull(doc);
                        }

                        downloadContent(session, doc, content, mimeType, name);


                        if (!isStopped()) {
                            if (!success.get()) {
                                buildFailedNotification(name);
                            } else {
                                reportProgress(name, 100);
                            }
                        }

                    } catch (Throwable e) {
                        if (!isStopped()) {
                            buildFailedNotification(name);
                        }
                        throw e;
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }


    private void downloadContent(@NonNull Session session, @NonNull DocumentFile doc, @NonNull Cid root,
                                 @NonNull String mimeType, @NonNull String name) throws Exception {
        downloadLinks(session, doc, root, mimeType, name);
    }


    private void download(@NonNull Session session, @NonNull DocumentFile doc,
                          @NonNull Cid cid) throws Exception {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start [" + (System.currentTimeMillis() - start) + "]...");


        String name = doc.getName();
        Objects.requireNonNull(name);
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        if (!ipfs.isDir(session, cid, this::isStopped)) {

            try (OutputStream os = getApplicationContext().
                    getContentResolver().openOutputStream(doc.getUri())) {

                ipfs.fetchToOutputStream(session, os, cid, new Progress() {
                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }

                    @Override
                    public void setProgress(int percent) {
                        reportProgress(name, percent);
                    }

                    @Override
                    public boolean doProgress() {
                        return true;
                    }
                });

            } catch (Throwable throwable) {
                success.set(false);

                try {
                    if (doc.exists()) {
                        doc.delete();
                    }
                } catch (Throwable e) {
                    LogUtils.error(TAG, e);
                }

                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }
        }
    }

    private void buildFailedNotification(@NonNull String name) {

        Notification.Builder builder = new Notification.Builder(
                getApplicationContext(), InitApplication.STORAGE_CHANNEL_ID);

        builder.setContentTitle(getApplicationContext().getString(R.string.download_failed, name));
        builder.setSmallIcon(R.drawable.download);
        Intent defaultIntent = new Intent(getApplicationContext(), MainActivity.class);
        int requestID = (int) System.currentTimeMillis();
        PendingIntent defaultPendingIntent = PendingIntent.getActivity(
                getApplicationContext(), requestID, defaultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        builder.setContentIntent(defaultPendingIntent);
        builder.setAutoCancel(true);
        builder.setGroup(InitApplication.STORAGE_GROUP_ID);
        Notification notification = builder.build();


        if (mNotificationManager != null) {
            mNotificationManager.notify(TAG.hashCode(), notification);
        }
    }

    private void reportProgress(@NonNull String title, int percent) {

        Notification notification = createNotification(title, percent);

        if (mNotificationManager != null) {
            mNotificationManager.notify(getId().hashCode(), notification);
        }

        setForegroundAsync(new ForegroundInfo(getId().hashCode(), notification));
    }


    private Notification createNotification(@NonNull String title, int progress) {

        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);

        PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent main = new Intent(getApplicationContext(), MainActivity.class);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                intent).build();

        builder.setContentTitle(title)
                .setSubText("" + progress + "%")
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setGroup(InitApplication.STORAGE_GROUP_ID)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true)
                .setOngoing(true);

        return builder.build();
    }


    private void evalLinks(@NonNull Session session, @NonNull DocumentFile doc,
                           @NonNull List<Link> links) throws Exception {
        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        for (Link link : links) {
            if (!isStopped()) {
                Cid cid = link.getCid();
                if (ipfs.isDir(session, cid, this::isStopped)) {
                    DocumentFile dir = doc.createDirectory(link.getName());
                    Objects.requireNonNull(dir);
                    downloadLinks(session, dir, cid, MimeTypeService.DIR_MIME_TYPE, link.getName());
                } else {
                    String mimeType = MimeTypeService.getMimeType(link.getName());
                    download(session, Objects.requireNonNull(
                                    doc.createFile(mimeType, link.getName())),
                            cid);
                }
            }
        }

    }


    private void downloadLinks(@NonNull Session session, @NonNull DocumentFile doc,
                               @NonNull Cid cid, @NonNull String mimeType,
                               @NonNull String name) throws Exception {

        IPFS ipfs = IPFS.getInstance(getApplicationContext());
        List<Link> links = ipfs.links(session, cid, false, this::isStopped);

        if (links.isEmpty()) {
            if (!isStopped()) {
                DocumentFile child = doc.createFile(mimeType, name);
                Objects.requireNonNull(child);
                download(session, child, cid);
            }
        } else {
            evalLinks(session, doc, links);
        }

    }

}
