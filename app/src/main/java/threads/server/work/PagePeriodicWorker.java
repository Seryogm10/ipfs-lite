package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.core.Reachability;
import threads.server.core.DOCS;
import threads.server.services.DaemonService;
import threads.server.services.LiteService;

public class PagePeriodicWorker extends Worker {

    private static final String TAG = PagePeriodicWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public PagePeriodicWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static PeriodicWorkRequest getWork(@NonNull Context context) {

        int time = LiteService.getPublishServiceTime(context);
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        return new PeriodicWorkRequest.Builder(PagePeriodicWorker.class, time, TimeUnit.HOURS)
                .addTag(TAG)
                .setConstraints(constraints)
                .setInitialDelay(time, TimeUnit.MINUTES)
                .build();

    }

    public static void publish(@NonNull Context context,
                               @NonNull ExistingPeriodicWorkPolicy existingPeriodicWorkPolicy) {
        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
                TAG, existingPeriodicWorkPolicy, getWork(context));
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, "Start ...");

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            if (!DaemonService.STARTED.get()) { // daemon should run
                if (DaemonService.REACHABILITY == Reachability.GLOBAL ||
                        ipfs.numReservations() > 0) {
                    // only when the node is global reachable, publishing of the page make sense
                    // it is assumed that with reservations (static or limited the node
                    // is reachable (not always true, at least with limited)
                    docs.publishPage(docs.getSession(), this::isStopped); // todo make separate session
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "Finish  onStart [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


}

