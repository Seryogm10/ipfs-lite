package threads.server.work;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;
import java.util.UUID;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.core.files.Proxy;

public class UploadFileWorker extends Worker {

    private static final String TAG = UploadFileWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFileWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFileWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static UUID load(@NonNull Context context, long idx) {
        OneTimeWorkRequest request = getWork(idx);
        WorkManager.getInstance(context).enqueue(request);
        return request.getId();
    }


    @NonNull
    @Override
    public Result doWork() {


        long idx = getInputData().getLong(Content.IDX, -1);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);


        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());
            EVENTS events = EVENTS.getInstance(getApplicationContext());


            files.setFileLeaching(idx);

            files.setFileWork(idx, getId());

            Proxy proxy = files.getFileByIdx(idx);
            Objects.requireNonNull(proxy);

            String url = proxy.getUri();
            Objects.requireNonNull(url);
            Uri uri = Uri.parse(url);


            // normal case like content of files
            final long size = proxy.getSize();

            try (InputStream inputStream = getApplicationContext().getContentResolver()
                    .openInputStream(uri)) {
                Objects.requireNonNull(inputStream);

                // todo make separate session
                Cid cid = ipfs.storeInputStream(docs.getSession(), inputStream, new Progress() {

                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }

                    @Override
                    public boolean doProgress() {
                        return false;
                    }

                    @Override
                    public void setProgress(int percent) {
                    }
                }, size);


                Objects.requireNonNull(cid);

                files.setFileDone(idx, cid);
                docs.finishDocument(idx);

                files.resetFileWork(idx);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
                files.setFilesDeleting(idx);
                events.warning(getApplicationContext().getString(
                        R.string.download_failed, proxy.getName()));
                throw throwable;
            }

        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

}
