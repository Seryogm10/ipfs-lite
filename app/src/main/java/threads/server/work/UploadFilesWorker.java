package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.server.InitApplication;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.provider.FileProvider;

public class UploadFilesWorker extends Worker {

    private static final String TAG = UploadFilesWorker.class.getSimpleName();

    private final NotificationManager mNotificationManager;

    @SuppressWarnings("WeakerAccess")
    public UploadFilesWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);

        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    public static OneTimeWorkRequest getWork(long parent, @NonNull Uri uri) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putLong(Content.IDX, parent);

        return new OneTimeWorkRequest.Builder(UploadFilesWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, long parent, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueue(getWork(parent, uri));
    }


    private void closeNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(getId().hashCode());
        }
    }

    @Override
    public void onStopped() {
        super.onStopped();
        closeNotification();
    }

    @NonNull
    @Override
    public Result doWork() {

        String uriFile = getInputData().getString(Content.URI);
        long parent = getInputData().getLong(Content.IDX, 0L);
        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ... ");

        try {
            FILES files = FILES.getInstance(getApplicationContext());
            DOCS docs = DOCS.getInstance(getApplicationContext());
            Objects.requireNonNull(uriFile);

            List<String> uris = new ArrayList<>();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                    getApplicationContext().getContentResolver()
                            .openInputStream(Uri.parse(uriFile))))) {
                Objects.requireNonNull(reader);
                while (reader.ready()) {
                    uris.add(reader.readLine());
                }
            }

            int maxIndex = uris.size();
            AtomicInteger index = new AtomicInteger(0);

            reportProgress(getApplicationContext().getString(R.string.uploading), 0, 0, maxIndex);


            for (String uriStr : uris) {
                Uri uri = Uri.parse(uriStr);
                if (!isStopped()) {
                    if (!FileProvider.hasReadPermission(getApplicationContext(), uri)) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getApplicationContext().getString(
                                        R.string.file_has_no_read_permission));
                        continue;
                    }

                    if (FileProvider.isPartial(getApplicationContext(), uri)) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getApplicationContext().getString(R.string.file_not_valid));
                        continue;
                    }

                    final int indexValue = index.incrementAndGet();


                    String name = FileProvider.getFileName(getApplicationContext(), uri);
                    String mimeType = FileProvider.getMimeType(getApplicationContext(), uri);

                    long size = FileProvider.getFileSize(getApplicationContext(), uri);

                    long idx = docs.createDocument(parent, mimeType, null, uri,
                            name, size, false);
                    IPFS ipfs = IPFS.getInstance(getApplicationContext());

                    try (InputStream inputStream = getApplicationContext().getContentResolver()
                            .openInputStream(uri)) {
                        Objects.requireNonNull(inputStream);

                        // todo make separate session
                        Cid cid = ipfs.storeInputStream(docs.getSession(), inputStream, new Progress() {

                            @Override
                            public boolean isCancelled() {
                                return isStopped();
                            }

                            @Override
                            public boolean doProgress() {
                                return true;
                            }

                            @Override
                            public void setProgress(int percent) {
                                reportProgress(name, percent, indexValue, maxIndex);
                            }
                        }, size);


                        Objects.requireNonNull(cid);

                        reportProgress(name, 100, indexValue, maxIndex);

                        files.setFileDone(idx, cid);

                        docs.finishDocument(idx);


                    } catch (Throwable e) {
                        files.setFilesDeleting(idx);
                    }
                }
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    private void reportProgress(@NonNull String info, int percent, int index, int maxIndex) {
        if (!isStopped()) {

            Notification notification = createNotification(info, percent, index, maxIndex);
            if (mNotificationManager != null) {
                mNotificationManager.notify(getId().hashCode(), notification);
            }
            setForegroundAsync(new ForegroundInfo(getId().hashCode(), notification));
        }
    }

    private Notification createNotification(@NonNull String title, int progress, int index, int maxIndex) {

        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);


        PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent main = new Intent(getApplicationContext(), MainActivity.class);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                intent).build();

        builder.setContentTitle(title)
                .setSubText("" + index + "/" + maxIndex)
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setGroup(InitApplication.STORAGE_GROUP_ID)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setOngoing(true);

        return builder.build();
    }

}
