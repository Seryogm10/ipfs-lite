package threads.server.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.lite.core.Session;
import threads.server.InitApplication;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.Content;
import threads.server.core.files.FILES;
import threads.server.core.files.Proxy;

public class BackupWorker extends Worker {
    private static final String WID = "BW";
    private static final String TAG = BackupWorker.class.getSimpleName();
    private final NotificationManager mNotificationManager;


    @SuppressWarnings("WeakerAccess")
    public BackupWorker(@NonNull Context context,
                        @NonNull WorkerParameters params) {
        super(context, params);

        mNotificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private static OneTimeWorkRequest getWork(@NonNull Uri uri) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());

        return new OneTimeWorkRequest.Builder(BackupWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void backup(@NonNull Context context, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                WID + uri, ExistingWorkPolicy.KEEP, getWork(uri));

    }

    private void closeNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(getId().hashCode());
        }
    }

    private void reportProgress(@NonNull String title, int percent) {

        if (!isStopped()) {
            Notification notification = createNotification(title, percent);
            if (mNotificationManager != null) {
                mNotificationManager.notify(getId().hashCode(), notification);
            }
            setForegroundAsync(new ForegroundInfo(getId().hashCode(), notification));
        }
    }


    private Notification createNotification(@NonNull String title, int progress) {


        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                InitApplication.STORAGE_CHANNEL_ID);

        PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                .createCancelPendingIntent(getId());
        String cancel = getApplicationContext().getString(android.R.string.cancel);

        Intent main = new Intent(getApplicationContext(), MainActivity.class);

        int requestID = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Notification.Action action = new Notification.Action.Builder(
                Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                intent).build();

        builder.setContentTitle(title)
                .setContentIntent(pendingIntent)
                .setProgress(100, progress, false)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.download)
                .addAction(action)
                .setGroup(InitApplication.STORAGE_GROUP_ID)
                .setCategory(Notification.CATEGORY_PROGRESS)
                .setUsesChronometer(true)
                .setOngoing(true);

        return builder.build();
    }


    @NonNull
    @Override
    public Result doWork() {

        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            FILES files = FILES.getInstance(getApplicationContext());

            try (Session session = ipfs.createSession()) {
                DocumentFile rootDocFile = DocumentFile.fromTreeUri(getApplicationContext(),
                        Uri.parse(uri));
                Objects.requireNonNull(rootDocFile);

                List<Proxy> children = files.getChildren(0);


                if (!children.isEmpty()) {

                    reportProgress(getApplicationContext().getString(R.string.backup_files), 0);

                    DocumentFile docFile = rootDocFile.createDirectory(getApplicationContext().getString(R.string.ipfs));
                    Objects.requireNonNull(docFile);


                    for (Proxy child : children) {
                        if (!isStopped()) {
                            if (!child.isDeleting() && child.isSeeding()) {
                                if (child.isDir()) {
                                    DocumentFile file = docFile.createDirectory(child.getName());
                                    Objects.requireNonNull(file);
                                    copyThreads(session, child, file);
                                } else {
                                    DocumentFile childFile = docFile.createFile(
                                            child.getMimeType(), child.getName());
                                    Objects.requireNonNull(childFile);
                                    if (!childFile.canWrite()) {
                                        // throw message
                                        LogUtils.error(TAG, "can not write");
                                    } else {
                                        copyThread(session, child, childFile);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();

    }

    @Override
    public void onStopped() {
        super.onStopped();
        closeNotification();
    }


    private void copyThreads(@NonNull Session session, @NonNull Proxy proxy, @NonNull DocumentFile file) {

        FILES files = FILES.getInstance(getApplicationContext());
        List<Proxy> children = files.getChildren(proxy.getIdx());
        for (Proxy child : children) {
            if (!isStopped()) {
                if (!child.isDeleting() && child.isSeeding()) {
                    if (child.isDir()) {
                        DocumentFile docFile = file.createDirectory(child.getName());
                        Objects.requireNonNull(docFile);
                        copyThreads(session, child, docFile);
                    } else {
                        DocumentFile childFile = file.createFile(
                                child.getMimeType(), child.getName());
                        Objects.requireNonNull(childFile);
                        if (!childFile.canWrite()) {
                            // throw message
                            LogUtils.error(TAG, "can not write");
                        } else {
                            copyThread(session, child, childFile);
                        }
                    }
                }
            }
        }
    }

    private void copyThread(@NonNull Session session, @NonNull Proxy proxy, @NonNull DocumentFile file) {

        if (isStopped()) {
            return;
        }


        Cid cid = proxy.getCid();
        Objects.requireNonNull(cid);

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            AtomicLong refresh = new AtomicLong(System.currentTimeMillis());
            try (OutputStream os = getApplicationContext().getContentResolver().
                    openOutputStream(file.getUri())) {
                Objects.requireNonNull(os);

                ipfs.fetchToOutputStream(session, os, cid, new Progress() {

                    @Override
                    public void setProgress(int percent) {
                        reportProgress(proxy.getName(), percent);
                    }

                    @Override
                    public boolean doProgress() {

                        long time = System.currentTimeMillis();
                        long diff = time - refresh.get();
                        boolean doProgress = (diff > Settings.REFRESH);
                        if (doProgress) {
                            refresh.set(time);
                        }
                        return doProgress;
                    }

                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }


                });
            } catch (Throwable e) {
                LogUtils.error(TAG, e);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            if (isStopped()) {
                if (file.exists()) {
                    file.delete();
                }
            }
        }
    }
}
