package threads.server.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.LogUtils;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.Content;
import threads.server.core.DOCS;

public class NewFolderDialogFragment extends DialogFragment {
    public static final String TAG = NewFolderDialogFragment.class.getSimpleName();


    private final AtomicBoolean notPrintErrorMessages = new AtomicBoolean(false);
    private long mLastClickTime = 0;
    private TextInputEditText mNewFolder;
    private TextInputLayout mFolderLayout;

    public static NewFolderDialogFragment newInstance(long parent) {
        Bundle bundle = new Bundle();
        bundle.putLong(Content.IDX, parent);

        NewFolderDialogFragment fragment = new NewFolderDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        notPrintErrorMessages.set(true);
        isValidFolderName(getDialog());
        notPrintErrorMessages.set(false);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());

        Bundle args = getArguments();
        Objects.requireNonNull(args);
        long parent = args.getLong(Content.IDX, 0);

        View view = inflater.inflate(R.layout.new_folder, null);
        mFolderLayout = view.findViewById(R.id.folder_layout);
        mNewFolder = view.findViewById(R.id.new_folder_text);
        mNewFolder.requestFocus();
        mNewFolder.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isValidFolderName(getDialog());
            }
        });


        builder.setView(view)
                // Add action buttons
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    removeKeyboards();


                    Editable text = mNewFolder.getText();
                    Objects.requireNonNull(text);
                    String name = text.toString();

                    ExecutorService executor = Executors.newSingleThreadExecutor();
                    executor.execute(() -> {
                        try {
                            DOCS docs = DOCS.getInstance(requireContext());
                            docs.createDirectory(parent, name);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    });
                })

                .setTitle(R.string.new_folder);


        return builder.create();
    }

    private void isValidFolderName(Dialog dialog) {
        if (dialog instanceof AlertDialog) {
            AlertDialog alertDialog = (AlertDialog) dialog;
            Editable text = mNewFolder.getText();
            Objects.requireNonNull(text);
            String multi = text.toString();


            boolean result = !multi.isEmpty();


            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(result);


            if (!notPrintErrorMessages.get()) {

                if (multi.isEmpty()) {
                    mFolderLayout.setError(getString(R.string.name_not_valid));
                } else {
                    mFolderLayout.setError(null);
                }

            } else {
                mFolderLayout.setError(null);
            }
        }
    }

    private void removeKeyboards() {

        try {
            InputMethodManager imm = (InputMethodManager)
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mNewFolder.getWindowToken(), 0);
            }
        } catch (Throwable e) {
            LogUtils.error(TAG, e);
        }

    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        removeKeyboards();
    }

}
