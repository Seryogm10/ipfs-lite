package threads.server.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.HttpAuthHandler;
import android.webkit.URLUtil;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.LogUtils;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;
import threads.server.MainActivity;
import threads.server.R;
import threads.server.Settings;
import threads.server.core.Content;
import threads.server.core.DOCS;
import threads.server.core.books.BOOKS;
import threads.server.core.books.Bookmark;
import threads.server.core.events.EVENTS;
import threads.server.utils.CustomWebChromeClient;
import threads.server.utils.SelectionViewModel;
import threads.server.work.CleanupBrowserDataWorker;


public class BrowserFragment extends Fragment implements AccessFragment {


    private static final String TAG = BrowserFragment.class.getSimpleName();


    private WebView mWebView;
    private LinearProgressIndicator mProgressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private long mLastClickTime = 0;

    private ActionMode mActionMode;


    private void goBack() {
        try {
            mWebView.stopLoading();
            mWebView.goBack();

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void goForward() {
        try {
            if (isResumed()) {
                mWebView.stopLoading();
                mWebView.goForward();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public boolean onBackPressed() {
        if (mWebView.canGoBack()) {
            goBack();
            return true;
        }
        return false;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        releaseActionMode();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container);

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                mSwipeRefreshLayout.setRefreshing(true);
                reload();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


        mProgressBar = view.findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.GONE);


        mWebView = view.findViewById(R.id.web_view);


        CustomWebChromeClient mCustomWebChromeClient = new CustomWebChromeClient(requireActivity());
        mWebView.setWebChromeClient(mCustomWebChromeClient);

        Settings.setWebSettings(mWebView, Settings.isJavascriptEnabled(requireContext()));


        SelectionViewModel mSelectionViewModel = new ViewModelProvider(requireActivity()).get(SelectionViewModel.class);


        mSelectionViewModel.getUri().observe(getViewLifecycleOwner(), (uri) -> {
            if (uri != null) {
                openUri(uri);
            }
        });

        mWebView.setDownloadListener((url, userAgent, contentDisposition, mimeType, contentLength) -> {

            try {
                LogUtils.error(TAG, "downloadUrl : " + url);
                String filename = URLUtil.guessFileName(url, contentDisposition, mimeType);
                Uri uri = Uri.parse(url);
                if (Objects.equals(uri.getScheme(), Content.IPFS) ||
                        Objects.equals(uri.getScheme(), Content.IPNS)) {
                    String res = uri.getQueryParameter("download");
                    if (Objects.equals(res, "0")) {
                        try {
                            EVENTS.getInstance(requireContext())
                                    .warning(getString(R.string.browser_handle_file, filename));
                        } finally {
                            mProgressBar.setVisibility(View.GONE);
                        }
                    } else {
                        ((MainActivity) requireActivity()).contentDownloader(uri);

                        mProgressBar.setVisibility(View.GONE);
                    }
                } else {
                    ((MainActivity) requireActivity()).fileDownloader(
                            uri, filename, mimeType, contentLength);
                    mProgressBar.setVisibility(View.GONE);
                }


            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        mWebView.setWebViewClient(new WebViewClient() {


            private final AtomicReference<String> host = new AtomicReference<>();

            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
                LogUtils.error(TAG, "onPageCommitVisible " + url);
                mProgressBar.setVisibility(View.GONE);
            }


            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {

                try {
                    WebViewDatabase database = WebViewDatabase.getInstance(requireContext());
                    String[] data = database.getHttpAuthUsernamePassword(host, realm);


                    String storedName = null;
                    String storedPass = null;

                    if (data != null) {
                        storedName = data[0];
                        storedPass = data[1];
                    }

                    LayoutInflater inflater = (LayoutInflater)
                            requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View form = inflater.inflate(R.layout.http_auth_request, null);


                    final EditText usernameInput = form.findViewById(R.id.user_name);
                    final EditText passwordInput = form.findViewById(R.id.password);

                    if (storedName != null) {
                        usernameInput.setText(storedName);
                    }

                    if (storedPass != null) {
                        passwordInput.setText(storedPass);
                    }

                    MaterialAlertDialogBuilder authDialog =
                            new MaterialAlertDialogBuilder(requireContext())
                                    .setTitle(R.string.authentication)
                                    .setView(form)
                                    .setCancelable(false)
                                    .setPositiveButton(android.R.string.ok, (dialog, whichButton) -> {

                                        String username = usernameInput.getText().toString();
                                        String password = passwordInput.getText().toString();

                                        database.setHttpAuthUsernamePassword(host, realm, username, password);

                                        handler.proceed(username, password);
                                        dialog.dismiss();
                                    })

                                    .setNegativeButton(android.R.string.cancel, (dialog, whichButton) -> {
                                        dialog.dismiss();
                                        view.stopLoading();
                                        handler.cancel();
                                    });


                    authDialog.show();
                    return;
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                super.onReceivedHttpAuthRequest(view, handler, host, realm);
            }


            @Override
            public void onLoadResource(WebView view, String url) {
                LogUtils.error(TAG, "onLoadResource : " + url);
                super.onLoadResource(view, url);
            }

            @Override
            public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
                super.doUpdateVisitedHistory(view, url, isReload);
                LogUtils.error(TAG, "doUpdateVisitedHistory : " + url + " " + isReload);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                LogUtils.error(TAG, "onPageStarted : " + url);
                try {
                    mProgressBar.setVisibility(View.VISIBLE);
                    releaseActionMode();
                    EVENTS.getInstance(requireContext()).toolbar(url);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                LogUtils.error(TAG, "onPageFinished : " + url);
                try {
                    Uri uri = Uri.parse(url);
                    if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                            Objects.equals(uri.getScheme(), Content.IPFS)) {
                        DOCS docs = DOCS.getInstance(requireContext());
                        if (docs.numUris() == 0) {
                            mProgressBar.setVisibility(View.GONE);
                        }
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                LogUtils.error(TAG, "" + error.getDescription());
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {

                try {
                    Uri uri = request.getUrl();
                    LogUtils.error(TAG, "shouldOverrideUrlLoading : " + uri);
                    DOCS docs = DOCS.getInstance(requireContext());

                    if (Objects.equals(uri.getScheme(), Content.ABOUT)) {
                        return true;
                    } else if (Objects.equals(uri.getScheme(), Content.HTTP)) {

                        Uri redirectUri = docs.redirectHttp(uri);
                        if (!Objects.equals(redirectUri, uri)) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                    requireContext(), MainActivity.class);
                            startActivity(intent);
                            return true;
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Content.HTTPS)) {
                        if (Settings.isRedirectUrlEnabled(requireContext())) {
                            Uri redirectUri = docs.redirectHttps(uri);
                            if (!Objects.equals(redirectUri, uri)) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, redirectUri,
                                        requireContext(), MainActivity.class);
                                startActivity(intent);
                                return true;
                            }
                        }
                        return false;
                    } else if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                            Objects.equals(uri.getScheme(), Content.IPFS)) {

                        String res = uri.getQueryParameter("download");
                        if (Objects.equals(res, "1")) {
                            ((MainActivity) requireActivity()).contentDownloader(uri);
                            mProgressBar.setVisibility(View.GONE);
                            return true;
                        }
                        mProgressBar.setVisibility(View.VISIBLE);

                        return false;

                    } else if (Objects.equals(uri.getScheme(), Content.MAGNET)) {
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);

                        } catch (Throwable ignore) {
                            LogUtils.error(TAG, "Not  handled uri " + uri);
                        }
                        return true;
                    } else {
                        try {
                            // all other stuff
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

                            startActivity(intent);

                        } catch (Throwable ignore) {
                        }
                        return true;
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return false;

            }

            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {

                try {
                    Uri uri = request.getUrl();
                    LogUtils.error(TAG, "shouldInterceptRequest : " + uri.toString());
                    host.set(uri.getHost());
                    DOCS docs = DOCS.getInstance(requireContext());
                    if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                            Objects.equals(uri.getScheme(), Content.IPFS)) {
                        long start = System.currentTimeMillis();

                        docs.attachUri(uri);

                        Session session = docs.getSession(uri.getAuthority());

                        Cancellable cancellable = () -> !docs.hasSession(uri.getAuthority());

                        try {
                            Uri redirectUri = uri;
                            if (Settings.isRedirectIndexEnabled(requireContext())) {
                                redirectUri = docs.redirectUri(session, uri, cancellable);
                                if (!Objects.equals(uri, redirectUri)) {
                                    return docs.createRedirectMessage(redirectUri);
                                }
                            }
                            return docs.getResponse(session, requireContext(),
                                    redirectUri, cancellable);
                        } catch (Throwable throwable) {
                            if (cancellable.isCancelled()) {
                                return docs.createEmptyResource();
                            }
                            return docs.createErrorMessage(throwable);
                        } finally {
                            docs.detachUri(uri);
                            LogUtils.info(TAG, "Finish page [" +
                                    (System.currentTimeMillis() - start) + "]...");
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
                return null;
            }
        });

    }

    public void reload() {
        try {
            if (isResumed()) {
                try {
                    mProgressBar.setVisibility(View.GONE);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                try {
                    DOCS docs = DOCS.getInstance(requireContext());
                    docs.cleanupResolver(Uri.parse(mWebView.getUrl()));
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                try {
                    mWebView.reload();
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private int dp48ToPixels() {
        float density = getResources().getDisplayMetrics().density;
        return Math.round((float) 48 * density);
    }

    private ActionMode.Callback createFindActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_find_action_mode, menu);


                MenuItem action_mode_find = menu.findItem(R.id.action_mode_find);
                EditText mFindText = (EditText) action_mode_find.getActionView();

                mFindText.setMinHeight(dp48ToPixels());
                mFindText.setWidth(400);
                mFindText.setBackgroundResource(android.R.color.transparent);
                mFindText.setSingleLine();
                mFindText.setTextSize(16);
                mFindText.setHint(R.string.find_page);
                mFindText.setFocusable(true);
                mFindText.requestFocus();

                mFindText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        mWebView.findAllAsync(mFindText.getText().toString());
                    }
                });


                mode.setTitle("0/0");

                mWebView.setFindListener((activeMatchOrdinal, numberOfMatches, isDoneCounting) -> {
                    try {
                        String result = "" + activeMatchOrdinal + "/" + numberOfMatches;
                        mode.setTitle(result);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                });

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int itemId = item.getItemId();
                if (itemId == R.id.action_mode_previous) {


                    if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                        return true;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();

                    try {
                        mWebView.findNext(false);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                    return true;
                } else if (itemId == R.id.action_mode_next) {


                    if (SystemClock.elapsedRealtime() - mLastClickTime < Settings.CLICK_OFFSET) {
                        return true;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    try {
                        mWebView.findNext(true);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }

                    return true;

                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                try {
                    mWebView.clearMatches();
                    mWebView.setFindListener(null);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        };

    }

    public void bookmark(@NonNull Context context) {
        try {
            if (isResumed()) {
                String url = mWebView.getUrl();
                Uri uri = Uri.parse(url);

                BOOKS books = BOOKS.getInstance(context);

                Bookmark bookmark = books.getBookmark(uri.toString());
                if (bookmark != null) {

                    String msg = bookmark.getTitle();

                    books.removeBookmark(bookmark);

                    if (msg.isEmpty()) {
                        msg = uri.toString();
                    }

                    EVENTS.getInstance(requireContext()).warning(
                            getString(R.string.bookmark_removed, msg));

                } else {
                    Bitmap bitmap = mWebView.getFavicon();
                    String title = mWebView.getTitle();
                    if (title == null) {
                        title = uri.getHost();
                    }


                    bookmark = books.createBookmark(uri.toString(), title);
                    if (bitmap != null) {
                        bookmark.setBitmapIcon(bitmap);
                    } else {
                        bookmark.resetBitmapIcon();
                    }

                    books.storeBookmark(bookmark);

                    String msg = title;
                    if (msg.isEmpty()) {
                        msg = uri.toString();
                    }

                    EVENTS.getInstance(requireContext()).warning(
                            getString(R.string.bookmark_added, msg));

                }

            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void clearBrowserData() {
        try {
            if (isResumed()) {
                mWebView.clearHistory();
                mWebView.clearCache(true);

                CleanupBrowserDataWorker.clearCache(requireContext());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.browser_view, container, false);
    }


    @Override
    public void findInPage() {
        try {
            if (isResumed()) {
                mActionMode = ((AppCompatActivity)
                        requireActivity()).startSupportActionMode(
                        createFindActionModeCallback());
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void openUri(@NonNull Uri uri) {
        long start = System.currentTimeMillis();

        try {
            EVENTS.getInstance(requireContext()).toolbar(uri.toString());

            mProgressBar.setVisibility(View.VISIBLE);
            DOCS docs = DOCS.getInstance(requireContext());
            docs.releaseSessions(uri.getAuthority());

            if (Objects.equals(uri.getScheme(), Content.IPNS) ||
                    Objects.equals(uri.getScheme(), Content.IPFS)) {
                docs.attachUri(uri);

                mWebView.getSettings().setJavaScriptEnabled(false);
            } else {
                mWebView.getSettings().setJavaScriptEnabled(
                        Settings.isJavascriptEnabled(requireContext())
                );
            }

            mWebView.stopLoading();

            mWebView.loadUrl(uri.toString());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, "finish openUri [" +
                    (System.currentTimeMillis() - start) + "]...");
        }
    }

    public boolean canGoForward() {
        try {
            if (isResumed()) {
                return mWebView.canGoForward();
            }
            return false;
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    public void enableSwipeRefresh(boolean enable) {
        try {
            if (isResumed()) {
                mSwipeRefreshLayout.setEnabled(enable);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    @Override
    public void releaseActionMode() {
        try {
            if (isResumed()) {
                if (mActionMode != null) {
                    mActionMode.finish();
                    mActionMode = null;
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
