/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.recovery;

import androidx.annotation.Nullable;

import net.luminis.LogUtils;
import net.luminis.quic.cc.CongestionController;
import net.luminis.quic.frame.AckFrame;
import net.luminis.quic.packet.PacketInfo;
import net.luminis.quic.packet.QuicPacket;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.stream.Collectors;


public class LossDetector {
    private static final String TAG = LossDetector.class.getSimpleName();
    private final RecoveryManager recoveryManager;
    private final RttEstimator rttEstimater;
    private final CongestionController congestionController;
    private final Runnable postProcessLostCallback;
    private final Map<Long, PacketStatus> packetSentLog = new ConcurrentHashMap<>();
    private final AtomicInteger ackElicitingInFlight = new AtomicInteger();
    private final AtomicLong largestAcked = new AtomicLong(-1);
    private final AtomicLong lost = new AtomicLong(0);
    private final AtomicBoolean isReset = new AtomicBoolean(false);
    private final AtomicReference<Instant> lossTime = new AtomicReference<>();
    private final AtomicReference<Instant> lastAckElicitingSent = new AtomicReference<>();


    public LossDetector(RecoveryManager recoveryManager, RttEstimator rttEstimator,
                        CongestionController congestionController, Runnable postProcessLostCallback) {
        this.recoveryManager = recoveryManager;
        this.rttEstimater = rttEstimator;
        this.congestionController = congestionController;
        this.postProcessLostCallback = postProcessLostCallback;
    }

    public void packetSent(QuicPacket packet, Instant sent, Consumer<QuicPacket> lostPacketCallback) {
        if (isReset.get()) {
            return;
        }
        congestionController.registerInFlight(packet);

        if (packet.isAckEliciting()) {
            ackElicitingInFlight.getAndAdd(1);
            lastAckElicitingSent.set(sent);
        }

        // This method is synchronized, because during a reset operation, no new packets must be logged as sent.
        packetSentLog.put(packet.getPacketNumber(), new PacketStatus(sent, packet, lostPacketCallback));

    }

    public void onAckReceived(AckFrame ackFrame, Instant timeReceived) {
        if (isReset.get()) {
            return;
        }

        largestAcked.updateAndGet(l -> Long.max(l, ackFrame.getLargestAcknowledged()));

        @SuppressWarnings("ConstantConditions") List<PacketStatus> newlyAcked = ackFrame.getAckedPacketNumbers()
                .filter(pn -> {
                    PacketStatus tmp = packetSentLog.get(pn);
                    if (tmp != null) {
                        return !tmp.acked();
                    } else {
                        return false;
                    }
                })
                .map(packetSentLog::get)
                .filter(Objects::nonNull)      // Could be null when reset is executed concurrently.
                .filter(PacketStatus::setAcked)   // Only keep the ones that actually got set to acked
                .collect(Collectors.toList());

        // Possible optimization: everything that follows only if newlyAcked not empty

        int ackedAckEliciting = (int) newlyAcked.stream().filter(packetStatus -> packetStatus.packet().isAckEliciting()).count();

        ackElicitingInFlight.getAndAdd(-1 * ackedAckEliciting);

        congestionController.registerAcked(filterInFlight(newlyAcked));

        detectLostPackets();

        recoveryManager.setLossDetectionTimer();

        rttEstimater.ackReceived(ackFrame, timeReceived, newlyAcked);

        // Cleanup
        newlyAcked.forEach(p -> packetSentLog.remove(p.packet().getPacketNumber()));
    }

    public void reset() {
        isReset.set(true);
        List<PacketStatus> inflightPackets = packetSentLog.values().stream()
                .filter(PacketStatus::inFlight)
                .filter(PacketStatus::setLost)   // Only keep the ones that actually were set to lost
                .collect(Collectors.toList());
        congestionController.discard(inflightPackets);
        ackElicitingInFlight.set(0);
        packetSentLog.clear();
        lossTime.set(null);
        lastAckElicitingSent.set(null);
    }

    void detectLostPackets() {
        if (isReset.get()) {
            return;
        }

        float kTimeThreshold = 9f / 8f;
        int lossDelay = (int) (kTimeThreshold * Integer.max(rttEstimater.getSmoothedRtt(), rttEstimater.getLatestRtt()));

        Instant lostSendTime = Instant.now().minusMillis(lossDelay);

        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-6.1
        // "A packet is declared lost if it meets all the following conditions:
        //   o  The packet is unacknowledged, in-flight, and was sent prior to an
        //      acknowledged packet.
        //   o  Either its packet number is kPacketThreshold smaller than an
        //      acknowledged packet (Section 6.1.1), or it was sent long enough in
        //      the past (Section 6.1.2)."
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-2
        // "In-flight:  Packets are considered in-flight when they have been sent
        //      and neither acknowledged nor declared lost, and they are not ACK-
        //      only."
        List<PacketStatus> lostPackets = packetSentLog.values().stream()
                .filter(PacketStatus::inFlight)
                .filter(p -> pnTooOld(p) || sentTimeTooLongAgo(p, lostSendTime))
                .filter(p -> !p.packet().isAckOnly())
                .collect(Collectors.toList());
        if (!lostPackets.isEmpty()) {
            declareLost(lostPackets);
        }

        Optional<Instant> earliestSentTime = packetSentLog.values().stream()
                .filter(PacketStatus::inFlight)
                .filter(p -> p.packet().getPacketNumber() <= largestAcked.get())
                .filter(p -> !p.packet().isAckOnly())
                .map(PacketInfo::timeSent)
                .min(Instant::compareTo);

        if (earliestSentTime.isPresent() && earliestSentTime.get().isAfter(lostSendTime)) {
            lossTime.set(earliestSentTime.get().plusMillis(lossDelay));
        } else {
            lossTime.set(null);
        }
    }

    @Nullable
    Instant getLossTime() {
        return lossTime.get();
    }

    @Nullable
    Instant getLastAckElicitingSent() {
        return lastAckElicitingSent.get();
    }

    boolean ackElicitingInFlight() {
        int actualAckElicitingInFlight = ackElicitingInFlight.get();
        return actualAckElicitingInFlight != 0;
    }

    List<QuicPacket> unAcked() {
        return packetSentLog.values().stream()
                .filter(PacketStatus::inFlight)
                .filter(p -> !p.packet().isAckOnly())
                .map(PacketInfo::packet)
                .collect(Collectors.toList());
    }

    private boolean pnTooOld(PacketStatus p) {
        int kPacketThreshold = 3;
        return p.packet().getPacketNumber() <= largestAcked.get() - kPacketThreshold;
    }

    private boolean sentTimeTooLongAgo(PacketStatus p, Instant lostSendTime) {
        return p.packet().getPacketNumber() <= largestAcked.get() && p.timeSent().isBefore(lostSendTime);
    }

    private void declareLost(List<PacketStatus> lostPacketsInfo) {
        lostPacketsInfo = lostPacketsInfo.stream()
                .filter(PacketStatus::setLost)   // Only keep the ones that actually were set to lost
                .collect(Collectors.toList());

        int lostAckEliciting = (int) lostPacketsInfo.stream().filter(packetStatus -> packetStatus.packet().isAckEliciting()).count();
        ackElicitingInFlight.getAndAdd(-1 * lostAckEliciting);

        lostPacketsInfo.forEach(packetStatus -> {
            // Retransmitting the frames in the lost packet is delegated to the lost frame callback, because
            // whether retransmitting the frame is necessary (and in which manner) depends on frame type,
            // see https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-13.3
            LogUtils.error(TAG, packetStatus.packet().toString());
            packetStatus.lostPacketCallback().accept(packetStatus.packet());
            lost.incrementAndGet();
        });
        postProcessLostCallback.run();

        congestionController.registerLost(filterInFlight(lostPacketsInfo));

        // Cleanup
        lostPacketsInfo.forEach(p -> packetSentLog.remove(p.packet().getPacketNumber()));
    }

    private List<PacketStatus> filterInFlight(List<PacketStatus> packets) {
        return packets.stream()
                .filter(packetInfo -> packetInfo.packet().isInflightPacket())
                .collect(Collectors.toList());
    }

    public long getLost() {
        return lost.get();
    }

    public boolean noAckedReceived() {
        return largestAcked.get() < 0;
    }

}
