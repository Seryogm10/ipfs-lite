/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic;

import static net.luminis.quic.EarlyDataStatus.Accepted;
import static net.luminis.quic.EarlyDataStatus.None;
import static net.luminis.quic.EarlyDataStatus.Requested;
import static net.luminis.quic.EncryptionLevel.App;
import static net.luminis.quic.EncryptionLevel.Handshake;
import static net.luminis.quic.EncryptionLevel.Initial;
import static net.luminis.quic.QuicConstants.TransportErrorCode.PROTOCOL_VIOLATION;
import static net.luminis.quic.QuicConstants.TransportErrorCode.TRANSPORT_PARAMETER_ERROR;
import static net.luminis.tls.util.ByteUtils.bytesToHex;

import androidx.annotation.Nullable;

import net.luminis.LogUtils;
import net.luminis.quic.cid.ConnectionIdInfo;
import net.luminis.quic.cid.ConnectionIdManager;
import net.luminis.quic.frame.AckFrame;
import net.luminis.quic.frame.FrameProcessor3;
import net.luminis.quic.frame.HandshakeDoneFrame;
import net.luminis.quic.frame.NewConnectionIdFrame;
import net.luminis.quic.frame.NewTokenFrame;
import net.luminis.quic.frame.PingFrame;
import net.luminis.quic.frame.RetireConnectionIdFrame;
import net.luminis.quic.packet.HandshakePacket;
import net.luminis.quic.packet.InitialPacket;
import net.luminis.quic.packet.QuicPacket;
import net.luminis.quic.packet.RetryPacket;
import net.luminis.quic.packet.ShortHeaderPacket;
import net.luminis.quic.packet.VersionNegotiationPacket;
import net.luminis.quic.packet.ZeroRttPacket;
import net.luminis.quic.send.SenderImpl;
import net.luminis.quic.server.ServerConnectionProxy;
import net.luminis.quic.server.ServerConnector;
import net.luminis.quic.stream.EarlyDataStream;
import net.luminis.quic.stream.FlowControl;
import net.luminis.quic.stream.StreamManager;
import net.luminis.quic.tls.QuicTransportParametersExtension;
import net.luminis.tls.CertificateWithPrivateKey;
import net.luminis.tls.CipherSuite;
import net.luminis.tls.NewSessionTicket;
import net.luminis.tls.extension.ApplicationLayerProtocolNegotiationExtension;
import net.luminis.tls.extension.EarlyDataExtension;
import net.luminis.tls.extension.Extension;
import net.luminis.tls.handshake.CertificateMessage;
import net.luminis.tls.handshake.CertificateVerifyMessage;
import net.luminis.tls.handshake.ClientHello;
import net.luminis.tls.handshake.ClientMessageSender;
import net.luminis.tls.handshake.FinishedMessage;
import net.luminis.tls.handshake.TlsClientEngine;
import net.luminis.tls.handshake.TlsStatusEventHandler;

import java.io.IOException;
import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.net.ssl.X509TrustManager;

/**
 * Creates and maintains a QUIC connection with a QUIC server.
 */
public class QuicClientConnectionImpl extends QuicConnectionImpl implements ServerConnectionProxy,
        QuicClientConnection, PacketProcessor, FrameProcessorRegistry<AckFrame>,
        TlsStatusEventHandler, FrameProcessor3 {

    private static final String TAG = QuicClientConnectionImpl.class.getSimpleName();
    private final String host;
    private final String alpn;
    private final int port;
    private final QuicSessionTicket sessionTicket;
    private final TlsClientEngine tlsEngine;
    private final DatagramSocket socket;
    private final SenderImpl sender;
    private final Receiver receiver;
    private final StreamManager streamManager;
    private final X509Certificate clientCertificate;
    private final PrivateKey clientCertificateKey;
    private final ConnectionIdManager connectionIdManager;
    private final CountDownLatch handshakeFinishedCondition = new CountDownLatch(1);
    private final AtomicReference<QuicSessionTicket> newSessionTicket = new AtomicReference<>();
    private final List<CipherSuite> cipherSuites;
    private final GlobalAckGenerator ackGenerator;
    private final List<FrameProcessor2<AckFrame>> ackProcessors = new CopyOnWriteArrayList<>();
    private final TransportParameters transportParams;
    private final AtomicBoolean processedRetryPacket = new AtomicBoolean(false);
    private final AtomicReference<EarlyDataStatus> earlyDataStatus = new AtomicReference<>(None);
    private final AtomicBoolean ignoreVersionNegotiation = new AtomicBoolean();
    private final AtomicReference<KeepAliveActor> keepAliveActor = new AtomicReference<>();
    private final AtomicInteger clientHelloEnlargement = new AtomicInteger();
    private final AtomicReference<TransportParameters> peerTransportParams =
            new AtomicReference<>();
    @Nullable
    private final ServerConnector serverConnector;

    private QuicClientConnectionImpl(String alpn, String host, int port,
                                     QuicSessionTicket sessionTicket,
                                     Version version, String proxyHost,
                                     Integer initialRtt, Integer cidLength,
                                     List<CipherSuite> cipherSuites,
                                     X509Certificate clientCertificate,
                                     PrivateKey clientCertificateKey,
                                     TransportParameters transportParameters,
                                     @Nullable ServerConnector serverConnector,
                                     Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer)
            throws UnknownHostException, SocketException {
        super(version, Role.Client);
        this.alpn = alpn;
        this.host = host;
        this.port = port;
        this.sessionTicket = sessionTicket;
        this.cipherSuites = cipherSuites;
        this.clientCertificate = clientCertificate;
        this.clientCertificateKey = clientCertificateKey;
        this.transportParams = transportParameters;
        this.serverConnector = serverConnector;

        this.newSessionTicket.set(sessionTicket); // to have a valid session ticket

        if (serverConnector == null) {
            this.socket = new DatagramSocket();
            this.receiver = new Receiver(socket, datagramPacket -> {
                try {
                    parsePackets(Instant.now(), ByteBuffer.wrap(datagramPacket.getData(),
                            0, datagramPacket.getLength()));

                } catch (Exception error) {
                    LogUtils.error(TAG, "Terminating receiver loop because of error", error);
                    abortConnection(error);
                }
            }, this::abortConnection);
        } else {
            this.socket = serverConnector.getSocket();
            this.receiver = serverConnector.getReceiver();
        }

        InetAddress serverAddress = InetAddress.getByName(proxyHost != null ? proxyHost : host);
        this.sender = new SenderImpl(version, connectionSecrets, getIdleTimer(), this, getMaxPacketSize(),
                socket, new InetSocketAddress(serverAddress, port),
                initialRtt, this::abortConnection);
        sender.enableAllLevels();
        ackGenerator = sender.getGlobalAckGenerator();
        registerProcessor(ackGenerator);


        streamManager = new StreamManager(this, version, sender, getFlowController(),
                Role.Client, 10, 10, streamDataConsumer);

        BiConsumer<Integer, String> closeWithErrorFunction = (error, reason) ->
                immediateCloseWithError(EncryptionLevel.App, error, reason, false);
        connectionIdManager = new ConnectionIdManager(cidLength, 2, sender,
                closeWithErrorFunction);


        tlsEngine = new TlsClientEngine(new ClientMessageSender() {
            @Override
            public void send(ClientHello clientHello) {
                CryptoStream cryptoStream = getCryptoStream(Initial);
                cryptoStream.write(clientHello, true);
                connectionState.set(Status.Handshaking);
            }

            @Override
            public void send(FinishedMessage finished) {
                CryptoStream cryptoStream = getCryptoStream(Handshake);
                cryptoStream.write(finished, true);
            }

            @Override
            public void send(CertificateMessage certificateMessage) {
                CryptoStream cryptoStream = getCryptoStream(Handshake);
                cryptoStream.write(certificateMessage, true);
            }

            @Override
            public void send(CertificateVerifyMessage certificateVerifyMessage) {
                CryptoStream cryptoStream = getCryptoStream(Handshake);
                cryptoStream.write(certificateVerifyMessage, true);
            }
        }, this);

        connectionIdManager.setMaxPeerConnectionIds(transportParams.getActiveConnectionIdLimit());
        transportParams.setInitialSourceConnectionId(connectionIdManager.getInitialConnectionId());
    }

    public static Builder newBuilder() {
        return new BuilderImpl();
    }


    @Override
    public void connect(int timeout) throws InterruptedException, ConnectException {
        connect(timeout, Collections.emptyList(), quicStreams -> {
        });
    }

    /**
     * Set up the connection with the server, enabling use of 0-RTT data.
     * The early data is sent on a bidirectional stream and the output stream is closed immediately after sending the data
     * if <code>closeOutput</code> is set in the <code>StreamEarlyData</code>.
     * If this connection object is not in the initial state, an <code>IllegalStateException</code> will be thrown, so
     * the connect method can only be successfully called once. Use the <code>isConnected</code> method to check whether
     * it can be connected.
     *
     * @param timeout   Timeout to establish connection
     * @param earlyData early data to send (RTT-0), each element of the list will lead to a bidirectional stream
     * @param consumer  list of streams that was created for the early data; the size of the list will be equal
     *                  to the size of the list of the <code>earlyData</code> parameter, but may contain <code>null</code>s if a stream
     *                  could not be created due to reaching the max initial streams limit.
     */
    @Override
    public synchronized List<QuicStream> connect(
            int timeout, List<StreamEarlyData> earlyData, Consumer<List<QuicStream>> consumer) throws
            InterruptedException, ConnectException {


        if (connectionState.get() != Status.Created) {
            throw new ConnectException("Cannot connect a connection that is in state " + connectionState);
        }
        Objects.requireNonNull(earlyData);

        LogUtils.info(TAG, String.format("Original destination connection id: %s (scid: %s)",
                bytesToHex(connectionIdManager.getOriginalDestinationConnectionId()),
                bytesToHex(connectionIdManager.getInitialConnectionId())));


        generateInitialKeys();

        // start the services
        if (serverConnector == null) {
            receiver.start();
        } else {
            byte[] scid = connectionIdManager.getInitialConnectionId();
            byte[] shortScid = Arrays.copyOf(scid, 4);
            serverConnector.registerConnection(this, scid);
            serverConnector.registerConnection(this, shortScid);
        }
        sender.start();

        startHandshake(alpn, !earlyData.isEmpty());

        List<QuicStream> earlyDataStreams = sendEarlyData(earlyData);

        try {
            boolean handshakeFinished = handshakeFinishedCondition.await(timeout, TimeUnit.SECONDS);
            if (!handshakeFinished) {
                abortHandshake();
                throw new ConnectException("Connection timed out after " + timeout + " s");
            } else if (connectionState.get() != Status.Connected) {
                abortHandshake();
                throw new ConnectException("Handshake error");
            }
        } catch (InterruptedException e) {
            abortHandshake();
            throw e;  // Should not happen.
        }

        if (!earlyData.isEmpty()) {
            if (earlyDataStatus.get() != Accepted) {
                LogUtils.info(TAG, "Server did not accept early data; " +
                        "retransmitting all data.");
            }
            for (QuicStream stream : earlyDataStreams) {
                if (stream != null) {
                    ((EarlyDataStream) stream).writeRemaining(
                            earlyDataStatus.get() == Accepted);
                }
            }
        }
        return earlyDataStreams;
    }

    private List<QuicStream> sendEarlyData(List<StreamEarlyData> streamEarlyDataList) {
        if (!streamEarlyDataList.isEmpty()) {
            TransportParameters rememberedTransportParameters = new TransportParameters();
            sessionTicket.copyTo(rememberedTransportParameters);
            setPeerTransportParameters(rememberedTransportParameters, false);  // Do not validate TP, as these are yet incomplete.
            // https://tools.ietf.org/html/draft-ietf-quic-tls-27#section-4.5
            // "the amount of data which the client can send in 0-RTT is controlled by the "initial_max_data"
            //   transport parameter supplied by the server"
            long earlyDataSizeLeft = sessionTicket.getInitialMaxData();

            List<QuicStream> earlyDataStreams = new ArrayList<>();
            for (StreamEarlyData streamEarlyData : streamEarlyDataList) {

                EarlyDataStream earlyDataStream = streamManager.createEarlyDataStream(/*Todo*/null, true);
                if (earlyDataStream != null) {
                    earlyDataStream.writeEarlyData(streamEarlyData.data, streamEarlyData.closeOutput, earlyDataSizeLeft);
                    earlyDataSizeLeft = Long.max(0, earlyDataSizeLeft - streamEarlyData.data.length);
                } else {
                    LogUtils.info(TAG, "Creating early data stream failed, max bidi streams = "
                            + rememberedTransportParameters.getInitialMaxStreamsBidi());
                }
                earlyDataStreams.add(earlyDataStream);
            }
            earlyDataStatus.set(Requested);
            return earlyDataStreams;
        } else {
            return Collections.emptyList();
        }
    }

    private void abortHandshake() {
        connectionState.set(Status.Failed);
        sender.stop();
        terminate();
    }

    @Override
    public void keepAlive(int pingInterval) {
        if (!connectionState.get().isConnected()) {
            throw new IllegalStateException("keep alive can only be set when connected");
        }

        if (keepAliveActor.get() != null) {
            throw new IllegalStateException("keep alive is already set");
        }

        keepAliveActor.set(new KeepAliveActor(pingInterval, sender));
    }

    public void ping() {
        if (connectionState.get().isConnected()) {
            throw new IllegalStateException("not connected");
        }
        sender.send(new PingFrame(), App);
        sender.flush();
    }

    private void generateInitialKeys() {
        connectionSecrets.computeInitialKeys(connectionIdManager.getCurrentPeerConnectionId());
    }

    private void startHandshake(String applicationProtocol, boolean withEarlyData) {
        tlsEngine.setServerName(host);
        tlsEngine.addSupportedCiphers(cipherSuites);
        if (clientCertificate != null && clientCertificateKey != null) {
            tlsEngine.setClientCertificateCallback(authorities -> {
                if (!authorities.contains(clientCertificate.getIssuerX500Principal())) {
                    LogUtils.warning(TAG, "Client certificate is not signed by one " +
                            "of the requested authorities: " + authorities);
                }
                return new CertificateWithPrivateKey(clientCertificate, clientCertificateKey);
            });
        }

        QuicTransportParametersExtension tpExtension = new QuicTransportParametersExtension(
                quicVersion, transportParams, Role.Client);
        if (clientHelloEnlargement.get() > 0) {
            tpExtension.addDiscardTransportParameter(clientHelloEnlargement.get());
        }
        tlsEngine.add(tpExtension);
        tlsEngine.add(new ApplicationLayerProtocolNegotiationExtension(applicationProtocol));
        if (withEarlyData) {
            tlsEngine.add(new EarlyDataExtension());
        }
        if (sessionTicket != null) {
            tlsEngine.setNewSessionTicket(sessionTicket);
        }

        try {
            tlsEngine.startHandshake();
        } catch (IOException e) {
            // Will not happen, as our ClientMessageSender implementation will not throw.
            LogUtils.error(TAG, e);
        }
    }

    @Override
    public void earlySecretsKnown() {
        connectionSecrets.computeEarlySecrets(tlsEngine);
    }

    @Override
    public void handshakeSecretsKnown() {
        // Server Hello provides a new secret, so:
        connectionSecrets.computeHandshakeSecrets(tlsEngine, tlsEngine.getSelectedCipher());
        hasHandshakeKeys();
    }

    public void hasHandshakeKeys() {
        HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
            if (handshakeState.transitionAllowed(HandshakeState.HasHandshakeKeys)) {
                return HandshakeState.HasHandshakeKeys;
            }
            return handshakeState;
        });

        if (state == HandshakeState.HasHandshakeKeys) {
            handshakeStateListeners.forEach(l -> l.handshakeStateChangedEvent(
                    HandshakeState.HasHandshakeKeys));
        } else {
            LogUtils.error(TAG, "Handshake state cannot be set to HasHandshakeKeys");
        }

        // https://tools.ietf.org/html/draft-ietf-quic-tls-29#section-4.11.1
        // "Thus, a client MUST discard Initial keys when it first sends a Handshake packet (...). This results in
        //  abandoning loss recovery state for the Initial encryption level and ignoring any outstanding Initial packets."
        // This is done as post-processing action to ensure ack on Initial level is sent.
        postProcessingActions.add(this::discard);
    }

    @Override
    public void handshakeFinished() {
        connectionSecrets.computeApplicationSecrets(tlsEngine, tlsEngine.getSelectedCipher());

        HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
            if (handshakeState.transitionAllowed(HandshakeState.HasAppKeys)) {
                return HandshakeState.HasAppKeys;
            }
            return handshakeState;
        });

        if (state == HandshakeState.HasAppKeys) {
            handshakeStateListeners.forEach(l -> l.handshakeStateChangedEvent(
                    HandshakeState.HasAppKeys));
        } else {
            LogUtils.error(TAG, "Handshake state cannot be set to HasAppKeys");
        }


        connectionState.set(Status.Connected);
        handshakeFinishedCondition.countDown();
    }

    @Override
    public void newSessionTicketReceived(NewSessionTicket ticket) {
        addNewSessionTicket(ticket);
    }

    @Override
    public void extensionsReceived(List<Extension> extensions) {
        extensions.forEach(ex -> {
            if (ex instanceof EarlyDataExtension) {
                setEarlyDataStatus(EarlyDataStatus.Accepted);
                LogUtils.info(TAG, "Server has accepted early data.");
            } else if (ex instanceof QuicTransportParametersExtension) {
                setPeerTransportParameters(((QuicTransportParametersExtension) ex).getTransportParameters());
            }
        });
    }

    @Override
    public boolean isEarlyDataAccepted() {
        return false;
    }

    private void discard() {
        sender.discard(PnSpace.Initial, "first Handshake message is being sent");
    }

    @Override
    public ProcessResult process(InitialPacket packet, Instant time) {
        connectionIdManager.registerInitialPeerCid(packet.getSourceConnectionId());
        processFrames(packet, time);
        ignoreVersionNegotiation.set(true);
        return ProcessResult.Continue;
    }

    @Override
    public ProcessResult process(HandshakePacket packet, Instant time) {
        processFrames(packet, time);
        return ProcessResult.Continue;
    }

    @Override
    public ProcessResult process(ShortHeaderPacket packet, Instant time) {
        connectionIdManager.registerConnectionIdInUse(packet.getDestinationConnectionId());
        processFrames(packet, time);
        return ProcessResult.Continue;
    }

    @Override
    public ProcessResult process(VersionNegotiationPacket vnPacket, Instant time) {
        if (!ignoreVersionNegotiation.get() &&
                !vnPacket.getServerSupportedVersions().contains(quicVersion)) {
            LogUtils.error(TAG, "Server doesn't support " + quicVersion + ", but only: " +
                    vnPacket.getServerSupportedVersions().stream().map(Version::toString).
                            collect(Collectors.joining(", ")));
            // throw new VersionNegotiationFailure(); // ReWi
        } else {
            // Must be a corrupted packet or sent because of a corrupted packet, so ignore.
            LogUtils.error(TAG, "Ignoring Version Negotiation packet");
        }
        return ProcessResult.Continue;
    }

    @Override
    public ProcessResult process(RetryPacket packet, Instant time) {
        if (packet.validateIntegrityTag(connectionIdManager.getOriginalDestinationConnectionId())) {
            if (!processedRetryPacket.getAndSet(true)) {
                // https://tools.ietf.org/html/draft-ietf-quic-transport-18#section-17.2.5
                // "A client MUST accept and process at most one Retry packet for each
                //   connection attempt.  After the client has received and processed an
                //   Initial or Retry packet from the server, it MUST discard any
                //   subsequent Retry packets that it receives."

                byte[] token = packet.getRetryToken();
                sender.setInitialToken(token);
                getCryptoStream(Initial).reset();  // Stream offset should restart from 0.
                byte[] peerConnectionId = packet.getSourceConnectionId();
                connectionIdManager.registerInitialPeerCid(peerConnectionId);
                connectionIdManager.registerRetrySourceConnectionId(peerConnectionId);

                generateInitialKeys();

                // https://tools.ietf.org/html/draft-ietf-quic-recovery-18#section-6.2.1.1
                // "A Retry or Version Negotiation packet causes a client to send another
                //   Initial packet, effectively restarting the connection process and
                //   resetting congestion control..."
                sender.getCongestionController().reset();

                try {
                    tlsEngine.startHandshake();
                } catch (IOException ioException) {
                    // Will not happen, as our ClientMessageSender implementation will not throw.
                    LogUtils.error(TAG, ioException);
                }
            } else {
                LogUtils.error(TAG, "Ignoring RetryPacket, because already processed one.");
            }
        } else {
            LogUtils.error(TAG, "Discarding Retry packet, because integrity tag is invalid.");
        }
        return ProcessResult.Continue;
    }

    @Override
    public ProcessResult process(ZeroRttPacket packet, Instant time) {
        // Intentionally discarding packet without any action (servers should not send 0-RTT packets).
        return ProcessResult.Abort;
    }

    @Override
    public void process(AckFrame ackFrame, QuicPacket packet, Instant timeReceived) {
        TransportParameters transportCwnd = peerTransportParams.get();
        if (transportCwnd != null) {
            ackFrame.setDelayExponent(transportCwnd.getAckDelayExponent());
        }
        ackProcessors.forEach(p -> p.process(ackFrame, packet.getPnSpace(), timeReceived));
    }

    @Override
    public void process(HandshakeDoneFrame handshakeDoneFrame, QuicPacket packet, Instant timeReceived) {

        HandshakeState state = handshakeState.updateAndGet(handshakeState -> {
            if (handshakeState.transitionAllowed(HandshakeState.Confirmed)) {
                return HandshakeState.Confirmed;
            }
            return handshakeState;
        });

        if (state == HandshakeState.Confirmed) {
            handshakeStateListeners.forEach(l -> l.handshakeStateChangedEvent(
                    HandshakeState.Confirmed));
        } else {
            LogUtils.error(TAG, "Handshake state cannot be set to Confirmed");
        }

        sender.discard(PnSpace.Handshake, "HandshakeDone is received");
        // TODO: discard handshake keys:
        // https://tools.ietf.org/html/draft-ietf-quic-tls-25#section-4.10.2
        // "An endpoint MUST discard its handshake keys when the TLS handshake is confirmed"
    }

    @Override
    public void process(NewConnectionIdFrame newConnectionIdFrame, QuicPacket packet, Instant timeReceived) {
        connectionIdManager.process(newConnectionIdFrame);
    }

    @Override
    public void process(NewTokenFrame newTokenFrame, QuicPacket packet, Instant timeReceived) {
    }

    @Override
    public void process(RetireConnectionIdFrame retireConnectionIdFrame, QuicPacket packet, Instant timeReceived) {
        connectionIdManager.process(retireConnectionIdFrame, packet.getDestinationConnectionId());
    }

    @Override
    protected void immediateCloseWithError(EncryptionLevel level, int error,
                                           String errorReason, boolean flush) {
        KeepAliveActor keepAliveActorCwnd = keepAliveActor.get();
        if (keepAliveActorCwnd != null) {
            keepAliveActorCwnd.shutdown();
        }
        super.immediateCloseWithError(level, error, errorReason, flush);
    }

    @Override
    public byte[] getOriginalDestinationConnectionId() {
        // todo wrong naming
        return connectionIdManager.getInitialConnectionId();
    }

    @Override
    public void parsePackets(Instant timeReceived, ByteBuffer data) {
        parseAndProcessPackets(timeReceived, data, null);
    }

    @Override
    public boolean isClosed() {
        return connectionState.get().isClosed();
    }

    /**
     * Closes the connection by discarding all connection state. Do not call directly, should be called after
     * closing state or draining state ends.
     */
    @Override
    public void terminate() {
        super.terminate();
        handshakeFinishedCondition.countDown();
        if (serverConnector == null) {
            receiver.shutdown();
            socket.close();
        }
    }


    public void updateKeys() {
        // https://tools.ietf.org/html/draft-ietf-quic-tls-31#section-6
        // "Once the handshake is confirmed (see Section 4.1.2), an endpoint MAY initiate a key update."
        if (handshakeState.get() == HandshakeState.Confirmed) {
            connectionSecrets.getClientSecrets(App).computeKeyUpdate(true);
        } else {
            LogUtils.error(TAG, "Refusing key update because handshake is not yet confirmed");
        }
    }

    @Override
    public int getMaxShortHeaderPacketOverhead() {
        return 1  // flag byte
                + connectionIdManager.getCurrentPeerConnectionId().length
                + 4  // max packet number size, in practice this will be mostly 1
                + 16 // encryption overhead
                ;
    }

    public TransportParameters getTransportParameters() {
        return transportParams;
    }

    public TransportParameters getPeerTransportParameters() {
        return peerTransportParams.get();
    }

    void setPeerTransportParameters(TransportParameters transportParameters) {
        setPeerTransportParameters(transportParameters, true);
    }

    private void setPeerTransportParameters(TransportParameters transportParameters, boolean validate) {
        if (validate) {
            if (!verifyConnectionIds(transportParameters)) {
                return;
            }
        }
        peerTransportParams.set(transportParameters);
        FlowControl flowController = getFlowController();
        if (!flowController.isInit()) {
            flowController.init(transportParameters.getInitialMaxData(),
                    transportParameters.getInitialMaxStreamDataBidiLocal(),
                    transportParameters.getInitialMaxStreamDataBidiRemote(),
                    transportParameters.getInitialMaxStreamDataUni()
            );
        } else {
            // If the client has sent 0-rtt, the flow controller will already have been initialized with "remembered" values
            LogUtils.error(TAG, "Updating flow controller with new transport parameters");
            // TODO: this should be postponed until all 0-rtt packets are sent
            flowController.updateInitialValues(transportParameters);
        }

        streamManager.setInitialMaxStreamsBidi(transportParameters.getInitialMaxStreamsBidi());
        streamManager.setInitialMaxStreamsUni(transportParameters.getInitialMaxStreamsUni());

        sender.setReceiverMaxAckDelay(transportParameters.getMaxAckDelay());
        connectionIdManager.registerPeerCidLimit(transportParameters.getActiveConnectionIdLimit());

        determineIdleTimeout(transportParams.getMaxIdleTimeout(), transportParameters.getMaxIdleTimeout());

        connectionIdManager.setInitialStatelessResetToken(transportParameters.getStatelessResetToken());

        if (processedRetryPacket.get()) {
            if (transportParameters.getRetrySourceConnectionId() == null ||
                    !connectionIdManager.validateRetrySourceConnectionId(
                            transportParameters.getRetrySourceConnectionId())) {
                immediateCloseWithError(Handshake, TRANSPORT_PARAMETER_ERROR.value,
                        "incorrect retry_source_connection_id transport parameter",
                        false);
            }
        } else {
            if (transportParameters.getRetrySourceConnectionId() != null) {
                immediateCloseWithError(Handshake, TRANSPORT_PARAMETER_ERROR.value,
                        "unexpected retry_source_connection_id transport parameter",
                        false);
            }
        }
    }

    private boolean verifyConnectionIds(TransportParameters transportParameters) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat absence of the initial_source_connection_id
        //   transport parameter from either endpoint or absence of the
        //   original_destination_connection_id transport parameter from the
        //   server as a connection error of type TRANSPORT_PARAMETER_ERROR."
        if (transportParameters.getInitialSourceConnectionId() == null || transportParameters.getOriginalDestinationConnectionId() == null) {
            LogUtils.error(TAG, "Missing connection id from server transport parameter");
            if (transportParameters.getInitialSourceConnectionId() == null) {
                immediateCloseWithError(Handshake, TRANSPORT_PARAMETER_ERROR.value,
                        "missing initial_source_connection_id transport parameter",
                        false);
            } else {
                immediateCloseWithError(Handshake, TRANSPORT_PARAMETER_ERROR.value,
                        "missing original_destination_connection_id transport parameter",
                        false);
            }
            return false;
        }

        // https://tools.ietf.org/html/draft-ietf-quic-transport-29#section-7.3
        // "An endpoint MUST treat the following as a connection error of type TRANSPORT_PARAMETER_ERROR or PROTOCOL_VIOLATION:
        //   *  a mismatch between values received from a peer in these transport parameters and the value sent in the
        //      corresponding Destination or Source Connection ID fields of Initial packets."
        if (!Arrays.equals(connectionIdManager.getCurrentPeerConnectionId(), transportParameters.getInitialSourceConnectionId())) {
            LogUtils.error(TAG, "Source connection id does not match corresponding transport parameter");
            immediateCloseWithError(Handshake, PROTOCOL_VIOLATION.value,
                    "initial_source_connection_id transport parameter does not match",
                    false);
            return false;
        }
        if (!Arrays.equals(connectionIdManager.getOriginalDestinationConnectionId(), transportParameters.getOriginalDestinationConnectionId())) {
            LogUtils.error(TAG, "Original destination connection id does not match corresponding transport parameter");
            immediateCloseWithError(Handshake, PROTOCOL_VIOLATION.value,
                    "original_destination_connection_id transport parameter does not match",
                    false);
            return false;
        }

        return true;
    }

    /**
     * Abort connection due to a fatal error in this client. No message is sent to peer; just inform client it's all over.
     *
     * @param error the exception that caused the trouble
     */
    public void abortConnection(Throwable error) {
        connectionState.set(Status.Closing);

        LogUtils.error(TAG, "Aborting connection " + getRemoteAddress().toString() +
                " because of error " + error.getMessage());
        handshakeFinishedCondition.countDown();
        sender.stop();
        terminate();
        streamManager.abortAll();
    }

    // https://tools.ietf.org/html/draft-ietf-quic-transport-19#section-5.1.2
    // "An endpoint can change the connection ID it uses for a peer to
    //   another available one at any time during the connection. "
    public byte[] nextDestinationConnectionId() {
        byte[] newConnectionId = connectionIdManager.nextPeerId();
        if (newConnectionId == null) {
            LogUtils.error(TAG, "Cannot switch to next destination connection " +
                    "id: no connection id's available");
        }
        return newConnectionId;
    }

    @Override
    protected boolean checkForStatelessResetToken(ByteBuffer data) {
        byte[] tokenCandidate = new byte[16];
        data.position(data.limit() - 16);
        data.get(tokenCandidate);
        return connectionIdManager.isStatelessResetToken(tokenCandidate);
    }

    public byte[][] newConnectionIds(int count, int retirePriorTo) {
        byte[][] newConnectionIds = new byte[count][];

        for (int i = 0; i < count; i++) {
            ConnectionIdInfo cid = connectionIdManager.sendNewConnectionId(retirePriorTo);
            if (cid != null) {
                newConnectionIds[i] = cid.getConnectionId();
                LogUtils.debug(TAG, "New generated source connection id" +
                        Arrays.toString(cid.getConnectionId()));
            }
        }
        sender.flush();

        return newConnectionIds;
    }

    public void retireDestinationConnectionId(Integer sequenceNumber) {
        connectionIdManager.retireConnectionId(sequenceNumber);
    }

    @Override
    protected SenderImpl getSender() {
        return sender;
    }

    @Override
    protected GlobalAckGenerator getAckGenerator() {
        return ackGenerator;
    }

    @Override
    protected TlsClientEngine getTlsEngine() {
        return tlsEngine;
    }

    @Override
    protected StreamManager getStreamManager() {
        return streamManager;
    }

    @Override
    protected int getSourceConnectionIdLength() {
        return connectionIdManager.getConnectionIdLength();
    }

    @Override
    public byte[] getSourceConnectionId() {
        return connectionIdManager.getCurrentConnectionId();
    }

    public Map<Integer, ConnectionIdInfo> getSourceConnectionIds() {
        return connectionIdManager.getAllConnectionIds();
    }

    @Override
    public byte[] getDestinationConnectionId() {
        return connectionIdManager.getCurrentPeerConnectionId();
    }

    public Map<Integer, ConnectionIdInfo> getDestinationConnectionIds() {
        return connectionIdManager.getAllPeerConnectionIds();
    }


    // For internal use only.
    @Override
    public long getInitialMaxStreamData() {
        return transportParams.getInitialMaxStreamDataBidiLocal();
    }

    @Override
    public void setMaxAllowedBidirectionalStreams(int max) {
        transportParams.setInitialMaxStreamsBidi(max);
    }

    @Override
    public void setMaxAllowedUnidirectionalStreams(int max) {
        transportParams.setInitialMaxStreamsUni(max);
    }

    @Override
    public void setDefaultStreamReceiveBufferSize(long size) {
        transportParams.setInitialMaxStreamData(size);
    }


    public void addNewSessionTicket(NewSessionTicket tlsSessionTicket) {
        if (tlsSessionTicket.hasEarlyDataExtension()) {
            if (tlsSessionTicket.getEarlyDataMaxSize() != 0xffffffffL) {
                // https://tools.ietf.org/html/draft-ietf-quic-tls-24#section-4.5
                // "Servers MUST NOT send
                //   the "early_data" extension with a max_early_data_size set to any
                //   value other than 0xffffffff.  A client MUST treat receipt of a
                //   NewSessionTicket that contains an "early_data" extension with any
                //   other value as a connection error of type PROTOCOL_VIOLATION."
                LogUtils.error(TAG, "Invalid quic new session ticket " +
                        "(invalid early data size); ignoring ticket.");
            }
        }
        newSessionTicket.set(new QuicSessionTicket(tlsSessionTicket, peerTransportParams.get()));
    }

    @Nullable
    @Override
    public QuicSessionTicket getSessionTicket() {
        return newSessionTicket.get();
    }

    public EarlyDataStatus getEarlyDataStatus() {
        return earlyDataStatus.get();
    }

    public void setEarlyDataStatus(EarlyDataStatus earlyDataStatus) {
        this.earlyDataStatus.set(earlyDataStatus);
    }

    public URI getUri() {
        try {
            return new URI("//" + host + ":" + port);
        } catch (URISyntaxException e) {
            // Impossible
            throw new IllegalStateException();
        }
    }

    @Override
    public void registerProcessor(FrameProcessor2<AckFrame> ackProcessor) {
        ackProcessors.add(ackProcessor);
    }


    @Override
    public InetSocketAddress getServerAddress() {
        return new InetSocketAddress(host, port);
    }


    public void trustManager(X509TrustManager trustManager) {
        tlsEngine.setTrustManager(trustManager);
        tlsEngine.setHostnameVerifier((hostname, serverCertificate) -> true);
    }

    private void enableQuantumReadinessTest(int nrDummyBytes) {
        clientHelloEnlargement.set(nrDummyBytes);
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return getServerAddress();
    }

    public interface Builder {
        QuicClientConnectionImpl build(Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) throws ConnectException;

        Builder connectTimeout(Duration duration);

        Builder version(Version version);

        Builder sessionTicket(QuicSessionTicket ticket);

        Builder proxy(String host);

        Builder uri(URI uri);

        Builder host(String host);

        Builder port(int port);

        Builder connectionIdLength(int length);

        Builder initialRtt(int initialRtt);

        Builder cipherSuite(CipherSuite cipherSuite);

        Builder trustManager(X509TrustManager trustManager);

        Builder quantumReadinessTest(int nrOfDummyBytes);

        Builder clientCertificate(X509Certificate certificate);

        Builder clientCertificateKey(PrivateKey privateKey);

        Builder transportParams(TransportParameters transportParams);

        Builder alpn(String alpn);

        Builder serverConnector(ServerConnector server);
    }

    private static class BuilderImpl implements Builder {
        private final List<CipherSuite> cipherSuites = new ArrayList<>();
        private String host;
        private int port;
        private QuicSessionTicket sessionTicket;
        private Version quicVersion = Version.getDefault();
        private String proxyHost;
        private Integer initialRtt;
        private Integer connectionIdLength;
        private X509TrustManager trustManager;
        private Integer quantumReadinessTest;
        private X509Certificate clientCertificate;
        private PrivateKey clientCertificateKey;
        private TransportParameters transportParams;
        private String alpn;
        private ServerConnector serverConnector;

        @Override
        public QuicClientConnectionImpl build(
                Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) throws ConnectException {
            if (!quicVersion.atLeast(Version.IETF_draft_29)) {
                throw new IllegalArgumentException("Quic version " + quicVersion + " not supported");
            }
            if (host == null) {
                throw new IllegalStateException("Cannot create connection when URI is not set");
            }
            if (initialRtt != null && initialRtt < 1) {
                throw new IllegalArgumentException("Initial RTT must be larger than 0.");
            }
            if (cipherSuites.isEmpty()) {
                cipherSuites.add(CipherSuite.TLS_AES_128_GCM_SHA256);
            }
            if (transportParams == null) {
                throw new IllegalStateException("Connection requires transport parameters");
            }
            if (alpn == null) {
                throw new IllegalStateException("Connection requires application protocol");
            }
            try {
                QuicClientConnectionImpl quicConnection =
                        new QuicClientConnectionImpl(alpn, host, port, sessionTicket, quicVersion,
                                proxyHost, initialRtt, connectionIdLength, cipherSuites,
                                clientCertificate, clientCertificateKey, transportParams,
                                serverConnector, streamDataConsumer);

                if (trustManager != null) {
                    quicConnection.trustManager(trustManager);
                }
                if (quantumReadinessTest != null) {
                    quicConnection.enableQuantumReadinessTest(quantumReadinessTest);
                }

                return quicConnection;

            } catch (Throwable throwable) {
                throw new ConnectException(throwable.getMessage());
            }

        }

        @Override
        public Builder connectTimeout(Duration duration) {
            return this;
        }

        @Override
        public Builder alpn(String alpn) {
            this.alpn = alpn;
            return this;
        }

        @Override
        public Builder serverConnector(ServerConnector server) {
            this.serverConnector = server;
            return this;
        }

        @Override
        public Builder transportParams(TransportParameters transportParams) {
            this.transportParams = transportParams;
            return this;
        }

        @Override
        public Builder version(Version version) {
            quicVersion = version;
            return this;
        }

        @Override
        public Builder sessionTicket(QuicSessionTicket ticket) {
            sessionTicket = ticket;
            return this;
        }

        @Override
        public Builder proxy(String host) {
            proxyHost = host;
            return this;
        }


        @Override
        public Builder uri(URI uri) {
            host = uri.getHost();
            port = uri.getPort();
            return this;
        }

        @Override
        public Builder host(String host) {
            this.host = host;
            return this;
        }

        @Override
        public Builder port(int port) {
            this.port = port;
            return this;
        }

        @Override
        public Builder connectionIdLength(int length) {
            if (length < 0 || length > 20) {
                throw new IllegalArgumentException("Connection ID length must between 0 and 20.");
            }
            connectionIdLength = length;
            return this;
        }

        @Override
        public Builder initialRtt(int initialRtt) {
            this.initialRtt = initialRtt;
            return this;
        }

        @Override
        public Builder cipherSuite(CipherSuite cipherSuite) {
            cipherSuites.add(cipherSuite);
            return this;
        }

        @Override
        public Builder trustManager(X509TrustManager trustManager) {
            this.trustManager = trustManager;
            return this;
        }

        @Override
        public Builder quantumReadinessTest(int nrOfDummyBytes) {
            this.quantumReadinessTest = nrOfDummyBytes;
            return this;
        }

        @Override
        public Builder clientCertificate(X509Certificate certificate) {
            this.clientCertificate = certificate;
            return this;
        }

        @Override
        public Builder clientCertificateKey(PrivateKey privateKey) {
            this.clientCertificateKey = privateKey;
            return this;
        }
    }


}
