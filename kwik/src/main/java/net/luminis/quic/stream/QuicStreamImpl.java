/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.stream;

import static net.luminis.quic.EncryptionLevel.App;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.LogUtils;
import net.luminis.quic.QuicConnection;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.Version;
import net.luminis.quic.frame.DataBlockedFrame;
import net.luminis.quic.frame.MaxStreamDataFrame;
import net.luminis.quic.frame.QuicFrame;
import net.luminis.quic.frame.ResetStreamFrame;
import net.luminis.quic.frame.StopSendingFrame;
import net.luminis.quic.frame.StreamDataBlockedFrame;
import net.luminis.quic.frame.StreamFrame;
import net.luminis.quic.send.Sender;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;


public class QuicStreamImpl implements QuicStream, FlowControlUpdateListener {
    protected static final float receiverMaxDataIncrementFactor = 0.10f;
    private static final String TAG = QuicStreamImpl.class.getSimpleName();
    // Minimum stream frame size: frame type (1), stream id (1..8), offset (1..8), length (1..2), data (1...)
    // Note that in practice stream id and offset will seldom / never occupy 8 bytes, so the minimum leaves more room for data.
    private static final int MIN_FRAME_SIZE = 1 + 8 + 8 + 2 + 1;
    protected final Version quicVersion;
    protected final int streamId;
    protected final FlowControl flowController;
    private final long receiverMaxDataIncrement;

    private final ConcurrentHashMap<String, Object> attributes = new ConcurrentHashMap<>();

    private final AtomicBoolean allDataReceived = new AtomicBoolean(false);
    // not required to be thread safe, only invoked from the receiver thread
    private final SortedSet<StreamFrame> frames = new TreeSet<>(); // no concurrency

    private final ByteBuffer END_OF_STREAM_MARKER = ByteBuffer.allocate(0);

    // Send request queued indicates whether a request to send a stream frame is queued with the sender.
    // Is used to avoid multiple requests being queued.
    // Thread safety: read/set by caller and by sender thread, so must be synchronized; guarded by lock
    private final ReentrantLock sendRequestLock = new ReentrantLock();
    // Send queue contains stream bytes to send in order. The position of the first byte buffer in the queue determines the next byte(s) to send.
    private final Queue<ByteBufferFuture> sendQueue = new ConcurrentLinkedDeque<>();
    //private final int maxBufferSize;
    private final AtomicInteger bufferedBytes;
    // Current offset is the offset of the next byte in the stream that will be sent.
    // Thread safety: only used by sender thread, so no synchronization needed.
    private final AtomicLong currentOffset = new AtomicLong(0);
    private final Sender sender;
    // Closed indicates whether the OutputStream is closed, meaning that no more bytes can be written by caller.
    // Thread safety: only use by caller
    private final AtomicBoolean closed = new AtomicBoolean(false);
    // Reset indicates whether the OutputStream has been reset.
    private final AtomicBoolean reset = new AtomicBoolean(false);
    private final AtomicLong resetErrorCode = new AtomicLong();
    @NonNull
    private final Consumer<RawStreamData> streamDataConsumer;
    private final QuicConnection connection;
    private long receiverFlowControlLimit; // no concurrency
    private long lastCommunicatedMaxData; // no concurrency
    private long processedToOffset = 0; // no concurrency
    // Stream offset at which the stream was last blocked, for detecting the first time stream is blocked at a certain offset.
    private long blockedOffset; // no concurrency

    public QuicStreamImpl(QuicConnection connection,
                          Version quicVersion, int streamId, Sender sender,
                          FlowControl flowController,
                          Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) {
        this.connection = connection;
        this.quicVersion = quicVersion;
        this.streamId = streamId;
        this.sender = sender;
        this.flowController = flowController;
        this.bufferedBytes = new AtomicInteger();


        this.flowController.register(streamId, this);
        this.flowController.streamOpened(this);

        this.receiverFlowControlLimit = flowController.getInitialMaxStreamDataBidiRemote();
        this.lastCommunicatedMaxData = receiverFlowControlLimit;
        this.receiverMaxDataIncrement = (long) (receiverFlowControlLimit * receiverMaxDataIncrementFactor);
        this.streamDataConsumer = streamDataConsumer.apply(this);
    }


    void add(StreamFrame frame) {
        boolean added = addFrame(frame);
        if (added) {
            broadcast();
        }
    }

    private void broadcast() {

        int bytesRead = 0;

        Iterator<StreamFrame> iterator = frames.iterator();
        boolean isFinal = false;
        while (iterator.hasNext()) {
            StreamFrame nextFrame = iterator.next();
            if (nextFrame.getOffset() <= processedToOffset) {
                if (nextFrame.getUpToOffset() > processedToOffset) {

                    bytesRead += nextFrame.getLength();

                    try {
                        streamDataConsumer.accept(new RawStreamData() {
                            @Override
                            public QuicStream getStream() {
                                return QuicStreamImpl.this;
                            }

                            @Override
                            public ByteBuffer getStreamData() {
                                return nextFrame.getStreamData();
                            }

                            @Override
                            public boolean isFinal() {
                                return nextFrame.isFinal();
                            }

                            @Override
                            public boolean isTerminated() {
                                return false;
                            }
                        });
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable.getMessage());
                    }

                    processedToOffset = nextFrame.getUpToOffset();

                    if (nextFrame.isFinal()) {
                        isFinal = true;
                    }
                }
            } else {
                break;
            }
        }

        removeParsedFrames();

        if (bytesRead > 0) {
            updateAllowedFlowControl(bytesRead);
        }

        if (frames.isEmpty()) {
            this.allDataReceived.set(isFinal);
        }
    }

    private void updateAllowedFlowControl(int bytesRead) {
        // Slide flow control window forward (with as much bytes as are read)
        receiverFlowControlLimit += bytesRead;
        // Avoid sending flow control updates with every single read; check diff with last send max data
        if (receiverFlowControlLimit - lastCommunicatedMaxData > receiverMaxDataIncrement) {
            sender.send(new MaxStreamDataFrame(streamId, receiverFlowControlLimit), App, this::retransmitMaxData);
            sender.flush();
            lastCommunicatedMaxData = receiverFlowControlLimit;
        }
    }

    private void retransmitMaxData(QuicFrame lostFrame) {
        sender.send(new MaxStreamDataFrame(streamId, receiverFlowControlLimit), App, this::retransmitMaxData);
        LogUtils.error(TAG, "Retransmitted max stream data, because lost frame " + lostFrame);
    }

    @Override
    public int getStreamId() {
        return streamId;
    }

    @Override
    public boolean isUnidirectional() {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-23#section-2.1
        // "The second least significant bit (0x2) of the stream ID distinguishes
        //   between bidirectional streams (with the bit set to 0) and
        //   unidirectional streams (with the bit set to 1)."
        return (streamId & 0x0002) == 0x0002;
    }

    @Override
    public boolean isClientInitiatedBidirectional() {
        // "Client-initiated streams have even-numbered stream IDs (with the bit set to 0)"
        return (streamId & 0x0003) == 0x0000;
    }

    @Override
    public boolean isServerInitiatedBidirectional() {
        // "server-initiated streams have odd-numbered stream IDs"
        return (streamId & 0x0003) == 0x0001;
    }

    @Override
    public void closeInput() {
        // Note that QUIC specification does not define application protocol error codes.
        // By absence of an application specified error code, the arbitrary code 0 is used.
        stopInput(0);
    }

    /**
     * https://www.rfc-editor.org/rfc/rfc9000.html#name-operations-on-streams
     * "reset the stream (abrupt termination), resulting in a RESET_STREAM frame (Section 19.4) if the stream was
     * not already in a terminal state."
     */
    public void stopSendingStream(long errorCode) {
        if (!closed.get() && !reset.get()) {
            reset.set(true);
            resetErrorCode.set(errorCode);
            // Use sender callback to ensure current offset used in reset frame is accessed by sender thread.
            sender.send(this::createResetFrame, ResetStreamFrame.getMaximumFrameSize(streamId, errorCode),
                    App, this::retransmitResetFrame);
            sender.flush();
            // Ensure write is not blocked because of full write buffer
        }
    }

    @NonNull
    @Override
    public String toString() {
        return "Stream " + streamId;
    }

    /**
     * Terminates the receiving input stream (abruptly). Is called when peer sends a RESET_STREAM frame
     * <p>
     * This method is intentionally package-protected, as it should only be called by the StreamManager class.
     */
    void terminateStream(long errorCode, long finalSize) {

        streamDataConsumer.accept(new RawStreamData() {
            @Override
            public QuicStream getStream() {
                return QuicStreamImpl.this;
            }

            @Override
            public ByteBuffer getStreamData() {
                return ByteBuffer.allocate(0);
            }

            @Override
            public boolean isFinal() {
                return false;
            }

            @Override
            public boolean isTerminated() {
                return true;
            }
        });

        terminate();
    }

    private void stopInput(long errorCode) {
        if (notAllDataReceived()) {
            sender.send(new StopSendingFrame(streamId, errorCode), App, this::retransmitStopInput);
            sender.flush();
        }
    }

    private void retransmitStopInput(QuicFrame lostFrame) {

        if (notAllDataReceived()) {
            sender.send(lostFrame, App, this::retransmitStopInput);
        }
    }

    /**
     * Determines whether not all data is received
     *
     * @return true if all not data has been received, false otherwise
     */
    protected boolean notAllDataReceived() {
        return !allDataReceived.get();
    }

    private void clearOutputStream() {
        sendQueue.forEach(byteBufferFuture -> byteBufferFuture.getFuture().completeExceptionally(
                new Exception("Output stream aborted")));
        sendQueue.clear();
    }

    public void terminate() {
        reset.compareAndSet(false, true);
        clearOutputStream();
        flowController.unregister(streamId);
        flowController.streamClosed(streamId);
    }

    /**
     * Resets the output stream so data can again be send from the start of the stream (offset 0). Note that in such
     * cases the caller must (again) provide the data to be sent.
     */
    protected void resetOutputStream() {
        // TODO: this is currently not thread safe, see comment in EarlyDataStream how to fix.
        restart();
    }

    @Override
    public CompletableFuture<QuicStream> writeOutput(byte[] data) {
        CompletableFuture<QuicStream> future = new CompletableFuture<>();
        checkState(future);
        sendQueue.add(new ByteBufferFuture(ByteBuffer.wrap(data), future));
        bufferedBytes.getAndAdd(data.length);
        sendNotification();
        return future;
    }

    @Override
    public QuicConnection getConnection() {
        return this.connection;
    }

    @Override
    public void setAttribute(String key, Object value) {
        attributes.put(key, value);
    }

    @Nullable
    @Override
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    @Override
    public CompletableFuture<QuicStream> closeOutput() {
        CompletableFuture<QuicStream> future = new CompletableFuture<>();
        if (!closed.get() && !reset.get()) {
            sendQueue.add(new ByteBufferFuture(END_OF_STREAM_MARKER, future));
            sendNotification();
            closed.set(true);
        } else {
            future.complete(this);
        }
        return future;
    }

    private void checkState(CompletableFuture<QuicStream> future) {
        if (closed.get() || reset.get()) {
            future.completeExceptionally(new IOException("output stream " +
                    (closed.get() ? "already closed" : "is reset")));
        }
    }

    private void sendNotification() {
        sendRequestLock.lock();
        try {
            sender.send(this::sendFrame, MIN_FRAME_SIZE, App, this::retransmitStreamFrame);
            sender.flush();
        } finally {
            sendRequestLock.unlock();
        }
    }

    QuicFrame sendFrame(int maxFrameSize) {
        if (reset.get()) {
            return null;
        }

        if (!sendQueue.isEmpty()) {
            long flowControlLimit = flowController.getFlowControlLimit(getStreamId());

            if (flowControlLimit < 0) {
                return null;
            }

            int maxBytesToSend = bufferedBytes.get();
            long currentOffsetTmp = currentOffset.get();
            if (flowControlLimit > currentOffsetTmp || maxBytesToSend == 0) {
                int nrOfBytes = 0;
                int dummyFrameLength = StreamFrame.frameLength(streamId,
                        currentOffsetTmp, 0);

                maxBytesToSend = Integer.min(maxBytesToSend, maxFrameSize - dummyFrameLength - 1);  // Take one byte extra for length field var int
                int maxAllowedByFlowControl = (int) (flowController.increaseFlowControlLimit(
                        getStreamId(), currentOffsetTmp + maxBytesToSend) - currentOffsetTmp);
                if (maxAllowedByFlowControl < 0) {
                    return null;
                }
                maxBytesToSend = Integer.min(maxAllowedByFlowControl, maxBytesToSend);

                byte[] dataToSend = new byte[maxBytesToSend];
                boolean finalFrame = false;
                while (nrOfBytes < maxBytesToSend && !sendQueue.isEmpty()) {
                    ByteBufferFuture bufferFuture = sendQueue.peek();
                    if (bufferFuture != null) {
                        int position = nrOfBytes;
                        if (bufferFuture.buffer.remaining() <= maxBytesToSend - nrOfBytes) {
                            // All bytes remaining in buffer will fit in stream frame
                            nrOfBytes += bufferFuture.buffer.remaining();
                            bufferFuture.buffer.get(dataToSend, position,
                                    bufferFuture.buffer.remaining());
                            ByteBufferFuture poll = sendQueue.poll();
                            if (poll != null) {
                                poll.getFuture().complete(this);
                            }
                        } else {
                            // Just part of the buffer will fit in (and will fill up) the stream frame
                            bufferFuture.buffer.get(dataToSend, position, maxBytesToSend - nrOfBytes);
                            nrOfBytes = maxBytesToSend;  // Short form of: nrOfBytes += (maxBytesToSend - nrOfBytes)
                        }
                    }
                }
                if (!sendQueue.isEmpty()) {
                    ByteBufferFuture peek = sendQueue.peek();
                    Objects.requireNonNull(peek);
                    if (peek.buffer == END_OF_STREAM_MARKER) {
                        finalFrame = true;
                        ByteBufferFuture poll = sendQueue.poll();
                        Objects.requireNonNull(poll);
                        poll.getFuture().complete(this);
                    }
                }
                if (nrOfBytes == 0 && !finalFrame) {
                    // Nothing to send really
                    return null;
                }

                bufferedBytes.getAndAdd(-1 * nrOfBytes);

                if (nrOfBytes < maxBytesToSend) {
                    // This can happen when not enough data is buffer to fill a stream frame, or length field is 1 byte (instead of 2 that was counted for)
                    dataToSend = Arrays.copyOfRange(dataToSend, 0, nrOfBytes);
                }
                StreamFrame streamFrame = new StreamFrame(streamId, currentOffsetTmp, dataToSend, finalFrame);
                currentOffset.getAndAdd(nrOfBytes);

                // todo why?
                if (!sendQueue.isEmpty()) {
                    sendNotification();
                }

                if (streamFrame.isFinal()) {
                    // Done! Retransmissions may follow, but don't need flow control.
                    flowController.unregister(streamId);
                    flowController.streamClosed(streamId);
                }
                return streamFrame;
            } else {
                // So flowControlLimit <= currentOffset
                // Check if this condition hasn't been handled before
                if (currentOffsetTmp != blockedOffset) {
                    // Not handled before, remember this offset, so this isn't executed twice for the same offset
                    blockedOffset = currentOffsetTmp;
                    // And let peer know
                    // https://www.rfc-editor.org/rfc/rfc9000.html#name-data-flow-control
                    // "A sender SHOULD send a STREAM_DATA_BLOCKED or DATA_BLOCKED frame to indicate to the receiver
                    //  that it has data to write but is blocked by flow control limits."
                    sender.send(this::sendBlockReason, StreamDataBlockedFrame.getMaxSize(streamId),
                            App, this::retransmitSendBlockReason);
                    sender.flush();
                }
            }
        }
        return null;
    }

    @Override
    public void streamNotBlocked(int streamId) {
        // Stream might have been blocked (or it might have filled the flow control window exactly), queue send request
        // and let sendFrame method determine whether there is more to send or not.
        sender.send(this::sendFrame, MIN_FRAME_SIZE, App, this::retransmitStreamFrame);  // No need to flush, as this is called while processing received message
    }

    /**
     * Sends StreamDataBlockedFrame or DataBlockedFrame to the peer, provided the blocked condition is still true.
     */
    private QuicFrame sendBlockReason(int maxFrameSize) {
        // Retrieve actual block reason; could be "none" when an update has been received in the meantime.
        BlockReason blockReason = flowController.getFlowControlBlockReason(streamId);
        QuicFrame frame = null;
        switch (blockReason) {
            case STREAM_DATA_BLOCKED:
                frame = new StreamDataBlockedFrame(streamId, currentOffset.get());
                break;
            case DATA_BLOCKED:
                frame = new DataBlockedFrame(flowController.getConnectionDataLimit());
                break;
        }
        return frame;
    }

    private void retransmitSendBlockReason(QuicFrame quicFrame) {
        sender.send(this::sendBlockReason, StreamDataBlockedFrame.getMaxSize(streamId),
                App, this::retransmitSendBlockReason);
        sender.flush();
    }

    private void retransmitStreamFrame(QuicFrame frame) {
        if (!reset.get()) {
            sender.send(frame, App, this::retransmitStreamFrame);
            LogUtils.error(TAG, "Retransmitted lost stream frame " + frame);
        }
    }


    private void restart() {
        currentOffset.set(0);
        clearOutputStream();
    }

    private QuicFrame createResetFrame(int maxFrameSize) {
        return new ResetStreamFrame(streamId, resetErrorCode.get(), currentOffset.get());
    }

    private void retransmitResetFrame(QuicFrame frame) {
        sender.send(frame, App, this::retransmitResetFrame);
    }

    /**
     * Add a stream frame to this stream. The frame can contain any number of bytes positioned anywhere in the stream;
     * the read method will take care of returning stream bytes in the right order, without gaps.
     *
     * @return true if the frame is adds bytes to this stream; false if the frame does not add bytes to the stream
     * (because the frame is a duplicate or its stream bytes where already received with previous frames).
     */
    protected boolean addFrame(StreamFrame frame) {
        if (frame.getUpToOffset() > processedToOffset) {
            frames.add(frame);
            return true;
        } else {
            return false;
        }
    }

    private void removeParsedFrames() {
        Iterator<StreamFrame> iterator = frames.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getUpToOffset() <= processedToOffset) {
                iterator.remove();
            } else {
                break;
            }
        }
    }

    private static class ByteBufferFuture {
        private final ByteBuffer buffer;
        private final CompletableFuture<QuicStream> future;

        private ByteBufferFuture(ByteBuffer buffer, CompletableFuture<QuicStream> future) {
            this.buffer = buffer;
            this.future = future;
        }

        public ByteBuffer getBuffer() {
            return buffer;
        }

        public CompletableFuture<QuicStream> getFuture() {
            return future;
        }
    }
}
