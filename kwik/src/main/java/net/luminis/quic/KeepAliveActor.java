/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic;

import static net.luminis.quic.EncryptionLevel.App;

import net.luminis.quic.frame.PingFrame;
import net.luminis.quic.send.Sender;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


public class KeepAliveActor {

    private final Sender sender;
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private final int pingInterval;

    public KeepAliveActor(int pingInterval, Sender sender) {
        this.sender = sender;
        this.pingInterval = pingInterval;
        scheduleNextPing();
    }

    private void ping() {
        sender.send(new PingFrame(), App);
        sender.flush();

        scheduleNextPing();
    }

    private void scheduleNextPing() {
        scheduler.schedule(this::ping, pingInterval, java.util.concurrent.TimeUnit.SECONDS);
    }

    public void shutdown() {
        scheduler.shutdown();
    }
}
