/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.tls.handshake;

import static net.luminis.tls.NamedGroup.secp256r1;
import static net.luminis.tls.NamedGroup.secp384r1;
import static net.luminis.tls.NamedGroup.secp521r1;
import static net.luminis.tls.NamedGroup.x25519;
import static net.luminis.tls.NamedGroup.x448;
import static net.luminis.tls.SignatureScheme.ecdsa_secp256r1_sha256;
import static net.luminis.tls.SignatureScheme.rsa_pss_rsae_sha256;
import static net.luminis.tls.SignatureScheme.rsa_pss_rsae_sha384;
import static net.luminis.tls.SignatureScheme.rsa_pss_rsae_sha512;

import androidx.annotation.NonNull;

import net.luminis.LogUtils;
import net.luminis.tls.CipherSuite;
import net.luminis.tls.NamedGroup;
import net.luminis.tls.SignatureScheme;
import net.luminis.tls.TlsState;
import net.luminis.tls.TrafficSecrets;
import net.luminis.tls.alert.BadCertificateAlert;
import net.luminis.tls.alert.ErrorAlert;
import net.luminis.tls.alert.HandshakeFailureAlert;
import net.luminis.tls.alert.InternalErrorAlert;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.ECGenParameterSpec;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public abstract class TlsEngine implements MessageProcessor, TrafficSecrets {
    private static final String TAG = TlsEngine.class.getSimpleName();
    protected PublicKey publicKey;
    protected PrivateKey privateKey;
    protected TlsState state;
    protected X509TrustManager customTrustManager;
    protected X509Certificate remoteCertificate;
    protected List<X509Certificate> remoteCertificateChain = Collections.emptyList();

    public abstract CipherSuite getSelectedCipher();


    public void setTrustManager(X509TrustManager customTrustManager) {
        this.customTrustManager = customTrustManager;
    }


    public X509Certificate getRemoteCertificate() {
        return remoteCertificate;
    }

    public List<X509Certificate> getRemoteCertificateChain() {
        return remoteCertificateChain;
    }


    protected void checkCertificateValidity(List<X509Certificate> certificates, boolean serverTrust) throws BadCertificateAlert {
        try {
            if (customTrustManager != null) {
                if (serverTrust) {
                    customTrustManager.checkServerTrusted(certificates.stream().toArray(X509Certificate[]::new), "RSA");
                } else {
                    customTrustManager.checkClientTrusted(certificates.stream().toArray(X509Certificate[]::new), "RSA");
                }
            } else {
                // https://docs.oracle.com/en/java/javase/11/docs/specs/security/standard-names.html#trustmanagerfactory-algorithms
                // "...that validate certificate chains according to the rules defined by the IETF PKIX working group in RFC 5280 or its successor"
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance("PKIX");
                trustManagerFactory.init((KeyStore) null);
                X509TrustManager trustMgr = (X509TrustManager) trustManagerFactory.getTrustManagers()[0];
                if (serverTrust) {
                    trustMgr.checkServerTrusted(certificates.stream().toArray(X509Certificate[]::new), "UNKNOWN");
                } else {
                    trustMgr.checkClientTrusted(certificates.stream().toArray(X509Certificate[]::new), "UNKNOWN");
                }
                // If it gets here, the certificates are ok.
            }
        } catch (NoSuchAlgorithmException e) {
            LogUtils.error(TAG, e.getMessage());
            // Impossible, as we're using the trust managers default algorithm
            throw new RuntimeException("unsupported trust manager algorithm");
        } catch (KeyStoreException e) {
            LogUtils.error(TAG, e.getMessage());
            // Impossible, as we're using the default (JVM) keystore
            throw new RuntimeException("keystore exception");
        } catch (CertificateException e) {
            LogUtils.error(TAG, e.getMessage());
            throw new BadCertificateAlert(extractReason(e).orElse("certificate validation failed"));
        }
    }


    @NonNull
    private Optional<String> extractReason(CertificateException exception) {
        Throwable cause = exception.getCause();
        if (cause instanceof CertPathValidatorException) {
            return Optional.of(cause.getMessage() + ": " + ((CertPathValidatorException) cause).getReason());
        } else if (cause instanceof CertPathBuilderException) {
            String msg = cause.getMessage();
            if (msg != null) {
                return Optional.of(msg);
            }
        }
        return Optional.empty();
    }

    protected void generateKeys(NamedGroup namedGroup) {
        try {
            KeyPairGenerator keyPairGenerator;
            if (namedGroup == secp256r1 || namedGroup == secp384r1 || namedGroup == secp521r1) {
                keyPairGenerator = KeyPairGenerator.getInstance("EC");
                keyPairGenerator.initialize(new ECGenParameterSpec(namedGroup.toString()));
            } else if (namedGroup == x25519) {
                keyPairGenerator = KeyPairGenerator.getInstance("X25519", new BouncyCastleProvider());
            } else if (namedGroup == x448) {
                keyPairGenerator = KeyPairGenerator.getInstance("X448", new BouncyCastleProvider());
            } else {
                throw new RuntimeException("unsupported group " + namedGroup);
            }

            KeyPair keyPair = keyPairGenerator.genKeyPair();
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();
        } catch (NoSuchAlgorithmException e) {
            // Invalid runtime
            throw new RuntimeException("missing key pair generator algorithm EC");
        } catch (InvalidAlgorithmParameterException e) {
            // Impossible, would be programming error
            throw new RuntimeException(e);
        }
    }

    /**
     * Compute the signature used in certificate verify message to proof possession of private key.
     *
     * @param content               the content to be signed (transcript hash)
     * @param certificatePrivateKey the private key associated with the certificate
     * @param client                whether the signature must be computed
     */
    protected byte[] computeSignature(byte[] content, PrivateKey certificatePrivateKey,
                                      SignatureScheme signatureScheme, boolean client) throws ErrorAlert {
        // https://tools.ietf.org/html/rfc8446#section-4.4.3

        //   The digital signature is then computed over the concatenation of:
        //   -  A string that consists of octet 32 (0x20) repeated 64 times
        //   -  The context string
        //   -  A single 0 byte which serves as the separator
        //   -  The content to be signed"
        try (ByteArrayOutputStream signatureInput = new ByteArrayOutputStream()) {
            signatureInput.write(new String(new byte[]{0x20}).repeat(64).getBytes(StandardCharsets.US_ASCII));
            String contextString = "TLS 1.3, " + (client ? "client" : "server") + " CertificateVerify";
            signatureInput.write(contextString.getBytes(StandardCharsets.US_ASCII));
            signatureInput.write(0x00);
            signatureInput.write(content);


            Signature signatureAlgorithm = getSignatureAlgorithm(signatureScheme);
            signatureAlgorithm.initSign(certificatePrivateKey);
            signatureAlgorithm.update(signatureInput.toByteArray());
            return signatureAlgorithm.sign();

        } catch (IOException | SignatureException e) {
            // sign() throws SignatureException: if this signature object is not initialized properly or if this
            //                                   signature algorithm is unable to process the input data provided.
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new InternalErrorAlert("invalid private key");
        }
    }


    protected boolean verifySignature(byte[] signatureToVerify, SignatureScheme signatureScheme,
                                      Certificate certificate, byte[] transcriptHash,
                                      boolean client) throws HandshakeFailureAlert {
        // https://tools.ietf.org/html/rfc8446#section-4.4.3
        // "The digital signature is then computed over the concatenation of:
        //   -  A string that consists of octet 32 (0x20) repeated 64 times
        //   -  The context string
        //   -  A single 0 byte which serves as the separator
        //   -  The content to be signed"
        String contextString = "TLS 1.3, " + (client ? "client" : "server") + " CertificateVerify";
        ByteBuffer contentToSign = ByteBuffer.allocate(64 + contextString.getBytes(
                StandardCharsets.ISO_8859_1).length + 1 + transcriptHash.length);
        for (int i = 0; i < 64; i++) {
            contentToSign.put((byte) 0x20);
        }
        // "The context string for a server signature is
        //   "TLS 1.3, server CertificateVerify". "
        contentToSign.put(contextString.getBytes(StandardCharsets.ISO_8859_1));
        contentToSign.put((byte) 0x00);
        // "The content that is covered
        //   under the signature is the hash output as described in Section 4.4.1,
        //   namely:
        //      Transcript-Hash(Handshake Context, Certificate)"
        contentToSign.put(transcriptHash);

        boolean verified = false;
        try {
            Signature signatureAlgorithm = getSignatureAlgorithm(signatureScheme);
            signatureAlgorithm.initVerify(certificate);
            signatureAlgorithm.update(contentToSign.array());
            verified = signatureAlgorithm.verify(signatureToVerify);
        } catch (InvalidKeyException e) {
            LogUtils.error(TAG, "Certificate verify: invalid key.");
        } catch (SignatureException e) {
            LogUtils.error(TAG, "Certificate verify: invalid signature.");
        }
        return verified;
    }

    // https://tools.ietf.org/html/rfc8446#section-4.4.4
    protected byte[] computeFinishedVerifyData(byte[] transcriptHash, byte[] baseKey) {
        short hashLength = state.getHashLength();
        byte[] finishedKey = state.hkdfExpandLabel(baseKey, "finished", "", hashLength);
        String macAlgorithmName = "HmacSHA" + (hashLength * 8);
        SecretKeySpec hmacKey = new SecretKeySpec(finishedKey, macAlgorithmName);

        try {
            Mac hmacAlgorithm = Mac.getInstance(macAlgorithmName);
            hmacAlgorithm.init(hmacKey);
            hmacAlgorithm.update(transcriptHash);
            return hmacAlgorithm.doFinal();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Missing " + macAlgorithmName + " support");
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    protected Signature getSignatureAlgorithm(SignatureScheme signatureScheme) throws HandshakeFailureAlert {
        Signature signatureAlgorithm;
        // https://tools.ietf.org/html/rfc8446#section-9.1
        // "A TLS-compliant application MUST support digital signatures with rsa_pkcs1_sha256 (for certificates),
        // rsa_pss_rsae_sha256 (for CertificateVerify and certificates), and ecdsa_secp256r1_sha256."
        if (signatureScheme.equals(rsa_pss_rsae_sha256)) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA256withRSA/PSS");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing RSASSA-PSS support");
            }
        } else if (signatureScheme.equals(rsa_pss_rsae_sha384)) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA384withRSA/PSS");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing RSASSA-PSS support");
            }
        } else if (signatureScheme.equals(rsa_pss_rsae_sha512)) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA512withRSA/PSS");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing RSASSA-PSS support");
            }
        } else if (signatureScheme.equals(ecdsa_secp256r1_sha256)) {
            try {
                signatureAlgorithm = Signature.getInstance("SHA256withECDSA");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Missing SHA256withECDSA support");
            }
        } else {
            // Bad luck, not (yet) supported.
            throw new HandshakeFailureAlert("Signature algorithm not supported " + signatureScheme);
        }
        return signatureAlgorithm;
    }

    @Override
    public byte[] getClientEarlyTrafficSecret() {
        if (state != null) {
            return state.getClientEarlyTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    @Override
    public byte[] getClientHandshakeTrafficSecret() {
        if (state != null) {
            return state.getClientHandshakeTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    @Override
    public byte[] getServerHandshakeTrafficSecret() {
        if (state != null) {
            return state.getServerHandshakeTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    @Override
    public byte[] getClientApplicationTrafficSecret() {
        if (state != null) {
            return state.getClientApplicationTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    @Override
    public byte[] getServerApplicationTrafficSecret() {
        if (state != null) {
            return state.getServerApplicationTrafficSecret();
        } else {
            throw new IllegalStateException("Traffic secret not yet available");
        }
    }

    enum Status {
        Initial,
        ClientHelloSent,
        ServerHelloReceived,
        EncryptedExtensionsReceived,
        CertificateRequestReceived,
        CertificateReceived,
        CertificateVerifyReceived,
        Finished,
        ClientHelloReceived,
        ServerHelloSent,
        EncryptedExtensionsSent,
        CertificateRequestSent,
        CertificateSent,
        CertificateVerifySent,
        FinishedSent,
        FinishedReceived
    }
}

